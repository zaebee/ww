# whatwhere
What Where search best events in the world

## How to get setup
0. run `sudo apt-get install elasticsearch postgresql-server-dev-9.5 postgis redis-server binutils libproj-dev gdal-bin`
1. run `sudo apt-get install gettext python3.5 python-setuptools python-pip python-virtualenv virtualenvwrapper`
2. run `echo source /usr/share/virtualenvwrapper/virtualenvwrapper.sh >> ~/.bashrc`
3. run `git clone -b develop https://github.com/alarstudios/whatwhere.git`
4. run `mkvirtualenv whatwhere -p /usr/bin/python3` (supports 3.4, 3.5, 3.6 versions or 2.7)
5. run `workon whatwhere`
6. run `cd whatwhere`
7. run `pip install -r requirements.txt`
8. Change .env file with your params, Default settings:
```
    DEBUG=true
    DJANGO_ENV=development
    # DJANGO_SETTINGS_MODULE=config.settings
    DB_NAME=whatwhere
    DB_HOST=
    DB_USER=
    DB_PASSWORD=
    ES_HOST=localhost
    REDIS_HOST=localhost

```
9. run `python manage.py migrate`
10. run `python manage.py createsuperuser`
11. Need for elasticsearch indexes build
    run `python manage.py es_manage --initialize --indexes event-index`
    run `python manage.py es_manage --initialize --indexes place-index`
    run `python manage.py es_manage --initialize --indexes hits-index`

### Frontend assets
    run `sudo apt-get install nodejs-dev npm`
    You need install `lessc` compilator `sudo npm install -g less`
    After that you can run `python manage.py collectstatic`

### Load fixtires
    1. `python manage.py loaddata users places categories`

### Run developer server
`python manage.py runserver`

## How to run celery
`celery -A config worker -l info -P gevent -c 10 --events`

## Translates

### Compile translate
`python manage.py compilemessages -l ru`

### Make translate
`python manage.py makemessages -l ru`

### Make translate for frontend
`python manage.py makemessages -l ru -l en -d djangojs`


##
`python manage.py cities --import  country,region,subregion,city,district`
