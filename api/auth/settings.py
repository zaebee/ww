# -*- coding: utf-8 -*-

from copy import deepcopy

from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.contrib.auth.password_validation import validate_password
password_validators = [validate_password]


default_settings = {
    'SEND_ACTIVATION_EMAIL': False,
    'SEND_CONFIRMATION_EMAIL': False,
    'SET_PASSWORD_RETYPE': False,
    'PASSWORD_RESET_CONFIRM_RETYPE': False,
    'PASSWORD_RESET_CONFIRM_URL': 'password/reset/confirm/{uid}/{token}',
    'ACTIVATION_URL': 'activate/{uid}/{token}',
    'PASSWORD_RESET_SHOW_EMAIL_NOT_FOUND': True,
    'ROOT_VIEW_URLS_MAPPING': {},
    'PASSWORD_VALIDATORS': password_validators,
    'SERIALIZERS': {
        'activation': 'api.auth.serializers.ActivationSerializer',
        'login': 'api.auth.serializers.LoginSerializer',
        'subscribe': 'api.auth.serializers.SubscribeSerializer',
        'password_reset': 'api.auth.serializers.PasswordResetSerializer',
        'password_reset_confirm': 'api.auth.serializers.PasswordResetConfirmSerializer',
        'password_reset_confirm_retype': 'api.auth.serializers.PasswordResetConfirmRetypeSerializer',
        'set_password': 'api.auth.serializers.SetPasswordSerializer',
        'set_password_retype': 'api.auth.serializers.SetPasswordRetypeSerializer',
        'set_headshot': 'api.auth.serializers.SetHeadshotSerializer',
        'user_registration': 'api.auth.serializers.UserRegistrationSerializer',
        'user': 'api.auth.serializers.UserSerializer',
        'token': 'api.auth.serializers.TokenSerializer',
        'gcm_device': 'api.auth.serializers.GCMDeviceSerializer',
        'apns_device': 'api.auth.serializers.APNSDeviceSerializer'
    },
    'LOGOUT_ON_PASSWORD_CHANGE': False,
}


def get(key):
    user_settings = merge_settings_dicts(
        deepcopy(default_settings), getattr(settings, 'DJOSER', {}))
    try:
        return user_settings[key]
    except KeyError:
        raise ImproperlyConfigured('Missing settings: DJOSER[\'{}\']'.format(key))


def merge_settings_dicts(a, b, path=None, overwrite_conflicts=True):
    """merges b into a, modify a in place

    Found at http://stackoverflow.com/a/7205107/1472229
    """
    if path is None:
        path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_settings_dicts(a[key], b[key], path + [str(key)], overwrite_conflicts=overwrite_conflicts)
            elif a[key] == b[key]:
                pass  # same leaf value
            else:
                if overwrite_conflicts:
                    a[key] = b[key]
                else:
                    conflict_path = '.'.join(path + [str(key)])
                    raise Exception('Conflict at %s' % conflict_path)
        else:
            a[key] = b[key]
    # Don't let this fool you that a is not modified in place
    return a
