from django.conf.urls import url
from api.auth import views
from django.contrib.auth import get_user_model

User = get_user_model()

base_urlpatterns = (
    url(r'^send_mail/$', views.SendMailView.as_view(), name='send_mail'),
    url(r'^register/$', views.RegistrationView.as_view(), name='register'),
    url(r'^subscribe/$', views.SubscribeView.as_view(), name='subscribe'),
    url(r'^activate/$', views.ActivationView.as_view(), name='activate'),
    url(r'^headshot/$', views.SetHeadshotView.as_view(), name='set_headshot'),
    url(r'^password/$', views.SetPasswordView.as_view(), name='set_password'),
    url(r'^password/reset/$', views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password/reset/confirm/$', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),

)

urlpatterns = base_urlpatterns + (url(r'^$', views.RootView.as_view(), name='root'),)
