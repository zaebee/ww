from django.conf.urls import url
from api.auth import views
from . import base


urlpatterns = (
    url(r'^apns/?$', views.APNSDeviceViewSet.as_view({'post': 'create'}), name='create_apns_device'),
    url(r'^gcm/?$', views.GCMDeviceViewSet.as_view({'post': 'create'}), name='create_gcm_device'),
)
