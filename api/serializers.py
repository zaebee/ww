# -*- coding: utf-8 -*-

from datetime import datetime, time
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from versatileimagefield.serializers import VersatileImageFieldSerializer

from app.templatetags.app_tags import get_schedule
from app.models import (
    User, Event, EventCollection,
    EventPlace, EventCategory,
    City,
    EventImage, EventVote, EventSchedule
)


class UserSerializer(serializers.ModelSerializer):
    headshot = VersatileImageFieldSerializer(sizes='user_headshot')

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'headshot', 'is_staff')


class CollectionSerializer(serializers.ModelSerializer):
    provider_id = serializers.CharField(source='id', read_only=True)
    author = UserSerializer(read_only=True)
    subscribers = UserSerializer(many=True, read_only=True)
    limit_date_from = serializers.DateField(
        format="%Y-%m-%d",
        input_formats=["%Y-%m-%d", "%Y-%m-%dT%H:%M:%S"],
        required=False,
        allow_null=True,
    )
    limit_date_to = serializers.DateField(
        format="%Y-%m-%d",
        input_formats=["%Y-%m-%d", "%Y-%m-%dT%H:%M:%S"],
        required=False,
        allow_null=True,
    )

    class Meta:
        model = EventCollection
        fields = ('id', 'provider_id', 'title', 'author', 'slug',
                  'application_id', 'date_added', 'date_updated',
                  'limit_date_from', 'limit_date_to', 'limit_dates',
                  'preview',
                  'subscribers', 'description', 'private', 'deleted')


class PlaceSerializer(serializers.ModelSerializer):
    provider = 'ww'
    provider_id = serializers.CharField(source='id', read_only=True)
    geometry = serializers.SerializerMethodField()
    title_localized = serializers.SerializerMethodField()
    address_localized = serializers.SerializerMethodField()
    description_localized = serializers.SerializerMethodField()
    place_id = serializers.SerializerMethodField()
    absolute_url = serializers.SerializerMethodField()
    events_count = serializers.SerializerMethodField()

    def get_geometry(self, obj):
        lat = obj.lat
        lon = obj.lng
        return {
            'lat': lat,
            'lon': lon,
        }

    def get_title_localized(self, obj):
        if getattr(obj, 'title_localized', False):
            return obj.title_localized.to_dict()

    def get_address_localized(self, obj):
        if getattr(obj, 'address_localized', False):
            return obj.address_localized.to_dict()

    def get_description_localized(self, obj):
        if getattr(obj, 'description_localized', False):
            return obj.description_localized.to_dict()

    def get_place_id(self, obj):
        place_id = None
        if getattr(obj, 'place_id', False):
            place_id = obj.place_id
        if not place_id:
            provider_id = getattr(obj, 'provider_id', False)
            if getattr(obj, 'provider', False) and provider_id:
                place_id = '%s_%s' % (obj.provider, provider_id)
        return place_id

    def get_absolute_url(self, obj):
        url = ''
        provider_id = getattr(obj, 'provider_id', False)
        if provider_id and obj.slug:
            url = reverse(
                'place-detail',
                kwargs={
                    'pk': obj.provider_id,
                    'slug': obj.slug
                }
            )
            url = 'https://stage.whatwhere.world%s' % url
        return url

    def get_events_count(self, obj):
        events_count = getattr(obj, 'events_count', 0)
        return events_count

    class Meta:
        model = EventPlace
        fields = ('id', 'title', 'slug', 'provider_id', 'place_id',
                  'title_localized', 'address_localized',
                  'description_localized', 'absolute_url',
                  'events_count',
                  'email', 'phone', 'website', 'description',
                  'address', 'lat', 'lng', 'city', 'geometry')


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EventCategory
        fields = (
            'id', 'title', 'slug', 'parent_id',
        )


class CategorySerializer(serializers.ModelSerializer):
    subcategories = SubCategorySerializer(many=True)

    class Meta:
        model = EventCategory
        fields = (
            'id', 'title', 'slug', 'description_short',
            'description_full', 'image', 'h1_seo', 'parent_id',
            'subcategories',
        )


class CitySerializer(GeoFeatureModelSerializer):
    class Meta:
        model = City
        geo_field = 'location'
        fields = (
            'id', 'name_std', 'location',
            'title', 'region', 'country', 'alt_names'
        )


class ImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField()

    class Meta:
        model = EventImage
        fields = ('id', 'image', 'title', 'alt', 'width', 'height')


class VoteSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = EventVote
        fields = ('id', 'rating', 'author')


class ScheduleSerializer(serializers.ModelSerializer):
    start_time = serializers.TimeField(
        required=True,
        read_only=False)
    start_date = serializers.DateTimeField(
        format="%d.%m.%YT%H:%M:%S",
        input_formats=["%Y-%m-%dT%H:%M:%S%z",
                       "%Y-%m-%dT%H:%M:%S",
                       "%d.%m.%Y %H:%M:%S",
                       "%d.%m.%Y %H:%M",
                       "%d.%m.%Y",
                       "%d.%m.%YT%H:%M:%S",
                       "%d.%m.%YT%H:%M"],
        required=True)
    end_time = serializers.TimeField(
        required=True,
        read_only=False)
    end_date = serializers.DateTimeField(
        format="%d.%m.%YT%H:%M:%S",
        input_formats=["%Y-%m-%dT%H:%M:%S%z",
                       "%Y-%m-%dT%H:%M:%S",
                       "%d.%m.%Y %H:%M:%S",
                       "%d.%m.%Y %H:%M",
                       "%d.%m.%Y",
                       "%d.%m.%YT%H:%M:%S",
                       "%d.%m.%YT%H:%M"],
        required=True)

    class Meta:
        model = EventSchedule
        fields = ('id', 'start_date', 'end_date', 'start_time', 'end_time')

    def validate(self, data):
        """
        Check that the start is before the stop.
        """
        data['start_date'] = datetime.combine(
            data['start_date'], data['start_time'])
        data['end_date'] = datetime.combine(
            data['end_date'], data['end_time'])

        if data['start_date'] > data['end_date']:
            raise serializers.ValidationError(
                _("End date must occur after start date"))
        return data


class EventShortSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    place = PlaceSerializer()

    class Meta:
        model = Event
        fields = (
            'id', 'title', 'slug', 'description',
            'author', 'category', 'place',
            'ticket_price', 'ticket_url',
        )


class EventSimilarSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True, required=False, read_only=True)
    actual_start_date = serializers.SerializerMethodField()
    place = PlaceSerializer()

    class Meta:
        model = Event
        fields = (
            'id', 'title', 'place', 'images', 'actual_start_date'
        )

    def get_actual_start_date(self, obj):
        return obj.actual_start_date


class EventSerializer(serializers.ModelSerializer):
    place = PlaceSerializer()
    schedules = ScheduleSerializer(many=True)
    images = ImageSerializer(many=True, required=False, read_only=True)
    image = serializers.SerializerMethodField()
    author = UserSerializer(read_only=True)
    votes = VoteSerializer(many=True, read_only=True)
    provider_id = serializers.CharField(source='id', read_only=True)
    provider = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = (
            'id', 'title', 'slug', 'description', 'video_url',
            'author', 'category', 'place', 'schedules',
            'ticket_price', 'ticket_url', 'get_absolute_url', 'provider_id',
            'image', 'images', 'votes', 'similar', 'currency',
            'provider', 'fb_url', 'vk_url', 'deleted',
        )

    def get_provider(self, obj):
        provider = getattr(obj, 'provider', 'ww')
        return provider

    def get_image(self, obj):
        return obj.image

    def get_actual_start_date(self, obj):
        return obj.actual_start_date

    def create(self, validated_data, *args, **kwargs):
        """ Create event with images, place and schedules.
        """
        request = self.context.get('request', None)

        place = validated_data.pop('place', None)
        if place:
            # TODO get_or_create based on `title`, `city` `lat`, `lng` fields
            place, created = EventPlace.objects.update_or_create(**place)

        schedules = validated_data.pop('schedules', [])

        event = Event.objects.create(**validated_data)

        if schedules:
            schedules = ScheduleSerializer(data=schedules, many=True)
            if schedules.is_valid():
                schedules = [EventSchedule(event=event,
                                           start_date=schedule['start_date'],
                                           end_date=schedule['end_date'])
                             for schedule in schedules.validated_data]
                schedules = EventSchedule.objects.bulk_create(schedules)

        if request:
            images = request.data.get('images', [])
            images = [image.get('id') for image in images]
            images = EventImage.objects.filter(id__in=images)
            event.images.add(*images)

            similar_events = request.data.get('similar', [])
            event.similar = similar_events

        event.place = place
        event.save()
        return event

    def update(self, instance, validated_data, *args, **kwargs):
        """ Update event with images, place and schedules.
        """
        request = self.context.get('request', None)

        # TODO fix new place creating for aviod doubles
        place_data = validated_data.pop('place', None)

        schedules = validated_data.pop('schedules')
        instance.schedules.all().delete()
        if request:
            schedules = request.data.get('schedules', []) or schedules
        schedules = ScheduleSerializer(data=schedules, many=True)
        if schedules.is_valid():
            schedules = [EventSchedule(event=instance,
                                       start_date=schedule['start_date'],
                                       end_date=schedule['end_date'])
                         for schedule in schedules.validated_data]
            schedules = EventSchedule.objects.bulk_create(schedules)

        if instance.place and place_data:
            place = EventPlace.objects.filter(id=instance.place.id)
            place.update(**place_data)
            instance.place = place.first()
        elif place_data:
            place = EventPlace.objects.create(**place_data)
            instance.place = place
        if request:
            images = request.data.get('images', [])
            images = [image.get('id') for image in images]
            images = EventImage.objects.filter(id__in=images)
            instance.images.add(*images)

            similar_events = request.data.get('similar', [])
            instance.similar = similar_events

        instance.title = validated_data.get('title', instance.title)
        instance.category = validated_data.get('category', instance.category)
        instance.description = validated_data.get('description', instance.description)
        instance.video_url = validated_data.get('video_url', instance.video_url)
        instance.vk_url = validated_data.get('vk_url', instance.vk_url)
        instance.fb_url = validated_data.get('fb_url', instance.fb_url)
        instance.ticket_price = validated_data.get('ticket_price', instance.ticket_price)
        instance.ticket_url = validated_data.get('ticket_url', instance.ticket_url)
        instance.currency = validated_data.get('currency', instance.currency)
        instance.save()
        return instance


class UserWithEventsSerializer(serializers.ModelSerializer):
    headshot = VersatileImageFieldSerializer(sizes='user_headshot')
    events = serializers.SerializerMethodField()
    collections = serializers.SerializerMethodField()
    votes = VoteSerializer(many=True, read_only=True)
    last_place = serializers.SerializerMethodField()

    def get_collections(self, obj):
        collections = EventCollection.index.filter(
            only_public=False,
            **{'author.id': [obj.id]})
        return list(collection.to_dict() for collection in collections)

    def get_events(self, obj):
        events = Event.index.filter(**{'author.id': obj.id})
        return list(event.to_dict() for event in events)

    def get_last_place(self, obj):
        last_event = obj.events.last()
        if last_event:
            place = PlaceSerializer(last_event.place)
            return place.data

    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name', 'short_bio',
            'date_joined', 'is_active', 'headshot', 'email',
            'events', 'collections', 'votes', 'last_place',
            'is_staff',
        )


class ScheduleElasticSerializer(ScheduleSerializer):
    start_date = serializers.DateTimeField(required=True)
    end_date = serializers.DateTimeField(required=True)

    class Meta:
        model = EventSchedule
        fields = ('start_date', 'end_date')


class DatesElasticSerializer(ScheduleSerializer):
    start_date = serializers.SerializerMethodField()
    start_time = serializers.SerializerMethodField()
    end_date = serializers.SerializerMethodField()
    end_time = serializers.SerializerMethodField()

    class Meta:
        model = EventSchedule
        fields = ('start_date', 'end_date', 'start_time', 'end_time')

    def get_start_date(self, obj):
        if obj.start_date:

            # Set StartDate to current date if origin start_date in a past
            if obj.start_date.date() < datetime.now().date():
                if obj.end_date and obj.end_date.date() >= datetime.now().date():
                    obj.start_date = datetime.now()

            return obj.start_date.date()

    def get_start_time(self, obj):
        if getattr(obj, 'start_time', None):
            if isinstance(obj.start_time, time):
                start_time = obj.start_time.strftime('%H:%M')
            else:
                start_time = obj.start_time
        elif obj.start_date:
            start_time = obj.start_date.strftime('%H:%M')
        return start_time

    def get_end_date(self, obj):
        if obj.end_date:
            end_date = obj.end_date.date()
        else:
            end_date = obj.start_date.date() if obj.start_date else None
        return end_date

    def get_end_time(self, obj):
        end_time = self.get_start_time(obj)
        if getattr(obj, 'end_time', None):
            if isinstance(obj.end_time, time):
                end_time = obj.end_time.strftime('%H:%M')
            else:
                end_time = obj.end_time
        elif obj.end_date:
            end_time = obj.end_date.strftime('%H:%M')
        return end_time


class EventElasticSerializer(EventSerializer):
    id = serializers.SerializerMethodField()
    schedules = ScheduleElasticSerializer(many=True)
    dates = DatesElasticSerializer(many=True, source='schedules')
    similar = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = (
            'id', 'title', 'slug', 'description', 'video_url',
            'author', 'place', 'schedules', 'dates',
            'ticket_price', 'ticket_url', 'get_absolute_url', 'provider_id',
            'image', 'images', 'votes', 'similar', 'currency',
            'provider', 'fb_url', 'vk_url', 'deleted',
        )

    def get_similar(self, obj):
        similar = obj.similar or []
        return [s.get('id') for s in similar]

    def get_id(self, obj):
        provider = obj.provider or 'ww'
        provider_id = obj.provider_id or obj.id
        return '%s_%s' % (provider, provider_id)


class EventWidgetSerializer(EventSerializer):
    id = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    actual_schedule = serializers.SerializerMethodField()
    dates = DatesElasticSerializer(many=True, source='schedules')
    provider_id = serializers.CharField(read_only=True)
    title_localized = serializers.SerializerMethodField()
    description_localized = serializers.SerializerMethodField()
    absolute_url = serializers.SerializerMethodField()
    collections = serializers.SerializerMethodField()
    min_price = serializers.SerializerMethodField()
    max_price = serializers.SerializerMethodField()
    is_free = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()
    formatted_price = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = (
            'id', 'title', 'slug', 'description',
            'place', 'dates', 'actual_schedule',
            'ticket_price', 'ticket_url', 'provider_id',
            'image', 'images', 'currency',
            'provider', 'fb_url', 'vk_url',
            'title_localized', 'description_localized',
            'absolute_url', 'collections',
            'min_price', 'max_price', 'is_free',
            'currency', 'formatted_price',
        )

    def __init__(self, *args, **kwargs):
        bucket_date = kwargs.pop('bucket_date', datetime.now())
        super(EventWidgetSerializer, self).__init__(*args, **kwargs)
        self.bucket_date = bucket_date

    def get_absolute_url(self, obj):
        if obj.provider_id and obj.slug:
            return reverse(
                'event-detail',
                kwargs={
                    'pk': obj.provider_id,
                    'slug': obj.slug
                }
            )
        return ''

    def get_id(self, obj):
        provider = obj.provider or 'ww'
        provider_id = obj.provider_id or obj.id
        return '%s_%s' % (provider, provider_id)

    def get_image(self, obj):
        result = obj.image
        images = self.get_images(obj)
        if len(images):
            image = images[0]
            result = image.get('image', result)
        return result

    def get_images(self, obj):
        images = obj.preview_images or obj.images or []
        return [image.to_dict() for image in images]

    def get_actual_schedule(self, obj):
        result = get_schedule(obj.schedules, self.bucket_date)
        schedule = DatesElasticSerializer(result)
        return schedule.data

    def get_title_localized(self, obj):
        return obj.title_localized.to_dict()

    def get_description_localized(self, obj):
        return obj.description_localized.to_dict()

    def get_collections(self, obj):
        if getattr(obj, 'collections', None):
            return [c.to_dict() for c in obj.collections]

    def get_min_price(self, obj):
        return obj.min_price

    def get_max_price(self, obj):
        return obj.max_price

    def get_is_free(self, obj):
        return obj.is_free

    def get_currency(self, obj):
        return obj.currency

    def get_formatted_price(self, obj):
        if obj.is_free:
            return 'бесплатно'
        price = obj.formatted_price
        return price or obj.ticket_price
