from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout

from rest_framework.test import APIClient
from rest_framework import status

from app.models import Event, User

class ModelEventTestCase(TestCase):
    """This class defines the test suite for the event model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.event_title = "Write world class code"

        User.objects.create_user('ringo', 'starr@thebeatles.com', 'yellow')
        self.user = authenticate(username='starr@thebeatles.com', password='yellow')
        self.event = Event(title=self.event_title, author=self.user)

    def test_model_can_create_a_event(self):
        """Test the event model can create a simple Event."""
        old_count = Event.objects.count()
        self.event.save()
        new_count = Event.objects.count()
        self.assertNotEqual(old_count, new_count)


# Define this after the ModelTestCase
class ViewEventTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        user = User.objects.create(username='ringo')
        self.client.force_authenticate(user=user)
        self.event_data = {
            'title': 'Go to Ibiza',
            'category': {
                'title': 'Simple category',
            },
            'place': {
                'title': 'Simple place',
                'city': 'moscow',
                'address': 'first street 40',
                'lat': 40,
                'lng': 40,
            },
            'schedules': [
                {
                    'start_date': '02.02.2017 16:00',
                    'end_date': '03.02.2017 16:00'
                }
            ],
        }
        self.response = self.client.post('/api/events/', self.event_data, format="json")

    def test_api_can_create_a_event(self):
        """Test the api has event creation capability."""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_authorization_is_enforced(self):
        """Test that the api has user authorization."""
        new_client = APIClient()
        res = new_client.post('/api/events/', self.event_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_can_get_a_event(self):
        """Test the api can get a given event."""
        event = Event.objects.get()
        response = self.client.get(
            '/api/events/',
            kwargs={'pk': event.id}, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, event)

    def test_api_can_update_event(self):
        """Test the api can update a given event."""
        event = Event.objects.get()
        event.title = 'Something new'
        res = self.client.put(
            '/api/events/%s/' % event.id,
            change_event, format='json'
        )
        print(res)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_event(self):
        """Test the api can delete a event."""
        event = Event.objects.get()
        self.assertEquals(event.deleted, False)
        response = self.client.delete(
            '/api/events/%s/' % event.id,
            format='json',
            follow=True)
        event = Event.objects.get(id=event.id)
        self.assertEquals(event.deleted, True)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
