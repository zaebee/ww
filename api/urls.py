# -*- coding: utf-8 -*-


from django.conf.urls import url, include
from . import views
from rest_framework.routers import DefaultRouter

from rest_framework_nested import routers

# Create a router and register our viewsets with it.
router = DefaultRouter()

router.register(r'events', views.EventsViewSet)
router.register(r'users', views.UsersViewSet)
router.register(r'places', views.PlacesViewSet)
router.register(r'collections', views.CollectionsViewSet)

router.register(r'search', views.SearchViewSet, base_name='search')
router.register(r'cities', views.CitiesViewSet)
router.register(r'categories', views.CategoriesViewSet)

collections_router = routers.NestedSimpleRouter(
    router,
    r'events',
    lookup='event'
)

collections_router.register(
    r'collections',
    views.CollectionActionsViewSet,
    base_name='event-collections'
)

collections_router.register(
    r'categories',
    views.CategoryActionsViewSet,
    base_name='event-categories'
)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^auth/', include('api.auth.urls.authtoken')),
    url(r'^auth/devices/', include('api.auth.urls.devices')),
    url(r'^', include(router.urls)),
    url(r'^', include(collections_router.urls)),
]
