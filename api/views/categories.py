# -*- coding: utf-8 -*-

import dateparser
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.mixins import CreateModelMixin
from rest_framework_gis.pagination import GeoJsonPagination
from rest_framework_gis.filters import InBBoxFilter
from rest_framework.decorators import list_route

from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Polygon

from app.models import EventCategory, City, Event
from api.serializers import (
    CategorySerializer,
    SubCategorySerializer,
    CitySerializer
)


class CategoriesViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = EventCategory.objects.all()
    serializer_class = CategorySerializer

    @list_route()
    def tree(self, request):
        """
            Return categories tree.
        """
        categories = EventCategory.objects.filter(parent__isnull=True)
        serializer = self.get_serializer(categories, many=True)
        return Response(serializer.data)

    def list(self, request):
        query = self.request.query_params.get('query', None)
        params = {
            'q': query
        }
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)

        if start_date:
            start_date = dateparser.parse(start_date, languages=['ru'])
            start_date = start_date.isoformat() if start_date else None
            params['start_date'] = start_date

        if end_date:
            end_date = dateparser.parse(end_date, languages=['ru'])
            end_date = end_date.isoformat() if end_date else None
            params['end_date'] = end_date

        search = Event.index.search(observable=True, **params)

        bounds = {}
        bottom_right = self.request.query_params.get('bottom_right', None)
        top_left = self.request.query_params.get('top_left', None)
        if query:
            search = search.filter('match', category__title=query)
        if bottom_right:
            lat, lon = bottom_right.split(',')
            bounds['bottom_right'] = {
                'lat': lat,
                'lon': lon
            }
        if top_left:
            lat, lon = top_left.split(',')
            bounds['top_left'] = {
                'lat': lat,
                'lon': lon
            }

        if bounds:
            search = search.query(
                'geo_bounding_box',
                place__geometry=bounds
            )
        search.aggs.bucket(
            'categories',
            'terms',
            field='category.title.raw',
            missing='UNKNOWN',
            size=50
        ).metric(
            'events',
            'top_hits',
            size=40,
            _source=['place.title', 'place.lat', 'place.lng'],
            sort='dates.start_date'
        )
        queryset = search.execute()
        buckets = queryset.aggregations.categories.buckets
        categories = [bucket.to_dict() for bucket in buckets]
        return Response(categories)


class CategoryActionsViewSet(viewsets.GenericViewSet, CreateModelMixin):
    """
    The **author** of the event collection may update or delete instances.
    """
    queryset = EventCategory.objects.all()

    def get_serializer_class(self):
        return CategorySerializer

    def create(self, request, pk=None, event_pk=None):
        """
        Add event into category.
        You must send category id in data body if category exists.
        By example `{"categories_id": 12}` or `{"categories_id": [12, 13]}`
        """
        # TODO permissions for ES event add to collection
        # if not collection.has_object_collection_permission(request):
        #    return Response(_('You does not have access to this action'),
        #                    status=status.HTTP_403_FORBIDDEN)
        if 'categories_id[]' in request.data:
            categories_id = request.data.getlist('categories_id[]')
        else:
            categories_id = request.data.get('categories_id', [])
        event = Event.index.get_or_None(event_pk)
        categories = EventCategory.objects.filter(
            id__in=categories_id,
        )
        serializer = SubCategorySerializer(categories, many=True)
        if event:
            event.category = serializer.data
            event.save_rank()
            event.save()
            data = {
                'text': '<p>Success!</p>',
                'status': _('Event was added to category'),
                'categories_count': categories.count()
            }
            return Response(data)
        else:
            return Response(_('You does not have access to this action'),
                            status=status.HTTP_403_FORBIDDEN)


class CitiesViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = City.objects.all()
    serializer_class = CitySerializer
    pagination_class = GeoJsonPagination
    page_size = 100

    bbox_filter_field = 'location'
    filter_backends = (InBBoxFilter, )
    bbox_filter_include_overlapping = True  # Optional

    def _list(self, request):
        bbox = self.request.query_params.get('bbox', None)
        queryset = City.objects.all()
        if bbox:
            polygon = Polygon.from_bbox(bbox.split(','))
            queryset = queryset.filter(location__within=polygon)
        serializer = CitySerializer(queryset, many=True)
        return Response(serializer.data)
