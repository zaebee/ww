# -*- coding: utf-8 -*-

from datetime import datetime
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.mixins import DestroyModelMixin, CreateModelMixin
from rest_framework.response import Response

from rest_framework_extensions.cache.decorators import cache_response

from django.core.cache import cache
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.core.cache.utils import make_template_fragment_key

from dry_rest_permissions.generics import DRYPermissions

from services.tasks import push_notification
from services.elastic.facets import CollectionSearch

from app.models import EventCollection, Event
from api.serializers import EventWidgetSerializer, CollectionSerializer


class CollectionsViewSet(viewsets.ModelViewSet):
    """
    The **author** of the event collection may update or delete instances.

    retrieve:
    Return a collection instance.

    list:
    Return all collections.

    create:
    Create a new collection.

    update:
    Update a collection that is owned by user.

    partial_update:
    Update one or more fields on an existing collection.

    delete:
    Detele a collection that is owned by user.

    """
    permission_classes = (DRYPermissions,)
    queryset = EventCollection.objects.all()
    serializer_class = CollectionSerializer

    def calculate_cache_key(self, view_instance, view_method,
                            request, args, kwargs):
        fragment = 'api:collections.%s' % view_instance.action
        cache_key = make_template_fragment_key(fragment, kwargs.values())
        return cache_key

    def _get_event(self, events, key):
        try:
            event = events[key]
        except KeyError:
            event = None
        return event

    def invalidate_cache(self, action, **kwargs):
        fragment = 'api:collections.%s' % action
        cache_key = make_template_fragment_key(fragment, kwargs.values())
        cache.delete(cache_key)

    @detail_route(methods=['GET'])
    @cache_response(60 * 60 * 1, key_func='calculate_cache_key')
    def widget(self, request, pk=None):
        return self.retrieve(request, pk=pk)

    def retrieve(self, request, pk=None):
        collection = EventCollection.index.get(id=pk)
        edit = request.query_params.get('edit', False)
        if not collection.events or edit:
            return Response(collection.to_dict())

        limit_date_from = None
        limit_date_to = None
        if collection.limit_date_from:
            limit_date_from = collection.limit_date_from.date()
        if collection.limit_date_to:
            limit_date_to = collection.limit_date_to.date()
        # get serialized events for collection
        search = CollectionSearch(query={
            'events': collection.events or [],
            'places': collection.places or [],
            'exclude_places': collection.exclude_places or [],
            'limit_date_from': limit_date_from,
            'limit_date_to': limit_date_to,
        })
        bucket = search[:len(collection.events)].execute()

        data = []
        widget = {}
        result = set()
        events = []
        dict_events = {event.id: event for event in bucket}
        day_buckets = bucket.facets.day_events
        # TODO common part for event doubles app/api
        # separate method `get_day_buckets(from, to)`
        if collection.limit_date_from or collection.limit_date_to:
            day_buckets = []
            for day_bucket in bucket.facets.day_events:
                if (collection.limit_date_from
                        and day_bucket.key >= collection.limit_date_from):
                    if (collection.limit_date_to
                            and day_bucket.key <= collection.limit_date_to):
                        day_buckets.append(day_bucket)

        if day_buckets:
            first = day_buckets[0]
            key = first.key.year, first.key.month
            for day_bucket in day_buckets:

                if key == (day_bucket.key.year, day_bucket.key.month):
                    for event in day_bucket.events:
                        if event.meta.id not in result:
                            result.add(event.meta.id)
                            events.append((event.meta.id, day_bucket.key))
                else:
                    data.append({
                        'key': first.key,
                        'doc_count': len(events),
                        'events': events
                    })
                    key = day_bucket.key.year, day_bucket.key.month
                    first = day_bucket
                    events = [(e.meta.id, day_bucket.key) for e in day_bucket.events]
                    result = set(e.meta.id for e in day_bucket.events)

            data.append({
                'key': day_bucket.key,
                'doc_count': len(events),
                'events': events
            })

        for day_bucket in data:
            serializer = EventWidgetSerializer(
                (
                    self._get_event(dict_events, event)
                    for event, key in day_bucket['events']
                    if self._get_event(dict_events, event)
                ),
                bucket_date=day_bucket['key'],
                many=True
            )
            day_bucket['events'] = serializer.data

        bucket.facets['day_events'] = data
        del bucket.facets.month_events
        collection.facets = bucket.facets.to_dict()
        return Response(collection.to_dict())

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def update(self, request, pk=None):
        self.invalidate_cache('widget', pk=pk)
        return super(CollectionsViewSet, self).update(request, pk)

    def destroy(self, request, pk=None):
        """
        Collection to mark as `deleted=True` instead of the actual remove.
        """
        collection = self.get_object()
        collection.deleted = True
        collection.save()
        self.invalidate_cache('widget', pk=pk)
        redirect_url = collection.author.get_absolute_url()
        result = {
            'success': True,
            'redirect_url': redirect_url,
            'status': _('Your collection was removed. '
                        'You will be redirected in your profile.')
        }
        return Response(result)

    @detail_route(methods=['POST'])
    def subscribe(self, request, pk):
        """ Action to subscribe EventCollection.
        """
        user_id = request.user.id
        collection = EventCollection.index.get(id=pk)
        subscribers = collection.subscribers or []
        if user_id not in subscribers:
            collection.add_subscriber(user_id)
        collection.save(refresh=True)
        self.invalidate_cache('widget', pk=pk)
        text = render_to_string(
            'include/counters/_collection_subscribers.html',
            {'collection': collection})
        data = {
            'text': text,
            'status': _('You subscribed to collection'),
            'subscribers_count': collection.subscribers_count,
        }
        return Response(data)

    @detail_route(methods=['POST'])
    def unsubscribe(self, request, pk):
        """ Action to unsubscribe EventCollection.
        """
        user_id = request.user.id
        collection = EventCollection.index.get(id=pk)
        subscribers = collection.subscribers or []
        if user_id in subscribers:
            collection.remove_subscriber(user_id)
        collection.save(refresh=True)
        self.invalidate_cache('widget', pk=pk)
        text = render_to_string(
            'include/counters/_collection_subscribers.html',
            {'collection': collection})
        data = {
            'text': text,
            'status': _('You unsubscribed to collection'),
            'subscribers_count': collection.subscribers_count,
        }
        return Response(data)

    @detail_route(methods=['POST'])
    def translate(self, request, pk):
        """ Action to localize EventCollection through google translator.
        """
        user = self.request.user
        collection = EventCollection.index.get(id=pk)
        can_translate = user.has_perm('app.translate_collection')
        is_premium = (
            user.is_authenticated() and
            user.business_account and
            user.id == collection.author.id
        )
        can_translate = can_translate or is_premium
        if can_translate:
            to_lang = request.data.get('to', 'en')
            from_lang = request.data.get('from', 'auto')
            task = collection.translate(from_lang, to_lang)
            status = {
                to_lang: task.status
            }
            collection.update(translate_status=status)
            self.invalidate_cache('widget', pk=pk)
            data = {
                'text': _('Within 5 minutes all events will be translated. '
                          'Come back later to check.'),
                'status': task.status,
            }
            return Response(data)
        else:
            return Response(_('You does not have access to this action'),
                            status=status.HTTP_403_FORBIDDEN)

    @detail_route(methods=['POST'])
    def push(self, request, pk):
        """ Action to send mobile push EventCollection.
        """
        user_id = request.user.id
        collection = EventCollection.index.get(id=pk)
        if collection.author.id == user_id:
            push = collection.push_notification('event')
            # send push notification
            task = push_notification.s(
                collection.application_id or collection.meta.id,
                push['text'],
                **push['params']
            ).delay()
            data = {
                'text': _('Collection push sent.'),
                'status': task.status,
                'params': push['params']
            }
            collection.last_push_date = datetime.now()
            collection.save()
            return Response(data)
        else:
            return Response(_('You does not have access to this action'),
                            status=status.HTTP_403_FORBIDDEN)

    @detail_route(methods=['POST'])
    def places(self, request, pk):
        """
        Add place into collection.
        You must send place id in data body.
        By example `{"place_id": "kdg_123"}`
        """
        collection = EventCollection.index.get(id=pk)
        place_id = request.data.get('place_id', None)
        enable = request.data.get('enable', True)
        exclude = request.data.get('exclude', False)
        if place_id:
            if enable == 'true':
                search = Event.index.model.search()
                search = search.query(
                    'term',
                    **{'place.place_id': place_id}
                ).query(
                    'range',
                    **{'schedules.start_date': {
                        'gte': 'now/d',
                        'lte': 'now+1y/d'
                    }}
                ).source('id')
                events = search[:search.count()].execute()
                collection.events.extend(event.id for event in events)
                collection.events = list(set(collection.events))
                collection.add_place(place_id)
                collection.save_push('place', place_id)
                text = _('Place was added to collection')
            else:
                collection.remove_place(place_id)
                text = _('Place was removed from collection')
            if exclude:
                collection.exclude_place(place_id)
                text = _('Place events was excluded from collection')
            collection.save(refresh=True)
            self.invalidate_cache('widget', pk=pk)
            data = {
                'text': text,
                'places': list(collection.places or []),
                'exclude_places': list(collection.exclude_places or []),
                'place_count': len(collection.places or []),
            }
            return Response(data)
        else:
            return Response(_('You does not have access to this action'),
                            status=status.HTTP_403_FORBIDDEN)


class CollectionActionsViewSet(viewsets.GenericViewSet,
                               CreateModelMixin,
                               DestroyModelMixin):
    """
    The **author** of the event collection may update or delete instances.
    """
    queryset = EventCollection.objects.all()
    permission_classes = (DRYPermissions,)

    def get_serializer_class(self):
        return CollectionSerializer

    def invalidate_cache(self, action, **kwargs):
        fragment = 'api:collections.%s' % action
        cache_key = make_template_fragment_key(fragment, kwargs.values())
        cache.delete(cache_key)

    def create(self, request, pk=None, event_pk=None):
        """
        Add event into collection.
        You must send collection id in data body if collection exists.
        By example `{"collections_id": 12}` or `{"collections_id": [12, 13]}`
        """
        # TODO permissions for ES event add to collection
        # if not collection.has_object_collection_permission(request):
        #    return Response(_('You does not have access to this action'),
        #                    status=status.HTTP_403_FORBIDDEN)
        if 'collections_id[]' in request.data:
            collections_id = request.data.getlist('collections_id[]')
        else:
            collections_id = request.data.get('collections_id', [])
        event = Event.index.get_or_None(event_pk)
        collections = EventCollection.index.filter(
            id=collections_id,
            only_public=False
        )
        old_collections = EventCollection.index.filter(
            events=[event_pk],
            only_public=False
        )
        for old_collection in old_collections:
            self.invalidate_cache('widget', pk=old_collection.id)
            if str(old_collection.id) not in collections_id:
                old_collection.remove_event(event_pk)
                old_collection.save(refresh=True)
        for collection in collections:
            self.invalidate_cache('widget', pk=collection.id)
            events = collection.events or []
            if event and event_pk not in events:
                collection.add_event(event_pk)
                collection.save_push('event', event_pk)

                if not collection.get_preview:
                    collection.get_preview = [{'image': None}] * 3
                url = {'image': event.image}
                collection.get_preview.pop(0)
                collection.get_preview.append(url)

                data = {
                    'get_preview': collection.get_preview,
                    'events_count': collection.events_count,
                    'events': collection.events
                }
                collection.save(refresh=True)
        event.save_rank()
        event.save()
        collections = EventCollection.index.filter(
            events=[event_pk],
            only_public=False,
            _source=False
        )
        text = render_to_string(
            'include/counters/_event_collections.html',
            {'collections': collections})
        data = {
            'text': text,
            'status': _('Event was added to collection'),
            'collections_count': collections.hits.total,
        }
        return Response(data)

    def destroy(self, request, pk=None, event_pk=None):
        """
        Detele event from collection.
        """
        collection = EventCollection.index.get(id=pk)
        events = collection.events or []
        # TODO permissions for ES event remove from  collection
        # if not collection.has_object_collection_permission(request):
        #    return Response(_('You does not have access to this action'),
        #                    status=status.HTTP_403_FORBIDDEN)
        if event_pk in events:
            self.invalidate_cache('widget', pk=collection.id)
            event = Event.index.get_or_None(event_pk)
            collection.remove_event(event_pk)
            collection.save(refresh=True)
            collections = EventCollection.index.filter(
                events=[event_pk],
                only_public=False
            )
            text = render_to_string(
                'include/counters/_event_collections.html',
                {'collection': collection})
            data = {
                'text': text,
                'status': _('Event was removed from collection'),
                'collections_count': collections.hits.total,
            }
            event.save_rank()
            event.save()
            return Response(data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
