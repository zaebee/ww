# -*- coding: utf-8 -*-

import logging
import coreapi
import dateparser
import datetime

from rest_framework import viewsets
from rest_framework import status
from rest_framework import filters
from rest_framework import pagination
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from dry_rest_permissions.generics import DRYPermissions

from app.models import Event, EventCollection, EventVote
from app.paginator import ESPaginator
from api.serializers import (
    EventSerializer,
    EventWidgetSerializer,
    VoteSerializer
)
from services.elastic.facets import CollectionSearch, CategorySearch

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class EventPagination(pagination.PageNumberPagination):
    """
    """
    django_paginator_class = ESPaginator

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'results': data
        })


class CollectionFilter(filters.BaseFilterBackend):
    """
    """

    search_param = 'collection'

    def filter_queryset(self, request, queryset, view):
        """
        Return a filtered queryset.
        """
        return queryset

    def get_schema_fields(self, view):
        return [coreapi.Field(
            name=self.search_param, required=False, location='query'
        )]


class EventsViewSet(viewsets.ModelViewSet):
    """
    The **author** of the event may update or delete Event instances.

    retrieve:
    Return a event instance.

    list:
    Return all events.

    create:
    Create a new event.

    update:
    Update a event that is owned by user.

    partial_update:
    Update one or more fields on an existing event.

    delete:
    Event to mark as `deleted=True` instead of the actual remove.
    Delete a event that is owned by user.

    """
    queryset = Event.objects.active()
    filter_backends = (CollectionFilter,)
    # TODO CRUD perms for admin roles
    # permission_classes = (DRYPermissions,)
    serializer_class = EventSerializer
    pagination_class = EventPagination

    def list(self, request):
        query = request.query_params.get('query', None)
        start_date = request.query_params.get('start_date', None)
        end_date = request.query_params.get('end_date', None)

        collection_id = request.query_params.get(
            'collection', None
        )
        categories = request.query_params.get(
            'categories', ''
        )
        categories = categories.split(',') if categories else []

        if start_date:
            start_date = dateparser.parse(start_date, languages=['ru'])
            start_date = start_date.isoformat() if start_date else None

        if end_date:
            end_date = dateparser.parse(end_date, languages=['ru'])
            end_date = end_date.isoformat() if end_date else None

        bounds = {}
        bottom_right = request.query_params.get('bottom_right', None)
        top_left = request.query_params.get('top_left', None)
        if bottom_right:
            lat, lon = bottom_right.split(',')
            bounds['bottom_right'] = {
                'lat': lat,
                'lon': lon
            }
        if top_left:
            lat, lon = top_left.split(',')
            bounds['top_left'] = {
                'lat': lat,
                'lon': lon
            }

        page_size = self.paginator.page_size
        page_number = request.query_params.get(
            self.paginator.page_query_param, 1)
        limit_to = int(page_number) * page_size
        limit_from = limit_to - page_size

        if collection_id:
            collection = EventCollection.index.get(id=collection_id)
            events = collection.events or []
            places = collection.places or []
            exclude_places = collection.exclude_places or []
            queryset = CollectionSearch(
                sort='dates.start_date',
                query={
                    'events': events or [],
                    'places': places or [],
                    'exclude_places': exclude_places or [],
                }
            )
            queryset = queryset[limit_from:limit_to].execute()
        elif categories or bounds:
            queryset = CategorySearch(
                sort='dates.start_date',
                query={
                    'categories': categories,
                    'bounds': bounds,
                    'q': query,
                    'start_date': start_date,
                    'end_date': end_date,
                }
            )
            queryset = queryset[limit_from:limit_to].execute()
        else:
            queryset = Event.index.search(
                q=query,
                start_date=start_date,
                end_date=end_date,
                limit_from=limit_from,
                limit_to=limit_to
            )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = EventWidgetSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = EventWidgetSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        if pk.isdigit():
            return super(EventsViewSet, self).retrieve(request, pk)
        else:
            event = Event.index.get_or_None(id=pk)
            collections = EventCollection.index.filter(
                events=[pk],
                only_public=False
            )
            collections = [{'id': collection.id} for collection in collections]
            if event and len(collections):
                event.collections = collections
        if event:
            similar = Event.index.similar(id=event.similar or [])
            similar = (s.to_dict() for s in similar)
            # event.similar = list(similar)
            serializer = EventWidgetSerializer([event], many=True)
            return Response(serializer.data[0])
        else:
            return Response(status=404)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def destroy(self, request, pk=None):
        """
        Event to mark as `deleted=True` instead of the actual remove.
        """
        event = self.get_object()
        event.deleted = True
        event.save()
        redirect_url = event.author.get_absolute_url()
        result = {
            'success': True,
            'redirect_url': redirect_url,
            'status': _('Your event was removed. You will be redirected in your profile.')
        }
        return Response(result)

    @list_route()
    def latest(self, request):
        """
        Action to get latest events from collection.

        `collection` param is required.
        By example, `{collection: 188}`
        """
        collection_id = request.query_params.get(
            'collection', None
        )

        if collection_id:
            collection = EventCollection.index.get(id=collection_id)
            date = datetime.datetime.now() - datetime.timedelta(days=2)
            events = collection.get_latest_ids_started_from(
                date=date, request=request
            )

            queryset = CollectionSearch(
                sort='dates.start_date',
                query={
                    'events': events or [],
                }
            )
            queryset = queryset.execute()

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = EventWidgetSerializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = EventWidgetSerializer(queryset, many=True)
            return Response(serializer.data)

        return Response(status=404)

    @detail_route(methods=['POST'])
    def vote(self, request, pk):
        """
        Action to vote event.

        You must send your rating in data body. By example, `{"rating": 1}`
        1 out of 5 points (1 - min, 5 - max).
        """

        user_vote = EventVote.index.get(pk, request.user.id)
        event = Event.index.get_or_None(pk)
        if user_vote:
            return Response({'status': _('You already voted.')},
                            status=status.HTTP_400_BAD_REQUEST)

        serializer = VoteSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            data['event_id'] = pk
            data['author'] = {
                'id': request.user.id,
                'username': request.user.username
            }
            EventVote.index.index_add(data)
            rating = EventVote.index.aggregate(pk)
            event.average_rate = rating.average_rate.value or 0
            event.save_rank()
            event.save()
            text = render_to_string('include/counters/_event_votes.html',
                                    {'rating': rating})
            data = {
                'text': text,
                'status': _('Your vote was added'),
                'votes_count': rating.votes.count,
            }
            return Response(data)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['POST'])
    def translate(self, request, pk):
        """
        Action to translate event title and description.
        """
        event = Event.index.get_or_None(pk)
        lang_code = request.data.get('lang_code', 'en')
        if event:
            result = event.translate(lang_code)
            event.update(**result)
            return Response(result)
        else:
            return Response(status=404)
