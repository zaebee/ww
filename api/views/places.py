# -*- coding: utf-8 -*-

import logging
import coreapi
from rest_framework import viewsets
from rest_framework import filters
from rest_framework import pagination
from rest_framework.decorators import list_route
from rest_framework.response import Response

from rest_framework_csv import renderers as r

from elasticsearch.exceptions import NotFoundError

from django.utils.translation import ugettext_lazy as _

from dry_rest_permissions.generics import DRYPermissions

from app.models import EventCollection, EventPlace
from app.paginator import ESPaginator
from api.serializers import (
    PlaceSerializer,
    EventWidgetSerializer,
)
from services.elastic.facets import CollectionSearch

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class PlacePagination(pagination.PageNumberPagination):
    """
    """
    django_paginator_class = ESPaginator

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'results': data
        })


class PlaceRenderer(r.CSVRenderer):
    header = [
        'title', 'city', 'address', 'lat', 'lng',
        'phone', 'website', 'absolute_url', 'events_count'
    ]


class CollectionFilter(filters.BaseFilterBackend):
    """
    """

    search_param = 'collection'

    def filter_queryset(self, request, queryset, view):
        """
        Return a filtered queryset.
        """
        return queryset

    def get_schema_fields(self, view):
        return [coreapi.Field(
            name=self.search_param, required=False, location='query'
        )]


class PlacesViewSet(viewsets.ReadOnlyModelViewSet):
    """
    The **author** of the place may update or delete Place instances.

    retrieve:
    Return a place instance.

    list:
    Return all places.

    create:
    Create a new place.

    update:
    Update a place that is owned by user.

    partial_update:
    Update one or more fields on an existing place.

    delete:
    Place to mark as `deleted=True` instead of the actual remove.
    Delete a place that is owned by user.

    """
    queryset = EventPlace.objects.all()
    filter_backends = (CollectionFilter,)
    # permission_classes = (DRYPermissions,)
    serializer_class = PlaceSerializer
    pagination_class = PlacePagination

    def list(self, request):
        collection_id = request.query_params.get(
            'collection', None
        )

        page_size = self.paginator.page_size
        page_number = request.query_params.get(
            self.paginator.page_query_param, 1)
        limit_to = int(page_number) * page_size
        limit_from = limit_to - page_size

        if collection_id:
            collection = EventCollection.index.get(id=collection_id)
            events = collection.events or []
            places = collection.places or []
            exclude_places = collection.exclude_places or []
            queryset = CollectionSearch(
                sort='dates.start_date',
                query={
                    'events': events or [],
                    'places': places or [],
                    'exclude_places': exclude_places or [],
                }
            )
            queryset = queryset.execute()
            place_ids = [place[0] for place in queryset.facets.places]
            if place_ids:
                queryset = EventPlace.index.model.mget(
                    place_ids[limit_from:limit_to]
                )
            else:
                queryset = []
        else:
            queryset = EventPlace.index.search(
                limit_from=limit_from,
                limit_to=limit_to
            )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = PlaceSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = PlaceSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        if pk.isdigit():
            return super(PlacesViewSet, self).retrieve(request, pk)

        try:
            place = EventPlace.index.model.get(pk)
        except NotFoundError:
            return Response(status=404)

        queryset = CollectionSearch(
            sort='dates.start_date',
            query={
                'events': [],
                'places': [pk],
                'exclude_places': [],
            }
        )
        queryset = queryset.execute()
        events = EventWidgetSerializer(queryset, many=True)
        if place:
            place.events = events.data
            return Response(place.to_dict())
        else:
            return Response(status=404)

    @list_route(renderer_classes=[PlaceRenderer])
    def csv(self, request, *args, **kwargs):
        lat = request.query_params.get('lat', None)
        lng = request.query_params.get('lng', None)
        size = request.query_params.get('size', 10000)
        size = int(size) if str(size).isnumeric() else 10000
        old = request.query_params.get('old', False)

        if old:
            params = {'lt': 1}
        else:
            params = {'gte': 1}
        if lat and lng:
            queryset = EventPlace.index.aggregate_cities(
                observable=True,
                size=size,
                geometry={
                    'lat': lat,
                    'lon': lng,
                }
            )
            queryset = queryset.query(
                'range',
                events_count=params
            )[10000:20000].execute()
        else:
            queryset = EventPlace.index.all()
        serializer = PlaceSerializer(queryset, many=True)
        return Response(serializer.data)
