# -*- coding: utf-8 -*-

from rest_framework import filters
from rest_framework import pagination

from drf_multiple_model.viewsets import MultipleModelAPIViewSet

from app.models import Event, EventCollection, EventPlace
from api.serializers import (
    EventElasticSerializer,
    EventWidgetSerializer,
    CollectionSerializer,
    PlaceSerializer
)


class SearchViewSet(MultipleModelAPIViewSet):
    """
    Search by events, collections and places by title.
    """
    pagination_class = pagination.LimitOffsetPagination
    # filter_backends = (filters.SearchFilter,)
    search_fields = ('title',)
    # objectify = True
    flat = True

    def get_queryList(self):
        query = self.request.query_params.get('query', None)
        lat = self.request.query_params.get('lat', None)
        lng = self.request.query_params.get('lng', None)
        radius = self.request.query_params.get('radius', 1000)
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)

        params = {
            'q': query,
            'start_date': start_date,
            'end_date': end_date
        }
        if lat and lng:
            params['lat'] = lat  # profile.get('lat', None),
            params['lng'] = lng  # profile.get('lng', None),
            params['radius'] = radius  # profile.get('radius', 80000),
        # TODO do requests to providers?
        # instead of orm `filter`
        # collections = EventCollection.index.search(q=query)
        events = Event.index.search(**params)

        queryList = (
            (events, EventWidgetSerializer, 'events'),
            # (collections, CollectionSerializer, 'collections'),
        )
        return queryList
