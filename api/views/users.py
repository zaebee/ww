# -*- coding: utf-8 -*-

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from dry_rest_permissions.generics import DRYPermissions

from app.models import User
from api.serializers import UserSerializer, UserWithEventsSerializer


class UsersViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions for users.

    retrieve:
    Return a user instance.

    list:
    Return all users.
    """
    permission_classes = (DRYPermissions,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_serializer_class(self):
        if self.action in ['profile', 'retrieve']:
            return UserWithEventsSerializer
        elif self.action in ['ww', 'fb', 'ebr', 'kdg']:
            return UserWithEventsSerializer
        return UserSerializer

    @list_route()
    def profile(self, request):
        """ Action for detail my user profile.
        """
        user = request.user
        serializer = self.get_serializer(user)
        return Response(serializer.data)
