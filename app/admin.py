# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin

from .models import (Event, EventCategory, EventPlace, EventCollection,
                     Subscriber, CityEventCategory,
                     EventImage, User, ActionLog, City)

from .forms import EventCategoryForm, CityEventCategoryForm, CityForm


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('admin_avatar',
                    'profile_link', 'email',
                    'business_account', 'is_staff',
                    'get_groups', 'date_joined')

    search_fields = ('email', 'first_name', )
    list_filter = ('business_account', )
    date_hierarchy = 'date_joined'

    search_fields = ('email', 'first_name', )
    list_filter = ('business_account', )

    def get_groups(self, obj):
        return list(obj.groups.values_list('name', flat=True))

    get_groups.short_description = 'Groups'


class EventImageInline(admin.TabularInline):
    model = EventImage
    extra = 2
    exclude = ['width', 'height']


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_per_page = 500
    list_display = (
        'admin_thumbnail', 'id', 'title', 'place', 'author', 'deleted',
    )
    search_fields = ('id', 'title',)
    list_filter = (
        ('place', admin.RelatedOnlyFieldListFilter),
    )

    inlines = [
        EventImageInline,
    ]


@admin.register(EventCollection)
class EventCollectionAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'application_id',
                    'author', 'deleted')


@admin.register(EventCategory)
class EventCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'slug', 'city_names')
    list_filter = (
        ('parent', admin.RelatedOnlyFieldListFilter),
    )
    form = EventCategoryForm


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    search_fields = ('slug', 'name', 'country__name')
    list_display = ('name', 'slug', 'country', 'population')
    exclude = (
        'alt_names', 'title', 'content',
        'meta_title', 'meta_description'
    )
    form = CityForm


@admin.register(CityEventCategory)
class CityEventCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'city', 'category', 'h1_seo',
        'city_category_link', 'events_count'
    )
    list_filter = (
        ('category__parent', admin.RelatedOnlyFieldListFilter),
    )
    form = CityEventCategoryForm
    actions = ['update_events_count']

    def update_events_count(self, request, queryset):
        search = Event.index.search(observable=True)
        count = queryset.count()
        for s in queryset:
            s.events_count = search.query(
                'term', category__slug=s.category.slug
            ).query(
                'geo_distance',
                distance='80000m',
                place__geometry={
                    'lat': s.city.location.y,
                    'lon': s.city.location.x
                }).count()
            s.save()
        self.message_user(
            request,
            "%s event(s) successfully events count updated." % count
        )
    update_events_count.short_description = _('Update events count')


@admin.register(EventPlace)
class EventPlaceAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'address', 'phone', 'email', 'website')


@admin.register(EventImage)
class EventImageAdmin(admin.ModelAdmin):
    list_display = ('event', 'image', 'title')


@admin.register(ActionLog)
class ActionLogAdmin(admin.ModelAdmin):
    list_display = ('date', 'kind', 'extend_info')

    def extend_info(self, obj):
        return str(obj)
    extend_info.short_description = _('Extend info')

    model = ActionLog
    list_per_page = 100


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    list_display = ('event_id', 'email')


# admin.site.register(City, admin.ModelAdmin)
