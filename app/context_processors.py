# -*- coding: utf-8 -*-

from django.conf import settings

GOOGLE_MAP_KEY = getattr(settings, 'GOOGLE_MAP_KEY', None)


def api_keys(request):
    return {
        'LANGUAGE_CODE': request.LANGUAGE_CODE,
        'GOOGLE_MAP_KEY': GOOGLE_MAP_KEY,
        'DEBUG': settings.DEBUG,
    }


def cities(request):
    # TODO get popular cities from storage
    cities = [
        {'name': 'moscow', 'alt_name': 'Москва'},
        {'name': 'saint- petersburg', 'alt_name': 'Санкт-Петербург'},
        {'name': 'novosibirsk', 'alt_name': 'Новосибирск'},
        {'name': 'yekaterinburg', 'alt_name': 'Екатеринбург'},
        {'name': 'nizhniy-novgorod', 'alt_name': 'Нижний Новгород'},
        # {'name': 'samara', 'alt_name': 'Самара'},
        # {'name': 'omsk', 'alt_name': 'Омск'},
        # {'name': 'kazan', 'alt_name': 'Казань'},
        # {'name': 'rostov-na-donu', 'alt_name': 'Ростов-на-Дону'},
        # {'name': 'chelyabinsk', 'alt_name': 'Челябинск'},
        # {'name': 'ufa', 'alt_name': 'Уфа'},
        # {'name': 'volgograd', 'alt_name': 'Волгоград'},
        # {'name': 'perm', 'alt_name': 'Пермь'},
        # {'name': 'krasnoyarsk', 'alt_name': 'Красноярск'},
        # {'name': 'saratov', 'alt_name': 'Саратов'},
        # {'name': 'krasnodar', 'alt_name': 'Краснодар'},
        # {'name': 'sochi', 'alt_name': 'Сочи'},
    ]
    return {
        'cities': cities
    }
