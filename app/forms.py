# -*- coding: utf-8 -*-

from django import forms
from registration import validators
from registration.forms import RegistrationFormUniqueEmail

from tinymce import TinyMCE
from ajax_select.fields import AutoCompleteSelectMultipleField, AutoCompleteSelectField

from . models import (
    User, Event, EventCollection, EventPlace, EventImage,
    EventCategory, City, CityEventCategory
)


class UserRegisterForm(RegistrationFormUniqueEmail):
    password2 = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    username = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    first_name = forms.CharField(
        required=True,
    )

    class Meta:
        model = User
        fields = [
            'first_name', 'username', 'email',
            'password1', 'password2', 'business_account'
        ]

    def clean_password2(self):
        pass

    def clean_username(self):
        username = self.data.get('email')
        return username


class UserProfileForm(forms.ModelForm):
    email = forms.EmailField(
        required=True,
    )
    username = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )

    class Meta:
        model = User
        fields = ('first_name', 'email', 'short_bio', 'headshot', 'username', 'business_account')

    def clean_username(self):
        """
        Set username as email.

        """
        username = self.data.get('email')
        return username

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        user = self.instance
        email = self.cleaned_data['email']
        if user.email != email and User.objects.filter(email__iexact=email):
            raise forms.ValidationError(validators.DUPLICATE_EMAIL)
        return email


class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        exclude = ('author',)


class EventCollectionForm(forms.ModelForm):

    class Meta:
        model = EventCollection
        exclude = ('author',)


class EventPlaceForm(forms.ModelForm):

    class Meta:
        model = EventPlace
        exclude = ('author',)


class EventImageForm(forms.ModelForm):
    image = forms.ImageField()

    class Meta:
        model = EventImage
        fields = ('image',)


class EventCategoryForm(forms.ModelForm):
    description_short = forms.CharField(
        widget=TinyMCE(mce_attrs={'width': 800}),
        required=False)
    description_full = forms.CharField(
        widget=TinyMCE(mce_attrs={'width': 800}),
        required=False)

    class Meta:
        model = EventCategory
        fields = (
            'title', 'description_short', 'description_full',
            'image', 'h1_seo', 'parent',
        )
        exclude = ('provider',)


class CityForm(forms.ModelForm):

    class Meta:
        model = City
        exclude = (
            'alt_names', 'title', 'content',
            'meta_title', 'meta_description'
        )


class CityEventCategoryForm(forms.ModelForm):
    city = AutoCompleteSelectField('city', required=False)
    category = AutoCompleteSelectField('category', required=False)
    description_short = forms.CharField(
        widget=TinyMCE(mce_attrs={'width': 800}),
        required=False)
    description_full = forms.CharField(
        widget=TinyMCE(mce_attrs={'width': 800}),
        required=False)

    class Meta:
        model = CityEventCategory
        fields = (
            'city', 'category',
            'h1_seo', 'title',
            'description_short', 'description_full',
        )
