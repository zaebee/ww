from django.utils.html import escape
from ajax_select import register, LookupChannel

from .models import City, EventCategory


@register('city')
class CityLookup(LookupChannel):

    model = City

    def get_query(self, q, request):
        return self.model.objects.filter(name__icontains=q)

    def format_match(self, obj):
        """ (HTML) formatted item for display in the dropdown """
        return "%s(<i>%s</i>)" % (escape(obj.name), escape(obj.country.code))

    def format_item_display(self, item):
        format = (item.name, item.country.code)
        return "<span class='tag'>%s (%s)</span>" % format


@register('category')
class CategoryLookup(LookupChannel):

    model = EventCategory

    def get_query(self, q, request):
        return self.model.objects.filter(title__icontains=q)

    def format_match(self, obj):
        """ (HTML) formatted item for display in the dropdown """
        return "%s(<i>%s</i>)" % (
            escape(obj.title),
            escape(obj.parent.title if obj.parent else 'all')
        )

    def format_item_display(self, item):
        format = (item.title, item.parent.title if item.parent else 'all')
        return "<span class='tag'>%s (%s)</span>" % format
