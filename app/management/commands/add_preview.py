import time
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from split import chop

from app.models import Event, EventCollection
from services.tasks import make_preview_events


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        search = Event.index.model.search()
        search = search.query('match_all')

        events_count = search.count()
        index = 0
        while index < events_count:
            next_index = index + settings.EVENTS_STEP
            events = search[index:next_index].execute()
            index = next_index
            try:
                make_preview_events(events)
            except:
                pass
            time.sleep(0.5)
