# -*- coding: utf-8 -*-

from django.utils import timezone
from django.db import models


class EventQuerySet(models.QuerySet):
    def active(self):
        """
        Get events not marked as `deleted`.
        """
        return self.filter(deleted=False)


class EventManager(models.Manager):
    def get_queryset(self):
        return EventQuerySet(self.model, using=self._db)  # Important!

    def active(self):
        return self.get_queryset().active()


class CollectionQuerySet(models.QuerySet):
    def public(self):
        """
        Get public event collections.
        """
        return self.filter(private=False)

    def private(self):
        """
        Get private event collections.
        """
        return self.filter(private=True)


class CollectionManager(models.Manager):
    def get_queryset(self):
        return CollectionQuerySet(self.model, using=self._db)  # Important!

    def public(self):
        return self.get_queryset().public()

    def private(self):
        return self.get_queryset().private()


class ScheduleQuerySet(models.QuerySet):
    def actual(self):
        """
        Get actiual schedules is started but not ended now.
        """
        now = timezone.now()
        return self.filter(start_date__lte=now, end_date__gte=now)

    def past(self):
        """
        Get schedules started in past.
        """
        now = timezone.now()
        return self.filter(end_date__lte=now)

    def future(self):
        """
        Get schedules wil be start in future.
        """
        now = timezone.now()
        return self.filter(start_date__gte=now)


class ScheduleManager(models.Manager):
    def get_queryset(self):
        return ScheduleQuerySet(self.model, using=self._db)  # Important!

    def actual(self):
        return self.get_queryset().actual()

    def past(self):
        return self.get_queryset().past()

    def future(self):
        return self.get_queryset().future()
