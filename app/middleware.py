# -*- coding: utf-8 -*-
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject

from django.contrib.gis.geoip2 import GeoIP2


def _get_real_ip(request):
    """
    Get IP from request.

    :param request: A usual request object
    :type request: HttpRequest
    :return: ipv4 string or None
    """
    try:
        # Trying to work with most common proxy headers
        real_ip = request.META['HTTP_X_FORWARDED_FOR']
        return real_ip.split(',')[0]
    except KeyError:
        return request.META['REMOTE_ADDR']
    except Exception:
        # Unknown IP
        return None


def get_location(request):
    g = GeoIP2()
    ip = _get_real_ip(request)
    if ip and ip != '127.0.0.1':
        try:
            location = g.city(ip)
        except:
            location = None
    else:
        location = None
    return location


class LocationMiddleware(MiddlewareMixin):

    def process_request(self, request):
        """ Don't detect location, until we request it implicitly """
        request.location = SimpleLazyObject(lambda: get_location(request))

