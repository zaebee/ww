# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-10 09:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_auto_20170208_2202'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventSchedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(verbose_name='Event start date')),
                ('end_date', models.DateTimeField(verbose_name='Event end date')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='schedules', to='app.Event', verbose_name='Event schedule')),
            ],
            options={
                'verbose_name': 'Event schedule',
                'verbose_name_plural': 'Event schedules',
            },
        ),
    ]
