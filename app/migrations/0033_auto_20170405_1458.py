# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-04-05 14:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0032_user_business_account'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='fb_url',
            field=models.URLField(blank=True, max_length=400, null=True, verbose_name='Facebook URL'),
        ),
        migrations.AddField(
            model_name='event',
            name='vk_url',
            field=models.URLField(blank=True, max_length=400, null=True, verbose_name='Vkontakte URL'),
        ),
    ]
