# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-24 20:16
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0038_auto_20170324_1322'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='provider_images',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Provider Images'),
        ),
    ]
