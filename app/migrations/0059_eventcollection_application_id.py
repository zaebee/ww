# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-08-21 13:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0058_auto_20170721_0741'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventcollection',
            name='application_id',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Application ID'),
        ),
    ]
