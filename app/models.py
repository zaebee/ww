# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os
import hashlib

from django.db import models
from django.dispatch import receiver
from django.conf import settings

from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.search import SearchVectorField
from django.core.urlresolvers import reverse
from django.core.cache import cache

from django.core.cache.utils import make_template_fragment_key

from django.utils.translation import ugettext_lazy as _

from django.core.validators import MaxValueValidator, MinValueValidator

from cities.models import BaseCity, AlternativeName

from versatileimagefield.fields import VersatileImageField, PPOIField
from versatileimagefield.placeholder import OnStoragePlaceholderImage
from dry_rest_permissions.generics import authenticated_users

from pytils.translit import slugify as default_slugify
from autoslug import AutoSlugField

from services.elastic.managers import (
    EventIndexManager, CollectionIndexManager,
    PlaceIndexManager, CityIndexManager, VoteIndexManager,
    CategoryIndexManager
)

from . managers import EventManager, CollectionManager, ScheduleManager


def md5_slugify(name):
    name = default_slugify(name)
    hash_name = hashlib.md5(name.encode())
    return hash_name.hexdigest()


def get_image_dir(instance, filename):
    name, ext = os.path.splitext(filename)
    return 'events/%s%s' % (md5_slugify(name), ext)


def get_avatar_dir(instance, filename):
    name, ext = os.path.splitext(filename)
    return 'users/%s%s' % (md5_slugify(name), ext)


class User(AbstractUser):
    headshot = VersatileImageField(_('Headshot'),
                                   upload_to=get_avatar_dir,
                                   blank=True, null=True,
                                   placeholder_image=OnStoragePlaceholderImage(
                                       path='placeholders/userplaceholder.png'
                                   ),
                                   ppoi_field='headshot_ppoi')
    username = models.CharField(_('username'), max_length=200,
                                unique=True, blank=True, null=True)
    first_name = models.CharField(_('Name'), max_length=200,
                                  blank=True, null=True)
    last_name = models.CharField(_('Last name'), max_length=200,
                                 blank=True, null=True)
    business_account = models.BooleanField(_('Business account'),
                                           default=False)
    vk_profile = JSONField(_('Vk profile'), blank=True, null=True)
    fb_profile = JSONField(_('Fb profile'), blank=True, null=True)
    favorite_places = JSONField(_('Favorite places'), blank=True, null=True)
    short_bio = models.TextField(_('Short biography'),
                                 blank=True, null=True)
    headshot_ppoi = PPOIField()

    REQUIRED_FIELDS = ['email', 'first_name']

    def __str__(self):
        return "%s" % (self.email)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def admin_avatar(self):
        url = self.headshot.crop['50x50'].url
        return '<img src="%s">' % url

    admin_avatar.short_description = _('avatar')
    admin_avatar.allow_tags = True

    def profile_link(self):
        url = self.get_absolute_url()
        if self.first_name:
            result = '<a target="_blank" href="%s">%s</a>' % (
                url,
                self.first_name[:25])
            return result

    profile_link.short_description = _('Profile')
    profile_link.allow_tags = True

    @models.permalink
    def get_absolute_url(self):
        return ('profile-detail', None, {'pk': str(self.pk)})

    @staticmethod
    def has_read_permission(request):
        """ Allow to any user read User list.
        """
        return True

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        """ Disallow to create new users through api.
        """
        return False

    @staticmethod
    def has_object_read_permission(request):
        """ Allow to any user read User object.
        """
        return True

    @authenticated_users
    def has_object_write_permission(self, request):
        """ Allow update only self profile
        """
        return request.user == self

    @staticmethod
    @authenticated_users
    def has_profile_permission(request):
        """ Allow show self profile.
        """
        return True


class Subscriber(models.Model):
    email = models.EmailField(_('email'))
    event_id = models.CharField(_('Event ID'), max_length=200)
    event_title = models.CharField(_('Event title'),
                                   max_length=500,
                                   blank=True, null=True)
    event_slug = models.CharField(_('Event slug'),
                                  max_length=500,
                                  blank=True, null=True)
    date_added = models.DateTimeField(_(u'Date Added'),
                                      auto_now_add=True)

    def __str__(self):
        return "%s" % (self.email)

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribers')


class Event(models.Model):
    title = models.CharField(_('Title'), max_length=200)
    slug = AutoSlugField(populate_from='title', unique=True)
    description = models.TextField(_('Description'), blank=True)

    video_url = models.URLField(_('Video URL'), max_length=200,
                                blank=True, null=True)

    vk_url = models.URLField(_('Vkontakte URL'),
                             max_length=1200,
                             blank=True,
                             null=True)

    fb_url = models.URLField(_('Facebook URL'),
                             max_length=1200,
                             blank=True,
                             null=True)

    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               related_name='events',
                               blank=True, null=True,
                               on_delete=models.CASCADE)

    collections = models.ManyToManyField('EventCollection',
                                         related_name='events',
                                         blank=True)
    place = models.ForeignKey('EventPlace',
                              verbose_name=_('Place'),
                              blank=True, null=True)
    category = models.CharField(_('Category'),
                                max_length=30,
                                blank=True, null=True)

    similar = JSONField(_('Similar events'), blank=True, null=True)
    ticket_price = models.CharField(_('Ticket price'),
                                    max_length=100,
                                    blank=True,
                                    null=True)
    ticket_url = models.URLField(_('Ticket URL'),
                                 blank=True, null=True)
    currency = models.CharField(_('Currency'),
                                max_length=200,
                                blank=True,
                                null=True)

    date_added = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    date_updated = models.DateTimeField(_(u'Date Updated'), auto_now=True)
    deleted = models.BooleanField(_('Mark as deleted'), default=False)

    provider = models.CharField(_('Provider'),
                                max_length=20,
                                blank=True, null=True)
    provider_id = models.BigIntegerField(_('Provider ID'),
                                         blank=True,
                                         null=True)
    provider_image = models.CharField(_('Provider Image'),
                                      max_length=400,
                                      blank=True, null=True)
    provider_images = JSONField(_('Provider Images'),
                                blank=True,
                                null=True)

    search = SearchVectorField(_('Search'),
                               blank=True,
                               null=True,
                               editable=False)

    objects = EventManager()
    index = EventIndexManager()

    def __str__(self):
        return "%s" % (self.title)

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        permissions = (
            ('translate_event', _('Translate Event')),
        )

    def admin_thumbnail(self):
        try:
            image = self.images.first().image
            url = image.crop['100x100'].url
            return '<img src="%s">' % url
        except:
            return None

    admin_thumbnail.short_description = _('preview')
    admin_thumbnail.allow_tags = True

    @property
    def average_rate(self):
        """
        Get the averate rate for event.
        """
        return self.votes.aggregate(value=models.Avg('rating'))

    @property
    def voted_users(self):
        """
        Get users id that voted for event.
        """
        return self.votes.values_list('author', flat=True)

    @property
    def actual_schedule(self):
        """
        Get the current schedule, or the near future.
        """
        return self.schedules.actual().first() or self.schedules.future().first()

    @property
    def actual_start_date(self):
        """
        Get start_date for actual schedule or the first in the future.
        """
        schedule = self.actual_schedule or self.schedules.first()
        if schedule:
            return schedule.start_date

    @property
    def actual_end_date(self):
        """
        Get end_date for actual schedule or the first in the future.
        """
        schedule = self.actual_schedule or self.schedules.first()
        if schedule:
            return schedule.end_date

    @property
    def mainimage(self):
        return self.images.first()

    @property
    def image(self):
        url = None
        if self.images.count():
            eventimage = self.images.first()
            image = eventimage.image
            try:
                url = image.crop['600x600'].url
            except IOError:
                pass
        return url

    @property
    def get_collection_preview(self):
        url = '/static/images/placeholder.png'
        if self.images.count():
            eventimage = self.images.first()
            image = eventimage.image
            try:
                url = image.crop['320x224'].url
            except IOError:
                pass
        return url

    @models.permalink
    def get_absolute_url(self):
        return ('event-detail', None, {'pk': str(self.id), 'slug': str(self.slug)})

    @staticmethod
    def has_read_permission(request):
        """ Allow to any user read Event list.
        """
        return True

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        """ Allow to authenticated users create Event object.
        """
        return True

    @staticmethod
    def has_object_read_permission(request):
        """ Allow to any user read Event object.
        """
        return True

    @authenticated_users
    def has_object_write_permission(self, request):
        """ Allow only author update/delete Event object.
        """
        return request.user == self.author

    @authenticated_users
    def has_object_collection_permission(self, request):
        """
        Allow add event to collection only user owner.
        Also allow create new empty collection.
        """
        has_permission = True
        collection_id = request.data.get('id')
        if collection_id:
            collections = request.user.collections.filter(id=collection_id)
            if collections.count():
                has_permission = True
            else:
                has_permission = False
        return has_permission

    @authenticated_users
    def has_object_vote_permission(self, request):
        """ Allow event vote only for authenticated users.
        """
        return True


class EventCollection(models.Model):
    title = models.CharField(_('Title'), max_length=200)
    slug = AutoSlugField(populate_from='title', unique=True)
    description = models.TextField(_('Description'), blank=True)
    application_id = models.CharField(_('Application ID'), max_length=200,
                                      blank=True, null=True)

    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               related_name='collections',
                               on_delete=models.CASCADE)
    private = models.BooleanField(_('Private collection'), default=False)
    deleted = models.BooleanField(_('Mark as deleted'), default=False)

    date_added = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    date_updated = models.DateTimeField(_(u'Date Updated'), auto_now=True)

    subscribers = models.ManyToManyField('User',
                                         related_name='subscribed_collections',
                                         blank=True)
    limit_dates = models.BooleanField(_('Limit dates'), default=False)
    limit_date_from = models.DateField(_('Limit date from'),
                                       null=True, blank=True)
    limit_date_to = models.DateField(_('Limit date to'),
                                     null=True, blank=True)

    preview = JSONField(_('Collection image'), blank=True, null=True)
    objects = CollectionManager()
    index = CollectionIndexManager()

    def __str__(self):
        return "%s" % (self.title)

    class Meta:
        verbose_name = _('Event collection')
        verbose_name_plural = _('Event collections')
        permissions = (
            ('translate_collection', _('Translate Collection')),
        )

    @models.permalink
    def get_absolute_url(self):
        return ('collection-detail', None, {'pk': self.pk, 'slug': str(self.slug)})

    @property
    def events_count(self):
        return self.events.count()

    @property
    def get_preview(self):
        """
        Get first image from any events in collection.
        """
        events = self.events.all()
        images = EventImage.objects.filter(event__in=events)
        if images.count():
            first_image = images.first()
            url = {'image': first_image.image.crop['320x224'].url}
        else:
            url = {'image': '/static/images/placeholder.png'}
        result = [url] * 3
        for image in images:
            result.pop(0)
            result.append({'image': image.image.crop['320x224'].url})
        return result

    @staticmethod
    def invalidate_cache(action, **kwargs):
        fragment = 'api:collections.%s' % action
        cache_key = make_template_fragment_key(fragment, kwargs.values())
        cache.delete(cache_key)

    @staticmethod
    def has_read_permission(request):
        """ Allow to any user read EventCollection list.
        """
        return True

    @staticmethod
    @authenticated_users
    def has_write_permission(request):
        """ Allow to authenticated users create EventCollection object.
        """
        return True

    @staticmethod
    def has_object_read_permission(request):
        """ Allow to any user read EventCollection object.
        """
        return True

    @authenticated_users
    def has_object_write_permission(self, request):
        """ Allow only author update/delete EventCollection object.
        """
        return request.user == self.author or request.user.is_staff

    @authenticated_users
    def has_object_subscribe_permission(self, request):
        """ Allow subscribe to NOT private collection only for authenticated users.
        """
        return not self.private


class EventPlace(models.Model):
    title = models.CharField(_('Title'), max_length=200)
    slug = AutoSlugField(populate_from='title', unique=True)
    description = models.TextField(_('Description'), blank=True, null=True)

    address = models.CharField(_('Address'), max_length=200)
    city = models.CharField(_('City'), max_length=255)

    lat = models.FloatField(_('Lat'))
    lng = models.FloatField(_('Lng'))

    phone = models.CharField(_('Phone'), max_length=200,
                             blank=True, null=True)
    email = models.EmailField(_('Email'), blank=True, null=True)
    website = models.URLField(_('Website'), blank=True, null=True)

    date_added = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    date_updated = models.DateTimeField(_(u'Date Updated'), auto_now=True)

    index = PlaceIndexManager()

    def __str__(self):
        return "%s" % (self.title)

    class Meta:
        verbose_name = _('Event place')
        verbose_name_plural = _('Event places')
        permissions = (
            ('translate_place', _('Translate Place')),
        )


class CityEventCategory(models.Model):
    city = models.ForeignKey('City',
                             related_name='event_categories')
    category = models.ForeignKey('EventCategory',
                                 related_name='city_categories')
    description_short = models.TextField(_('Short description'), blank=True)
    description_full = models.TextField(_('Description'), blank=True)
    image = models.CharField(_('Image'), max_length=200,
                             blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255, blank=True, null=True)
    h1_seo = models.CharField(_('H1 Seo'), max_length=200,
                              blank=True, null=True)
    events_count = models.PositiveIntegerField(_('Events count'),
                                               blank=True, null=True)

    @models.permalink
    def get_absolute_url(self):
        _id, city_slug = self.city.slug.split('-', 1)
        if self.category and self.category.parent:
            return ('city-subcategory-detail', None, {
                'city_name': city_slug.lower(),
                'category_slug': self.category.parent.slug,
                'subcategory_slug': self.category.slug,
            })
        else:
            return ('city-category-detail', None, {
                'city_name': city_slug.lower(),
                'category_slug': self.category.slug,
            })

    def city_category_link(self):
        url = self.get_absolute_url()
        return '<a target="_blank" href="%s">URL</a>' % url

    city_category_link.short_description = _('Link')
    city_category_link.allow_tags = True


class EventCategory(models.Model):
    title = models.CharField(_('Title'), max_length=200)
    slug = AutoSlugField(populate_from='title', unique=True)
    description_short = models.TextField(_('Short description'), blank=True)
    description_full = models.TextField(_('Description'), blank=True)
    provider = models.CharField(_('Provider'), max_length=20,
                                blank=True, null=True)

    image = models.CharField(_('Image'), max_length=200,
                             blank=True, null=True)
    h1_seo = models.CharField(_('H1 Seo'), max_length=200,
                              blank=True, null=True)

    date_added = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    date_updated = models.DateTimeField(_(u'Date Updated'), auto_now=True)
    parent = models.ForeignKey('self',
                               verbose_name=_('Parent'),
                               blank=True, null=True,
                               related_name='subcategories')

    cities = models.ManyToManyField('City',
                                    through=CityEventCategory,
                                    related_name='categories',
                                    blank=True)
    index = CategoryIndexManager()

    def city_names(self):
        cities = self.cities.values_list('name', flat=True)
        cities = ', '.join(cities)
        return cities

    city_names.short_description = _('Cities')
    city_names.allow_tags = True

    def __str__(self):
        if self.parent:
            return "%s / %s" % (self.parent, self.title)
        else:
            return "%s" % self.title

    class Meta:
        verbose_name = _('Event category')
        verbose_name_plural = _('Event categories')
        permissions = (
            ('translate_category', _('Translate Category')),
        )


class EventImage(models.Model):
    event = models.ForeignKey(Event,
                              related_name='images',
                              blank=True, null=True)
    image = VersatileImageField(_('Image'),
                                upload_to=get_image_dir,
                                width_field='width',
                                height_field='height',
                                ppoi_field='ppoi')
    height = models.PositiveIntegerField(_('Image Height'),
                                         blank=True, null=True)
    width = models.PositiveIntegerField(_('Image Width'),
                                        blank=True, null=True)
    ppoi = PPOIField(_('Image PPOI'))

    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    title = models.CharField(_('Title'), max_length=255,
                             blank=True, null=True)
    alt = models.CharField(_('Alt'), max_length=255,
                           blank=True, null=True)
    is_mainimage = models.BooleanField(_('Is mainimage'), default=False)

    date_added = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    date_updated = models.DateTimeField(_(u'Date Updated'), auto_now=True)

    class Meta:
        verbose_name = _('Event image')
        verbose_name_plural = _('Event images')


class EventSchedule(models.Model):
    event = models.ForeignKey(Event, related_name='schedules',
                              verbose_name=_('Event schedule'))

    start_date = models.DateTimeField(_('Event start date'), db_index=True)
    end_date = models.DateTimeField(_('Event end date'), db_index=True)

    objects = ScheduleManager()

    @property
    def start_time(self):
        return self.start_date.strftime('%H:%M')

    @property
    def end_time(self):
        return self.end_date.strftime('%H:%M')

    class Meta:
        verbose_name = _('Event schedule')
        verbose_name_plural = _('Event schedules')
        ordering = ['id']


class EventVote(models.Model):
    event = models.ForeignKey(Event, related_name='votes',
                              blank=True, null=True)

    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='votes',
                               on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(_('User Rating'),
                                         validators=[
                                             MinValueValidator(1),
                                             MaxValueValidator(5)
                                         ])

    date_added = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    date_updated = models.DateTimeField(_(u'Date Updated'), auto_now=True)

    index = VoteIndexManager()

    class Meta:
        verbose_name = _('Event vote')
        verbose_name_plural = _('Event votes')


class ActionLog(models.Model):
    ORG_REGISTRATION = 'org_reg'
    COL_CREATE = 'col_cre'
    EVT_CREATE_WW = 'evt_cre_ww'
    EVT_LONG_LINK = 'evt_link'

    TYPE_CHOICES = (
        (ORG_REGISTRATION, _('Organization registration')),
        (COL_CREATE, _('Create collection')),
        (EVT_CREATE_WW, _('Create event at ww')),
        (EVT_LONG_LINK, _('Create event with long link')),
    )

    date = models.DateTimeField(_(u'Date Added'), auto_now_add=True)
    kind = models.CharField(_('Kind'), max_length=11, choices=TYPE_CHOICES)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user',
                               blank=True, null=True)
    collection = models.ForeignKey(EventCollection, related_name='collection',
                                   blank=True, null=True)
    event = models.ForeignKey(Event, related_name='event',
                              blank=True, null=True)

    def __str__(self):
        author_profile = reverse('profile-detail', kwargs={'pk': self.author.pk})

        if self.kind == ActionLog.ORG_REGISTRATION:
            representation = '%s | %s' % (
                author_profile, self.author.email
            )
        elif self.kind == ActionLog.COL_CREATE:
            collection_detail = reverse('collection-detail',
                                        kwargs={
                                            'pk': self.collection.pk,
                                            'slug': self.collection.slug
                                        })
            representation = '%s | %s' % (
                author_profile, collection_detail
            )
        elif self.kind == ActionLog.EVT_CREATE_WW:
            event_detail = reverse('event-detail',
                                   kwargs={
                                       'pk': self.event.pk,
                                       'slug': self.event.slug
                                   })
            representation = '%s | %s | %s' % (
                author_profile,
                event_detail,
                self.event.place.city if self.event.place else ''
            )
        elif self.kind == ActionLog.EVT_LONG_LINK:
            representation = '%s | %s | %s' % (
                self.event.pk, self.date.strftime('%d.%m.%Y %H:%M'), self.kind
            )
        else:
            representation = '%s | %s | %s' % (
                self.id, self.date.strftime('%d.%m.%Y %H:%M'), self.kind
            )
        return representation

    class Meta:
        verbose_name = _('Action log')
        verbose_name_plural = _('Action logs')
        ordering = ['-date']


class City(BaseCity, models.Model):
    alt_names = models.ManyToManyField(AlternativeName)
    title = models.CharField(_('Page Title'),
                             max_length=200,
                             blank=True,
                             null=True)
    content = models.TextField(_('Content'),
                               blank=True,
                               null=True)

    meta_title = models.CharField(_('Page Metatitle'),
                                  max_length=200,
                                  blank=True,
                                  null=True)
    meta_description = models.TextField(_('Meta Description'),
                                        blank=True,
                                        null=True)

    index = CityIndexManager()

    class Meta(BaseCity.Meta):
        pass


@receiver(models.signals.post_delete, sender=EventImage)
def delete_EventImageModel_images(sender, instance, **kwargs):
    """
    Deletes EventImageModel image renditions on post_delete.
    """
    # Deletes Image Renditions
    instance.image.delete_all_created_images()
    # Deletes Original Image
    instance.image.delete(save=False)


@receiver(models.signals.post_save, sender=Event)
def update_event_index(sender, instance, created, **kwargs):
    from api.serializers import EventElasticSerializer
    serializer = EventElasticSerializer(instance=instance)
    data = serializer.data
    if created:
        sender.index.index_add(data)
        if instance.category:
            EventCategory.objects.get_or_create(
                title=instance.category,
                provider='ww')
        ActionLog.objects.create(
            kind=ActionLog.EVT_CREATE_WW,
            author=instance.author,
            event=instance)
        if len(instance.fb_url or '') > 600 or len(instance.vk_url or '') > 600:
            ActionLog.objects.create(
                kind=ActionLog.EVT_LONG_LINK,
                author=instance.author,
                event=instance)
    else:
        sender.index.index_update(data)


@receiver(models.signals.post_save, sender=EventCollection)
def update_collection_index(sender, instance, created, **kwargs):
    from api.serializers import CollectionSerializer
    serializer = CollectionSerializer(instance=instance)
    data = serializer.data
    if created:
        sender.index.index_add(data)
        ActionLog.objects.create(
            kind=ActionLog.COL_CREATE,
            author=instance.author,
            collection=instance)
    else:
        sender.index.index_update(data)


@receiver(models.signals.post_delete, sender=EventCollection)
def delete_collection_index(sender, instance, *args, **kwargs):
    collection = sender.index.get(id=instance.id)
    collection.delete()


@receiver(models.signals.post_save, sender=User)
def update_collection_user_index(sender, instance, created, **kwargs):
    if not created:
        from api.serializers import CollectionSerializer, UserSerializer
        serialized_user = UserSerializer(instance=instance)
        queryset = instance.collections.all()
        serializer = CollectionSerializer(queryset, many=True)
        qs = []
        for collection in serializer.data:
            row = {
                'id': collection['id'],
                'provider_id': collection['provider_id'],
                'author': serialized_user.data,
            }
            qs.append(row)
        EventCollection.index.bulk_index(queryset=qs)
