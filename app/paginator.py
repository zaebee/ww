# -*- coding: utf-8 -*-

from django.core.paginator import Paginator, Page, cached_property

from app.utils import get_page_numbers


class ESPaginator(Paginator):
    """
    Override Django's built-in Paginator class to take
    in a count/total number of items;
    Elasticsearch provides the total as a part of the query results,
    so we can minimize hits.
    """
    def __init__(self, *args, **kwargs):
        super(ESPaginator, self).__init__(*args, **kwargs)

    def page(self, number):
        # this is overridden to prevent any slicing of
        # the object_list - Elasticsearch has returned the sliced data already.
        number = self.validate_number(number)
        return Page(self.object_list, number, self)

    @cached_property
    def count(self):
        """
        Returns the total number of objects, across all pages.
        """
        try:
            return self.object_list.hits.total
        except (AttributeError, TypeError):
            # AttributeError if object_list has no count() method.
            # TypeError if object_list.count() requires arguments
            # (i.e. is of type list).
            return len(self.object_list)

    def elastic_page_range(self, page):
        """
        Returns a range of pages for iterating through within
        a template for loop.
        """
        return get_page_numbers(page, self.num_pages)
