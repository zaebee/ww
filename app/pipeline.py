# -*- coding: utf-8 -*-

import requests

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage

from django.shortcuts import redirect
from social_core.pipeline.partial import partial


def save_avatar(backend, user, response, *args, **kwargs):
    """
    Save social picture as user headshot (avatar).
    """
    if backend.name == 'facebook':
        avatar_url = 'https://graph.facebook.com/%s/picture' % response.get('id')
        params = {'height': 400}
    if backend.name == 'vk-oauth2':
        avatar_url = 'https://api.vk.com/method/users.get?user_id=%s&v=5.23&fields=photo_400_orig' % response.get('user_id')
        avatar_response = requests.get(avatar_url)
        try:
            avatar_response = avatar_response.json()['response']
            if len(avatar_response):
                avatar_url = avatar_response[0]['photo_400_orig']
        except:
            avatar_url = response.get('user_photo')

        params = {}

    try:
        # avatar already exists
        avatar_file = user.headshot.file
    except ValueError:
        # user has no avatar
        if avatar_url:
            image_path = 'users/%s.jpg' % response.get('id', user.id)
            response = requests.get(avatar_url, params=params, stream=True)
            content = ContentFile(response.content)
            default_storage.save(image_path, content)
            user.headshot = image_path
            user.save()


def save_extra_data(backend, user, response, *args, **kwargs):
    """
    Store social data to user specified field.
    It is need for getting link to social profile and other data.
    """
    if backend.name == 'facebook':
        user.fb_profile = response
    if backend.name == 'vk-oauth2':
        user.vk_profile = response
    user.save()


def remove_extra_data(strategy, entries, user_storage, *args, **kwargs):
    provider = kwargs.get('name')
    user = kwargs.get('user')
    if provider == 'facebook':
        user.fb_profile = None
    if provider == 'vk-oauth2':
        user.vk_profile = None
    user.save()


@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    """
    Required email if social provider has no email in response.
    """
    if user and user.email:
        return
    if is_new and not details.get('email'):
        if strategy.session_get('saved_email'):
            details['email'] = strategy.session_pop('saved_email')
        else:
            strategy.session_set('partial_pipeline', details)
            return redirect('require_email')
