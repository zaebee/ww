from gevent import subprocess
from django.conf import settings


def run(*args):
    """
    Backup `provider` events for 120 days from now.
    Example:
        `./manage.py runscript backup --script-args clt now now+120d`
    """
    start = 'now'
    end = 'now+120d'
    provider = 'ww'  # only ww events default dump
    es_host = getattr(settings, 'ES_HOSTS', ['localhost:9000'])
    es_host = es_host[0]

    # filter by collection_id
    #
    if len(args) == 1:
        provider = args[0]

    params = {
        'es_host': es_host,
        'provider': provider,
        # 'start': start,
        # 'end': end
    }
    # NOTE need npm install -g elasticdump
    cmd = str(
        'elasticdump '
        '--input=http://%(es_host)s/event-index '
        '--searchBody '
        '\'{"query":{"term":{"provider": "%(provider)s"}}}\' '

        '--output=$ '
        '| gzip > /tmp/event-index-%(provider)s.json.gz'
    )
    cmd = cmd % params
    result = subprocess.getoutput(cmd)
    return result
