from services.tasks import calculate_rank


def run(*args):
    """
    Update `rank` for new events.
    """
    params = {}
    provider = None
    if len(args) == 1:
        # indexing only events without rank
        if args[0] == 'only_new':
            params = {
                'only_new': True
            }
        # filter by provider
        else:
            provider = args[0]

    # filter by provider and query params
    elif len(args) == 2:
        provider = args[0]
        params = {
            'q': args[1]
        }
    # filter by geolocation
    elif len(args) == 3:
        params = {
            'lat': args[0],
            'lng': args[1],
            'radius': args[2],
        }
    # filter by provider and geolocation
    elif len(args) == 4:
        params = {
            'q': args[0],
            'lat': args[1],
            'lng': args[2],
            'radius': args[3]
        }
    task = calculate_rank.s(provider, **params)  # queue='rank'
    response = task.delay()
    return response.result
