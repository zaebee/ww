from datetime import datetime
from app.models import Event

from split import chop


def get_queryset(search):
    for event in search.scan():
        dates = []
        now = datetime.now()
        schedules = event.schedules

        for schedule in schedules:
            actual = {}
            if schedule.start_date or schedule.end_date:
                end = schedule.end_date or None
                not_ended = end.date() >= now.date() if end else False

                start = schedule.start_date or None
                not_started = start.date() >= now.date() if start else False

                if end and not_ended:
                    # Event finish in future
                    start_date = start.date() if not_started else now.date()
                    start_time = start.strftime('%H:%M') if start else None
                    actual['start_date'] = start_date
                    actual['start_time'] = start_time
                    actual['end_date'] = end.date()
                    actual['end_time'] = end.strftime('%H:%M')
                elif start and not_started:
                    # Event start in future
                    start_date = start.date() if start else None
                    start_time = start.strftime('%H:%M')
                    actual['start_date'] = start_date
                    actual['start_time'] = start_time
                    actual['end_date'] = end.date() if end else None
                    actual['end_time'] = end.strftime('%H:%M') if end else None
                if actual:
                    dates.append(actual)

        yield {
            'dates': dates,
            'provider': event.provider,
            'provider_id': event.provider_id,
        }


def run(*args):
    """
    Cut off old schedules for actual events.
    """
    search = Event.index.search(observable=True)
    if len(args):
        collection = args[0]
        search = search.filter('terms', id=collection.events or [])
    else:
        search = search.filter('range', dates__start_date={'lt': 'now/d'})
    total = search.count()
    search = search.params(scroll='100m')
    queryset = get_queryset(search)
    chunks = chop(500, queryset)
    # TODO parallel write into elastic
    for chunk in chunks:
        Event.index.bulk_index(queryset=chunk, refresh=False)
    return total
