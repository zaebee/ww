from split import chop

from hitcount.models import HitCount
from services.clt.api import ApiWrapper
from app.models import Event, EventVote


def run(*args):
    """
        `./manage.py runscript import_clt
    """

    api = ApiWrapper()
    search = Event.index.search(provider='clt', observable=True)
    # search = search.query('range', rank={'gt': 0})
    search = search.source([
        'id', 'provider', 'provider_id', 'slug', 'rank'
    ]).sort('rank')

    chunk_size = 50
    chunks = chop(chunk_size, search.scan())
    for chunk in chunks:
        for event in chunk:
            votes = EventVote.index.aggregate(event.id)
            hitcount = HitCount.get_for_object(event)
            event.likes = votes.votes.count
            event.views = hitcount.hits
        request = api.import_events(chunk)
        response = request.send()
        print(response.response)
        # yield request
