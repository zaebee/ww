# -*- coding: utf-8 -*-

from app.models import EventCollection
from api.serializers import CollectionSerializer


def run(*args):
    """
    Import collections from postgres to ES.
    """
    qs = EventCollection.objects.all()
    serializer = CollectionSerializer(qs, many=True)
    EventCollection.index.bulk_index(queryset=serializer.data)
    search = EventCollection.index.model.search()
    print('Indexed collections: %s' % search.count())
