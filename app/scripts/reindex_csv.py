import re
import csv
import logging
import requests
from app.models import User, Event, EventPlace, EventCollection


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

pattern = re.compile(
    # r'^/(?P<prefix>[\w]+)/(?P<pk>[\d]+)-(?P<slug>[-_\d\w]+)/$'
    r'(?P<host>[\w\d./:]+)?/(?P<doc>[\w]+)/(?P<pk>[\d]+)-(?P<slug>[-_\d\w]+)/$'
)


def run_collections(data):
    for row in data:
        url = row['URL Коллекции']
        result = pattern.search(url)
        if result:
            result = result.groupdict()
            try:
                collection = EventCollection.index.get(id=result['pk'])

                collection.update(
                    title_seo={
                        'raw': row['Title'],
                        # 'en': row['title EN']
                    },
                    h1_seo={
                        'raw': row['H1'],
                        # 'en': row['H1 EN'],
                    }
                )
                print('SAVED', collection.title)
            except:
                print('ERROR', url)
                logger.error('Collection not found: %s' % url)


def run_events(data):
    for row in data:
        status = int(row['currentHttpCode'])  # 200 or 404, or ....
        if status == 404:
            url = row['url']
            response = requests.get('https://whatwhere.world%s' % url)
            result = pattern.search(url)
            if result and response.status_code in [404, 500]:
                match = result.groupdict()
                try:
                    instance = Event.index.get(match['pk'])
                except:
                    instance = Event.index.get_or_None(match['pk'])

                if instance:
                    instance.place.place_id = '%s_%s' % (
                        instance.provider or 'ww', instance.place.provider_id)
                    instance.save()
                    print('saved %s:%s' % (match['doc'], match['pk']))
                else:
                    print('NOT FOUND %s:%s' % (match['doc'], match['pk']))
            else:
                print('response status: %s - %s' % (
                    response.status_code, row['url']))


def run_places(data):
    for row in data:
        status = int(row['currentHttpCode'])  # 200 or 404, or ....
        if status == 404:
            url = row['url']
            response = requests.get('https://whatwhere.world%s' % url)
            result = pattern.search(url)
            if result and response.status_code in [404, 500]:
                match = result.groupdict()
                try:
                    instance = EventPlace.index.get(provider_id=match['pk'])
                except:
                    instance = None
                if instance:
                    instance.place_id = '%s_%s' % (
                        instance.provider or 'ww', instance.provider_id)
                    instance.save()
                    print('saved %s:%s' % (match['doc'], match['pk']))
                else:
                    print('NOT FOUND %s:%s' % (match['doc'], match['pk']))
            else:
                print('response status: %s - %s' % (
                    response.status_code, row['url']))


def run(*args):
    """
    Reindex actual events/places in Elasticsearch from csv file.
    Example:
        python manage.py runscript reindex_csv --script-args /tmp/events.csv
    """
    if len(args) == 2:
        doc = args[0]
        file_path = args[1]
    else:
        doc = ''
        file_path = '/tmp/events.csv'

    data = csv.DictReader(open(file_path, 'r'))
    if doc is 'events':
        run_events(data)
    elif doc is 'places':
        run_places(data)
    elif doc is 'collections':
        run_collections(data)
