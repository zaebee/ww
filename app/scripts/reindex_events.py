import logging
from split import chop
from datetime import datetime
from app.models import Event, default_slugify


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def run(*args):
    """
    Reindex actual events in Elasticsearch storage.
    """
    if len(args) == 1:
        provider = args[0]
        event_ids = []
    elif len(args) == 2:
        provider = args[0]
        event_ids = args[1].split(',')
    else:
        provider = None
        event_ids = []

    search = Event.index.model.search()
    # search = Event.index.search(observable=True)
    search = search.source(
        ['id', 'slug', 'provider_id', 'provider', 'place', 'title']
    )
    if provider:
        search = search.query('term', provider=provider)
    if event_ids:
        search = search.query('terms', id=event_ids)
    search = search.exclude('exists', field='slug.raw')

    chunk_size = 500
    ids = (e for e in search.scan())
    chunks = chop(chunk_size, ids)
    for chunk in chunks:
        logger.info('Reindex events prepare: %s' % len(chunk))
        for event in chunk:
            event.place = event.place if event.place else {}
            if not event.place.place_id:
                event.place.place_id = '%s_%s' % (
                    event.provider, event.place.provider_id or 'unknown')

            if event.place.title:
                title = event.place.title.replace('&quot;', '')
                event.place.title = title
                if title in ['0', 'none']:
                    event.place.title = 'unknown'
                    event.place.title_localized = {
                        'ru': 'без названия',
                        'en': 'unknown'
                    }
            # empty place title
            else:
                event.place.title = 'unknown'
                event.place.title_localized = {
                    'ru': 'без названия',
                    'en': 'unknown'
                }

            if event.place.slug:
                slug = event.place.slug.replace('&quot;', '')
                event.place.slug = slug
                if slug in ['0', 'none']:
                    event.place.slug = 'unknown'
            # empty place slug
            else:
                slug = default_slugify(event.place.title)
                event.place.slug = slug
            # event.save()
        # TODO instead of bulk
        Event.index.bulk_index(queryset=[e.to_dict() for e in chunk])
        logger.info('Reindex events success: %s \n' % len(chunk))
        # yield chunk
