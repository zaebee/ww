import logging
from split import chop
from app.models import EventPlace, default_slugify


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def run(*args):
    """
    Reindex actual places in Elasticsearch storage.
    """
    if len(args) == 1:
        slug = args[0]
    else:
        slug = None

    search = EventPlace.index.model.search()
    search = search.source(['provider', 'provider_id', 'slug', 'title'])
    if slug:
        # TODO reindex places cases:
        # search = search.query('term', slug=0)
        # search = search.query('term', slug='none')
        # search = search.query('match', slug='&quot;')
        search = search.query('term', slug=slug)
    else:
        # search = search.exclude('exists', field='place_id')
        search = search.exclude('exists', field='slug.raw')
        search = search.query('exists', field='title')

    chunk_size = 500
    ids = (e for e in search.scan())
    chunks = chop(chunk_size, ids)
    logger.info('Total places prepare: %s' % search.count())
    for chunk in chunks:
        logger.info('Reindex places prepare: %s' % len(chunk))
        for place in chunk:
            if not place.place_id:
                place.place_id = '%s_%s' % (
                    place.provider or 'ww', place.provider_id)
            if place.title:
                place.title = place.title.replace('&quot;', '')
                if place.title in ['0', 'none']:
                    place.title = 'unknown'
                    place.title_localized = {
                        'ru': 'без названия',
                        'en': 'unknown'
                    }
            if place.slug:
                place.slug = place.slug.replace('&quot;', '')
                if place.slug in ['0', 'none']:
                    place.slug = 'unknown'
            else:
                place.slug = default_slugify(place.title)
        # TODO make async bulk_index
        EventPlace.index.bulk_index(queryset=[e.to_dict() for e in chunk])

        logger.info('Reindex events success: %s \n' % len(chunk))
