import logging
from split import chop
from datetime import datetime
from app.models import Event
from services.api import api_providers


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def run(*args):
    """
    """
    if len(args) == 1:
        provider = args[0]
    else:
        provider = 'clt'

    api = api_providers[provider]
    search = Event.index.model.search()
    qs = search.query('term', provider=provider).source('provider_id')
    ids = [e.provider_id for e in qs.scan()]
    logger.info('Total events prepare: %s' % len(ids))
    chunk_size = 50
    unindexed = set()
    chunks = chop(chunk_size, ids)
    for chunk in chunks:
        logger.info('Reindex events prepare: %s' % len(chunk))
        # TODO implement get_events method for other providers
        # kdg/fb/ebr/tpad/vk
        request = api(page_size=chunk_size).get_events(ids=chunk)
        request.send()
        indexer = request.response.indexer(request.response)
        events = indexer('event')

        ids = set([event['id'] for event in events])
        not_indexed = set(chunk).difference(ids)
        unindexed.update(not_indexed)
        logger.info('Reindex events success: %s \n' % len(events))
    logger.info('Total unindexed events: %s' % len(unindexed))
    logger.info('Unindexed events ids: %s' % str(unindexed))
