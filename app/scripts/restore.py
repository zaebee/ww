import re
import requests

from app.models import EventCollection


def run(*args):
    """
    Restore collections from google/yandex cache.
    Example:
        `./manage.py runscript restore --script-args <collection_id> <url>
    """
    # google pattern
    # pattern = re.compile(r'data-event-id=&quot;([\d\w_]+)&quot;')
    # yandex pattern
    pattern = re.compile(r'data-event-id="([\d\w_]+)"')

    # filter by collection_id
    #
    if len(args) == 2:
        col_id = args[0]
        url = args[1]

    collection = EventCollection.index.get(id=col_id)
    response = requests.get(url)
    ids = pattern.findall(u'%s' % response.content)
    collection.events = ids
    collection.events_count = len(ids)
    collection.save()
