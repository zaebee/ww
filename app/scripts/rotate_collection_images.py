# import random

from app.models import Event, EventCollection


def run(*args):
    """
    Rotate collection image preview from actual events gallery.
    Example with `collection_id=ww_221`:
        `./manage.py runscript rotate_collection_images --script-args ww_221`
    """
    collection_id = None
    # filter by provider
    if len(args) == 1:
        collection_id = args[0]

    search = EventCollection.index.model.search()
    search = search.filter('term', deleted=False)
    search = search.query('range', **{'events_count': {'gt': 0}})
    if collection_id:
        search = search.query('term', id=collection_id)
    for collection in search.scan():
        if not collection.get_preview:
            pass
        events = collection.events
        events = Event.index.model.search().query(
            'terms', id=events
            # ).query(
            #     'range',
            #     **{'dates.start_date': {'gte': 'now'}}
        ).source(
            ['image', 'images', 'preview_images']
        ).sort(
            'dates.start_date'
        )[:3]

        previews = ({'image': event.image} for event in events.execute())
        collection.get_preview = list(previews)
        collection.save()
