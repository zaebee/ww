from services.tasks import update_places


def run(*args):
    """
    Send push messages to mobile devices.
    Only for collections that have `collection_id`.
    """
    # set `use_fcm_notifications` for andriod devices
    if len(args) == 1:
        notify = args[0]
    else:
        notify = False

    task = update_places.s(notify) if notify else update_places.s()
    return task.delay()
