import logging

from django.urls import resolve
from split import chop

from app.models import Event, EventCollection, EventCategory, CityEventCategory

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def run(*args):
    """
    Import events from collection to category.
    """
    if len(args) == 1:
        collection_id = args[0]
        category_id = False
    elif len(args) == 2:
        collection_id = args[0]
        category_slug = args[1]
        category_id = args[1]
    else:
        collection_id = False
        category_slug = False
        category_id = False

    search = Event.index.search(observable=True)
    search = search.source([
        'id', 'provider', 'provider_id', 'category',
    ])
    if collection_id:
        collection = EventCollection.index.get(id=collection_id)
        search = search.query('terms', id=collection.events or [])

    if category_slug:
        try:
            view = resolve(category_slug)
            kwargs = view.kwargs
            category__slug = kwargs.get('subcategory_slug', None)
            if not category__slug:
                category__slug = kwargs.get('category_slug', None)
            city_category = CityEventCategory.objects.get(
                city__slug__icontains=kwargs['city_name'],
                category__slug=category__slug
            )
            category = EventCategory.objects.get(id=city_category.category.id)
            parent = None
            if category and category.parent_id:
                parent = EventCategory.objects.get(id=category.parent_id)
        except:
            if category_id:
                parent = None
                category = EventCategory.objects.get(id=category_id)
                if category.parent_id:
                    parent = EventCategory.objects.get(id=category.parent_id)
            else:
                category = None
                parent = None

    queryset = search.scan()
    chunk_size = 500
    queryset = (e for e in search.scan())
    chunks = chop(chunk_size, queryset)

    logger.info('Total events prepare: %s' % search.count())

    for chunk in chunks:
        logger.info('Reindex places prepare: %s' % len(chunk))
        for event in chunk:
            if hasattr(event.category, 'to_dict'):
                _category = [event.category]
            else:
                _category = event.category or []
            if {'id': category.id, 'title': category.title, 'slug': category.slug} not in _category:
                _category.append({
                    'id': category.id,
                    'slug': category.slug,
                    'title': category.title,
                })
            if parent:
                if {'id': parent.id, 'title': parent.title, 'slug': parent.slug} not in _category:
                    _category.append({
                        'id': parent.id,
                        'slug': parent.slug,
                        'title': parent.title,
                    })
            event.category = _category

        queryset = (event.to_dict() for event in chunk)
        Event.index.bulk_index(queryset=queryset)
    return None
