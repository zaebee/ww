from hitcount.models import HitCount

from services.tasks import push_notification
from services.elastic.facets import CollectionSearch

from app.models import EventCollection


def run(*args):
    """
    Update collection counnters: events, places, views.
    Also generate mobile pushes if new events in collection.
    Example:
        `./manage.py runscript update_collection --script-args ww_21`
    """
    collection_id = None
    old_events = []
    actual_events = []

    # filter by collection_id
    if len(args) == 1:
        collection_id = args[0]
    else:
        return

    collection = EventCollection.index.get(id=collection_id)
    # TODO save hits event/place action
    # for every collection through application_id
    collection.application_id = collection.application_id or collection.meta.id
    search = EventCollection.index.model.search()
    hitcount = HitCount.get_for_object(collection)

    events = collection.events or []
    places = collection.places or []
    exclude_places = collection.exclude_places or []

    limit_date_from = None
    limit_date_to = None
    if collection.limit_date_from:
        limit_date_from = collection.limit_date_from.date()
    if collection.limit_date_to:
        limit_date_to = collection.limit_date_to.date()

    old_events = search.doc_type('events').exclude(
        'exists',
        field='dates',
    ).query('terms', id=events)
    old_events = (event.id for event in old_events.scan())

    if events:
        search = CollectionSearch(query={
            'events': events,
            'places': places,
            'exclude_places': exclude_places,
            'limit_date_from': limit_date_from,
            'limit_date_to': limit_date_to,

        })
        bucket = search[:search.count()].execute()
        actual_events = (event.id for event in bucket)

    events = set(events)
    old_events = set(old_events)
    actual_events = set(actual_events)

    # remove old events from collection
    events = events.difference(old_events)

    # new events from places
    new_events = actual_events.difference(events)

    stats = {
        # 'old_total': len(old_events),
        # 'old_events': list(old_events),

        'actual_total': len(events),
        'actual_events': list(events),

        'new_total': len(new_events),
        'new_events': list(new_events),
        'hits': hitcount.hits,

    }
    collection.stats = stats

    for event_pk in collection.stats.new_events:
        # collect hits for new events
        collection.save_push('event', event_pk)

    if collection.stats.new_events:

        # update event counters
        collection.stats.actual_events.extend(
            collection.stats.new_events
        )
        # get push data for new events
        push = collection.push_notification(
            'action',
            events=list(collection.stats.new_events)
        )
        # and send push to users run celery task
        if push['params']['count'] and collection.stats.new_total:
            push['params']['notify'] = True
            task = push_notification.s(
                collection.application_id or collection.meta.id,
                push['text'],
                **push['params']
            )
            task.delay()

    collection.events = list(set(collection.stats.actual_events))
    collection.events_count = len(collection.events)
    collection.save(update_date=False)

    return collection
