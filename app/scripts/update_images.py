from app.models import Event, EventCollection
from services.tasks import make_preview_events_ids


def run(*args):
    """
    Crop and upload event `images` to our amazon s3 bucket.
    Example:
        `./manage.py runscript update_images --script-args kdg`
        `./manage.py runscript update_images --script-args ebr music|art`
        `./manage.py runscript update_images --script-args 42.291 18.840 80000`
    """
    params = {}
    provider = None
    collection_id = None
    # filter by provider or collection_id
    if len(args) == 1:
        if str(args[0]).isnumeric():
            collection_id = args[0]
        else:
            provider = args[0]

    # filter by provider and query params
    # ./manage.py runscript update_images --script-args kdg выставки
    elif len(args) == 2:
        provider = args[0]
        params = {
            'q': args[1]
        }
    # filter by geolocation
    # ./manage.py runscript update_images --script-args 51.675 39.2088 80000
    elif len(args) == 3:
        params = {
            'lat': args[0],
            'lng': args[1],
            'radius': args[2],
        }
    # filter by provider and geolocation
    # ./manage.py runscript update_images --script-args рок-коцерты 51.6754 39.2089 80000
    elif len(args) == 4:
        params = {
            'q': args[0],
            'lat': args[1],
            'lng': args[2],
            'radius': args[3]
        }

    events = Event.index.search(
        observable=True,
        **params
    ).source(['id', 'preview_images'])
    # TODO does not work exclude by preview_images
    events = events.exclude('exists', field='preview_images')

    events = events.filter('range',
                           **{'schedules.start_date': {'gte': 'now/d'}})
    if provider:
        events = events.query('term', provider=provider)
    if collection_id:
        collection = EventCollection.index.get(id=collection_id)
        events = events.query('terms', id=collection.events)

    total = events.count()
    events = events.params(scroll='100m').scan()
    ids = [event.id for event in events]
    make_preview_events_ids.apply_async([ids], queue='images')
    return total
