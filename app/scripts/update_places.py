from services.observers import EventObserver
from services.api import ApiRequestFactory, api_providers

from app.models import EventCollection


def run(*args):
    """
    Download new events from places which collecton subscribed.
    Example:
        `./manage.py runscript update_places`
    """
    observer = EventObserver()
    api = ApiRequestFactory()
    for provider_prefix, provider_api in api_providers.items():
        # enable all available providers
        api.set(provider_api)

    search = EventCollection.index.model.search()
    if len(args):
        collection_id = args[0]
        search = search.filter('term', id=collection_id)
    collections = (collection for collection in search.scan())

    def on_completed():
        # update collection stats
        # and send pushes for collection with application_id
        from services.tasks import update_collection
        print('run on_completed')
        s = search.query('exists', field='places')
        for collection in s.scan():
            task = update_collection.s(collection.id)
            task.delay()

    observable = api.observable_collections(*collections)
    observable.buffer_with_count(
        # indexing events by 50
        50
    ).subscribe(
        observer.on_next,
        observer.on_error,
        on_completed  # update collecton after completed chain
    )
