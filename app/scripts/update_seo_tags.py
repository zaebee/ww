import logging

from random import shuffle
from split import chop
from pymorphy2 import MorphAnalyzer

from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from app.models import Event, EventCollection

seo_event_title = _('%(title)s in %(city)s, %(pattern)s')
seo_event_title_pattern = _('event_meta_title_tags')

seo_place_title = _('%(title)s: %(pattern)s')
seo_place_title_pattern = _('place_meta_title_tags')

morph = MorphAnalyzer()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def run(*args):
    """
    Send push messages to mobile devices.
    Only for collections that have `collection_id`.
    """
    locale = 'ru'
    if len(args) == 1:
        collection_id = args[0]
    elif len(args) == 2:
        collection_id = args[0]
        locale = args[1]
    else:
        collection_id = False

    translation.activate(locale)
    search = Event.index.search(observable=True)
    search = search.source([
        'id', 'provider', 'provider_id', 'place',
        'title', 'title_seo', 'title_localized'
    ])
    if collection_id:
        collection = EventCollection.index.get(id=collection_id)
        search = search.query('terms', id=collection.events or [])

    queryset = search.scan()
    chunk_size = 500
    queryset = (e for e in search.scan())
    chunks = chop(chunk_size, queryset)

    logger.info('Total events prepare: %s' % search.count())

    # random shuffle title meta suffix
    pattern = translation.ugettext(seo_event_title_pattern)
    pattern = pattern.split(', ')

    for chunk in chunks:
        logger.info('Reindex places prepare: %s' % len(chunk))
        for event in chunk:
            title = getattr(event.title_localized, locale, '') or event.title
            if event.place:
                city = morph.parse(event.place.city or '')[0]
                city = city.inflect(set(['loct']))
                city = city.word.capitalize() if city else event.place.city
            else:
                city = 'unknown'

            shuffle(pattern)
            suffix_pattern = ', '.join(pattern)

            title_seo = seo_event_title % {
                'title': title,
                'city': city,
                'pattern': suffix_pattern,
            }
            title_seo = translation.ugettext(title_seo)
            event.title_seo = {
                locale: title_seo
            }
        queryset = (event.to_dict() for event in chunk)
        Event.index.bulk_index(queryset=queryset)
    return None
