# -*- coding: utf-8 -*-

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from datetime import datetime
from django import template

register = template.Library()


@register.filter
def nbsp(title):
    title = title.split(' ')
    title = '&nbsp;'.join(title)
    return title


@register.filter
def get_account(social_accounts, provider_name):
    accounts = social_accounts.filter(provider=provider_name)
    return accounts.first()


@register.filter
def get_vote(votes, user):
    vote = [vote for vote in votes.voted_users if vote.author.id == user.id]
    if vote:
        vote = vote[0]
        return vote.rating


@register.filter
def str_to_date(date_string):
    try:
        result = datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S')
    except:
        try:
            result = datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S%z')
        except:
            result = date_string
    return result


@register.filter
def date_to_key(date):
    return date.replace(day=1)


@register.filter
def is_past_bucket(date_key):
    """
    Return True if date_key in past.
    """
    now = datetime.now()
    now = datetime(now.year, now.month, 1)
    return (date_key - now).days < 0


@register.filter
def get_schedule(schedules, bucket_date_key):
    """
    Get actual schedule from event schedules list.
    """
    # TODO get actual start_date if past
    schedules = schedules or []
    sorted_dates = sorted(schedules, key=lambda schedule: -schedule.age.days)
    try:
        result = next(date for date in sorted_dates if date.age.days <= 0)
    except StopIteration:
        result = None
    for schedule in sorted_dates:
        if schedule.start_date.date() == bucket_date_key.date():
            result = schedule
            break
        else:
            if (schedule.start_date.year == bucket_date_key.year
                and schedule.start_date.month == bucket_date_key.month
                and schedule.age.days <= 0):
                result = schedule
                break
    if not result and len(sorted_dates):
        result = sorted_dates[0]
    return result


@register.filter
def get_event(events, event_id):
    """
    """
    try:
        event = events[event_id]
    except KeyError:
        event = None
    return event


@register.filter
def fromtimestamp(timestamp):
    try:
        result = datetime.fromtimestamp(timestamp)
    except:
        result = timestamp
    return result


@register.filter
def duration(schedule):
    result = 0  # 0 days
    start = schedule.start_date
    end = schedule.end_date
    if start and end:
        result = (end - start).days
    return result


@register.filter
def empty(value, arg):
    """If value is unavailable, use given default."""
    try:
        result = value or arg
    except:
        result = value
    return result


@register.filter
def can_remove(event, user):
    return event.author.id == user.id


@register.filter
def locale(source, lang_code):
    result = getattr(source, 'raw', None)
    if source:
        return source.get_language(lang_code) or result


@register.filter
def splits(source):
    if source:
        return source.split()


@register.filter
def get_domain(url):
    if url:
        url = urlparse(url)
        return url.hostname or url.path


@register.filter
def get_og_image(instance):
    image = instance.image
    if instance and instance.preview_images:
        preview = instance.preview_images[0]
        image = preview.retina or image
    return image


class ActualScheduleNode(template.Node):
    def __init__(self, schedules, start_date, end_date):
        self.schedules = template.Variable(schedules)
        self.start_date = template.Variable(start_date)
        self.end_date = template.Variable(end_date)

    def render(self, context):
        schedules = self.schedules.resolve(context) or []
        start_date = self.start_date.resolve(context)
        end_date = self.end_date.resolve(context)
        sorted_dates = sorted(schedules, key=lambda d: -d.age.days)
        try:
            result = next(date for date in sorted_dates if date.age.days <= 0)
        except StopIteration:
            result = None
        for schedule in schedules:
            if (schedule.start_date.date() >= start_date.date() and
                schedule.start_date.date() <= end_date.date()):
                result = schedule
                break
        if not result and len(sorted_dates):
            result = sorted_dates[0]
        context['actual_schedule'] = result
        return ''


@register.tag(name='actual_schedule')
def do_actual_schedule(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, schedules, start_date, end_date = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires exactly three arguments" % token.contents.split()[0]
        )
    return ActualScheduleNode(schedules, start_date, end_date)


@register.assignment_tag
def define(val=None):
    """
    Allow set variable in templates
    {% define "true" as have_label %}
    """
    return val
