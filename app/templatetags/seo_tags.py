# -*- coding: utf-8 -*-

from django import template
from django.utils.translation import ugettext_lazy as _

register = template.Library()

seo_event_title_pattern = _('event_meta_title_tags')
seo_place_title_pattern = _('place_meta_title_tags')


@register.filter
def seo_title(instance, title):
    return


@register.filter
def get_city_name(city_slug):
    city_id, city_name = city_slug.split('-', 1)
    return city_name.lower()


class SeoTitleNode(template.Node):
    def __init__(self, title, meta_title_tags):
        self.title = template.Variable(title)
        self.meta_title_tags = template.Variable(meta_title_tags)

    def render(self, context):
        title = self.title.resolve(context) or []
        meta_title_tags = self.meta_title_tags.resolve(context)

        # TODO metatitle dynamic
        # add params to prefix `<title> <city> <category>`
        # random generate suffix pattern for event/place/collection
        seo_title = seo_event_title_pattern
        seo_title = seo_title.strip(', ').split(',')
        context['seo_title'] = seo_title
        context['meta_title'] = meta_title_tags or title
        return ''


@register.tag(name='get_seo_title')
def do_seo_title(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, title, meta_title_tags = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires exactly two arguments" % token.contents.split()[0]
        )
    return SeoTitleNode(title, meta_title_tags)
