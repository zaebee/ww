"""
URLconf for events app

"""

from django.conf.urls import url, include
from django.views.generic import TemplateView
from ajax_select import urls as ajax_select_urls

from . import views


urlpatterns = [
    url(r'^$', views.MainPageView.as_view(), name="mainpage"),

    url(r'^goto/$', views.redirect_view, name="goto-redirect"),
    url(r'^404/$', views.View404.as_view(), name="not-found"),
    url(r'^help/$', views.HelpPageView.as_view(), name="help-page"),
    url(r'^terms/$', views.TermsPageView.as_view(), name="terms-page"),
    url(r'^about/$', views.AboutPageView.as_view(), name="about-page"),
    url(r'^organizers/$',
        views.OrganizersPageView.as_view(), name="organizers-page"),

    url(r'^search/$', views.SearchView.as_view(), name="search-results"),

    url(r'^require_email/$', views.require_email, name='require_email'),

    # User profiles endpoints
    url(r'^profiles/$',
        views.UserProfileList.as_view(), name='profile-list'),
    url(r'^profiles/edit/$',
        views.profile_update_redirect, name='profile-update-redirect'),
    url(r'^profiles/(?P<pk>[\d]+)/edit/$',
        views.UserProfileUpdate.as_view(), name='profile-update'),
    url(r'^profiles/(?P<pk>[-_\d\w]+)/$',
        views.UserProfileDetail.as_view(), name='profile-detail'),

    url(r'^image/upload/$', views.ImageCreate.as_view(), name='image-create'),
    url(r'^image/delete/$', views.ImageDelete.as_view(), name='image-delete'),

    # User collections endpoints
    url(r'^collections/$',
        views.CollectionList.as_view(), name='collection-list'),
    url(r'^collections/add/$',
        views.CollectionCreate.as_view(), name='collection-add'),
    url(r'^collections/(?P<pk>[\d]+)/edit/$',
        views.CollectionUpdate.as_view(), name='collection-update'),
    url(r'^collections/(?P<pk>[\d]+)-(?P<slug>[-_\d\w]+)/$',
        views.CollectionDetail.as_view(), name='collection-detail'),
    url(r'^collections/(?P<pk>[\d]+)-(?P<slug>[-_\d\w]+)/rss/$',
        views.CollectionDetailFeed(), name='collection-detail-feed'),

    # Events endpoints
    url(r'^events/$', views.EventList.as_view(), name='event-list'),
    url(r'^events/add/$', views.EventCreate.as_view(), name='event-add'),
    url(r'^events/(?P<pk>[_\d\w]+)/edit/$',
        views.EventUpdate.as_view(), name='event-update'),
    url(r'^events/(?P<pk>[\d]+)-(?P<slug>[-_\d\w]+)/$',
        views.EventDetail.as_view(), name='event-detail'),

    # Places endpoints
    url(r'^places/$',
        views.PlaceList.as_view(), name='place-list'),
    url(r'^places/(?P<pk>[\d]+)-(?P<slug>[-_\d\w]+)/$',
        views.PlaceDetail.as_view(), name='place-detail'),
    url(r'^places/(?P<city_name>[-_\w]+)/$',
        views.PlaceCityList.as_view(), name='place-city-list'),

    url(r'^city/(?P<pk>[\d]+)-(?P<city_name>[-_\w]+)/$',
        views.CityDetail.as_view(), name='city-detail'),

    # Categories endpoints
    url(r'^categories/$', views.CategoryList.as_view(), name='category-list'),
    url(r'^categories/(?P<pk>[\d]+)-(?P<category_slug>[-_\d\w]+)/$',
        views.CategoryDetail.as_view(),
        name='category-detail'),
    url(r'^categories/(?P<category_slug>[-_\d\w]+)/(?P<subcategory_slug>[-_\d\w]+)/$',
        views.SubCategoryDetail.as_view(),
        name='subcategory-detail'),

    url(r'^cities/$', views.CityList.as_view(), name='city-list'),
    url(r'^(?P<city_name>[-_\w]+)/categories/(?P<category_slug>[-_\d\w]+)/$',
        views.CityCategoryDetail.as_view(),
        name='city-category-detail'),

    url(r'^(?P<city_name>[-_\w]+)/categories/(?P<category_slug>[-_\d\w]+)/(?P<subcategory_slug>[-_\d\w]+)/$',
        views.CitySubCategoryDetail.as_view(),
        name='city-subcategory-detail'),

]

urlpatterns += [
    url(r'google5e682b3d95e1b8ef.html$',
        TemplateView.as_view(template_name='google-auth.html')),
    url(r'yandex_5486c91ec180b084.html$',
        TemplateView.as_view(template_name='yandex-auth.html')),
]

urlpatterns += [
    url(r'^ajax_select/', include(ajax_select_urls)),
    url(r'^tinymce/', include('tinymce.urls')),
]
