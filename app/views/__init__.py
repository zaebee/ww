# -*- coding: utf-8 -*-

from .mainpage import (
    View404, MainPageView, redirect_view,
    TermsPageView, HelpPageView, AboutPageView,
    OrganizersPageView
)

from .search import SearchView

from .images import ImageCreate, ImageDelete

from .events import (
    EventCreate, EventUpdate, EventDetail, EventList, OracleView
)

from .places import PlaceDetail, PlaceList, PlaceCityList

from .collections import (
    CollectionCreate, CollectionUpdate,
    CollectionDetail, CollectionList
)

from .profiles import (
    profile_update_redirect, require_email,
    UserProfileUpdate, UserProfileDetail, UserProfileList
)

from .cities import CityDetail, CityList

from .categories import (
    CategoryList, CategoryDetail,
    SubCategoryDetail,
    CityCategoryDetail, CitySubCategoryDetail
)

from .feeds import CollectionDetailFeed
