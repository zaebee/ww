# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from hitcount.views import HitCountDetailView
from annoying.functions import get_object_or_None

from services.elastic.facets import CategorySearch
from app.models import EventCategory, EventPlace, City, CityEventCategory


class CategoryList(ListView):
    context_object_name = 'category_list'
    queryset = EventCategory.objects.filter(parent__isnull=True)
    template_name = 'categories/category_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryList, self).get_context_data(**kwargs)
        city_categories = CityEventCategory.objects.all()
        context['city_categories'] = city_categories
        return context


class CategoryDetail(DetailView):
    city = None
    count_hit = True
    context_object_name = 'category'
    template_name = 'categories/category_detail.html'
    event_card_template = 'events/card_event_small.html'
    collection_card_template = 'collections/collection_card.html'

    def get_bucket(self, instance):
        if instance.parent:
            categories = [instance.slug, instance.parent.slug]
        else:
            categories = [instance.slug]
        search = CategorySearch(query={
            'categories': categories,
            'lng': self.city.location.x if self.city else 0.0,
            'lat': self.city.location.y if self.city else 0.0,
            'radius': 80000,

        })
        size = 100
        bucket = search[:size].execute()
        instance.facets = bucket.facets.to_dict()
        instance.events = bucket.hits

        data = []
        events = []
        result = set()
        day_buckets = bucket.facets.day_events

        # TODO common part for event doubles app/api
        # separate method `get_day_buckets(from, to)`
        if day_buckets:
            first = day_buckets[0]
            key = first.key.year, first.key.month
            for day_bucket in day_buckets:

                if key == (day_bucket.key.year, day_bucket.key.month):
                    for event in day_bucket.events:
                        if event.meta.id not in result:
                            result.add(event.meta.id)
                            events.append((event.meta.id, day_bucket.key))
                else:
                    data.append({
                        'key': first.key,
                        'events': events
                    })
                    key = day_bucket.key.year, day_bucket.key.month
                    first = day_bucket
                    events = [(e.meta.id, day_bucket.key)
                              for e in day_bucket.events]
                    result = set(e.meta.id for e in day_bucket.events)

            data.append({
                'key': day_bucket.key,
                'events': events
            })

        place_ids = [place[0] for place in bucket.facets.places]
        if place_ids:
            top_places = EventPlace.index.model.mget(place_ids)
        else:
            top_places = []
        instance.top_places = top_places

        return {
            'month_events': data,
            'events_count': bucket.hits.total,
            'events': {event.id: event for event in bucket}
        }

    def get_object(self):
        slug = self.kwargs.get('category_slug')
        pk = self.kwargs.get('pk')
        obj = get_object_or_404(
            EventCategory,
            parent__isnull=True,
            slug=slug,
            id=pk
        )
        return obj

    def get_context_data(self, **kwargs):
        context = super(CategoryDetail, self).get_context_data(**kwargs)
        instance = context.get('category', None)
        bucket = self.get_bucket(instance)
        context['event_card_template'] = self.event_card_template
        context['collection_card_template'] = self.collection_card_template

        if instance:
            children = instance.subcategories.all()
        if children.count():
            context['children'] = children
        context.update(**bucket)
        return context


class SubCategoryDetail(CategoryDetail):
    count_hit = True
    context_object_name = 'category'
    template_name = 'categories/category_detail.html'
    event_card_template = 'events/card_event_small.html'
    collection_card_template = 'collections/collection_card.html'

    def get_object(self):
        parent_slug = self.kwargs.get('category_slug')
        child_slug = self.kwargs.get('subcategory_slug')
        obj = get_object_or_404(
            EventCategory,
            parent__slug=parent_slug,
            slug=child_slug,
        )
        return obj

    def get_context_data(self, **kwargs):
        context = super(SubCategoryDetail, self).get_context_data(**kwargs)
        context['event_card_template'] = self.event_card_template
        context['collection_card_template'] = self.collection_card_template
        return context


class CityCategoryDetail(CategoryDetail):

    def get_city(self, instance):
        obj = None
        if 'city_name' in self.kwargs:
            obj = get_object_or_404(
                instance.cities.all(),
                slug__icontains=self.kwargs['city_name']
            )
        return obj

    def get_object(self):
        slug = self.kwargs.get('category_slug')
        obj = get_object_or_404(
            EventCategory,
            parent__isnull=True,
            slug=slug,
        )
        return obj

    def get_context_data(self, **kwargs):
        instance = self.get_object()
        self.city = self.get_city(instance)
        city_category = get_object_or_404(
            CityEventCategory,
            city=self.city,
            category=instance
        )
        context = super(CityCategoryDetail, self).get_context_data(**kwargs)

        context['city'] = self.city
        context['city_category'] = city_category
        if city_category:
            children = city_category.category.subcategories.filter(
                cities=city_category.city
            )
            if children.count():
                context['children'] = children
            else:
                context['children'] = []

        # TODO choose lang code using request locale
        alt_names = self.city.alt_names.filter(
            language_code='ru'
        )
        context['alt_name'] = alt_names.first()
        return context


class CitySubCategoryDetail(CityCategoryDetail):

    def get_object(self):
        parent_slug = self.kwargs.get('category_slug')
        child_slug = self.kwargs.get('subcategory_slug')
        obj = get_object_or_404(
            EventCategory,
            parent__slug=parent_slug,
            slug=child_slug,
        )
        return obj

    def get_context_data(self, **kwargs):
        context = super(CitySubCategoryDetail, self).get_context_data(**kwargs)
        return context
