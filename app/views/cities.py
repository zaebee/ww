# -*- coding: utf-8 -*-

from django.http import Http404
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic import View, DetailView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from django.contrib import messages

from annoying.functions import get_object_or_None
from hitcount.views import HitCountDetailView

from cities.models import AlternativeName

from app.models import Event, EventCollection, City


class CityDetail(DetailView):
    count_hit = True
    context_object_name = 'city'
    queryset = City.objects.all()

    template_name = 'cities/city_detail.html'
    event_card_template = 'events/event_card.html'
    place_card_template = 'places/place_card.html'
    collection_card_template = 'collections/collection_card.html'

    def get_city(self):
        obj = None
        if 'pk' in self.kwargs and 'city_name' in self.kwargs:
            slug = '%s-%s' % (self.kwargs['pk'], self.kwargs['city_name'])
            obj = get_object_or_None(City, slug__iexact=slug)
        return obj

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CityDetail, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        selected_city = self.get_city()
        context = super(CityDetail, self).get_context_data(**kwargs)
        event_buckets = Event.index.aggregate_in_city(
            city_names=[selected_city.name_std]
        )
        context['event_buckets'] = event_buckets.aggregations.list.buckets

        event_ids = []
        for bucket in event_buckets.aggregations.list.buckets:
            for resp in bucket:
                for e in resp:
                    event_ids.append(e.id)

        collections = EventCollection.index.get_with_specified_events(
            event_ids=event_ids
        )
        context['collections'] = collections

        context['event_card_template'] = self.event_card_template
        context['place_card_template'] = self.place_card_template
        context['collection_card_template'] = self.collection_card_template
        return context


class CityList(ListView):
    context_object_name = 'cities'
    template_name = 'cities/city_list.html'
    place_card_template = 'places/place_card.html'

    def get_city(self, city_name):
        alt_name = AlternativeName.objects.prefetch_related(
            'city_set').filter(name__iexact=city_name).first()
        city = alt_name.city_set.first() if alt_name else None
        return city

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CityList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CityList, self).get_context_data(**kwargs)
        context['location'] = self.request.location
        # context['city'] = self.get_city(**self.kwargs)
        # context['place_card_template'] = self.place_card_template
        # TODO add extra data
        return context

    def get_queryset(self):
        return City.objects.none()
