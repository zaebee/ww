# -*- coding: utf-8 -*-

import logging

from django.http import Http404
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from django.contrib import messages

from el_pagination.views import AjaxListView

from annoying.functions import get_object_or_None
from hitcount.models import HitCount
from hitcount.views import HitCountDetailView

from services.elastic.facets import CollectionSearch

from app.models import EventCollection, EventPlace, User
from app.forms import EventCollectionForm

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CollectionCreate(CreateView):
    model = EventCollection
    form_class = EventCollectionForm
    template_name = 'collections/collection_create.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return super(CollectionCreate, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        """ Save Collection instance if valid.
        """
        collection = form.save(commit=False)
        collection.author = self.request.user
        collection.save()

        messages.success(self.request, u'Event was created.')
        return super(CollectionCreate, self).form_valid(form)


class CollectionUpdate(UpdateView):
    model = EventCollection
    form_class = EventCollectionForm
    template_name = 'collections/collection_update.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        if self.get_object().author != request.user:
            raise Http404
        return super(CollectionUpdate, self).get(request, *args, **kwargs)


class CollectionDetail(HitCountDetailView):
    count_hit = True
    queryset = EventCollection.objects.all()
    context_object_name = 'collection'
    template_name = 'collections/collection_detail.html'
    event_card_template = 'events/event_card.html'

    def get_object(self):
        provider_id = self.kwargs.get('pk')
        slug = self.kwargs.get('slug')
        obj = EventCollection.index.get(provider_id=provider_id)
        # set _meta for hitcount app calculate views
        if slug != obj.slug:
            raise Http404
        if obj.private and obj.author.id != self.request.user.id:
            raise Http404
        obj._meta = EventCollection._meta
        return obj

    def get_context_data(self, **kwargs):
        context = super(CollectionDetail, self).get_context_data(**kwargs)
        instance = context.get('collection', None)
        user = self.request.user
        is_premium = (
            user.is_authenticated() and
            user.business_account and
            user.id == instance.author.id
        )
        can_translate = user.has_perm('app.translate_collection')
        can_translate = can_translate or is_premium
        show_remove_button = user.is_staff
        if instance:
            context['instance'] = instance
            context['author'] = get_object_or_None(User, id=instance.author.id)
            events = instance.events or []
            places = instance.places or []
            exclude_places = instance.exclude_places or []
            limit_date_from = None
            limit_date_to = None
            if instance.limit_date_from:
                limit_date_from = instance.limit_date_from.date()
            if instance.limit_date_to:
                limit_date_to = instance.limit_date_to.date()
            if events:
                # TODO get month buckets sorted by day, -rank
                # paginate by size/offset
                search = CollectionSearch(query={
                    'events': events,
                    'places': places,
                    'exclude_places': exclude_places,
                    'limit_date_from': limit_date_from,
                    'limit_date_to': limit_date_to,

                })
                bucket = search[:len(events)].execute()
                instance.facets = bucket.facets.to_dict()
                instance.events = bucket.hits

                data = []
                events = []
                result = set()
                day_buckets = bucket.facets.day_events
                # TODO common part for event doubles app/api
                # separate method `get_day_buckets(from, to)`
                if instance.limit_date_from or instance.limit_date_to:
                    day_buckets = []
                    for day_bucket in bucket.facets.day_events:
                        if (instance.limit_date_from
                                and day_bucket.key >= instance.limit_date_from):
                            if (instance.limit_date_to
                                    and day_bucket.key <= instance.limit_date_to):
                                day_buckets.append(day_bucket)

                if day_buckets:
                    first = day_buckets[0]
                    key = first.key.year, first.key.month
                    for day_bucket in day_buckets:

                        if key == (day_bucket.key.year, day_bucket.key.month):
                            for event in day_bucket.events:
                                if event.meta.id not in result:
                                    result.add(event.meta.id)
                                    events.append((event.meta.id, day_bucket.key))
                        else:
                            data.append({
                                'key': first.key,
                                'events': events
                            })
                            key = day_bucket.key.year, day_bucket.key.month
                            first = day_bucket
                            events = [(e.meta.id, day_bucket.key) for e in day_bucket.events]
                            result = set(e.meta.id for e in day_bucket.events)

                    data.append({
                        'key': day_bucket.key,
                        'events': events
                    })

                place_ids = [place[0] for place in instance.facets.places]
                if place_ids:
                    top_places = EventPlace.index.model.mget(place_ids)
                else:
                    top_places = []
                instance.top_places = top_places

                context['month_events'] = data
                context['events_count'] = bucket.hits.total
                context['event'] = bucket.hits[0] if bucket.hits.total else None
                context['events'] = {event.id: event for event in bucket}
                context['coming_event'] = True
                context['start'] = instance.limit_date_from
                context['end'] = instance.limit_date_to
        context['event_card_template'] = self.event_card_template
        context['can_translate'] = can_translate
        context['show_remove_button'] = show_remove_button
        return context


class CollectionList(AjaxListView):
    context_object_name = 'collections'
    template_name = 'collections/collection_list.html'
    collection_card_template = 'collections/collection_card.html'
    page_template = 'collections/collection_card.html'

    @property
    def top_collections(self):
        # TODO private/public or empty collection filter
        size = 6
        hits = HitCount.search()
        hits = hits.query(
            'term', **{'content_object.raw': 'collections'}
        )
        hits = hits.sort('-hits').execute()
        collections = [hit.object_id for hit in hits if hit.hits > 0]
        # not empty documents
        if collections:
            collections = EventCollection.index.model.mget(collections)
            collections = [item for item in collections[:size] if item]
        return collections

    @property
    def public_collections(self):
        # TODO limit:offset pagination or scan cursor
        size = 50
        queryset = EventCollection.index.public()
        queryset = queryset.sort('-date_updated', '-date_added')
        queryset = queryset.extra(size=size)
        # queryset = queryset.query('terms', id=self.top_collections)
        return queryset.execute()

    def get_context_data(self, **kwargs):
        context = super(CollectionList, self).get_context_data(**kwargs)
        context['collection_card_template'] = self.collection_card_template
        context['show_collection_author'] = True

        context['top_collections'] = self.top_collections
        context['public_collections'] = self.public_collections
        context['collections'] = self.public_collections
        context['paginated'] = True

        return context

    def get_queryset(self):
        queryset = EventCollection.index.public()
        queryset = queryset.sort('-date_updated', '-date_added')
        # TODO limit:offset pagination or scan cursor
        return queryset.extra(size=100).execute()
