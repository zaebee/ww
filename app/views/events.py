# -*- coding: utf-8 -*-
import os
import pickle
from datetime import datetime
from django.http import Http404
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from hitcount.views import HitCountDetailView

from app.models import (
    Event, EventPlace, EventImage,
    EventVote, EventCollection
)
from app.forms import EventForm, EventPlaceForm


class EventCreate(CreateView):
    model = Event
    form_class = EventForm
    place_form_class = EventPlaceForm
    template_name = 'events/event_create.html'

    def get_context_data(self, **kwargs):
        place = None
        context = super(EventCreate, self).get_context_data(**kwargs)
        object = context.get('object')
        if object:
            images = object.images.all()
            place = object.place
        else:
            images = EventImage.objects.filter(
                author=self.request.user,
                event__isnull=True
            )
        context['images'] = images

        if 'form' not in context:
            context['form'] = self.form_class(initial={})
        if 'placeform' not in context:
            context['placeform'] = self.place_form_class(instance=place, prefix='place')
        return context

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return super(EventCreate, self).get(request, *args, **kwargs)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """ POST request for Event instance and Place instance.
        """
        self.object = None
        form = self.form_class(request.POST)
        placeform = self.place_form_class(request.POST, prefix='place')
        kwargs = {
            'form': form,
            'placeform': placeform,
        }
        if form.is_valid() and placeform.is_valid():
            return self.form_valid(**kwargs)
        else:
            return self.form_invalid(**kwargs)

    def form_valid(self, **kwargs):
        """ Save Event instance and Place instance if valid.
        """
        form = kwargs.get('form')
        placeform = kwargs.get('placeform')

        place = placeform.save()
        event = form.save(commit=False)
        event.author = self.request.user
        event.place = place
        event.save()

        images = EventImage.objects.filter(
            author=self.request.user,
            event__isnull=True
        )
        event.images.add(*images)

        messages.success(self.request, u'Event was created.')
        return super(EventCreate, self).form_valid(form)

    def form_invalid(self, **kwargs):
        return self.render_to_response(self.get_context_data(**kwargs))


class EventUpdate(EventCreate, UpdateView):
    model = Event
    form_class = EventForm
    template_name = 'events/event_update.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        event = self.get_object()
        if event.deleted or event.author != request.user:
            raise Http404
        return super(EventUpdate, self).get(request, *args, **kwargs)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """ POST request for Event instance and Place instance.
        """
        self.object = self.get_object()
        form = self.form_class(request.POST, instance=self.object)
        placeform = self.place_form_class(request.POST,
                                          instance=self.object.place,
                                          prefix='place')
        kwargs = {
            'form': form,
            'placeform': placeform,
        }
        if form.is_valid() and placeform.is_valid():
            return self.form_valid(**kwargs)
        else:
            return self.form_invalid(**kwargs)


class EventDetail(HitCountDetailView):
    count_hit = True
    context_object_name = 'event'
    template_name = 'events/event_detail.html'
    event_card_template = 'events/event_card.html'
    collection_card_template = 'collections/collection_card.html'

    def get_object(self):
        provider_id = self.kwargs.get('pk')
        slug = self.kwargs.get('slug')
        obj = Event.index.get(provider_id=provider_id, slug=slug)
        # set _meta for hitcount app calculate views
        obj._meta = Event._meta
        return obj

    def get_context_data(self, **kwargs):
        context = super(EventDetail, self).get_context_data(**kwargs)
        instance = context.get('event', None)
        if instance:
            context['instance'] = instance
            # sort by start_date
            if instance.schedules:
                instance.schedules = sorted(instance.schedules, key=lambda x: x['start_date'])
            context['rating'] = EventVote.index.aggregate(instance.id)

            # similar events
            if instance.place and instance.place.place_id:
                similar_place = Event.index.similar(
                    size=3,
                    exclude_id=instance.id,
                    place__place_id=[instance.place.place_id]
                )
                context['similar_place'] = similar_place
            similar = Event.index.similar(id=instance.similar or [])
            context['events'] = similar
            context['collections'] = EventCollection.index.filter(
                events=[instance.meta.id]
            )
            try:
                context['place'] = EventPlace.index.get(
                    slug=instance.place.slug,
                    provider_id=instance.place.provider_id
                )
            except:
                context['place'] = None
        context['collection_card_template'] = self.collection_card_template
        context['event_card_template'] = self.event_card_template
        context['show_collection_author'] = True
        context['bucket_date_key'] = datetime.now()
        return context


class EventList(ListView):
    queryset = Event.objects.active()
    context_object_name = 'event_list'
    template_name = 'events/event_list.html'


def load_oracle():
    file_path = os.path.join(os.path.abspath(os.path.dirname(__name__)), 'dtc.pickle')
    if not os.path.exists(file_path):
        return None

    with open(file_path, 'rb') as fh:
        return pickle.load(fh)


ORACLE = load_oracle()


class OracleView(TemplateView):

    template_name = 'events/oracle.html'

    def get_context_data(self, **kwargs):
        import json
        from sklearn.feature_extraction.text import TfidfTransformer

        def load_obj(filename):
            with open(filename + '.pickle', 'rb') as fh:
                return pickle.load(fh)

        def vectorize_sample(sample):
            vectorized = load_obj('vectorized')
            vect_matrix = vectorized.transform(sample)
            vect_matrix_termfreq = TfidfTransformer().fit_transform(vect_matrix)
            return vect_matrix_termfreq

        def load_genres():
            genres = {}
            filename = os.path.join(os.path.abspath(os.path.dirname(__name__)), 'genres.json')
            with open(filename, 'r') as fh:
                genres_raw = json.load(fh)

            for g in genres_raw['data']:
                genres[g['id']] = g['name']
            return genres

        context = super(OracleView, self).get_context_data(**kwargs)
        event_text = self.request.GET.get('text', None)
        if event_text:
            context['event_text'] = event_text
            if not ORACLE:
                context['genre'] = 'module not installed'
            else:
                test_text = [event_text, ]
                test_sample = vectorize_sample(test_text)

                clf = load_obj('dtc')
                predict_val = clf.predict(test_sample)[0]

                genres = load_genres()
                context['genre'] = genres[predict_val]
        return context
