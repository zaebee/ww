from django.shortcuts import reverse
from django.contrib.syndication.views import Feed


from app.models import EventCollection
from services.elastic.facets import CollectionSearch


class CollectionDetailFeed(Feed):

    def get_object(self, request, pk, slug):
        obj = EventCollection.index.get(provider_id=pk)
        return obj

    def title(self, obj):
        return obj.title

    def link(self, obj):
        link = reverse('collection-detail', kwargs={
            'pk': obj.id,
            'slug': obj.slug
        })
        return link

    def description(self, obj):
        return obj.description

    def items(self, obj):
        search = CollectionSearch(query={
            'events': obj.events or [],
            'places': obj.places or [],
            'exclude_places': obj.exclude_places or [],
            'limit_date_from': obj.limit_date_from,
            'limit_date_to': obj.limit_date_to

        })
        bucket = search[:len(obj.events)].execute()
        return bucket

    def item_link(self, item):
        link = reverse('event-detail', kwargs={
            'pk': item.provider_id,
            'slug': item.slug
        })
        return link

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_pubdate(self, item):
        return item.date_added
