# -*- coding: utf-8 -*-

from django.http import JsonResponse
from django.views.generic.edit import CreateView, DeleteView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

#from annoying.decorators import ajax_request
from annoying.functions import get_object_or_None

from app.models import EventImage
from app.forms import EventImageForm


class ImageCreate(CreateView):
    model = EventImage
    form_class = EventImageForm

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """ POST request for creating Image instance.
        """
        self.object = None
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        """ Save Event instance and Place instance if valid.
        """
        eventimage = form.save(commit=False)
        eventimage.author = self.request.user
        eventimage.save()
        return JsonResponse({
            'success': True,
            'id': eventimage.id,
            'name': eventimage.image.url,
            'size': eventimage.image.size,
        })
        return super(ImageCreate, self).form_valid(form)

    def form_invalid(self, form):
        response = JsonResponse(form.errors)
        response.status_code = 400
        return response


class ImageDelete(DeleteView):
    model = EventImage

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        """ POST request for deleting Image instance.
        """
        pk = request.POST.get('id')
        # TODO fix delete image if no `id` - only filename hash
        if pk.isdigit():
            image = get_object_or_None(self.model, author=request.user, pk=pk)
        else:
            return JsonResponse({'success': False, 'id': pk})
        if image:
            image.delete()
            return JsonResponse({
                'success': True,
                'id': pk,
            })
        else:
            return JsonResponse({
                'success': False,
                'id': pk,
            })
