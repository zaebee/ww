# -*- coding: utf-8 -*-

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from datetime import datetime, timedelta
from django.db.models import Max, Count

from django.shortcuts import redirect
from django.views.generic.base import TemplateView

from hitcount.models import HitCount

from app.models import User, Event, EventSchedule, EventCollection
from app.views.search import SearchView


class View404(TemplateView):
    template_name = '404.html'


class MainPageView(SearchView):

    template_name = 'mainpage.html'
    paginated_template_name = 'events/events_paginated.html'

    def top_collections(self, **kwargs):
        # TODO private/public or empty collection filter
        size = 6
        hits = HitCount.search()
        hits = hits.query(
            'term', **{'content_object.raw': 'collections'}
        )
        hits = hits.sort('-hits').execute()
        collections = [hit.object_id for hit in hits if hit.hits > 0]
        # not empty documents
        if collections:
            collections = EventCollection.index.model.mget(collections)
            collections = [item for item in collections[:size] if item]
        return collections

    def get_template_names(self):
        page = self.request.GET.get('page', None)
        if page and page != '1':
            return self.paginated_template_name
        else:
            return self.template_name

    def get_context_data(self, **kwargs):
        context = super(MainPageView, self).get_context_data(**kwargs)
        return context


class TermsPageView(TemplateView):
    template_name = 'terms.html'


class HelpPageView(TemplateView):
    template_name = 'help.html'


class OrganizersPageView(TemplateView):
    template_name = 'organizers.html'
    authors_limit = 3

    def get_context_data(self, **kwargs):
        params = {}
        context = super(OrganizersPageView, self).get_context_data(**kwargs)
        location = self.request.location
        if location and 'latitude' in location and 'longitude' in location:
            params['lat'] = location['latitude']
            params['lng'] = location['longitude']
            params['radius'] = 20000

        upcoming_events = Event.index.search(actual=True, **params)
        authors = User.objects.filter(collections__private=False).distinct()[:self.authors_limit]
        context['upcoming_events'] = upcoming_events
        context['authors'] = authors
        return context


class AboutPageView(OrganizersPageView):
    template_name = 'about.html'
    authors_limit = 10

    def get_context_data(self, **kwargs):
        context = super(AboutPageView, self).get_context_data(**kwargs)
        return context


def redirect_view(request):
    url = request.GET.get('url', None)
    if url:
        url = urlparse(url)
        url = url._replace(scheme='http') if not url.scheme else url
        url = url.geturl()
    else:
        url = '/'
    return redirect(url)
