# -*- coding: utf-8 -*-

from django.http import Http404
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from django.contrib import messages

from annoying.functions import get_object_or_None
from hitcount.views import HitCountDetailView

from cities.models import AlternativeName

from app.models import Event, EventCollection, EventPlace, User


class PlaceDetail(HitCountDetailView):
    count_hit = True
    context_object_name = 'place'
    template_name = 'places/place_detail.html'
    event_card_template = 'events/event_card.html'
    collection_card_template = 'collections/collection_card.html'

    def get_object(self):
        slug = self.kwargs.get('slug')
        provider_id = self.kwargs.get('pk')
        obj = EventPlace.index.get(
            slug=slug,
            provider_id=provider_id
        )
        # set _meta for hitcount app calculate views
        obj._meta = EventPlace._meta
        return obj

    def get_context_data(self, **kwargs):
        context = super(PlaceDetail, self).get_context_data(**kwargs)
        instance = context.get('place', None)
        if instance:
            # context['author'] = get_object_or_None(User, id=instance.author.id)
            event_buckets = Event.index.aggregate_places(
                geometry=instance.geometry or None,
                place_id=instance.place_id)
            context['event_buckets'] = event_buckets.aggregations.list.buckets
            if 'past' in event_buckets.aggregations:
                context['past_buckets'] = event_buckets.aggregations.past.buckets

            event_ids = []
            for bucket in event_buckets.aggregations.list.buckets:
                for resp in bucket:
                    for e in resp:
                        event_ids.append(e.id)

            if event_ids:
                collections = EventCollection.index.get_with_specified_events(
                    event_ids=event_ids
                )
                context['events_count'] = len(event_ids)
                context['collections'] = collections

        context['event_card_template'] = self.event_card_template
        context['collection_card_template'] = self.collection_card_template
        return context


class PlaceList(ListView):
    context_object_name = 'places'
    template_name = 'places/place_list.html'
    place_card_template = 'places/place_card.html'

    def get_context_data(self, **kwargs):
        context = super(PlaceList, self).get_context_data(**kwargs)
        context['show_place_author'] = True
        context['place_card_template'] = self.place_card_template
        return context

    def get_queryset(self):
        return EventPlace.index.all()


class PlaceCityList(ListView):
    context_object_name = 'places'
    template_name = 'places/place_list.html'
    place_card_template = 'places/place_card.html'

    def get_city(self, city_name):
        alt_name = AlternativeName.objects.prefetch_related(
            'city_set').filter(name__iexact=city_name).first()
        city = alt_name.city_set.first() if alt_name else None
        return city

    def get_context_data(self, **kwargs):
        context = super(PlaceCityList, self).get_context_data(**kwargs)
        context['show_place_author'] = True
        context['city'] = self.get_city(**self.kwargs)
        context['place_card_template'] = self.place_card_template
        return context

    def get_queryset(self):
        city = self.get_city(**self.kwargs)
        if city:
            geometry = city.location.coords
            return Event.index.aggregate_cities(geometry=geometry, size=50)
        else:
            return None
