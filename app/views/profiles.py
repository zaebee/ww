# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render, resolve_url
from django.http import Http404
from django.utils.http import is_safe_url

from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from annoying.functions import get_object_or_None

from app.models import User, Event, EventCollection
from app.forms import UserProfileForm, UserRegisterForm


def require_email(request):
    ## TODO get correct email/password for social register
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        if request.method == 'POST':
            request.session['saved_email'] = request.POST.get('email')
            request.session['password'] = request.POST.get('password')
            backend = request.session.get('social_auth_last_login_backend', 'email')
            return redirect('social:complete', backend=backend)
    else:
        pipeline = request.session.get('partial_pipeline')
        next = request.session.get('next')
        context = {
            'pipeline': pipeline,
            'next': next,
        }
        return render(request, 'registration/confirm.html', context)


def profile_update_redirect(request):
    """
    Simple view for redirecting to user profile edit url.
    """
    next = request.GET.get('next', None)
    if request.user.is_authenticated():
        url = resolve_url('profile-update', request.user.id)
    else:
        url = '/'
    if next and is_safe_url(next):
        url = '%s?next=%s' % (url, next)
    return redirect(url)


@method_decorator(login_required, name='dispatch')
class UserProfileUpdate(UpdateView):
    model = User
    form_class = UserProfileForm
    template_name = 'profiles/profile_update.html'

    def get(self, request, *args, **kwargs):
        if self.get_object() != request.user:
            raise Http404
        get_object_or_None(User, id=request.user.id)
        return super(UserProfileUpdate, self).get(request, *args, **kwargs)

    def get_success_url(self):
        """
        Returns the supplied URL.
        """
        next = self.request.GET.get('next', None)
        if next and is_safe_url(next):
            url = next
        else:
            url = super(UserProfileUpdate, self).get_success_url()
        return url


class UserProfileDetail(DetailView):
    queryset = User.objects.all()
    context_object_name = 'profile_detail'

    template_name = 'profiles/profile_detail.html'
    event_card_template = 'events/event_card.html'
    collection_card_template = 'collections/collection_card.html'

    def get_template_names(self):
        # TODO set different templates for event/collection pagination
        # usign querystringkey = event/collection
        if self.request.is_ajax():
            return self.event_card_template
        else:
            return self.template_name

    def get_context_data(self, **kwargs):
        context = super(UserProfileDetail, self).get_context_data(**kwargs)
        user = context['profile_detail']
        context['event_card_template'] = self.event_card_template
        context['collection_card_template'] = self.collection_card_template
        context['events_vote'] = Event.index.get_by_author_votes(user.id)
        # TODO Show only `not archived` events and `public` collections
        context['events'] = Event.index.filter(**{'author.id': user.id})
        if self.request.user == user:
            context['user_collections'] = EventCollection.index.filter(
                only_public=False,
                **{'author.id': [user.id]}
            )
        else:
            context['user_collections'] = EventCollection.index.filter(
                **{'author.id': [user.id]}
            )
        context['subscribed_collections'] = EventCollection.index.filter(
            subscribers=[user.id]
        )
        return context


class UserProfileList(ListView):
    queryset = User.objects.all()
    context_object_name = 'profile_list'
    template_name = 'profiles/profile_list.html'
