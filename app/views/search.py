# -*- coding: utf-8 -*-

import time
import logging
import dateparser

from django.core.cache import cache
from django.http.response import JsonResponse

from django.core.paginator import EmptyPage, PageNotAnInteger

from django.views.generic.base import TemplateView

from elasticsearch_dsl.response import Response

from cities.models import AlternativeName

from services.api import ApiRequestFactory
from services.tasks import observe_events

from app.models import Event, EventCollection
from app.paginator import ESPaginator


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class SearchView(TemplateView):

    template_name = 'search/search.html'
    paginated_template_name = 'events/events_paginated.html'
    event_card_template = 'events/event_card.html'
    collection_card_template = 'collections/collection_card.html'
    page_size = 18

    def render_to_response(self, context):
        if self.request.is_ajax():
            events = context.get('events', [])
            events = (event.to_dict() for event in events)
            return JsonResponse({'events': list(events)[:10]})
        else:
            return super(SearchView, self).render_to_response(context)

    def get_template_names(self):
        page = self.request.GET.get('page', None)
        if page and page != '1':
            return self.paginated_template_name
        else:
            return self.template_name

    def get_city(self, city_name):
        alt_name = AlternativeName.objects.prefetch_related(
            'city_set').filter(name__iexact=city_name).first()
        city = alt_name.city_set.first() if alt_name else None
        return city

    def get_next_page(self, **kwargs):
        api = ApiRequestFactory(**kwargs)
        next_data = cache.get_or_set(
            api.cache_key,
            api.get_next,
            timeout=60 * 60 * 24
        )
        _next = next_data.get('next', {}) if next_data else {}
        if _next:
            _next.pop('provider', None)
            # cached fb/ebr/kdg download task 24 hours
            # TODO set correct cache_key for next page
            api.params = _next
            task = observe_events.s(**_next)
            response = cache.get_or_set(
                api.cache_key,
                task.delay,
                timeout=60 * 60 * 24
            )
            # return celery task _indexed_response
            _next['response'] = response.result
        return _next

    def top_collections(self, **kwargs):
        return EventCollection.index.search(limit=10, **kwargs)

    def top_cached_events(self, page=1, **kwargs):
        search = Event.index.search(
            observable=True,
            actual=True,
            **kwargs
        )
        api = ApiRequestFactory(**kwargs)
        # task = api.observe_events.s(**api.params)
        task = observe_events.s(**api.params)
        queryset = cache.get('top:events:%s' % api.cache_key)
        if not queryset:
            # cached hits provider response 24 hours
            # _indexed_response = api.cache.get_or_set(
            _indexed_response = cache.get_or_set(
                api.cache_key,
                # search.execute
                task.delay,
                # api.get_next,
                timeout=60 * 60 * 24
            )
            # cached task result
            response = _indexed_response.result
            cached_response = Response(
              search=search._clone(),
              response=response
            )

            if response and cached_response:
                try:
                    events_count = cached_response.hits.total
                    queryset = cached_response
                except TypeError as e:
                    events_count = 0
                    logger.error('CACHE error: %s' % e)
                    queryset = search._clone().execute()

                if not events_count:
                    queryset = search.execute()
                    # _indexed_response._cache['result'] = queryset.to_dict()
                    # cached top hits response 1 hour
                    # cached hits from provider
                    # cached fb/ebr/kdg download task 24 hours
                    # cache.set('top:events:%s' % api.cache_key, queryset)
        return queryset

    def top_places(self, **kwargs):
        pass

    def get_context_data(self, **kwargs):
        city = None

        # Location radius in meters searching by coords
        querydict = self.request.GET.copy()

        debug = querydict.get('debug', False)
        radius = querydict.get('raduis', 20000)
        page = querydict.get('page', 1)
        querydict.pop('page', 1)

        exclude_events = querydict.getlist('exclude_events[]', [])
        provider = querydict.get('provider', None)
        search_query = querydict.get('q', '')

        city_name = querydict.get('city', None)
        city_name = city_name or querydict.get('name', None)

        start_date = querydict.get('start', None)
        end_date = querydict.get('end', None)

        lat = querydict.get('lat', None)
        lng = querydict.get('lng', None)

        context = super(SearchView, self).get_context_data(**kwargs)

        try:
            page = int(page)
        except ValueError:
            page = 1

        limit_to = page * self.page_size
        limit_from = limit_to - self.page_size
        params = {
            'page': page,
            'q': search_query,
            'limit_from': limit_from,
            'limit_to': limit_to,
        }

        if exclude_events:
            params['exclude_events'] = exclude_events

        if provider:
            params['provider'] = provider

        if start_date:
            start_date = dateparser.parse(start_date, languages=['ru'])
            start_date = start_date.isoformat() if start_date else None
            params['start_date'] = start_date

        if end_date:
            end_date = dateparser.parse(end_date, languages=['ru'])
            end_date = end_date.isoformat() if end_date else None
            params['end_date'] = end_date

        location = self.request.location
        if not search_query:
            # use coords of user location extracted from geoIP
            if location and 'latitude' in location and 'longitude' in location:
                params['lat'] = location['latitude'] or 0
                params['lng'] = location['longitude'] or 0
                params['radius'] = radius

        if city_name:
            # use coords extracted from city model if `city_name` passed
            get_city = lambda: self.get_city(city_name)
            # 1 day cache
            city = cache.get_or_set(city_name, get_city, timeout=60 * 60 * 24)
            if city:
                params['city'] = [city.name]
                params['lat'] = city.location.y
                params['lng'] = city.location.x
                params['radius'] = radius

            city_name = city_name.capitalize()
            params['cities'] = city_name

        if lat and lng:
            params['lat'] = lat.strip('/')
            params['lng'] = lng.strip('/')
            params['radius'] = radius

        # Generate requests task fb/ebr/kdg/clt providers api
        ####
        if page == 1:
            next_data = self.get_next_page(**params)
            upcoming_events = Event.index.search(actual=True, **params)
            # upcoming_events = self.top_cached_events(**params)
            # get 6 top cached events
            upcoming_events = upcoming_events[:6] if upcoming_events else []
            if not len(upcoming_events):
                upcoming_events = Event.index.search(actual=True, **params)
            upcoming_events_id = [event.id for event in upcoming_events]
            params['exclude_events'] = upcoming_events_id
        else:
            upcoming_events = []

        queryset = Event.index.search(**params)
        paginator = ESPaginator(queryset, self.page_size)
        try:
            events = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            events = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            events = paginator.page(paginator.num_pages)

        collections = self.top_collections(**params)

        context['collection_card_template'] = self.collection_card_template
        context['event_card_template'] = self.event_card_template

        context['events'] = events
        context['upcoming_events'] = upcoming_events
        context['events_count'] = len(upcoming_events) + queryset.hits.total

        context['collections'] = collections
        context['collections_count'] = len(collections) if collections else 0

        context['authors'] = []
        context['show_collection_author'] = True
        context['search_query'] = search_query
        context['start_date'] = start_date
        context['end_date'] = end_date

        context['location'] = location
        context['selected_city'] = city
        context['city_name'] = city_name if (lat and lng) else ''
        context['lat'] = lat
        context['lng'] = lng

        # show paginated events on main/search page
        context['search_params'] = '?' + querydict.urlencode()
        context['page_range'] = paginator.elastic_page_range(page)
        context['paginated'] = True
        context['DEBUG'] = debug

        return context
