PIPELINE = {
    # 'PIPELINE_ENABLED': True,
    'COMPILERS': (
        'pipeline.compilers.less.LessCompiler',
    ),
    'JS_COMPRESSOR': 'pipeline.compressors.jsmin.JSMinCompressor',
    'JAVASCRIPT': {
        'vendor': {
            'source_filenames': (
                'js/vendor/jquery-3.1.1.min.js',
                'js/vendor/jquery.cookie.js',
                'js/vendor/jquery.scrollTo.min.js',
                'js/vendor/jquery.easing.1.3.js',
                'js/vendor/select2.full.js',
                'js/vendor/select2.ru.js',
                'js/vendor/jquery.inputmask.bundle.js',
                'js/vendor/moment-with-locales.js',
                'js/vendor/store-json2.min.js',

                'js/vendor/ractive.js',
                'js/vendor/lodash.js',
                'js/vendor/backbone.js',
                'js/vendor/ractive-adaptors-backbone.js',
                'js/vendor/backbone-validation-min.js',

                'js/vendor/markerclusterer.js',
                'js/vendor/phonecodes.js',
                'js/vendor/datetimepicker.js',
                'js/vendor/dropzone.js',
                'js/vendor/easyModal.js',
                'js/vendor/slick.js',
                'js/vendor/el-pagination.js',
                'js/vendor/jquery.geocomplete.js',
                'js/vendor/bootstrap-tooltip.js',
            ),
            'output_filename': 'js/gen/vendor.js',
        },
        'app': {
            'source_filenames': (
                'js/app/app.js',
                'js/app/models.js',
                'js/app/rater.js',

                'js/app/lang.js',
                'js/app/logging.js',

                'js/app/decorators/dropzone.js',
                'js/app/decorators/select2.js',
                'js/app/decorators/select2-placemap.js',
                'js/app/decorators/select2-similar.js',
                'js/app/decorators/datetimepicker.js',

                'js/app/components/login-modal.js',
                'js/app/components/create-collection-modal.js',
                'js/app/components/create-event-form.js',
                'js/app/components/add-to-event-widget.js',
                'js/app/components/add-to-collection-widget.js',
                'js/app/components/translate-collection-widget.js',
                'js/app/components/restore-password-modal.js',
                'js/app/components/remove-place-events-confirmation-modal.js',
                'js/app/components/category-widget.js',
                'js/app/components/search-widget.js',
                'js/app/map.js',
                'js/app/init.js',
            ),
            'output_filename': 'js/gen/app.js',
        }
    },
    'CSS_COMPRESSOR': 'pipeline.compressors.cssmin.CSSMinCompressor',
    'STYLESHEETS': {
        'app': {
            'source_filenames': (
                'less/base.less',
                'css/base.css',

                # atoms
                'css/grid.css',
                'css/links.css',
                'css/buttons.css',
                'css/lists.css',
                'css/tables.css',
                'css/dropdown.css',
                'css/media-queries.css',

                # molecules
                'css/select2.css',
                'css/datetimepicker.css',
                'css/datetimepicker-standalone.css',
                'css/slick.css',
                'css/slick-theme.css',
                'css/sprite.css',
                'css/modal.css',
                'css/rating.css',


                # organizmus
                'css/pages/mainpage.css',
                'css/pages/signup.css',
                'css/pages/event_create.css',
                'css/pages/event_detail.css',

                'css/pages/profile_detail.css',
            ),
            'output_filename': 'css/gen/app.css',
            'extra_context': {
                'media': 'screen',
            },
        },
    },

}
