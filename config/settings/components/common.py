import environ


root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)  # set default values and casting

environ.Env.read_env(root('.env'))  # reading .env file

SITE_ROOT = root()

ugettext = lambda s: s

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'kv)seqfs=h@d(h%vd3q)u$pbg819v65*g!4sscbr+gr-uyban#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [
    '.whatwhere.world',
]
SITE_ID = 1

ADMINS = [
    ('zaebee', 'sinezub@yandex.ru'),
]

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',
    'django.contrib.postgres',
    'django.contrib.gis',

    'ajax_select',
    'annoying',
    'constance',
    'corsheaders',
    'el_pagination',
    'django_extensions',
    'django_pymorphy2',
    'django_celery_results.apps.CeleryResultConfig',
    'django_celery_monitor.apps.CeleryMonitorConfig',
    'template_timings_panel',
    'debug_toolbar',
    'static_sitemaps',
    'storages',
    'drf_multiple_model',
    'dry_rest_permissions',
    'geoipdb_loader',
    'guardian',
    'hitcount',
    'mailer',
    'pipeline',
    'pytils',
    'push_notifications',
    'raven.contrib.django.raven_compat',
    'tinymce',

    'registration',
    'rest_framework',
    'rest_framework_gis',
    'rest_framework.authtoken',
    'rest_framework_swagger',

    'cities',
    'social_django',
    'vk_cities',
    'versatileimagefield',
    'widget_tweaks',

    'api.apps.ApiConfig',
    'app.apps.EventsConfig',
    'services.apps.ServicesConfig',
]

MIDDLEWARE_CLASSES = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
    'app.middleware.LocationMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True
# CORS_URLS_REGEX = r'^/api/collections/.*$'

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            root('templates/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
                'app.context_processors.api_keys',
                'app.context_processors.cities',
            ],
        },
    },
]

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 4,
        },
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'app.User'

LOGOUT_REDIRECT_URL = '/'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

AUTHENTICATION_BACKENDS = (
    'social_core.backends.email.EmailAuth',
    'social_core.backends.vk.VKOAuth2',
    'social_core.backends.facebook.FacebookOAuth2',
    'app.backends.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATICFILES_DIRS = (
    root('static/'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.PipelineFinder',
)

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = [
    ('en', ugettext('eng')),
    ('ru', ugettext('rus')),
]

ANONYMOUS_USER_NAME = None

LOCALE_PATHS = (
    root('locale/'),
)

CITIES_LOCALES = ['en', 'und', 'ru']
CITIES_CITY_MODEL = 'app.City'
CITIES_FILES = {
    'city': {
        'filename': 'cities1000.zip',
        'urls': ['http://download.geonames.org/export/dump/'+'{filename}']
    }
}

GEOIP_PATH = root('geoip/')

STATICSITEMAPS_ROOT_SITEMAP = 'config.urls.sitemaps'
STATICSITEMAPS_MOCK_SITE_PROTOCOL = 'https'

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
    'SEARCH_PARAM': 'query',
}

VERSATILEIMAGEFIELD_RENDITION_KEY_SETS = {
    'user_headshot': [
        ('full', 'url'),
        ('small', 'crop__58x58')
    ],
    'image_gallery': [
        ('large', 'crop__800x600'),
        ('small', 'crop__50x50')
    ],
}

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
    'template_timings_panel.panels.TemplateTimings.TemplateTimings'
]

INTERNAL_IPS = [
    '178.57.99.106',
]
