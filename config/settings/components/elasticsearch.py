import environ

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)  # set default values and casting
env.read_env(root('.env'))  # reading .env file

ES_HOSTS = env('ES_HOST', default='localhost:9200')
ES_HOSTS = ES_HOSTS.split(';')

ELASTICSEARCH_CONNECTION_PARAMS = {
    'hosts': ES_HOSTS,
    'timeout': 60,
}

ELASTICSEARCH_TYPE_CLASSES = (
    'services.elastic.models.Event',
    'services.elastic.models.EventVote',
    'services.elastic.models.CityPlace',
    'services.elastic.models.EventPlace',
    'services.elastic.models.Collection',
    'hitcount.models.Hit',
    'hitcount.models.HitCount',
    'hitcount.models.PushNotification',
)

ELASTICSEARCH_DEFAULT_INDEX_SETTINGS = {
    'settings': {
        'index': {
            'number_of_replicas': 1
        },
        'analysis': {
            'filter': {
                'trigrams_filter': {
                    'type': 'ngram',
                    'min_gram': 3,
                    'max_gram': 3
                },
                'ru_stemming': {
                    'type': 'snowball',
                    'language': 'Russian',
                }
            },
            'analyzer': {
                'trigrams': {
                    'type': 'custom',
                    'tokenizer': 'standard',
                    'filter':   [
                        'lowercase',
                        'trigrams_filter'
                    ]
                },
                'russian': {
                    'type': 'custom',
                    'tokenizer': 'standard',
                    'filter': ['lowercase', 'stop', 'ru_stemming'],
                },
                'left': {
                    'filter': [
                        'standard',
                        'lowercase',
                        'stop'
                    ],
                    'type': 'custom',
                    'tokenizer': 'left_tokenizer'
                },
                'no_filter': {
                    'tokenizer': 'whitespace'
                }
            },
            'tokenizer': {
                'left_tokenizer': {
                    'side': 'front',
                    'max_gram': 12,
                    'type': 'edgeNGram'
                }
            }
        }
    }
}


EVENTS_STEP = 10
