HITCOUNT_KEEP_HIT_ACTIVE = {'minutes': 30}
PUSH_NOTIFICATION_KEEP_ACTIVE = {'days': 1}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.handlers.SentryHandler',
        },
    },
    'loggers': {
        'django.security.DisallowedHost': {
            'handlers': ['null'],
            'propagate': False,
        },
        'elasticsearch': {
            'handlers': ['logfile'],
            'level': 'INFO',
            'propagate': False,
        },
        'app': {
            'handlers': ['sentry'],
            'level': 'INFO',
            'propagate': False,
        },
        'api': {
            'handlers': ['logfile'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'services': {
            'handlers': ['logfile', 'sentry'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'hitcount': {
            'handlers': ['logfile', 'sentry'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['logfile'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'raven': {
            'level': 'INFO',
            'handlers': ['console'],
        },
    }
}
