import os
import environ


root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)  # set default values and casting

environ.Env.read_env(root('.env'))  # reading .env file

SITE_ROOT = root()


def read_proxy_list():
    try:
        proxys = []
        with open(os.path.join(
            SITE_ROOT,
            'config',
            'files',
            'http_proxylist.txt'
        ), 'r') as fh:
            proxys = fh.readlines()
        return list(map(str.strip, proxys))
    except:
        # TODO FileNotFoundError: [Errno 2] No such file or directory:
        return []


PROXY_LIST = read_proxy_list()
