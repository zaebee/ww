import environ
from collections import OrderedDict

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)  # set default values and casting
env.read_env(root('.env'))  # reading .env file


CELERY_BROKER_URL = 'redis://%s:6379/2' % env('REDIS_HOST', default='localhost')
CELERY_RESULT_BACKEND = 'django-db'

CONSTANCE_REDIS_CONNECTION = {
    'host': env('REDIS_HOST', default='localhost'),
    'port': 6379,
    'db': 3,
}

CONSTANCE_CONFIG = OrderedDict([
    ('RATING_MULTI', (1.0, 'Rating multiplier')),
    ('COLLECTION_MULTI', (1.0, 'Collection multiplier')),
    ('EBR_MULTI', (1.0, 'Eventbrite multiplier')),
    ('KDG_MULTI', (1.0, 'Kudago multiplier')),
    ('FB_MULTI', (1.0, 'Facebook multiplier')),
    ('CLT_MULTI', (1.0, 'Culture.ru multiplier')),
    ('VK_MULTI', (1.0, 'Vkontakte multiplier')),
    ('TPAD_MULTI', (1.0, 'Timepad multiplier')),
    ('WW_MULTI', (1.0, 'Whatwhere multiplier')),
    ('RECALCULATE_RANK', (False, 'Recalculate rank')),
])
