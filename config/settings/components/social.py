EVENTBRITE_TOKEN = '5IXLT4CZM6XPRQ2ZVJW7'
GOOGLE_MAP_KEY = 'AIzaSyAFzETh7AEn0R1AVSUQjq9TM05m8LGqWQo'
FACEBOOK_APP_TOKEN = '1728903230667958|c-Dt6DSPvG8rZfFCkkdl4O-ln6Y'

SOCIAL_AUTH_VK_OAUTH2_KEY = '5881490'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'QbpiAEVSi1zI0FsScORq'

SOCIAL_AUTH_FACEBOOK_KEY = '1728903230667958'
SOCIAL_AUTH_FACEBOOK_SECRET = '501bd197f6dc2be7a45f39250931756e'

SOCIAL_AUTH_RAISE_EXCEPTIONS = False
SOCIAL_AUTH_USER_MODEL = 'app.User'
SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['first_name', 'last_name', 'email']

SOCIAL_AUTH_FACEBOOK_SCOPE = ['public_profile', 'email', 'user_friends']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'locale': 'ru_RU',
    'fields': 'id, name, email, picture, link',
}
SOCIAL_AUTH_VK_OAUTH2_SCOPE = ['friends']

LOGIN_ERROR_URL = '/'

SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/profiles/edit/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ['email', 'first_name', 'last_name']

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    #'app.pipeline.require_email',
    'social_core.pipeline.user.get_username',
    #'social_core.pipeline.mail.mail_validation',
    'social_core.pipeline.social_auth.associate_by_email',
    #'app.pipeline.user_by_email',
    'social_core.pipeline.user.create_user',
    'app.pipeline.save_avatar',
    'app.pipeline.save_extra_data',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)
SOCIAL_AUTH_DISCONNECT_PIPELINE = (
    'social_core.pipeline.disconnect.allowed_to_disconnect',
    'social_core.pipeline.disconnect.get_entries',
    'social_core.pipeline.disconnect.revoke_tokens',
    'social_core.pipeline.disconnect.disconnect',
    'app.pipeline.remove_extra_data',
)
