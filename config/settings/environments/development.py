import environ

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, True),)  # set default values and casting

SITE_ROOT = root()

STATIC_ROOT = root('staticfiles/')
MEDIA_ROOT = root('media/')

DEBUG = env('DEBUG')

SOCIAL_AUTH_FACEBOOK_KEY = '347275225718608'
SOCIAL_AUTH_FACEBOOK_SECRET = 'ad4beb79d5548f3e658f4c6f272b6325'
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

RAVEN_CONFIG = {
    'dsn': '',
}
EVENTBRITE_TOKEN = '5WIWFXRCLEMIO27JGJKP'
