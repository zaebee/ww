# -*- coding: utf-8 -*-

from django.contrib.sitemaps import Sitemap
from django.shortcuts import resolve_url
from django.core import paginator

from app.models import (
    Event, EventPlace, EventCollection,
    EventCategory, CityEventCategory
)


class EventSitemap(Sitemap):
    changefreq = "daily"
    protocol = 'https'
    priority = 0.7
    limit = 5000

    def items(self):
        # TODO paginate ES events and filter only future events
        search = Event.index.model.search()
        search = search.exclude('term', deleted=True)
        search = search.query(
            'range',
            **{'dates.start_date': {
                'lte': 'now+1y/d',
                'gte': 'now/d'
            }}
        )
        search = search.params(scroll='100m', size=self.limit)
        return search.execute()

    def location(self, obj):
        return resolve_url('event-detail', obj.provider_id, obj.slug)

    @property
    def paginator(self):
        return paginator.Paginator(list(self.items()), self.limit)


class PlaceSitemap(Sitemap):
    changefreq = "daily"
    protocol = 'https'
    priority = 0.6
    limit = 5000

    def location(self, obj):
        return resolve_url('place-detail', obj.provider_id, obj.slug)

    def items(self):
        search = EventPlace.index.model.search()
        search = search.exclude('term', deleted=True)
        search = search.query('exists', field='provider_id')
        search = search.query('exists', field='slug')
        search = search.params(scroll='100m', size=self.limit)
        return search.execute()

    @property
    def paginator(self):
        return paginator.Paginator(list(self.items()), self.limit)


class CollectionSitemap(Sitemap):
    changefreq = "daily"
    protocol = 'https'
    priority = 0.5

    def location(self, obj):
        return resolve_url('collection-detail', obj.id, obj.slug)

    def items(self):
        objects = EventCollection.index.search()
        return objects


class CategorySitemap(Sitemap):
    changefreq = "daily"
    protocol = 'https'
    priority = 0.5

    def location(self, obj):
        if obj.parent:
            return resolve_url('subcategory-detail', obj.parent.slug, obj.slug)
        else:
            return resolve_url('category-detail', obj.id, obj.slug)

    def items(self):
        objects = EventCategory.objects.all()
        return objects


class CityCategorySitemap(Sitemap):
    changefreq = "daily"
    protocol = 'https'
    priority = 0.5

    def location(self, obj):
        _id, city_name = obj.city.slug.split('-', 1)
        if obj.category.parent:
            return resolve_url(
                'city-subcategory-detail',
                city_name.lower(),
                obj.category.parent.slug,
                obj.category.slug)
        else:
            return resolve_url(
                'city-category-detail',
                city_name.lower(),
                obj.category.slug)

    def items(self):
        objects = CityEventCategory.objects.all()
        return objects
