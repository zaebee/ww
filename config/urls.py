"""whatwhere URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
import debug_toolbar

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from django.contrib import admin
from django.views.generic import TemplateView
from django.views.i18n import JavaScriptCatalog

from django.views.i18n import set_language
from django.views.decorators.csrf import csrf_exempt

from rest_framework_swagger.views import get_swagger_view

from registration.backends.simple.views import RegistrationView

from app.forms import UserRegisterForm

from .sitemap import (
    EventSitemap, PlaceSitemap, CollectionSitemap,
    CategorySitemap, CityCategorySitemap
)

api_schema_view = get_swagger_view(title='WhatWhere API')

sitemaps = {
    'events': EventSitemap,
    'places': PlaceSitemap,
    'cities': CityCategorySitemap,
    'categories': CategorySitemap,
    'collections': CollectionSitemap,
}

## admin and api urls
urlpatterns = [
    url(r'^sitemap.xml', include('static_sitemaps.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/docs/$', api_schema_view),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(packages=['app']), name='javascript-catalog'),
    url('', include('social_django.urls', namespace='social')),
]

## auth/register urls
urlpatterns += [
    url(r'^', include('app.auth_urls')),
    url(r'^register/$', RegistrationView.as_view(form_class=UserRegisterForm), name='registration_register'),
    url(r'^i18n/setlang/$', csrf_exempt(set_language), name='set_language'),
]

## main app urls
urlpatterns += [
    url(r'^', include('app.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
