"""
WSGI config for whatwhere project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
from gevent import monkey

if not monkey.is_module_patched('_gevent_saved_patch_all'):
    monkey.patch_all()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

from psycogreen.gevent import patch_psycopg
patch_psycopg()

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

try:
    import newrelic.agent
    import newrelic.api.exceptions
except ImportError:
    newrelic = False


if newrelic:
    NEWRELIC_CONFIG = os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        'newrelic.ini'
    )
    try:
        os.environ.setdefault("NEW_RELIC_CONFIG_FILE", NEWRELIC_CONFIG)
        newrelic.agent.initialize(NEWRELIC_CONFIG)
    except newrelic.api.exceptions.ConfigurationError:
        newrelic = False

if newrelic:
    application = newrelic.agent.WSGIApplicationWrapper(application)
