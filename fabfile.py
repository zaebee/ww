#
# A fabric deploy script for a django project using uwsgi + nginx (is debian/ubuntu focused)
#
# The nginx virtual host conf and uwsgi.ini should reside in the project root:
#     - virtual host conf filename should be: PROJECT_NAME_nginx.conf
#     - uwsgi conf filename should be: uwsgi.ini
#
# The deployment is mercurial based, but the script requires little changes for using git
# deployment works as follow:
#     $ fab (devel|staging|production) install_system_deps #only once
#     $ fab (devel|staging|production) deploy[:branch_name]
#
# rollback:
#     $ fab (devel|staging|production) rollback
#

import time
import os.path

from fabric.api import *
from fabric.contrib import project


PROJECT_NAME = "whatwhere"

# Remote location deploy dir
DOC_ROOT = "/data/"
SITE_ROOT = "/data/www/"
# Sub directories to be created inside SITE_ROOT
SUB_DIRS = {
    'VENV': 'venv',
    'CONF': 'conf',
    'STATIC': 'static',
    'MEDIA': 'media',
    'MEDIA_USERS': 'media/users'
}

# The place where all the code revisions will live
CODE_DIR = os.path.join(SITE_ROOT, PROJECT_NAME)
# Symbolic link name for the current release
CURRENT_RELEASE = "current"
# Remote config dir
CONF_DIR = os.path.join(DOC_ROOT, SUB_DIRS['CONF'])
# Python Virtual Environment for the project
VENV_DIR = os.path.join(DOC_ROOT, SUB_DIRS['VENV'])
# Temp folder
TMP_DIR = "/tmp"

# Max number of items kept in releases folder
RELEASES_HISTORY = 5


def _create_dirs(site_root, sub_dirs):
    """
    Creates the remote directory structure for deploying the project
    """
    with settings(warn_only=True):
        sudo("mkdir %s" % site_root)
        with cd(site_root):
            for key, dir in sub_dirs.items():
                sudo("mkdir %s" % dir)
        _fix_perms(site_root)


def _check_folders(site_root, sub_dirs):
    """
    Checks the existence of all needed folders
    """
    with settings(warn_only=True):
        if run("test -d %s" % site_root).failed:
            return False
        status = True
        with cd(site_root):
            for key, dir in sub_dirs.items():
                status &= not run("test -d %s" % dir).failed

        return status


def _fix_perms(site_root):
    """
    Updates the folder permissions
    """
    sudo("chown -R %s:%s %s" % (env.user, "www-data", site_root))
    sudo("chmod -R g+rw %s" % site_root)


def _create_virtual_env(venv_dir):
    """
    Creates the virtual environment for the project
    """
    #run("virtualenv --system-site-packages %s" % venv_dir)
    run("virtualenv %s -p python3" % venv_dir)


def devel():
    """
    Sets the environment variables to deploy to localhost
    """
    env.DJANGO_ENV = 'development'
    env.hosts = ['localhost']


def qa():
    """
    Sets the environment variables to deploy to staging environment
    """
    env.DJANGO_ENV = 'qa'
    env.hosts = ['139.162.210.105']
    env.user = 'deployer'


def staging():
    """
    Sets the environment variables to deploy to staging environment
    """
    env.DJANGO_ENV = 'staging'
    env.hosts = ['139.162.130.247']
    env.user = 'deployer'
    env.code_root = '/data/www/src'


def production():
    """
    Sets the environment variables to deploy to production environment
    """
    env.DJANGO_ENV = 'production'
    env.hosts = ['172.104.130.45', '172.104.131.51']
    env.user = 'ww'


def git_pull(branch):
    """Does a git stash then a git pull on the project"""
    run('cd %s;'
        'git fetch origin;'
        'git reset --hard origin/%s' % (env.code_root, branch))


def _update_project(code_dir, branch, release_date):
    """
    Updates the project code in the remote host(s)
    """

    destination_dir = os.path.join(code_dir, release_date)
    run("mkdir %s" % destination_dir)
    with cd(code_dir):
        exclude_pattern = ['.git/', '.gitignore', '.idea/', '*.pyc',
                           'media/', 'settings_local.py', 'data/']
        project.rsync_project(remote_dir=destination_dir,
                              exclude=exclude_pattern)

    _create_code_symlink(code_dir, destination_dir)


def _migrate_database(venv_dir, code_dir, project_name):
    """
    Migrate database project in the remote host(s)
    """
    project_root = os.path.join(code_dir, CURRENT_RELEASE, project_name)
    with cd(project_root):
        activate_venv = os.path.join(venv_dir, "bin", "activate")
        with prefix("source %s" % activate_venv):
            run("python manage.py migrate --noinput")


def _install_requirements(venv_dir, code_dir, release_date, project_name):
    """
    Install the requirements for the project as listed in requirements.txt file
    """
    requirements = os.path.join(code_dir, release_date, project_name, "requirements.txt")
    with cd(venv_dir):
        run("source bin/activate && pip install -r %s" % requirements)


def _create_code_symlink(code_dir, target):
    """
    Creates symbolic link in the releases folder
    """
    link_path = os.path.join(code_dir, CURRENT_RELEASE)
    with cd(code_dir):
        with settings(warn_only=True):
            run("unlink %s" % link_path)
            run("ln -s %s %s" % (target, link_path))


def _get_releases_list(code_dir):
    """
    Returns a list of all releases directories, ignoring the symbolic link
    to the 'current' release
    """
    dirs = run("ls -r -1 %s" % code_dir)
    try:
        from contextlib import nested  # Python 2
    except ImportError:
        from contextlib import ExitStack

        class nested(ExitStack):
            def __init__(self, *managers):
                super(nested, self).__init__()
                for manager in managers:
                    self.enter_context(manager)

    with nested(settings(warn_only=True), hide('warnings', 'running', 'stdout', 'stderr')):
        with cd(code_dir):
            # All directories but the 'current' symbolic link
            releases = [os.path.join(code_dir, item) for item in dirs.splitlines() if run("test -L %s" % item).failed]

    return releases


def _clean_history(code_dir, max_history):
    """
    Deletes the oldest version of code deployed in releases folder
    """
    releases = _get_releases_list(code_dir)
    if len(releases) <= max_history:
        return

    [run("rm -rf %s" % item) for item in releases[max_history:]]


def _link_uwsgi_conf(conf_dir, code_dir, uwsgi_ini_file):
    """
    uWSGI configuration
    """

    conf_destination = os.path.join(conf_dir, uwsgi_ini_file)
    uwsgi_ini_file = 'config/files/%s' % uwsgi_ini_file
    conf_source = os.path.join(code_dir, CURRENT_RELEASE, PROJECT_NAME, uwsgi_ini_file)

    with settings(warn_only=True):
        run("unlink %s" % conf_destination)
        run("ln -s %s %s" % (conf_source, conf_destination))


def _restart_uwsgi(conf_dir, uwsgi_ini_file):
    """
    Restart the uWSGI container
    """
    pid_file_name = PROJECT_NAME + "_uwsgi.sock"
    pid_full_path = os.path.join(SITE_ROOT, pid_file_name)

    with settings(warn_only=True):
        conf_destination = os.path.join(conf_dir, uwsgi_ini_file)
        if run("test -f %s" % pid_full_path).failed:
            run("uwsgi --ini %s" % conf_destination)
        else:
            run("uwsgi --touch-reload %s" % conf_destination)


def _link_supervisor_conf(conf_dir, code_dir, supervisor_file):
    """
    Supervisor configuration
    """

    supervisor_conf_dir = "/etc/supervisor/conf.d"
    supervisor_file = 'config/files/%s' % supervisor_file

    conf_source = os.path.join(code_dir, CURRENT_RELEASE, PROJECT_NAME, supervisor_file)
    with settings(warn_only=True):
        sudo("cp %s %s" % (conf_source, supervisor_conf_dir))


def _restart_supervisor():
    """
    Restart the supervisor container
    """
    sudo("supervisorctl reread")
    sudo("supervisorctl update")
    sudo("supervisorctl restart %s" % PROJECT_NAME)


def _link_nginx_conf(code_dir, vhost_file):
    """
    Nginx configuration
    """
    include_dir = "/etc/nginx/include.d"
    sites_available = "/etc/nginx/sites-available"
    sites_enabled = "/etc/nginx/sites-enabled"
    vhost_path = 'config/files/%s' % vhost_file
    common_vhost = 'config/files/whatwhere_nginx_common.conf'

    conf_source = os.path.join(code_dir, CURRENT_RELEASE, PROJECT_NAME, vhost_path)
    common_source = os.path.join(code_dir, CURRENT_RELEASE, PROJECT_NAME, common_vhost)
    with settings(warn_only=True):
        sudo("cp %s %s" % (common_source, include_dir))
        sudo("cp %s %s" % (conf_source, sites_available))
        sudo("ln -s %s %s" % (os.path.join(sites_available, vhost_file), os.path.join(sites_enabled, vhost_file)))


def _restart_nginx():
    """
    Simply restart the nginx service
    """
    sudo("nginx -t")
    sudo("service nginx reload")


def _collect_static_files(venv_dir, code_dir, project_name):
    """
    Collect all the static files into the final destination that will be served directly by the web server
    """
    project_root = os.path.join(code_dir, CURRENT_RELEASE, project_name)
    with cd(project_root):
        activate_venv = os.path.join(venv_dir, "bin", "activate")
        with prefix("source %s" % activate_venv):
            run("python manage.py collectstatic --noinput")


def _compile_messages(venv_dir, code_dir, project_name):
    """
    Collect all the static files into the final destination that will be served directly by the web server
    """
    project_root = os.path.join(code_dir, CURRENT_RELEASE, project_name)
    with cd(project_root):
        activate_venv = os.path.join(venv_dir, "bin", "activate")
        with prefix("source %s" % activate_venv):
            run("python manage.py compilemessages -l ru -l en")


def install_system_deps():
    """
    Install the S.O needed packages (debian focus)
    and install 'virtualenv' and 'uwsgi'
    as global python packages
    """
    sudo("apt-get install -y python-dev python-pip "
         "libgmp-dev libxml2-dev libxslt1-dev nginx")
    sudo("apt-get install nodejs-dev npm")
    sudo("pip install virtualenv")
    sudo("pip install python3.5 python-setuptools "
         "python-pip python-virtualenv "
         "virtualenvwrapper")
    # sudo("pip install elasticsearch")
    sudo("npm install -g less")
    sudo("pip install uwsgi")


def update_project(branch='master'):
    if not _check_folders(DOC_ROOT, SUB_DIRS):
        _create_dirs(DOC_ROOT, SUB_DIRS)
    release_date = time.strftime("%Y%m%d%H%M%S")
    _update_project(CODE_DIR, branch, release_date)
    _fix_perms(SITE_ROOT)

    _install_requirements(VENV_DIR, CODE_DIR, release_date, PROJECT_NAME)
    _collect_static_files(VENV_DIR, CODE_DIR, PROJECT_NAME)
    _compile_messages(VENV_DIR, CODE_DIR, PROJECT_NAME)
    _migrate_database(VENV_DIR, CODE_DIR, PROJECT_NAME)
    _clean_history(CODE_DIR, RELEASES_HISTORY)


def deploy(branch='master'):
    """
    deploy[:branch], deploys the code (in the specified branch or default)
    and installs the dependencies
    """
    if not _check_folders(DOC_ROOT, SUB_DIRS):
        _create_dirs(DOC_ROOT, SUB_DIRS)

    release_date = time.strftime("%Y%m%d%H%M%S")
    uwsgi_ini_file = "uwsgi.ini"
    supervisor_file = "%s_supervisor.conf" % PROJECT_NAME
    vhost_file = "%s_nginx.conf" % PROJECT_NAME

    _update_project(CODE_DIR, branch, release_date)
    _create_virtual_env(VENV_DIR)
    _fix_perms(SITE_ROOT)

    _install_requirements(VENV_DIR, CODE_DIR, release_date, PROJECT_NAME)
    _collect_static_files(VENV_DIR, CODE_DIR, PROJECT_NAME)
    _compile_messages(VENV_DIR, CODE_DIR, PROJECT_NAME)
    _migrate_database(VENV_DIR, CODE_DIR, PROJECT_NAME)

    _link_uwsgi_conf(CONF_DIR, CODE_DIR, uwsgi_ini_file)

    _link_supervisor_conf(CONF_DIR, CODE_DIR, supervisor_file)
    _restart_supervisor()
    _link_nginx_conf(CODE_DIR, vhost_file)
    _restart_nginx()

    _clean_history(CODE_DIR, RELEASES_HISTORY)


def rollback():
    """
    Rollbacks the code to the previous release if exists
    """
    releases = _get_releases_list(CODE_DIR)

    if len(releases) < 2:
        return

    _create_code_symlink(CODE_DIR, releases[1])
    run("rm -rf %s" % releases[0])
    _fix_perms(SITE_ROOT)
    _restart_supervisor()
    _restart_nginx()
