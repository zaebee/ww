# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import timedelta

from django.conf import settings
from django.utils import timezone

try:
    from django.core.management.base import BaseCommand
except ImportError:
    from django.core.management.base import NoArgsCommand as BaseCommand

from hitcount.models import HitCount


class Command(BaseCommand):
    help = "Can be run as a cronjob or directly to increase HitCounts objects from the database."

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle(self, *args, **kwargs):
        self.handle_noargs()

    def handle_noargs(self, **options):
        grace = getattr(settings, 'HITCOUNT_KEEP_HIT_INCREASE', {'minutes': 60})
        period = timezone.now() - timedelta(**grace)
        search = HitCount.search()
        search = search.filter(
            'term', content_object='events'
        ).query(
            'range', modified={'gte': period}
        ).sort('-hits')
        qs = search.scan()
        for hit in qs:
            hit.increase()
        self.stdout.write('Successfully increased %s HitCounts' % search.count())
