# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta, datetime

import logging
from django.db import models
from django.conf import settings
from django.db.models import F
from django.utils import timezone
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _

from .signals import delete_hit_count

from elasticsearch.exceptions import TransportError
from elasticsearch_dsl.connections import connections

from elasticsearch_dsl import (DocType, Date, Boolean, Object, Keyword,
                               Ip, Long, Integer, String, MetaField)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# configure default ES connection
connections.configure(default=settings.ELASTICSEARCH_CONNECTION_PARAMS)


@receiver(delete_hit_count)
def delete_hit_count_handler(sender, instance, save_hitcount=False, **kwargs):
    """
    Custom callback for the Hit.delete() method.

    Hit.delete(): removes the hit from the associated HitCount object.
    Hit.delete(save_hitcount=True): preserves the hit for the associated
    HitCount object.

    """
    if not save_hitcount:
        instance.hitcount.decrease()


class HitCount(DocType):
    """
    Model that stores the hit totals for any content object.

    """
    hits = Long()
    modified = Date()
    object_id = String(fields={'raw': Keyword()})
    content_object = String(fields={'raw': Keyword()})

    class Meta:
        index = 'hits-index'
        doc_type = 'hitcount'

    def increase(self, **kwargs):
        self.hits = (self.hits or 0) + 1
        self.modified = datetime.now()
        try:
            super(HitCount, self).save(**kwargs)
        # TODO raise 409, 'version_conflict_engine_exception'
        except TransportError as e:  # TransportError:
            logger.error('HitCount increase: %s' % str(e))

    def decrease(self, **kwargs):
        self.hits = (self.hits or 0) - 1
        self.modified = datetime.now()
        try:
            super(HitCount, self).save(**kwargs)
        # TODO raise 409, 'version_conflict_engine_exception'
        except TransportError as e:  # TransportError:
            logger.error('HitCount decrease: %s' % str(e))

    @classmethod
    def get_for_object(self, obj):
        search = self.search()
        search = search.filter(
            'match', content_object=obj.meta.doc_type
        ).query('term', object_id=obj.meta.id)
        hit_count = search.execute()
        if hit_count.hits.total:
            return hit_count[0]
        else:
            hit_count = self(hits=0,
                             content_object=obj.meta.doc_type,
                             object_id=obj.meta.id)
            hit_count.save()
        return hit_count

    def hits_in_last(self, **kwargs):
        """
        Returns hit count for an object during a given time period.

        This will only work for as long as hits are saved in the Hit database.
        If you are purging your database after 45 days, for example, that means
        that asking for hits in the last 60 days will return an incorrect
        number as that the longest period it can search will be 45 days.

        For example: hits_in_last(days=7).

        Accepts days, seconds, microseconds, milliseconds, minutes,
        hours, and weeks.  It's creating a datetime.timedelta object.

        """
        assert kwargs, "Must provide at least one timedelta arg (eg, days=1)"

        search = Hit.search()
        search = search.query('has_child', type='hit')
        period = timezone.now() - timedelta(**kwargs)
        return self.hit_set.filter(created__gte=period).count()


class Hit(DocType):
    """
    Model captures a single Hit by a visitor.

    None of the fields are editable because they are all dynamically created.
    Browsing the Hit list in the Admin will allow one to blacklist both
    IP addresses as well as User Agents. Blacklisting simply causes those
    hits to not be counted or recorded.

    Depending on how long you set the HITCOUNT_KEEP_HIT_ACTIVE, and how long
    you want to be able to use `HitCount.hits_in_last(days=30)` you can choose
    to clean up your Hit table by using the management `hitcount_cleanup`
    management command.

    """
    created = Date()
    ip = Ip()
    session = String()
    user_agent = String()
    user = Object()

    class Meta:
        _parent = MetaField(type='hitcount')
        index = 'hits-index'
        doc_type = 'hit'

    def save(self, **kwargs):
        """
        The first time the object is created and saved, we increment
        the associated HitCount object by one. The opposite applies
        if the Hit is deleted.

        """
        self.created = datetime.now()
        if 'id' not in self.meta:
            HitCount.get(self.meta.parent).increase()
        super(Hit, self).save(**kwargs)

    def delete(self, save_hitcount=False):
        """
        If a Hit is deleted and save_hitcount=True, it will preserve the
        HitCount object's total. However, under normal circumstances, a
        delete() will trigger a subtraction from the HitCount object's total.

        NOTE: This doesn't work at all during a queryset.delete().

        """
        delete_hit_count.send(
            sender=self, instance=self, save_hitcount=save_hitcount)
        super(Hit, self).delete()

    @classmethod
    def filter_active(self, *args, **kwargs):
        """
        Return only the 'active' hits.

        How you count a hit/view will depend on personal choice: Should the
        same user/visitor *ever* be counted twice?  After a week, or a month,
        or a year, should their view be counted again?

        The default is to consider a visitor's hit still 'active' if they
        return within a the last seven days..  After that the hit
        will be counted again.  So if one person visits once a week for a year,
        they will add 52 hits to a given object.

        Change how long the expiration is by adding to settings.py:

        HITCOUNT_KEEP_HIT_ACTIVE  = {'days' : 30, 'minutes' : 30}

        Accepts days, seconds, microseconds, milliseconds, minutes,
        hours, and weeks.  It's creating a datetime.timedelta object.

        """
        search = self.search()
        grace = getattr(settings, 'HITCOUNT_KEEP_HIT_ACTIVE', {'days': 7})
        period = timezone.now() - timedelta(**grace)
        search = search.query(
            'range',
            **{'created': {
                'gte': period
            }}
        )
        return search


@python_2_unicode_compatible
class BlacklistIP(models.Model):

    ip = models.CharField(max_length=40, unique=True)

    class Meta:
        db_table = "hitcount_blacklist_ip"
        verbose_name = _("Blacklisted IP")
        verbose_name_plural = _("Blacklisted IPs")

    def __str__(self):
        return '%s' % self.ip


@python_2_unicode_compatible
class BlacklistUserAgent(models.Model):

    user_agent = models.CharField(max_length=255, unique=True)

    class Meta:
        db_table = "hitcount_blacklist_user_agent"
        verbose_name = _("Blacklisted User Agent")
        verbose_name_plural = _("Blacklisted User Agents")

    def __str__(self):
        return '%s' % self.user_agent


class PushNotification(DocType):
    """
    Model that stores the push_notifications for collection object.
    """
    created = Date()
    new_events_count = Long()      # 0
    application_id = Keyword()     # world.whatwhere
    content_object = String(
        fields={'raw': Keyword()}  # event/place
    )
    object_id = String(
        fields={'raw': Keyword()}  # ebr_1234667123
    )

    class Meta:
        index = 'hits-index'
        doc_type = 'push'

    def save(self, **kwargs):
        self.created = datetime.now()
        try:
            super(PushNotification, self).save(**kwargs)
        except TransportError as e:  # TransportError:
            logger.error('Push add: %s' % str(e))

    @classmethod
    def get_for_application(self, application_id, **kwargs):
        search = self.search()
        search = search.filter(
            'term', application_id=application_id
        )
        last_push_date = kwargs.get('last_push_date', None)
        grace = getattr(settings, 'PUSH_NOTIFICATION_KEEP_ACTIVE', {'days': 1})
        now = timezone.now()
        if last_push_date:
            period = last_push_date
        else:
            period = now - timedelta(**grace)
        search = search.query(
            'range',
            **{'created': {
                'lte': now,
                'gte': period
            }}
        )
        return search
