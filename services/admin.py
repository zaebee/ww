# -*- coding: utf-8 -*-

from django.contrib import admin
from django.dispatch import receiver

from django_celery_monitor.models import TaskState

from constance.signals import config_updated
from constance.admin import ConstanceAdmin, ConstanceForm, Config

from services.tasks import calculate_rank


class ConfigForm(ConstanceForm):
    def __init__(self, *args, **kwargs):
        super(ConfigForm, self).__init__(*args, **kwargs)
        active_task = TaskState.objects.filter(
            name='services.tasks.calculate_rank',
            state='STARTED')
        self.initial['task'] = active_task.first()

    def save(self):
        super(ConfigForm, self).save()
        active_task = TaskState.objects.filter(
            name='services.tasks.calculate_rank',
            state='STARTED')
        self.initial['task'] = active_task.first()


class ConfigAdmin(ConstanceAdmin):
    change_list_form = ConfigForm
    change_list_template = 'admin/config/settings.html'


@receiver(config_updated)
def constance_updated(sender, key, old_value, new_value, **kwargs):
    if key == 'RECALCULATE_RANK' and sender.RECALCULATE_RANK is True:
        calculate_rank.delay()
        calculate_rank.apply_async(['ww'])  # queue='rank'
        calculate_rank.apply_async(['fb'])  # queue='rank'
        calculate_rank.apply_async(['kdg'])  # queue='rank'
        calculate_rank.apply_async(['ebr'])  # queue='rank'
        calculate_rank.apply_async(['clt'])  # queue='rank'
        calculate_rank.apply_async(['tpad'])  # queue='rank'
        sender.RECALCULATE_RANK = False


admin.site.unregister([Config])
admin.site.register([Config], ConfigAdmin)
