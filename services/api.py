# -*- coding: utf-8 -*-

import time
import logging
import grequests
import gevent


from itertools import chain
from functools import reduce
from requests import HTTPError

from rx import Observable

# TODO remove hardcoded providers
from . vk.api import ApiWrapper as VkontakteApi
from . fb.api import ApiWrapper as FacebookApi
from . kdg.api import ApiWrapper as KudagoApi
from . clt.api import ApiWrapper as CultureApi
from . ebr.api import ApiWrapper as EventbriteApi
from . tpad.api import ApiWrapper as TimepadApi

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# All available providers
api_providers = {
    # 'vk': VkontakteApi,
    'fb': FacebookApi,
    'kdg': KudagoApi,
    'clt': CultureApi,
    'ebr': EventbriteApi,
    'tpad': TimepadApi,
}


class ApiWrapper:

    """
    Common API wrapper for all providers.
    Implement proxy for `provider.search_events`,
    `provider.list_events` and `provider.search_places` methods.
    """

    def __init__(self, providers=None, **kwargs):
        self.timeout = 10
        self.page_size = 50
        self.params = kwargs
        self.app_token = kwargs.get('app_token', None)
        self.user_token = kwargs.get('user_token', None)
        self.providers = providers if providers else [
            'kdg', 'ebr', 'fb', 'clt', 'tpad'
        ]

    @property
    def cache_key(self):
        """
        Builds cache key based on `params`.
        """
        cache_key = tuple(
            (k, self.params[k]) for k in sorted(self.params.keys())
        )
        return str(cache_key)

    def _exception_handler(self, request, exception):
        logger.error('Catch exception %s %s' % (request.url, str(exception)))

    def get_requests(self):
        """
        Return generator for api requests to all `providers`.
        """
        for prefix in self.providers:
            provider = api_providers[prefix]
            provider = provider()
            if 'tpad' == prefix:
                yield provider.search_events(**self.params)

            if 'vk' == prefix:
                yield provider.search_events(**self.params)

            if 'clt' == prefix:
                yield provider.search_events(**self.params)

            if 'ebr' == prefix:
                yield provider.search_events(**self.params)

            if 'kdg' == prefix:
                if 'q' in self.params and self.params['q']:
                    yield provider.search_events(**self.params)
                if (
                        'radius' in self.params or
                        'start_date' in self.params or
                        'end_date' in self.params):
                    yield provider.list_events(**self.params)

            if 'fb' == prefix:
                yield provider.search_places(**self.params)
                if self.user_token and 'q' in self.params and self.params['q']:
                    provider.user_token = self.user_token
                    yield provider.search_events(**self.params)

    def get_threads(self):
        """
        Spawn async call `provider` api url.
        """
        requests = self.get_requests()
        for response in grequests.imap(
                requests,
                size=100,
                stream=False,
                exception_handler=self._exception_handler):

            logger.info('request url: %s' % response.url)
            try:
                response.raise_for_status()
                yield gevent.spawn(self.index_events, response)
            except HTTPError as e:
                response.close()
                logger.error('%s(%s) provider %s: %s' % (
                    response.reason, response.status_code,
                    response.provider, str(e)))
                result = {
                    'places_count': 0,
                    'events_count': 0,
                    'has_next': False,
                    'provider': response.provider,
                }
                yield gevent.spawn(lambda: result)

    def index_events(self, response):
        """
        Call hook to serialize/index response events.
        """
        from services.tasks import make_preview_events_ids

        gevent.sleep(0)
        t0 = time.time()
        # save events response from api to elasticsearch
        # see `services/<provider>/api.py` hook
        indexer = response.indexer(response)
        events = indexer('event')
        places = indexer('place')
        events_count = len(events)
        places_count = len(places)
        events_id = [event['id'] for event in events]
        if len(events_id):
            pass
            # make_preview_events_ids.apply_async([events_id], queue='images')
        logger.debug('%s %d events succesfully indexed (%.2f sec)' % (
            response.provider, events_count, time.time() - t0))
        return {
            'provider': response.provider,
            'events_count': events_count,
            'places_count': places_count,
            'events_id': events_id
        }

    def get_next(self):
        """
        Return next page params.
        """
        t0 = time.time()
        params = self.params
        threads = self.get_threads()
        result = gevent.joinall(list(threads))
        events_count = reduce(
            lambda x, y: x + y.value.get('events_count'),
            result, 0)

        places_count = reduce(
            lambda x, y: x + y.value.get('places_count'),
            result, 0)

        logger.debug('Next page params: %s' % str(params))
        logger.debug('Total %s events fetched complete (%.2f sec)' % (
            events_count, time.time() - t0))

        params['page'] = params.get('page', 1) + 1
        params['has_next'] = True
        return {
            'next': params,
            'total_events': events_count,
            'total_places': places_count,
            'provider': [r.value.get('provider', '') for r in result],
            # 'events_id': [r.value.get('events_id', []) for r in result],
        }


class ApiRequestFactory(ApiWrapper):
    """
    Factory is provided request generator for all enabled providers.
    """
    def __init__(self, *args, **kwargs):
        super(ApiRequestFactory, self).__init__(*args, **kwargs)
        self.factories = {}

    @property
    def _index_cache_key(self):
        """
        Builds ElasticSearch index cache key based on `params`.
        """
        index_key = 'event-index-{city}-{query}'  # {start_date}-{end_date}
        city = self.params.get('city', 'all')
        query = self.params.get('q', 'all')
        return index_key.format(
            city=city,
            query=query,
        )

    def set(self, factory):
        instance = factory()

        if not hasattr(instance, 'provider'):
            raise ValueError('factory requires provider/interface type')

        self.factories[instance.provider] = factory

    def remove(self, factory):
        for provider, _factory in self.factories.items():
            if factory == _factory:
                del self.factories[provider]
                return

    def dispose(self):
        # TODO dispose observable/pool/session
        # self.session.close()
        pass

    def observe(self, provider, *args, **kwargs):
        """
        Observe events only from selected `provider`.
        """
        params = kwargs or self.params
        if provider not in self.factories:
            return None

        factory = self.factories.get(provider, None)

        if factory is None:
            return None
        return factory().observe(*args, **params)

    def observable_events(self, *args, **kwargs):
        """
        Observe events from all providers.
        """
        return Observable.merge([
            self.observe(provider, *args, **kwargs)
            for provider, factory in self.factories.items()
        ])

    def observable_places(self, *args, **kwargs):
        """
        Observe places from all providers.
        """
        # places = list(args)
        logger.debug('Observe places %s' % str(kwargs))
        provider = kwargs.get('provider', None)
        places = kwargs.get('places', Observable.empty())
        if provider in self.factories.keys():
            factory = self.factories[provider]
            logger.debug('Run factory %s' % str(factory))
            places = places.buffer_with_count(
                50
            ).flat_map(
                lambda ids: factory().observe_places(places=ids)
            )
        return places

    def observable_collections(self, *args, **kwargs):
        """
        Observe collections from all providers.
        """
        collections = list(args)
        places = set(chain.from_iterable(
            collection.places for collection in collections
            if collection.places
        ))
        logger.debug('observe places %s' % places)

        # places = collections
        # collections = Observable.from_(places).publish()
        # collections.connect()

        places = Observable.from_(places).map(
            # get pair from `place` 'fb_1634545243479847'
            lambda place: place.split('_')
        ).group_by(
            # place is list of two : ['fb', '1634545243479847']
            lambda place: place[0],  # `key`  is provider prefix
            lambda place: place[1],  # `selector` is provider place id
        ).map(
            lambda group: {'provider': group.key, 'places': group}
        ).flat_map(
            lambda group: self.observable_places(**group)
        )

        return places
