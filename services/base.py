# -*- coding: utf-8 -*-

import time
import logging
import grequests

from itertools import chain
from dateutil import parser

from app.models import Event, EventPlace

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiBase:
    """
    Base API wrapper.
    Implement proxy for `search_events` and `get_events_request` methods.
    """

    def __init__(self, session=None, token=None, **kwargs):
        self.timeout = kwargs.get('timeout', 10)
        self.page_size = kwargs.get('page_size', 25)
        self.api = grequests
        self.provider = None

        self.event_serializer = None
        self.event_model = Event
        self.event_index = self.event_model.index

        self.place_serializer = None
        self.place_model = EventPlace
        self.place_index = self.place_model.index

        self.token = None

    def _search_callback(self, provider, time_start, **params):

        def hook(response, **kwargs):
            time_end = time.time() - time_start
            logger.debug(
                '%s provider response (%.2f sec)' % (provider, time_end)
            )
            response.provider = provider
            response.indexer = self._prepare_response_for_indexing
            response.type = params.get('type', None)
            logger.debug(
                '%s provider url: %s' % (response.provider, response.url)
            )
            return response
        return hook

    def _prepare_response_for_indexing(self, response):
        """
        Serialize `response.provider` to events
        and store to ElasticSearch.
        """
        def _index_events(_indexer, _response=response):

            data = _response.json()
            events = []
            places = []
            if response.provider == 'fb':
                places = data.get('data', [])
                events_data = chain.from_iterable(
                    place['events']['data'] for place in places
                    if 'events' in place)
            elif response.provider == 'kdg':
                events_data = data.get('results', [])
            elif response.provider == 'ebr':
                events_data = data.get('events', [])
            elif response.provider == 'clt':
                events_data = data.get('events', [])
            elif response.provider == 'tpad':
                events_data = data.get('values', [])
            elif response.provider == 'vk':
                events_data = data.get('response', [])
            else:
                events_data = events

            if _indexer == 'event':
                events = self.event_serializer(events_data, many=True)
                _indexer = self.event_index.bulk_index(queryset=events.data)
                result = events.data
            elif _indexer == 'place':
                places = [event['place'] for event
                          in self.event_serializer(events_data, many=True).data
                          if 'place' in event]
                _indexer = self.place_index.bulk_index(queryset=places)
                result = places
            else:
                result = []
            return result
        return _index_events

    def _is_actual_event(self, event):
        return len(event['dates'])

    def _serialize_event(self, event):
        serializer = self.event_serializer(event)
        return serializer.data

    def _serialize_place(self, event):
        serializer = self.place_serializer(event)
        return serializer.data

    def search_events(self, **params):
        raise NotImplementedError(
            'Doesnt exists search_events method for %s' % self.provider
        )

    def get_events_request(self, **params):
        """
        Generate requests to provider api with `params`
        """
        raise NotImplementedError(
            'Doesnt exists get_events_request method for %s' % self.provider
        )

    def search_places(self, **params):
        raise NotImplementedError(
            'Doesnt exists search_places method for %s' % self.provider
        )

    def get_places_request(self, **params):
        raise NotImplementedError(
            'Doesnt exists get_places_request method for %s' % self.provider
        )

    def observe(self, *args, **kwargs):
        # TODO rename method to `observe_events`
        raise NotImplementedError(
            'Doesnt exists observe method for %s' % self.provider
        )

    def observe_places(self, *args, **kwargs):
        raise NotImplementedError(
            'Doesnt exists observe method for %s' % self.provider
        )
