# -*- coding: utf-8 -*-

import time
import json
import logging

from datetime import datetime

from django.conf import settings
from django.shortcuts import resolve_url

from services.base import ApiBase

from . import validators
from . import schema
from . observers import CultureObservable

API_URL = 'https://all.culture.ru/api/2.2'

APP_TOKEN = getattr(settings, 'CULTURE_TOKEN', '')

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiWrapper(ApiBase):
    """
    Culture.ru API wrapper.
    Implement proxy for `search_events`, `list_events` methods.
    """

    def __init__(self, session=None, token=None, **kwargs):
        super(ApiWrapper, self).__init__(session, token, **kwargs)
        self.provider = 'clt'
        self.app_token = token or APP_TOKEN
        self.event_serializer = schema.EventSerializer
        self.place_serializer = schema.PlaceSerializer

    def get_locales(self, **params):
        """
        Culture api locales method.
        """
        url = '%s/locales/' % API_URL
        request = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
        )
        request.send()
        data = request.response.json() if request.response else {}
        locales = [str(locale['_id']) for locale in data.get('locales', [])]
        return locales

    def search_events(self, **params):
        """
        Culture api search method. params `q` required.
        """
        time_start = time.time()
        url = '%s/events/' % API_URL
        cities = params.get('cities', None)
        page = params.get('page', 1)
        params = validators.validate_input(**params)
        params['limit'] = self.page_size
        params['status'] = 'accepted'
        if cities:
            locales = self.get_locales(nameQuery=cities)
            if len(locales):
                params['locales'] = ','.join(locales)
        if page > 1:
            params['offset'] = (page * self.page_size) - self.page_size
        response = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
            callback=self._search_callback(self.provider, time_start)
        )
        return response

    def get_events(self, **params):
        """
        Culture api search method. Get events by `ids`.
        """
        ids = params.get('ids', None)
        if ids:
            params['ids'] = ','.join([str(i) for i in ids])
        return self.search_events(**params)

    def get_events_request(self, **params):
        """
        """
        def request(page=1, params=params):
            params['page'] = page
            return self.get_events(**params)
        return request

    def get_places(self, **params):
        """
        Culture api search method. Get places events by `places ids`.
        """
        places = params.get('places', None)
        if places:
            params['places'] = ','.join([str(i) for i in places])
        return self.search_events(**params)

    def get_places_request(self, **params):
        """
        """
        def request(page=1, params=params):
            params['page'] = page
            return self.get_places(**params)
        return request

    def import_events(self, items=None, **params):
        """
        Culture import partner data.
        """
        url = '%s/import/?apiKey=%s' % (API_URL, self.app_token)

        params['items'] = []
        for item in items:
            item_url = resolve_url('event-detail', item.provider_id, item.slug)
            updated = datetime.now().timestamp() * 1000
            params['items'].append({
                'entity': {
                    '_id': item.provider_id,
                    'type': 'events'
                },
                'views': getattr(item, 'views', 0),
                'likes': getattr(item, 'likes', 0),
                # 'rank': getattr(item, 'rank', 0),
                'url': 'https://whatwhere.world%s' % item_url,
                'statuses': ['featured', 'published'],
                'updateDate': int(updated)
            })
        request = self.api.post(
            url,
            timeout=self.timeout,
            data=json.dumps(params),
            headers={'content-type': 'application/json'},
        )
        return request

    def observe(self, *args, **kwargs):
        """
        """
        if any(kwargs.values()):
            request = self.get_events_request(**kwargs)
            events = CultureObservable.from_provider(request).publish()
        else:
            events = CultureObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events

    def observe_places(self, *args, **kwargs):
        """
        places = kwargs.get('places', [])
        """
        # get place ids
        # places = ['clt_12417', 'clt_12417', 'clt_8791', 'clt_12417']
        # places = kwargs.get('places', [])
        if any(kwargs.values()):
            request = self.get_places_request(**kwargs)
            events = CultureObservable.from_provider(request).publish()
        else:
            events = CultureObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events
