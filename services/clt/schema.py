# -*- coding: utf-8 -*-


try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from pytz import timezone
from datetime import datetime
from uuid import uuid4
from pytils.translit import slugify
from services.schema import BasePlaceSerializer, BaseEventSerializer


class PlaceSerializer(BasePlaceSerializer):
    provider = 'clt'

    def get_place(self, obj):
        places = obj.get('places', [])
        if places:
            return places[0]
        else:
            return obj.get('organizerPlace', None)

    def get_place_id(self, obj):
        return '%s_%s' % (self.provider, self.get_provider_id(obj))

    def get_provider_id(self, obj):
        place = self.get_place(obj)
        if place:
            place_id = place.get('_id', 0)
            if not place_id:
                place = obj.get('organizerPlace', {})
                place_id = place.get('_id', 0)
                if not place_id:
                    place = obj.get('organization', {})
                    place_id = place.get('_id', 0)
            return place_id

    def get_slug(self, obj):
        title = self.get_title(obj)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def get_title(self, obj):
        place = self.get_place(obj)
        if place:
            title = place.get('name', None)
            if 'address' in place and 'comment' in place['address']:
                title = title or place['address']['comment']
        if not title:
            place = obj.get('organization', {})
            title = place.get('name', None)
        return title

    def get_address(self, obj):
        place = self.get_place(obj)
        if place:
            address = place.get('address', None)
            if address:
                return address.get('street', None)

    def get_city(self, obj):
        place = self.get_place(obj)
        if place:
            locale = place.get('locale', None)
            if locale:
                return locale.get('name', None)

    def get_lat(self, obj):
        place = self.get_place(obj)
        lat = 0
        try:
            coords = place['address']['mapPosition']['coordinates']
            if len(coords) == 2:
                lat, lng = coords
                lat = float(lat)
        except KeyError:
            lat = 0
        return lat

    def get_lng(self, obj):
        place = self.get_place(obj)
        lng = 0
        try:
            coords = place['address']['mapPosition']['coordinates']
            if len(coords) == 2:
                lat, lng = coords
                lng = float(lng)
        except KeyError:
            lng = 0
        return lng

    def get_geometry(self, obj):
        return {
            'lat': self.get_lat(obj),
            'lon': self.get_lng(obj),
        }

    def get_email(self, obj):
        return obj.get('email', None)

    def get_website(self, obj):
        return obj.get('site_url', None)

    def get_phone(self, obj):
        return obj.get('phone', None)

    def get_description(self, obj):
        return obj.get('shortDescription', '')


class EventSerializer(BaseEventSerializer):
    provider = 'clt'
    place = PlaceSerializer(required=False, allow_null=True, source='*')

    def get_id(self, obj):
        return '%s_%s' % (self.provider, obj['_id'])

    def get_provider_id(self, obj):
        return obj.get('_id', 0)

    def get_title(self, obj):
        return obj.get('name', '')

    def get_description(self, obj):
        return obj.get('description', '')

    def get_slug(self, obj):
        title = self.get_title(obj)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def _get_category(self, obj):
        category = obj.get('category', None)
        if category:
            return {
                'title': category.get('name', None),
                'slug': category.get('sysName', None)
            }

    def get_images(self, obj):
        url = 'https://all.culture.ru/uploads/{name}'
        gallery = obj.get('gallery', None)
        data = []
        image = self.get_image(obj)
        if image:
            data.append({
                'image': image
            })
        if gallery:
            for image in gallery:
                image_url = url.format(**image)
                data.append({
                    'image': image_url
                })
        return data

    def get_image(self, obj):
        thumbnail = obj.get('image', None)
        if thumbnail:
            image_url = 'https://all.culture.ru/uploads/{name}'.format(
                **thumbnail
            )
            return image_url

    def get_schedules(self, obj):
        try:
            tz = obj['organization']['locale']['timezone']
        except KeyError:
            tz = 'UTC'
        tz = timezone(tz)
        now = datetime.now(tz)
        seances = obj.get('seances', [])
        schedules = []
        for seance in seances:
            start = seance.get('start', None)
            end = seance.get('end', None)
            if start:
                start_date = datetime.fromtimestamp(start/1000, tz)
                start = start_date.strftime('%Y-%m-%dT%H:%M:%S%z')
            if end:
                end_date = datetime.fromtimestamp(end/1000, tz)
                end = end_date.strftime('%Y-%m-%dT%H:%M:%S%z')
            if (end and end_date > now) or (start and start_date > now):
                schedules.append({
                    'start_date': start,
                    'end_date': end,
                })
        return schedules

    def get_counters(self, obj):
        external_info = obj.get('externalInfo', [])
        views = sum(info.get('views', 0) for info in external_info)
        likes = sum(info.get('likes', 0) for info in external_info)
        return {
            'favorites_count': views + likes
        }

    def get_author(self, obj):
        pass

    def get_collections(self, obj):
        pass

    def get_ticket_price(self, obj):
        max_price = obj.get('maxPrice', None)
        price = obj.get('price', None)
        if max_price and price != max_price:
            return '%s - %s руб' % (price, max_price)
        elif price:
            return '%s руб' % price
        else:
            return 0  # is Free event

    def get_ticket_url(self, obj):
        return obj.get('saleLink', None)

    def get_video_url(self, obj):
        pass

    def get_source_url(self, obj):
        service_name = 'Культура.рф'
        info = obj.get('externalInfo', [])
        source = filter(
            lambda x: x.get('serviceName', None) == service_name,
            info
        )
        try:
            # get cultura.ru source link
            source = next(source)
        except StopIteration:
            # take first item from externalInfo if no cultura.u source
            if len(info):
                source = info[0]
        if isinstance(source, dict):
            return source.get('url', None)

    def get_min_price(self, obj):
        return obj.get('price', None)

    def get_max_price(self, obj):
        return obj.get('maxPrice', None)

    def get_is_free(self, obj):
        return obj.get('isFree', False)

    def get_currency(self, obj):
        return None  # 'RUB'

    def get_formatted_price(self, obj):
        return self.get_ticket_price(obj)
