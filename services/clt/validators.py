# -*- coding: utf-8 -*-

import dateparser
from cerberus import Validator


def get_timestamp_start(doc):
    start = doc.get('start_date', None)
    if start:
        start = dateparser.parse(start)
        return int(start.timestamp() * 1000)
    else:
        return 0


def get_timestamp_end(doc):
    end = doc.get('end_date', None)
    if end:
        end = dateparser.parse(end)
        return int(end.timestamp() * 1000)
    else:
        return 0


def validate_input(**kwargs):
    """
    Convert our request search params to Culture.ru search params.
    """
    schema = {
        'q': {
            'type': 'string',
            'rename': 'nameQuery',
        },
        'ids': {
            'type': 'string',
        },
        'places': {
            'type': 'string',
        },
        'fields': {
            'type': 'string',
        },
        'expand': {
            'type': 'string'
        },
        'sort': {
            'type': 'string'
        },
        'limit': {
            'type': 'integer'
        },
        'offset': {
            'type': 'integer'
        },
        'start_date': {
            'type': 'string'
        },
        'end_date': {
            'type': 'string'
        },
        'start': {
            'default_setter': get_timestamp_start
        },
        'end': {
            'default_setter': get_timestamp_end
        },
        'nameQuery': {
            'type': 'string',
        },
    }
    validator = Validator(schema, purge_unknown=True)
    if validator.validate(kwargs):
        params = validator.normalized(kwargs)
        params.pop('start_date', None)
        params.pop('end_date', None)
        return params
    else:
        # TODO raise validation error
        return validator.errors
