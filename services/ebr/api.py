# -*- coding: utf-8 -*-

import time
import logging

from django.conf import settings

from services.base import ApiBase

from . import validators
from . import schema
from . observers import EventbriteObservable

APP_TOKEN = getattr(settings, 'EVENTBRITE_TOKEN', '')
API_URL = 'https://www.eventbriteapi.com/v3'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiWrapper(ApiBase):
    """
    Eventbrite API wrapper.
    Implement proxy for `search_events` and `get_event` methods.
    """

    def __init__(self, session=None, token=None):
        super(ApiWrapper, self).__init__(session, token)
        self.provider = 'ebr'
        self.event_serializer = schema.EventSerializer
        self.place_serializer = schema.PlaceSerializer
        self.app_token = token or APP_TOKEN

    def search_events(self, **params):
        time_start = time.time()
        url = '%s/events/search/' % API_URL
        params['expand'] = str(
            'venue,logo,category,location,bookmark_info,ticket_classes'
        )
        params['sort_by'] = '-date'
        params['token'] = self.app_token
        params = validators.validate_input(**params)
        request = self.api.get(
            url,
            params=params,
            timeout=self.timeout,
            callback=self._search_callback(self.provider, time_start)
        )
        return request

    def get_events_request(self, **params):
        """
        """
        def request(page=1, params=params):
            params['page'] = page
            return self.search_events(**params)
        return request

    def get_places_request(self, **params):
        """
        """
        def request(page=1, params=params):
            venue_id = params.get('places', None)
            time_start = time.time()
            url = '%s/venues/%s/events/' % (API_URL, venue_id)
            params['expand'] = 'venue'
            params['token'] = self.app_token
            params = validators.validate_input(**params)
            request = self.api.get(
                url,
                params=params,
                timeout=self.timeout,
                callback=self._search_callback(self.provider, time_start)
            )
            return request
        return request

    def observe(self, *args, **kwargs):
        """
        """
        if any(kwargs.values()):
            request = self.get_events_request(**kwargs)
            events = EventbriteObservable.from_provider(request).publish()
        else:
            events = EventbriteObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events

    def observe_places(self, *args, **kwargs):
        """
        """
        # get place ids
        # places = ['ebr_619726138154445', 'ebr_619726138154445', 'ebr_619726138154445']
        places = kwargs.get('places', [])
        requests = [self.get_places_request(places=place) for place in places]

        if any(kwargs.values()):
            events = EventbriteObservable.merge([
                EventbriteObservable.from_provider(request)
                for request in requests
            ]).publish()
        else:
            events = EventbriteObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events
