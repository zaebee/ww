# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

import logging
from gevent.pool import Pool
from requests import HTTPError
from rx import Observable, AnonymousObservable

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

pool = Pool(5)


def retry_request(request):
    retry_count = 3
    while not request.response and retry_count:
        retry_count -= 1
        request.send()
    return request


class EventbriteObservable(Observable):

    @classmethod
    def from_provider(cls, get_request):

        def subscribe(observer):

            def callback(_request, page):
                response = _request.response
                # timeout error
                try:
                    # check status is not 200 OK
                    if response:
                        response.raise_for_status()
                except HTTPError as e:
                    observer.on_error(e)

                if response:
                    data = response.json()

                    events = data.get('events', [])
                    for event in events:
                        observer.on_next(event)

                    paging = data.get('pagination', {})
                    if paging.get('has_more_items', None) and page <= 10:
                        page = (page or 1) + 1
                        logger.debug("Request page: %s" % page)
                        action(page)
                    else:
                        observer.on_completed()
                else:
                    observer.on_error('No response')

            def action(page=1):
                _request = get_request(page)
                pool.apply_async(
                    retry_request,
                    args=(_request,),
                    callback=lambda x: callback(x, page))

            action()

        return AnonymousObservable(subscribe)
