# -*- coding: utf-8 -*-

from uuid import uuid4
from pytils.translit import slugify

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from services.schema import BasePlaceSerializer, BaseEventSerializer


class PlaceSerializer(BasePlaceSerializer):
    provider = 'ebr'

    def get_slug(self, obj):
        title = obj.get('name', None)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def get_title(self, obj):
        # TODO venue.title in ['0', 'none', '', 'empty', 'test']
        # generate title
        title = obj.get('name', None)
        return title

    def get_address(self, obj):
        address = obj.get('address', None)
        if address and isinstance(address, dict):
            return address.get('address_1', None)

    def get_city(self, obj):
        address = obj.get('address', None)
        if address and isinstance(address, dict):
            return address.get('city', None)

    def get_lat(self, obj):
        address = obj.get('address', None)
        lat = 0
        if address and isinstance(address, dict) and 'latitude' in address:
            lat = float(address['latitude'])
        return lat

    def get_lng(self, obj):
        address = obj.get('address', None)
        lng = 0
        if address and isinstance(address, dict) and 'longitude' in address:
            lng = float(address['longitude'])
        return lng

    def get_geometry(self, obj):
        return {
            'lat': self.get_lat(obj),
            'lon': self.get_lng(obj),
        }

    def get_email(self, obj):
        return obj.get('email', None)

    def get_website(self, obj):
        return obj.get('site_url', None)

    def get_phone(self, obj):
        return obj.get('phone', None)


class EventSerializer(BaseEventSerializer):
    provider = 'ebr'
    place = PlaceSerializer(source='venue')

    def get_provider_id(self, obj):
        return obj.get('id', 0)

    def get_title(self, obj):
        name = obj.get('name', {})
        return name.get('text', None)

    def get_description(self, obj):
        description = obj.get('description', {})
        return description.get('text', None)

    def get_slug(self, obj):
        url = obj.get('url', '')
        path = urlparse(url).path
        path = path.split('/').pop()
        return path

    def _get_category(self, obj):
        category = obj.get('category', None)
        if category and isinstance(category, dict):
            category = category.get('name', None)
            return {
                'title': category
            }

    def get_images(self, obj):
        image = obj.get('logo', None)
        if image and isinstance(image, dict):
            original = image.get('original', None)
            if original:
                return [{
                    'image': original.get('url', '')
                }]

    def get_image(self, obj):
        image = obj.get('logo', None)
        if image and isinstance(image, dict):
            image = image.get('url', '')
            return image

    def get_schedules(self, obj):
        start = obj.get('start', None)
        end = obj.get('end', None)
        if start and end:
            if isinstance(start, dict) and isinstance(end, dict):
                return [{
                    'start_date': start.get('local', None),
                    'end_date': end.get('local', None),
                }]

    def get_counters(self, obj):
        bookmark_info = obj.get('bookmark_info', {})
        return {
            'favorites_count': bookmark_info.get('count', 0)
        }

    def get_author(self, obj):
        pass

    def get_collections(self, obj):
        pass

    def get_video_url(self, obj):
        pass

    def get_source_url(self, obj):
        return obj.get('url', '')

    def get_tickets(self, obj):
        ticket_classes = obj.get('ticket_classes', [])
        tickets = [
            dict(
                free=ticket.get('free', False),
                cost=ticket.get('cost', None),
                fee=ticket.get('fee', None),
                include_fee=ticket.get('include_fee', False)
            )
            for ticket in ticket_classes
            if ticket['on_sale_status'] == 'AVAILABLE'
        ]
        return tickets

    def get_ticket_price(self, obj):
        price = None
        min_price = self.get_min_price(obj)
        max_price = self.get_max_price(obj)
        if min_price and max_price and min_price != max_price:
            price = '%s - %s' % (min_price, max_price)
        elif min_price:
            price = min_price
        elif max_price:
            price = max_price
        return price

    def get_ticket_url(self, obj):
        pass

    def get_min_price(self, obj):
        tickets = self.get_tickets(obj)
        if tickets:
            ticket = min(
                tickets,
                key=lambda x: x['cost']['value'] if x['cost'] else 0
            )
            if not ticket['cost']:
                return None
            if ticket['include_fee']:
                min_price = float(ticket['cost']['major_value'])
            else:
                fee = float(ticket['fee']['major_value'])
                min_price = float(ticket['cost']['major_value'])
                min_price = fee + min_price
            return min_price

    def get_max_price(self, obj):
        tickets = self.get_tickets(obj)
        if tickets:
            ticket = max(
                tickets,
                key=lambda x: x['cost']['value'] if x['cost'] else 0
            )
            if not ticket['cost']:
                return None
            if ticket['include_fee']:
                max_price = float(ticket['cost']['major_value'])
            else:
                fee = float(ticket['fee']['major_value'])
                max_price = float(ticket['cost']['major_value'])
                max_price = fee + max_price
            return max_price

    def get_is_free(self, obj):
        tickets = self.get_tickets(obj)
        return all(ticket['free'] for ticket in tickets)

    def get_currency(self, obj):
        tickets = self.get_tickets(obj)
        try:
            currency = next(
                ticket['cost']['currency']
                for ticket in tickets if ticket['cost']
            )
        except StopIteration:
            currency = None
        return currency

    def get_formatted_price(self, obj):
        currency = self.get_currency(obj)
        price = self.get_ticket_price(obj)
        if price and currency:
            return '%s %s' % (price, currency)
