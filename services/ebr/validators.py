# -*- coding: utf-8 -*-

from cerberus import Validator


def validate_input(**kwargs):
    """
    Convert our request search params to Eventbrite search params.
    """
    schema = {
        'q': {
            'type': 'string',
        },
        'page': {
            'type': 'integer',
            'coerce': int
        },
        'expand': {
            'type': 'string',
        },
        'sort_by': {
            'type': 'string',
        },
        'token': {
            'type': 'string',
        },
        'radius': {
            'rename': 'location.within',
        },
        'lat': {
            'rename': 'location.latitude',
        },
        'lng': {
            'rename': 'location.longitude',
        },
        'start_date': {
            'type': 'string',
            'rename': 'start_date.range_start',
        },
        'end_date': {
            'type': 'string',
            'rename': 'start_date.range_end',
        },

        'location.within': {
            'type': 'string',
            ## convert meters to km
            'coerce': lambda x: '%skm' % int(x / 1000),
        },
        'location.latitude': {
            'type': 'float',
            'coerce': float,
        },
        'location.longitude': {
            'type': 'float',
            'coerce': float,
        },
        'start_date.range_start': {
            'type': 'string',
        },
        'start_date.range_end': {
            'type': 'string',
        },
    }
    validator = Validator(schema, purge_unknown=True)
    if validator.validate(kwargs):
        params = validator.normalized(kwargs)
        return params
    else:
        ## TODO raise validation error
        return validator.errors
