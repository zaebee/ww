from datetime import datetime

from elasticsearch_dsl import (
    Q, FacetedSearch,
    RangeFacet, TermsFacet, DateHistogramFacet
)

from .models import Event, Collection


class PlaceTermsFacet(TermsFacet):
    """
    Build popular places in collection.
    """
    def get_aggregation(self):
        # size = 5
        agg = super().get_aggregation()
        return agg

    def _add_filter(self, filter_values):
        """ Create a terms filter instead of bool containing term filters.  """
        if filter_values:
            return Q('terms', **{self._params['field']: filter_values})


class EventDateFacet(DateHistogramFacet):

    def get_aggregation(self):
        size = 10
        source = False
        agg = super().get_aggregation()
        agg = agg.metric('events', 'top_hits',
                         size=size, _source=source,
                         sort={'rank': {'order': 'desc'}})
        return agg

    def get_value(self, bucket):
        """
        """
        try:
            return datetime.utcfromtimestamp(int(bucket['key']) / 1000)
        except TypeError:
            return bucket['key']

    def get_values(self, data, filter_values):
        """
        Turn the raw bucket data into a list of tuples containing the key,
        number of documents, source events and a flag indicating
        whether this value has been
        selected or not.
        """
        for bucket in data:
            key = self.get_value(bucket)
            bucket.key = key
            bucket.selected = self.is_filtered(key, filter_values)
        return data


class CollectionSearch(FacetedSearch):
    index = 'event-index'
    doc_types = [Event, Collection]
    # fields that should be searched
    # fields = ['id', 'title', 'description']
    fields = ['title_localized.*^3', 'title^2',
              'description', 'place.title', 'place.address']

    facets = {
        # 'cities': TermsFacet(field='place.city.raw'),
        # 'collections': TermsFacet(field='')
        'places': PlaceTermsFacet(field='place.place_id.raw'),
        'day_events': EventDateFacet(
            field='dates.start_date',
            interval='day',
            min_doc_count=1
        ),
        'month_events': EventDateFacet(
            field='dates.start_date',
            interval='month',
            min_doc_count=1
        ),
    }

    def _filter(self, search):
        # override methods to add custom pieces
        pass

    def _search(self):
        ' Override search to add your own filters '
        s = super(CollectionSearch, self).search()
        return s

    def query(self, search, query):
        """
        Add query part to ``search``.
        """
        limit_date_from = query.get('limit_date_from', None)
        limit_date_to = query.get('limit_date_to', None)
        events = query.get('events', [])
        places = query.get('places', [])
        exclude = query.get('exclude_places', [])
        exclude = Q('terms', **{'place.place_id': exclude})
        places = Q('terms', **{'place.place_id': places})
        events = Q('terms', id=events)

        search = search.query('term', deleted=False)
        search = search.filter(
            'range',
            **{'schedules.end_date': {'gte': 'now/d'}}
        )
        if limit_date_from:
            search = search.filter(
                'range',
                **{'dates.start_date': {'gte': limit_date_from}}
            )
        if limit_date_to:
            search = search.filter(
                'range',
                **{'dates.start_date': {'lte': limit_date_to}}
            )
        search = search.exclude(exclude).query(places | events)
        return search


class CategorySearch(CollectionSearch):
    def query(self, search, query):
        """
        Add query part to ``search``.
        """
        search = search.query('term', deleted=False)
        search = search.filter(
            'range',
            **{'dates.start_date': {'gte': 'now/d'}}
        )
        q = query.get('q', None)
        start_date = query.get('start_date', [])
        end_date = query.get('end_date', [])
        categories = query.get('categories', [])
        bounds = query.get('bounds', {})

        radius = query.get('radius', 0)
        lat = query.get('lat', 0)
        lng = query.get('lng', 0)

        if q:
            search = search.query(
                'multi_match',
                query=q,
                type='most_fields',
                minimum_should_match='75%',
                operator='and',
                tie_breaker=0.8,
                fields=['title_localized.*^3', 'title^4', 'description',
                        'place.city', 'place.title^2', 'place.address']
            )
        if start_date and end_date:
            search = search.query(
                'range',
                **{'dates.start_date': {
                    'gte': '{start_date}||/d'.format(**query),
                    'lte': '{start_date}||/d'.format(**query)
                }}
            )

        elif start_date:
            search = search.sort('-rank', 'dates.start_date')
            search = search.query(
                'range',
                **{'dates.start_date': {
                    'gte': '{start_date}||/d'.format(**query),
                    'lte': '{start_date}||/d'.format(**query)
                }}
            )
        elif end_date:
            search = search.sort('-rank', 'dates.start_date')
            search = search.query(
                'range',
                **{'dates.start_date': {
                    'gte': '{start_date}||/d'.format(**query),
                    'lte': '{start_date}||/d'.format(**query)
                }}
            )

        if bounds:
            search = search.query(
                'geo_bounding_box',
                place__geometry=bounds
            )

        if radius and lat and lng:
            search = search.query('bool', filter=Q(
                'geo_distance',
                distance='%sm' % radius,
                **{'place.geometry': {
                    'lat': lat, 'lon': lng
                }}
            ))

        if 'UNKNOWN' in categories:
            search = search.exclude('exists', field='category.title')
        elif categories:
            for category in categories:
                slugs = Q('term', category__slug__raw=category)
                titles = Q('term', category__title__raw=category)
                search = search.query(slugs | titles)
        return search
