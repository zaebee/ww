# -*- coding: utf-8 -*-

from django.http import Http404

from elasticsearch import NotFoundError
from elasticsearch_dsl import Q

from .models import (
    Event, Collection, EventPlace,
    EventVote, CityPlace, EventCategory
)
from .mixins import ElasticsearchMixin


class EventIndexManager(ElasticsearchMixin):

    index = 'event-index'
    doc_type = 'events'
    model = Event

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def get_or_None(self, id, **kwargs):
        try:
            return self.model.get(id, **kwargs)
        except NotFoundError:
            event = self.filter(id=id)
            if len(event):
                return event[0]
            return None

    def get(self, provider_id=None, slug=None, **kwargs):
        search = self.model.search()
        if slug:
            search = search.query('term', **{'slug.raw': slug})
        search = search.query('term', provider_id=provider_id)[:1]
        search = search.exclude('term', deleted=True)
        result = search.execute()
        if result:
            event = result[0]
            return event
        else:
            raise Http404

    def similar(self, **kwargs):
        search = self.search(observable=True)
        if 'exclude_id' in kwargs:
            exclude = kwargs.pop('exclude_id')
            search = search.exclude('term', id=exclude)
        if 'size' in kwargs:
            size = kwargs.pop('size')
            search = search.extra(size=size)
        if '_source' in kwargs:
            _source = kwargs.pop('_source')
            search = search.source(_source)
        search = search.query('terms', **kwargs)
        result = search.execute()
        return result

    def filter(self, **kwargs):
        search = self.model.search()
        if '_source' in kwargs:
            _source = kwargs.pop('_source')
            search = search.source(_source)
        search = search.exclude('term', deleted=True)
        search = search.query('match', **kwargs)
        limit = search.count()
        result = search[:limit].execute()
        return result

    def search(self, limit=200, **kwargs):
        """
        Build ES query fillter by `kwargs` params.
        kwargs = {
            'q': 'query string',
            'start_date': 'now'
            'end_date': 'now+7d',
            'radius': 0,
            'lat': 0.0,
            'lng': 0.0,
            'actual': False,
            ....
        }
        """
        search = self.model.search()
        search = search.exclude('term', deleted=True)

        observable = kwargs.get('observable', False)
        show_actual = kwargs.get('actual', False)
        start_date = kwargs.get('start_date', None)
        end_date = kwargs.get('end_date', None)

        limit_from = kwargs.get('limit_from', 0)
        limit_to = kwargs.get('limit_to', 18)

        if not start_date:
            search = search.query(
                'range',
                **{'schedules.end_date': {
                    'lte': 'now+1y/d',
                    'gte': 'now/d'
                }}
            )

        if 'exclude_events' in kwargs and kwargs['exclude_events']:
            search = search.exclude(
                'terms',
                id=kwargs['exclude_events'],
            )

        if 'provider' in kwargs and kwargs['provider']:
            search = search.query(
                'term',
                provider=kwargs['provider'],
            )

        if 'q' in kwargs and kwargs['q']:
            search = search.query(
                'multi_match',
                query=kwargs['q'],
                type='most_fields',
                minimum_should_match='75%',
                operator='and',
                tie_breaker=0.8,
                fields=['title_localized.*^3', 'title^4', 'description',
                        'place.city', 'place.title^2', 'place.address']
            )

        if start_date and end_date:
            search = search.query(
                'range',
                **{'schedules.start_date': {
                    'gte': '{start_date}||/d'.format(**kwargs),
                    'lte': '{end_date}||/d'.format(**kwargs)
                }}
            )

        elif start_date:
            search = search.sort('-rank', 'dates.start_date')
            search = search.query(
                'range',
                **{'schedules.start_date': {
                    'gte': '{start_date}||/d'.format(**kwargs),
                    'lte': '{start_date}||/d'.format(**kwargs)
                }}
            )
        elif end_date:
            search = search.sort('-rank', 'dates.start_date')
            search = search.query(
                'range',
                **{'schedules.start_date': {
                    'gte': '{end_date}||/d'.format(**kwargs),
                    'lte': '{end_date}||/d'.format(**kwargs)
                }}
            )

        if 'radius' in kwargs and 'lat' in kwargs and 'lng' in kwargs:
            search = search.query('bool', filter=Q(
                'geo_distance',
                distance='{radius}m'.format(**kwargs),
                **{'place.geometry': {
                    'lat': kwargs['lat'], 'lon': kwargs['lng']
                }}
            ))

        if show_actual:
            limit_from = 0
            limit_to = 6
            search = search.query(
                'range',
                **{'dates.start_date': {
                    'gte': 'now/d',
                    'lte': 'now+14d/d'
                }}
            )

        search = search.sort('dates.start_date', '-rank')
        search = search[limit_from:limit_to]
        return search if observable else search.execute()

    def aggregate(self, **kwargs):
        """
        Return buckets by `day` for collecion_id.
        """
        size = 20
        if 'id' in kwargs:
            size = len(kwargs['id'])
        search = self.model.search()
        search = search.filter('range',
                               **{'dates.start_date': {'gte': 'now/d'}})
        search = search.filter('terms', **kwargs)
        search.aggs.bucket(
            'day_events',
            'date_histogram',
            field='dates.start_date',
            interval='day',
            min_doc_count=1,
        ).metric('events', 'top_hits',
                 size=size, sort={'rank': {'order': 'desc'}})
        result = search.execute()
        return result

    def aggregate_places(self, **kwargs):
        """
        Return buckets by `place location`.
        """
        size = 20
        past_events = None
        search = self.model.search()
        search = search.extra(size=0)  # return only aggs result
        search = search.filter(
            'range',
            dates__start_date={'gte': 'now/d'}
        )
        if 'geo_bounding_box' in kwargs:
            box = kwargs.get('geo_bounding_box', {})
            if box:
                search = search.filter(
                    'geo_bounding_box',
                    **box
                )
                search.aggs.bucket(
                    'categories',
                    'terms',
                    size=size,
                    field='category.title.raw'
                )
        if 'place_id' in kwargs:
            search = search.filter(
                'term',
                place__place_id=kwargs['place_id']
            )
            search.aggs.bucket(
                'list',
                'date_histogram',
                field='dates.start_date',
                interval='month',
                min_doc_count=1,
            ).metric(
                'events',
                'top_hits',
                size=size,
                sort='dates.start_date'
            )
            past_search = search.query(
                'range',
                **{'schedules.start_date': {
                    'lte': 'now-1d/d'
                }}
            )
            past_search.aggs.bucket(
                'past',
                'date_histogram',
                field='schedules.start_date',
                interval='month',
                min_doc_count=1,
            ).metric(
                'events',
                'top_hits',
                size=size,
                sort='schedules.start_date'
            )
            past_events = past_search.execute()
        if 'geometry' in kwargs and kwargs['geometry']:
            search.aggs.bucket(
                'near',
                'geo_distance',
                field='place.geometry',
                origin=kwargs['geometry'],
                distance_type='plane',
                ranges=[
                    {'to': 100},  # city center 100m
                    {'from': 100, 'to': 500},  # from 100m to 500m
                    {'from': 500, 'to': 100000}  # from 500m to 100km
                ],
            ).metric(
                'events',
                'top_hits',
                size=size,
                sort='dates.start_date'
            ).pipeline(
                'categories',
                'terms',
                size=size,
                field='category.title.raw'
            )
        result = search.execute()
        if past_events:
            result.aggregations.past = past_events.aggregations.past
        return result

    def aggregate_cities(self, **kwargs):
        """
        Return buckets by `city location`.
        """
        size = 20
        search = self.model.search()
        if 'size' in kwargs:
            size = kwargs['size']
        search = search.filter('range',
                               **{'dates.start_date': {'gte': 'now/d'}})
        if 'geometry' in kwargs:
            search = search.filter('geo_distance',
                                   distance='80km',
                                   **{'place.geometry': kwargs['geometry']})
        search.aggs.bucket(
            'places', 'terms', field='place.title.raw', size=size
        ).metric(
            'events', 'top_hits', _source=['place', 'image'], size=3
        ).pipeline('centroid', 'geo_centroid', field='place.geometry')
        result = search.execute()
        return result.aggregations.places.buckets

    def aggregate_in_city(self, **kwargs):
        """
        Return aggr by city name.
        """
        size = 20
        search = self.model.search()
        search = search.extra(size=0)  # return only aggs result
        if 'city_names' in kwargs:
            should_q = []
            for cn in kwargs['city_names']:
                should_q.append(Q('match', place__city=cn))

            q = Q('bool',
                  should=should_q,
                  minimum_should_match=1
                  )
            search = search.query(q)

            search.aggs.bucket(
                'list',
                'date_histogram',
                field='dates.start_date',
                interval='month',
                min_doc_count=1,
            ).metric('events', 'top_hits',
                     size=size, sort='dates.start_date')
        result = search.execute()
        return result

    def get_by_author_votes(self, author_id):
        search = EventVote.search()
        search = search.query('term', **{'author.id': author_id})
        # TODO get only 10 user votes
        users_votes = search.execute()
        event_ids = [uv.event_id for uv in users_votes]
        if event_ids:
            result = Event.mget(event_ids, missing='skip', raise_on_error=False)
        else:
            result = None
        return result


class CollectionIndexManager(ElasticsearchMixin):

    index = 'event-index'
    doc_type = 'collections'
    model = Collection

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def get(self, **kwargs):
        search = self.model.search()
        search = search.exclude('term', deleted=True)
        search = search.query('term', **kwargs)[:1]
        result = search.execute()
        if result:
            return result[0]
        else:
            raise Http404

    def public(self, **kwargs):
        search = self.model.search()
        search = search.exclude('term', deleted=True)
        search = search.query('term', private=False)
        search = search.query('range', **{
            'events_count': {
                'gt': 0
            }
        })
        return search

    def search(self, **kwargs):
        search = self.model.search()
        limit = kwargs.get('limit', None)
        if not limit:
            limit = search.count()
        search = search.exclude('term', deleted=True)
        if 'q' in kwargs and kwargs['q']:
            search = search.query(
                'multi_match',
                query=kwargs['q'],
                fields=['title', 'description']
            )

        search = search.query('range', **{'events': {'gte': 1}})
        search = search.query('term', private=False)
        search = search.sort('-date_updated')
        result = search[:limit].execute()
        return result

    def filter(self, only_public=True, **kwargs):
        search = self.model.search()
        search = search.exclude('term', deleted=True)
        if '_source' in kwargs:
            _source = kwargs.pop('_source')
            search = search.source(_source)
        if only_public:
            # exclude private collections
            search = search.exclude('term', private=True)
        if kwargs:
            search = search.query('terms', **kwargs)
        search = search.sort('-date_updated')
        limit = search.count()
        result = search[:limit].execute()
        return result

    def get_with_specified_events(self, event_ids):
        size = 20
        search = self.model.search()
        search = search.exclude('term', deleted=True)
        search = search.extra(size=0)  # return only aggs result

        should_q = []
        for ei in event_ids:
            should_q.append(Q('match', events=ei))

        q = Q('bool',
              should=should_q,
              minimum_should_match=1)
        search = search.query(q)
        result = search[:size].execute()
        return result


class PlaceIndexManager(ElasticsearchMixin):

    index = 'place-index'
    doc_type = 'places'
    model = EventPlace

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def get(self, slug=None, provider_id=None, **kwargs):
        """
        Return event place instance.
        """
        search = self.model.search()
        search = search.exclude('term', deleted=True)
        if slug:
            search = search.query('term', **{'slug.raw': slug})
        if provider_id:
            search = search.query('term', provider_id=provider_id)
        result = search.execute()
        if result.hits.total:
            return result[0]
        else:
            raise Http404

    def search(self, limit=200, **kwargs):
        """
        Build ES query fillter by `kwargs` params.
        kwargs = {
            'q': 'query string',
            'limit_from': 0,
            'limit_to': 18,
            ....
        }
        """
        search = self.model.search()
        search = search.exclude('term', deleted=True)

        limit_from = kwargs.get('limit_from', 0)
        limit_to = kwargs.get('limit_to', 18)

        if 'q' in kwargs and kwargs['q']:
            search = search.query(
                'multi_match',
                query=kwargs['q'],
                type='most_fields',
                minimum_should_match='75%',
                operator='and',
                tie_breaker=0.8,
                fields=['title_localized.*^3', 'title^4', 'description',
                        'city', 'title^2', 'address']
            )
        search = search.sort('-date_added')
        search = search[limit_from:limit_to]
        return search.execute()

    def all(self, **kwargs):
        size = kwargs.get('size', 200)
        search = self.model.search()
        return search[:size].execute()

    def aggregate_cities(self, **kwargs):
        """
        Return buckets by `city location`.
        """
        size = 20
        search = self.model.search()
        observable = kwargs.get('observable', False)
        if 'size' in kwargs:
            size = kwargs['size']
        if 'id' in kwargs and isinstance(kwargs['id'], list):
            size = len(kwargs['id'])
            search = search.filter('terms', **kwargs)
        search = search.filter('bool', filter=Q(
            'geo_distance',
            distance='20km',
            **{'geometry': kwargs['geometry']}
        ))
        search = search.sort('date_added')[:size]
        return search if observable else search.execute()


class CityIndexManager(ElasticsearchMixin):

    index = 'place-index'
    doc_type = 'cities'
    model = CityPlace

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def get(self, provider_id=None, **kwargs):
        """
        Return event place instance.
        """
        search = self.model.search()
        search = search.query('term', provider_id=provider_id)[:1]
        search = search.exclude('term', deleted=True)
        result = search.execute()
        if result:
            return result[0]
        else:
            raise Http404

    def get_by_slug(self, slug=None, **kwargs):
        """
        Return event place instance by slug.
        """
        search = self.model.search()
        search = search.query('term', slug=slug)[:1]
        result = search.execute()
        if result:
            return result[0]
        else:
            raise Http404

    def all(self, **kwargs):
        size = kwargs.get('size', 200)
        search = self.model.search()
        return search[:size].execute()


class VoteIndexManager(ElasticsearchMixin):

    index = 'event-index'
    doc_type = 'votes'
    model = EventVote

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def get(self, event_id, user_id):
        """
        Return rating for `user_id` if user already voted.
        """
        search = self.model.search()
        search = search.filter('term', event_id=event_id)
        search = search.filter('match', **{'author.id': user_id})[:1]
        result = search.execute()
        if result:
            return result[0]
        else:
            return None

    def aggregate(self, event_id):
        """
        Return votes for event
        `{
            average_rate: {'value': 3.4},
            'votes':{'count': 4 },
            'votes': {user},{user}
        }`
        """
        search = self.model.search()
        search = search.filter('term', event_id=event_id)
        search.aggs.metric('average_rate', 'avg', field='rating')
        # TODO check if user already voted instead of get all voted_users
        # size = search.count()
        result = search[:1000].execute()
        result.aggregations.voted_users = result.hits
        result.aggregations.votes = {
            'count': result.hits.total
        }
        result.aggregations.average_rate.value = round(
            result.aggregations.average_rate.value or 0, 2)
        return result.aggregations


class CategoryIndexManager(ElasticsearchMixin):

    index = 'event-index'
    doc_type = 'categories'
    model = EventCategory

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def get(self, category_id):
        """
        Return category detail.
        """
        search = self.model.search()
        search = search.filter('term', id=category_id)
        result = search.execute()
        if result:
            return result[0]
        else:
            return None

    def aggregate(self, category_id):
        """
        Return events for category
        `{
            average_rate: {'value': 3.4},
            'votes':{'count': 4 },
            'votes': {user},{user}
        }`
        """
        search = self.model.search()
        return search
