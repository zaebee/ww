# -*- coding: utf-8 -*-

from datetime import datetime
from django.utils import timezone
from django.conf import settings

from constance import config

from elasticsearch_dsl.connections import connections

from elasticsearch_dsl import (
    DocType, Date, Nested, Boolean, Object,
    Long, Integer, String, GeoPoint, Float,
    InnerObjectWrapper, Completion, Keyword, Text
)

from hitcount.models import PushNotification
from services import translator

# configure default ES connection
connections.configure(default=settings.ELASTICSEARCH_CONNECTION_PARAMS)


class Schedule(InnerObjectWrapper):
    @property
    def is_actual(self):
        return self.age.days >= 0

    @property
    def age(self):
        if self.start_date.tzinfo:
            now = datetime.now(timezone.utc)
        else:
            now = datetime.now()
        return now - self.start_date


class Place(InnerObjectWrapper):
    def get_language(self, lang_code):
        return getattr(self, lang_code, None)


class Counter(InnerObjectWrapper):
    @property
    def total(self):
        as_dict = self.to_dict()
        return sum(as_dict.values())


class Locales(InnerObjectWrapper):
    def get_language(self, lang_code):
        return getattr(self, lang_code, None)


class TranslateStatus(InnerObjectWrapper):
    @property
    def status(self):
        as_dict = self.to_dict()
        return as_dict.values()


class Event(DocType):
    title = Text(multi=True, fields={
        'en': String(analyzer='english'),
        'ru': String(analyzer='russian'),
        # 'raw': String(analyzer='trigrams'),
        'raw': Keyword(),
        },
        analyzer='russian')
    title_localized = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    title_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    title_suggest = Completion()
    description = Text(analyzer='russian')
    description_localized = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    slug = Text(fields={'raw': Keyword()})
    provider = Text()
    provider_id = Text()
    ticket_price = Text()
    ticket_url = Text()
    video_url = Text()
    deleted = Boolean()
    image = Text()
    rank = Float()
    format_rank = String()
    average_rate = Float()
    category = Object(
        properties={
            'title': Text(fields={'raw': Keyword()}, analyzer='russian'),
            'slug': Text(fields={'raw': Keyword()})
        }
    )
    place = Object(
        doc_class=Place,
        properties={
            'place_id': Text(fields={'raw': Keyword()}),
            'provider_id': Text(fields={'raw': Keyword()}),
            'slug': String(),
            'title': Text(fields={'raw': Keyword()}, analyzer='russian'),
            'title_localized': Object(
                doc_class=Locales,
                properties={
                    'raw': String(analyzer='trigrams'),
                    'en': String(analyzer='english'),
                    'ru': String(analyzer='russian'),
                    'de': String(analyzer='dutch'),
                    'fr': String(analyzer='french'),
                    'es': String(analyzer='spanish'),
                }
            ),
            'address': Text(fields={'raw': Keyword()}, analyzer='russian'),
            'address_localized': Object(
                doc_class=Locales,
                properties={
                    'raw': String(analyzer='trigrams'),
                    'en': String(analyzer='english'),
                    'ru': String(analyzer='russian'),
                    'de': String(analyzer='dutch'),
                    'fr': String(analyzer='french'),
                    'es': String(analyzer='spanish'),
                }
            ),
            'city': Text(fields={'raw': Keyword()}, analyzer='russian'),
            'description': Text(),
            'description_localized': Object(
                doc_class=Locales,
                properties={
                    'raw': String(analyzer='trigrams'),
                    'en': String(analyzer='english'),
                    'ru': String(analyzer='russian'),
                    'de': String(analyzer='dutch'),
                    'fr': String(analyzer='french'),
                    'es': String(analyzer='spanish'),
                }
            ),
            'lat': Float(),
            'lng': Float(),
            'geometry': GeoPoint(),
            'email': Text(),
            'website': Text(),
            'phone': Text()
        }
    )
    images = Nested(
        properties={
            'image': Text(),
        }
    )
    preview_images = Nested(
        properties={
            'image': Text(),
            'retina': Text(),
        }
    )
    similar = String()
    dates = Object(
        doc_class=Schedule,
        properties={
            'start_date': Date(
                format="YYYY-MM-dd||"
                "YYYY-MM-dd'T'HH:mm:ss"
            ),
            'end_date': Date(
                format="YYYY-MM-dd||"
                "YYYY-MM-dd'T'HH:mm:ss"
            )
        }
    )
    schedules = Object(
        doc_class=Schedule,
        properties={
            'start_date': Date(
                format="YYYY-MM-dd||"
                "YYYY-MM-dd'T'HH:mm:ss.SSS||"
                "YYYY-MM-dd'T'HH:mm:ss||"
                "YYYY-MM-dd'T'HH:mm:ssZ||"
                "dd.MM.YYYY'T'HH:mm:ss"
            ),
            'end_date': Date(
                format="YYYY-MM-dd||"
                "YYYY-MM-dd'T'HH:mm:ss.SSS||"
                "YYYY-MM-dd'T'HH:mm:ss||"
                "YYYY-MM-dd'T'HH:mm:ssZ||"
                "dd.MM.YYYY'T'HH:mm:ss"
            )
        }
    )
    date_added = Date()
    date_updated = Date()
    counters = Object(
        doc_class=Counter,
        properties={
            'favorites_count': Integer(),
            'attending_count': Integer(),
            'interested_count': Integer(),
            'maybe_count': Integer(),
        }
    )
    source_url = String()
    min_price = Integer()
    max_price = Integer()
    is_free = Boolean()
    currency = String()
    formatted_price = String()

    class Meta:
        doc_type = 'events'
        index = 'event-index'

    @property
    def get_translate_fields(self):
        return {
            'title': self.title or '',
            'description': self.description or '',
            'place.title': self.place.title if self.place else '',
            'place.address': self.place.address if self.place else '',
            'place.description': self.place.description if self.place else '',
        }

    def translate(self, from_lang='auto', to_lang='en'):
        """
        Auto translate event fields
            `title`, `description`,
            `place.title`, `place.address`, `place.description`
        """
        source_dict = self.get_translate_fields
        return translator.translate_event(self.provider,
                                          self.provider_id,
                                          source_dict,
                                          from_lang,
                                          to_lang)

    def save_rank(self, **kwargs):
        """
        Calculate event rank by formula
        B = V + R*kR + L*kL
        """
        from app.models import EventCollection, EventVote
        from hitcount.models import HitCount

        rating_multiplier = config.RATING_MULTI
        collections_multiplier = config.COLLECTION_MULTI

        if self.provider == 'ebr':
            provider_multiplier = config.EBR_MULTI
        elif self.provider == 'kdg':
            provider_multiplier = config.KDG_MULTI
        elif self.provider == 'fb':
            provider_multiplier = config.FB_MULTI
        elif self.provider == 'clt':
            provider_multiplier = config.CLT_MULTI
        elif self.provider == 'vk':
            provider_multiplier = config.VK_MULTI
        elif self.provider == 'tpad':
            provider_multiplier = config.TPAD_MULTI
        elif self.provider == 'ww':
            provider_multiplier = config.WW_MULTI
        else:
            provider_multiplier = 1

        hit = HitCount.get_for_object(self)
        vote = EventVote.index.aggregate(self.meta.id)
        vote_users_count = vote.votes.count
        rating = (self.average_rate or 0) * vote_users_count

        collections = EventCollection.index.filter(
            events=[self.meta.id], _source=False)

        views = hit.hits + self.counters.total * provider_multiplier

        # how to calculate rank
        # (hits + provider_multi * counters) +
        # (rating_muti * rating) +
        # (collection_multi * collection_count)
        self.format_rank = str(
            '(%s + %s * %s) + '
            '(%s * %s) + '
            '(%s * %s)') % (
            hit.hits, provider_multiplier, self.counters.total,
            rating_multiplier, rating,
            collections_multiplier, collections.hits.total
        )
        # TODO add fields to culture import data
        self.views = views
        self.likes = vote_users_count
        self.date_updated = datetime.now()
        self.rank = (
            views +
            rating_multiplier * rating +
            collections_multiplier * collections.hits.total)
        return round(self.rank, 1)

    def save(self, **kwargs):
        if not self.date_added:
            self.date_added = datetime.now()
        return super(Event, self).save(**kwargs)


class Collection(DocType):
    title = Text(multi=True, fields={
        'en': String(analyzer='english'),
        'ru': String(analyzer='russian'),
        # 'raw': String(analyzer='trigrams'),
        'raw': Keyword(),
        },
        analyzer='russian')
    h1_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    title_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    title_localized = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    title_suggest = Completion()
    translate_status = Object(
        doc_class=TranslateStatus,
        properties={
            'en': String(),
            'ru': String(),
            'de': String(),
            'fr': String(),
            'es': String(),
        }
    )
    slug = String()
    provider_id = Text(fields={'raw': Keyword()})
    description = Text(analyzer='russian')
    description_localized = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    public = Boolean()
    deleted = Boolean()
    events = String()
    subscribers = String()
    subscribers_count = Integer()
    events_count = Long()
    places = String()
    exclude_places = String()
    get_preview = Nested(
        properties={
            'image': Text(),
        }
    )
    date_added = Date()
    date_updated = Date()
    last_push_date = Date()

    application_id = Keyword()
    source_url = String()
    limit_dates = Boolean()
    limit_date_from = Date()
    limit_date_to = Date()

    def add_event(self, event_id):
        if self.events:
            self.events.append(event_id)
        else:
            self.events = [event_id]
        self.events_count = len(self.events)
        self.date_updated = datetime.now()

    def remove_event(self, event_id):
        if self.events and event_id in self.events:
            self.events.remove(event_id)
            self.events_count = len(self.events)
            self.date_updated = datetime.now()

    def add_subscriber(self, user_id):
        if self.subscribers:
            self.subscribers.append(user_id)
        else:
            self.subscribers = [user_id]
        self.subscribers_count = len(self.subscribers)

    def remove_subscriber(self, user_id):
        if self.subscribers and user_id in self.subscribers:
            self.subscribers.remove(user_id)
            self.subscribers_count = len(self.subscribers)

    def add_place(self, place_id):
        if self.places:
            self.places.append(place_id)
        else:
            self.places = [place_id]
        self.date_updated = datetime.now()

    def remove_place(self, place_id):
        if self.places and place_id in self.places:
            self.places.remove(place_id)
        self.date_updated = datetime.now()

    def exclude_place(self, place_id):
        if self.exclude_places:
            self.exclude_places.append(place_id)
        else:
            self.exclude_places = [place_id]
        self.date_updated = datetime.now()

    def translate(self, from_lang='auto', to_lang='en'):
        """
        Auto translate events in collection.
        """
        from services.tasks import translate_collection
        task = translate_collection.s(self.id, from_lang, to_lang)
        return task.delay()

    def save_push(self, content_object, object_id):
        application_id = self.application_id
        push = PushNotification(
            application_id=application_id or self.meta.id,
            content_object=content_object,
            object_id=object_id
        )
        push.save()

    def get_latest_ids_started_from(self, date, **kwargs):
        """
        Return ids of latest collection pushes
        :param date: from what date get will get pushes
        :param kwargs:
        :return:
        """
        pushes = PushNotification.get_for_application(
                self.application_id or self.meta.id,
                last_push_date=date,
                **kwargs
            )
        pushes = pushes[:pushes.count()].execute()
        return [push.object_id for push in pushes
                if push.content_object == 'event']

    def push_notification(self, action, **kwargs):
        """
        Push to ios/android new events or places in collection.
        """
        events = kwargs.get('events', [])

        if events:
            event_ids = events
        else:
            event_ids = self.get_latest_ids_started_from(
                self.last_push_date, **kwargs
            )

        text = 'Добавлено %s новых событий' % len(event_ids)
        if event_ids:
            events = Event.mget(event_ids)
            if len(events) == 1:
                event = events[0]
                text = 'Добавлено новое событие: %s' % event.title
        kwargs = {
            'count': len(event_ids),
            'title': self.title,
            'link': event_ids[:10]
        }
        return {
            'text': text,
            'params': kwargs
        }

    class Meta:
        doc_type = 'collections'
        index = 'event-index'

    def save(self, **kwargs):
        update_date = kwargs.pop('update_date', False)
        if update_date:
            self.date_updated = datetime.now()
        if not self.date_added:
            self.date_added = datetime.now()
        return super(Collection, self).save(**kwargs)


class EventCategory(DocType):
    date_added = Date()
    date_updated = Date()
    parent_id = Text(fields={'raw': Keyword()})
    category_id = Text(fields={'raw': Keyword()})

    slug = String(fields={'raw': Keyword()})
    title = Text(fields={'raw': Keyword()}, analyzer='russian')
    title_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    image = Text()
    h1_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    description_short = Text(analyzer='russian')
    description_short_localized = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    description_full = Text(analyzer='russian')
    description_full_localized = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )

    class Meta:
        doc_type = 'categories'
        index = 'event-index'

    def save(self, **kwargs):
        if not self.date_added:
            self.date_added = datetime.now()
        return super(EventCategory, self).save(**kwargs)


class EventPlace(DocType):
    place_id = Text(fields={'raw': Keyword()})
    provider_id = Text(fields={'raw': Keyword()})
    provider = Text()
    date_added = Date()
    slug = String(fields={'raw': Keyword()})
    title = Text(fields={'raw': Keyword()}, analyzer='russian')
    title_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    h1_seo = Object(
        doc_class=Locales,
        properties={
            'raw': String(analyzer='trigrams'),
            'en': String(analyzer='english'),
            'ru': String(analyzer='russian'),
            'de': String(analyzer='dutch'),
            'fr': String(analyzer='french'),
            'es': String(analyzer='spanish'),
        }
    )
    address = Text(fields={'raw': Keyword()}, analyzer='russian')
    city = Text(fields={'raw': Keyword()}, analyzer='russian')
    description = Text()
    lat = Float()
    lng = Float()
    geometry = GeoPoint()
    email = Text()
    website = Text()
    phone = Text()

    class Meta:
        doc_type = 'places'
        index = 'place-index'

    def save(self, **kwargs):
        if not self.date_added:
            self.date_added = datetime.now()
        return super(EventPlace, self).save(**kwargs)


class CityPlace(DocType):
    date_added = Date()
    slug = String()
    provider_id = Text(fields={'raw': Keyword()})
    title = Text(fields={'raw': Keyword()}, analyzer='russian')
    description = Text()
    alt_names = Object(
        properties={
            'title': Keyword(),
        })
    lat = Float()
    lng = Float()
    geometry = GeoPoint()

    class Meta:
        doc_type = 'cities'
        index = 'place-index'

    def save(self, **kwargs):
        if not self.date_added:
            self.date_added = datetime.now()
        return super(CityPlace, self).save(**kwargs)


class EventVote(DocType):
    event_id = String()
    rating = Float()
    author = Object(
        properties={
            'id': Long(),
            'username': String(),
        }
    )
    date_added = Date()

    class Meta:
        doc_type = 'votes'
        index = 'event-index'

    def save(self, **kwargs):
        if not self.date_added:
            self.date_added = datetime.now()
        return super(EventVote, self).save(**kwargs)
