# -*- coding: utf-8 -*-

import time
import logging

from django.conf import settings

from services.base import ApiBase

from . import validators
from . import schema
from . observers import FacebookObservable

APP_TOKEN = getattr(settings, 'FACEBOOK_APP_TOKEN', '')

# TODO get user ACCESS_TOKEN
USER_TOKEN = getattr(settings, 'FACEBOOK_USER_TOKEN', '')
API_URL = 'https://graph.facebook.com/v2.9'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiWrapper(ApiBase):
    """
    Facebook API wrapper.
    Implement proxy for `event_search` and `get_event` methods.
    """

    def __init__(self, session=None, token=None, user_token=None, **kwargs):
        super(ApiWrapper, self).__init__(session, token)
        self.provider = 'fb'
        self.event_serializer = schema.EventSerializer
        self.place_serializer = schema.PlaceSerializer
        self.app_token = token or APP_TOKEN
        self.user_token = user_token or USER_TOKEN

    def get_request(self, url):
        """
        Build request facebook graph place url.
        """
        time_start = time.time()
        return self.api.get(
            url,
            timeout=self.timeout,
            callback=self._search_callback(
                self.provider, time_start, type='event'
            )
        )

    def search_events(self, **params):
        """
        Facebook graph /search/ `type=event`.
        Params `q` or `city`  is required.
        """
        time_start = time.time()
        url = '%s/search/' % API_URL
        city = params.get('city', '')
        query = params.get('q', '')
        if query:
            query = '|'.join(query.split(' '))
            if city:
                query = '%s|%s' % ('|'.join(city), query)
            params['q'] = query
        params['type'] = 'event'
        params['limit'] = self.page_size
        params['fields'] = str(
            'category,cover,start_time,end_time,event_times,ticket_uri,'
            'timezone,attending_count,interested_count,maybe_count,'
            'place{name,phone,emails,website,description,location,link},'
            'description,id,owner,name,photos{source}'
        )
        # TODO do not call api if has no `user_token`
        params['access_token'] = self.user_token
        params = validators.validate_input(**params)
        response = self.api.get(
            url,
            timeout=self.timeout, params=params,
            callback=self._search_callback(self.provider, time_start, **params)
        )
        return response

    def search_places(self, **params):
        """
        Facebook graph /search/ `type=place`.
        Params `radius` or `lat,lng`  is required.
        """
        time_start = time.time()
        url = '%s/search/' % API_URL
        query = params.get('q', '')
        if query:
            params['q'] = '|'.join(query.split(' '))
        params['type'] = 'place'
        params['limit'] = self.page_size
        params['fields'] = str(
            'name,phone,emails,website,description,location,link,'
            'events{place,category,cover,start_time,end_time,ticket_uri,event_times,'
            'attending_count,interested_count,maybe_count,'
            'timezone,description,id,owner,name,photos{source}}'
        )
        params['access_token'] = self.app_token
        params = validators.validate_input(**params)
        response = self.api.get(
            url,
            timeout=self.timeout, params=params,
            callback=self._search_callback(self.provider, time_start, **params)
        )
        return response

    def get_places_request(self, **params):
        """
        Build request Facebook graph /search/ with `type=place`.
        Use `after` param for next page request.
        """
        def request(after=None, action='search', params=params):
            time_start = time.time()
            action = params.get('action', action)

            url = '%s/%s/' % (API_URL, action)
            params['type'] = 'place'
            params['limit'] = self.page_size
            params['fields'] = str(
                'name,phone,emails,website,description,location,link,'
                'events{place,category,cover,start_time,end_time,ticket_uri,event_times,'
                'attending_count,interested_count,maybe_count,'
                'timezone,description,id,owner,name,photos{source}}'
            )
            if after:
                params['after'] = after
            params['access_token'] = self.app_token
            params = validators.validate_input(**params)
            response = self.api.get(
                url,
                timeout=self.timeout, params=params,
                callback=self._search_callback(
                    self.provider,
                    time_start,
                    **params
                )
            )
            return response
        return request

    def events_source(self, events):
        request = self.get_request
        source = FacebookObservable.events_from_place(events, request)
        return source

    def observe(self, *args, **kwargs):
        """
        """
        request = self.get_places_request(**kwargs)

        places = FacebookObservable.from_places(request).publish()

        events = places.map(lambda x: x.get('events', {}))
        actual_events = events.flat_map(
            self.events_source
        ).map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        places.connect()
        return actual_events

    def observe_places(self, *args, **kwargs):
        """
        places = kwargs.get('places', [])
        """
        # get place ids
        # places = ['fb_619726138154445', 'fb_619726138154445', 'fb_619726138154445']
        places = kwargs.get('places', [])
        requests = [self.get_places_request(action=place) for place in places]

        places = FacebookObservable.merge([
            FacebookObservable.from_places(request)
            for request in requests
        ]).publish()

        events = places.map(lambda x: x.get('events', {}))
        actual_events = events.flat_map(
            self.events_source
        ).map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        places.connect()
        return actual_events
