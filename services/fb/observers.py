# -*- coding: utf-8 -*-

import logging
from gevent.pool import Pool
from requests import HTTPError

from rx import Observable, AnonymousObservable

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

pool = Pool(5)


def retry_request(request):
    retry_count = 3
    while not request.response and retry_count:
        retry_count -= 1
        request.send()
    return request


class FacebookObservable(Observable):

    @classmethod
    def from_places(cls, get_places_request):

        def subscribe(observer):

            def callback(_request, after):
                response = _request.response
                # timeout error
                try:
                    # check status is not 200 OK
                    if response:
                        response.raise_for_status()
                except HTTPError as e:
                    observer.on_error(e)

                if response:
                    data = response.json()

                    places = data.get('data', [data])
                    for place in places:
                        observer.on_next(place)

                    paging = data.get('paging', {})
                    if 'cursors' in paging and 'after' in paging['cursors']:
                        after = paging['cursors']['after']
                        logger.debug("Request page: %s" % after)
                        action(after)
                    else:
                        observer.on_completed()
                else:
                    observer.on_error('No response')

            def action(after=None):
                _request = get_places_request(after)
                pool.apply_async(
                    retry_request,
                    args=(_request,),
                    callback=lambda x: callback(x, after))

            action()

        return AnonymousObservable(subscribe)

    @classmethod
    def events_from_place(cls, events, get_place_request):

        def subscribe(observer):

            def callback(_request):
                response = _request.response
                # timeout error
                try:
                    # check status is not 200 OK
                    if response:
                        response.raise_for_status()
                except HTTPError as e:
                    observer.on_error(e)

                if response:
                    _events = response.json()
                    action(_events)
                else:
                    observer.on_completed()

            def action(_events):
                data = _events.get('data', [])
                for event in data:
                    observer.on_next(event)

                paging = _events.get('paging', {})
                if 'next' in paging and len(data):
                    next = paging['next']
                    _request = get_place_request(next)
                    pool.apply_async(
                        retry_request,
                        args=(_request,),
                        callback=lambda x: callback(x))
                else:
                    observer.on_completed()

            action(events)

        return AnonymousObservable(subscribe)
