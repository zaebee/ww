# -*- coding: utf-8 -*-

from uuid import uuid4
from pytils.translit import slugify

from services.schema import BasePlaceSerializer, BaseEventSerializer
from dateutil.parser import parse
import datetime


class PlaceSerializer(BasePlaceSerializer):
    provider = 'fb'

    def get_slug(self, obj):
        title = obj.get('name', None)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def get_title(self, obj):
        return obj.get('name', '')

    def get_address(self, obj):
        try:
            return obj['location']['street']
        except KeyError:
            return None

    def get_city(self, obj):
        try:
            return obj['location']['city']
        except KeyError:
            return None

    def get_lat(self, obj):
        lat = 0
        try:
            lat = obj['location']['latitude']
        except KeyError:
            lat = 0
        return lat

    def get_lng(self, obj):
        lng = 0
        try:
            lng = obj['location']['longitude']
        except KeyError:
            lng = 0
        return lng

    def get_geometry(self, obj):
        return {
            'lat': self.get_lat(obj),
            'lon': self.get_lng(obj),
        }

    def get_email(self, obj):
        emails = obj.get('emails', [])
        return ', '.join(emails)

    def get_website(self, obj):
        website = obj.get('website', None)
        return website or obj.get('link', None)

    def get_phone(self, obj):
        return obj.get('phone', None)


class EventSerializer(BaseEventSerializer):
    provider = 'fb'
    place = PlaceSerializer(required=False, allow_null=True)

    def get_provider_id(self, obj):
        return obj.get('id', 0)

    def get_title(self, obj):
        return obj.get('name', '')

    def get_description(self, obj):
        return obj.get('description', '')

    def get_schedules(self, obj):

        event_times = obj.get('event_times', None)

        if event_times:
            date = datetime.datetime.now().date()
            return sorted([
                dict(
                    start_date=event['start_time'],
                    end_date=event['end_time']
                )
                for event in event_times
                if parse(event['start_time']).date() >= date
            ], key=lambda k: k['start_date'])

        start = obj.get('start_time', None)
        end = obj.get('end_time', None)
        if start or end:
            return [{
                'start_date': start,
                'end_date': end,
            }]

    def get_slug(self, obj):
        title = obj.get('name', None)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def _get_category(self, obj):
        category = obj.get('category', None)
        if category:
            return {
                'title': category
            }

    def get_images(self, obj):
        result = []
        try:
            images = obj['photos']['data']
            for image in images:
                picture = {
                    'image': image['source']
                }
                result.append(picture)
        except KeyError:
            pass
        return result

    def get_image(self, obj):
        cover = obj.get('cover', None)
        if cover:
            return cover.get('source', None)

    def get_author(self, obj):
        owner = obj.get('owner', None)
        if owner:
            return {
                'id': owner.get('id', None),
                'username': owner.get('name', None)
            }

    def get_counters(self, obj):
        attending_count = obj.get('attending_count', 0)
        interested_count = obj.get('interested_count', 0)
        return {
            'attending_count': attending_count,
            'interested_count': interested_count,
        }

    def get_collections(self, obj):
        pass

    def get_ticket_price(self, obj):
        pass

    def get_ticket_url(self, obj):
        return obj.get('ticket_uri', None)

    def get_video_url(self, obj):
        pass

    def get_source_url(self, obj):
        provider_id = self.get_provider_id(obj)
        return 'https://fb.com/%s' % provider_id

    def get_min_price(self, obj):
        pass

    def get_max_price(self, obj):
        pass

    def get_is_free(self, obj):
        pass

    def get_currency(self, obj):
        pass

    def get_formatted_price(self, obj):
        # TODO set `price is not specified` for all fb events
        pass
