# -*- coding: utf-8 -*-

from cerberus import Validator

def get_center(doc):
    lat = doc.get('lat', 0)
    lng = doc.get('lng', 0)
    if lat and lng:
        try:
            lat = float(lat)
            lng = float(lng)
            return '%s,%s' % (round(lat, 1), round(lng, 1))
        except ValueError:
            return ''
    else:
        return ''

def validate_input(**kwargs):
    """
    Convert our request search params to Facebook search params.
    """
    schema = {
        'q': {
            'type': 'string',
        },
        'limit': {
            'type': 'integer',
            'coerce': int
        },
        'after': {
            'type': 'string',
        },
        'type': {
            'type': 'string',
        },
        'fields': {
            'type': 'string',
        },
        'access_token': {
            'type': 'string',
        },
        'radius': {
            'rename': 'distance',
        },
        'distance': {
            'type': 'integer',
        },
        'lat': {
            'type': 'float',
            'coerce': float
        },
        'lng': {
            'type': 'float',
            'coerce': float
        },
        'center': {
            'type': 'string',
            'default_setter': get_center
        },
    }
    validator = Validator(schema, purge_unknown=True)
    if validator.validate(kwargs):
        params = validator.normalized(kwargs)
        return params
    else:
        ## TODO raise validation error
        return validator.errors
