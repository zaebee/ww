import time
import hashlib
import random

import gevent
import requests

from requests import ConnectTimeout, ReadTimeout
from gevent.pool import Pool

from io import BytesIO
from django.conf import settings

import PIL
from PIL import Image

from django.core.files.base import ContentFile
from storages.backends.s3boto3 import S3Boto3Storage

pool = Pool(10)


def save_to_s3(filename, image):

    image_path = '%s/%s' % (settings.PREVIEW_IMAGES_DIR, filename)
    content = ContentFile(image)
    storage = S3Boto3Storage()
    storage.save(image_path, content)
    new_url = '%s/%s/%s' % (settings.PREVIEW_URL_DOMAIN,
                            settings.AWS_STORAGE_BUCKET_NAME,
                            image_path)
    return new_url


def download_crop_image(event_id, url):

    def _convert_image(img, maxsize):
        img_byte_arr = BytesIO()
        if img.size[0] > maxsize[0]:
            wpercent = (img.size[0] / float(maxsize[0]))
            hsize = int((float(img.size[1]) / float(wpercent)))
            img = img.resize((maxsize[0], hsize), PIL.Image.ANTIALIAS)

        if img.mode == "CMYK":
            img = img.convert("RGB")

        img.save(img_byte_arr, format='PNG', optimize=True)
        return img_byte_arr

    is_downloaded = False
    try:
        response = requests.get(url, timeout=5)
        file = BytesIO(response.content)
        try:
            img = Image.open(file)
            time.sleep(0.3)
            is_downloaded = True
        except OSError:
            is_downloaded = False
    except (ConnectTimeout, ReadTimeout):
        # try dl with proxy
        proxy_list = settings.PROXY_LIST[:]
        while not is_downloaded and len(proxy_list) > 0:
            choised_proxy = random.choice(proxy_list)
            try:
                response = requests.get(url,
                                        proxies={'http': choised_proxy},
                                        timeout=4)
                file = BytesIO(response.content)
            except (ConnectTimeout, ReadTimeout):
                proxy_list.remove(choised_proxy)
            else:
                img = Image.open(file)
                is_downloaded = True

    if is_downloaded:
        image_hash = hashlib.md5(url.encode()).hexdigest()

        img_byte_arr = _convert_image(img, (976, 384))
        filename = '%s_2x.png' % image_hash
        img_url_retina = save_to_s3(filename, img_byte_arr.getvalue())

        img_byte_arr = _convert_image(img, (488, 192))
        filename = '%s.png' % image_hash
        img_url = save_to_s3(filename, img_byte_arr.getvalue())
    else:
        img_url = None
        img_url_retina = None
    return {
        'image': img_url,
        'retina': img_url_retina
    }


def crop_title_images(event):
    """
    """
    images = event.images or []
    urls = [image['image'] for image in images]

    for url in urls:
        yield pool.apply_async(download_crop_image, args=(event.id, url))


def make_preview_events(events):
    cropped_images = {}
    for event in events:
        if not event.preview_images:
            cropped_images = crop_title_images(event)
            provider, provider_id = event.id.split('_')
            yield {
                'provider': provider,
                'provider_id': provider_id,
                'preview_images': [item.get() for item in cropped_images]
            }
