# -*- coding: utf-8 -*-

import time
import logging

from services.base import ApiBase

from . import validators
from . import schema
from . observers import KudagoObservable

API_URL = 'https://kudago.com/public-api/v1.3'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiWrapper(ApiBase):
    """
    Kudago API wrapper.
    Implement proxy for `search_events`, `list_events`,
    `get_event`, `search_places` methods.
    """

    def __init__(self, session=None, token=None):
        super(ApiWrapper, self).__init__(session, token)
        self.provider = 'kdg'
        self.event_serializer = schema.EventSerializer
        self.place_serializer = schema.PlaceSerializer

    def get_request(self, url):
        """
        Build request kudago url.
        """
        time_start = time.time()
        return self.api.get(
            url,
            timeout=self.timeout,
            callback=self._search_callback(self.provider, time_start)
        )

    def search_events(self, **params):
        """
        Kudago api search method. params `q` is required.
        """
        time_start = time.time()
        url = '%s/search/' % API_URL
        params = validators.validate_input(**params)
        params['page_size'] = self.page_size
        params['ctype'] = 'event'
        # TODO why kdg so slow if sort by dates
        # params['order_by'] = '-dates'
        params['fields'] = str('id,dates,title,slug,place,description,'
                               'location,images,categories,price,images,'
                               'favorites_count,site_url,item_url,is_free')
        params['expand'] = 'images,location,place,dates,categories'
        request = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
            callback=self._search_callback(self.provider, time_start)
        )
        return request

    def list_events(self, **params):
        """
        Kudago api events list.
        Params `date_start/date_end` or `radius/lat/lng` is required.
        """
        return self.get_events(**params)

    def get_events(self, **params):
        """
        Kudago api events list. Get by `ids`.
        """
        time_start = time.time()
        url = '%s/events/' % API_URL
        params['page_size'] = self.page_size
        # params['order_by'] = '-dates'
        params = validators.validate_input(**params)
        params['expand'] = 'images,location,place,dates,categories'
        params['fields'] = str('id,dates,title,sluglace,description,'
                               'location,images,categoriesrice,images,'
                               'favorites_count,site_url,item_url,is_free')
        request = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
            callback=self._search_callback(self.provider, time_start)
        )
        return request

    def search_places(self, **params):
        """
        Kudago api places list.
        """
        time_start = time.time()
        url = '%s/places/' % API_URL
        params = validators.validate_input(**params)
        params['page_size'] = self.page_size
        params['expand'] = 'images'
        params['fields'] = str('id,title,short_title,slug,address,'
                               'location,timetable,phone,is_stub,'
                               'images,description,body_text,site_url,'
                               'foreign_url,coords,favorites_count,'
                               'comments_count,is_closed,categories,tags')
        request = self.api.get(
            url,
            timeout=self.timeout, params=params,
            callback=self._search_callback(self.provider, time_start)
        )
        return request

    def get_events_request(self, **params):
        """
        """
        def request(page=1, params=params):
            params['page'] = page
            return self.search_events(**params)
        return request

    def get_places_request(self, **params):
        """
        """
        # get events for event `ids` list
        ids = params.get('ids', None)
        if ids:
            params['ids'] = ','.join([str(i) for i in ids])

        # get events only for `place_id`
        place_id = params.pop('places', None)
        if place_id:
            params['place_id'] = place_id

        def request(page=1, params=params):
            params['page'] = page
            return self.get_events(**params)
        return request

    def observe(self, *args, **kwargs):
        """
        """
        if any(kwargs.values()):
            request = self.get_events_request(**kwargs)
            events = KudagoObservable.from_provider(request).publish()
        else:
            events = KudagoObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events

    def observe_places(self, *args, **kwargs):
        """
        """
        # get place ids
        # places = []
        places = kwargs.get('places', [])
        requests = [self.get_places_request(places=place) for place in places]

        if any(kwargs.values()):
            events = KudagoObservable.merge([
                KudagoObservable.from_provider(request)
                for request in requests
            ]).publish()
        else:
            events = KudagoObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events
