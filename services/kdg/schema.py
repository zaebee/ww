# -*- coding: utf-8 -*-

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from pytils.translit import slugify
from datetime import datetime
from services.schema import BasePlaceSerializer, BaseEventSerializer


class PlaceSerializer(BasePlaceSerializer):
    provider = 'kdg'

    def get_slug(self, obj):
        return obj.get('slug', '')

    def get_title(self, obj):
        return obj.get('title', '')

    def get_address(self, obj):
        return obj.get('address', '')

    def get_city(self, obj):
        return obj.get('location', '')

    def get_lat(self, obj):
        lat = 0
        coords = obj.get('coords', None)
        if coords and isinstance(coords, dict):
            lat = float(coords.get('lat', 0))
        return lat

    def get_lng(self, obj):
        lng = 0
        coords = obj.get('coords', None)
        if coords and isinstance(coords, dict):
            lng = float(coords.get('lon', 0))
        return lng

    def get_geometry(self, obj):
        coords = obj.get('coords', None)
        if coords and isinstance(coords, dict):
            return {
                'lat': self.get_lat(obj),
                'lon': self.get_lng(obj)
            }

    def get_email(self, obj):
        return obj.get('email', None)

    def get_website(self, obj):
        return obj.get('site_url', None)

    def get_phone(self, obj):
        return obj.get('phone', None)


class EventSerializer(BaseEventSerializer):
    provider = 'kdg'
    place = PlaceSerializer(required=False, allow_null=True)

    def get_provider_id(self, obj):
        return obj.get('id', 0)

    def get_title(self, obj):
        return obj.get('title', '')

    def get_description(self, obj):
        return obj.get('description', '')

    def get_slug(self, obj):
        slug = obj.get('slug', '')
        if not slug:
            url = obj.get('item_url', '')
            path = urlparse(url).path
            slug = path.strip('/').split('/').pop()
        if not slug:
            slug = slugify(self.get_title(obj))
        return slug

    def _get_category(self, obj):
        categories = obj.get('categories', None)
        if categories and isinstance(categories, list):
            return {
                'title': categories[0]
            }

    def get_images(self, obj):
        result = []
        images = obj.get('images', None)
        image = obj.get('first_image', None)
        if image:
            result.append({
                'image': image.get('image', '')
            })
        if images and isinstance(images, list):
            result += [dict(image=image.get('image', '')) for image in images]
        return result

    def get_image(self, obj):
        image = obj.get('first_image', None)
        images = obj.get('images', None)
        if image and isinstance(image, dict):
            image = image.get('thumbnails', None)
            if image and isinstance(image, dict):
                image = image.get('640x384', '')
        elif images and isinstance(images, list):
            image = images[0]
            image = image.get('thumbnails', None)
            if image and isinstance(image, dict):
                image = image.get('640x384', '')
        return image

    def get_schedules(self, obj):
        dates = obj.get('dates', None)
        daterange = obj.get('daterange', None)
        if daterange and isinstance(daterange, dict):
            start = daterange.get('start', None)
            end = daterange.get('end', None)
            try:
                if start:
                    start = datetime.fromtimestamp(start).isoformat()
                if end:
                    end = datetime.fromtimestamp(end).isoformat()
                return [{
                    'start_date': start,
                    'end_date': end,
                }]
            except:
                pass
        if dates and isinstance(dates, list):
            for date in dates:
                try:
                    date['start_date'] = datetime.fromtimestamp(
                        date['start']).isoformat()
                    date['end_date'] = datetime.fromtimestamp(
                        date['end']).isoformat()
                except:
                    pass
            return dates

    def get_counters(self, obj):
        favorites_count = obj.get('favorites_count', 0)
        return {
            'favorites_count': favorites_count
        }

    def get_author(self, obj):
        pass

    def get_collections(self, obj):
        pass

    def get_ticket_price(self, obj):
        return obj.get('price', None)

    def get_ticket_url(self, obj):
        pass

    def get_video_url(self, obj):
        pass

    def get_source_url(self, obj):
        url = obj.get('item_url', None)
        if not url:
            url = obj.get('site_url', None)
        return url

    def get_min_price(self, obj):
        # TODO parse price field
        pass

    def get_max_price(self, obj):
        # TODO parse price field
        pass

    def get_is_free(self, obj):
        return obj.get('is_free', False)

    def get_currency(self, obj):
        return None
        return obj.get('currency', 'RUB')

    def get_formatted_price(self, obj):
        return self.get_ticket_price(obj)
