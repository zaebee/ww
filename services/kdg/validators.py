# -*- coding: utf-8 -*-

from cerberus import Validator


def validate_input(**kwargs):
    """
    Convert our request search params to Kudago search params.
    """
    schema = {
        'q': {
            'type': 'string',
        },
        'page': {
            'type': 'integer',
            'coerce': int
        },
        'ids': {
            'type': 'string',
        },
        'page_size': {
            'type': 'integer',
            'coerce': int
        },
        'fields': {
            'type': 'string',
        },
        'expand': {
            'type': 'string'
        },
        'order_by': {
            'type': 'string'
        },
        'ctype': {
            'type': 'string'
        },
        'radius': {
            'type': 'integer',
            'coerce': int
        },
        'lat': {
            'type': 'float',
            'coerce': float,
        },
        'lng': {
            'rename': 'lon',
        },
        'lon': {
            'type': 'float',
            'coerce': float,
        },
        'start_date': {
            'type': 'string',
            'rename': 'actual_since',
        },
        'end_date': {
            'type': 'string',
            'rename': 'actual_until',
        },
        'actual_since': {
            'type': 'string',
        },
        'actual_until': {
            'type': 'string',
        },
        'place_id': {
            'type': 'string',
        },
    }
    validator = Validator(schema, purge_unknown=True)
    if validator.validate(kwargs):
        params = validator.normalized(kwargs)
        return params
    else:
        ## TODO raise validation error
        return validator.errors
