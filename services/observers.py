# -*- coding: utf-8 -*-

import logging
from gevent.pool import Pool

from rx import Observer

from . elastic.mixins import ElasticsearchMixin

logger = logging.getLogger('observers')
logger.setLevel(logging.DEBUG)


class EventObserver(Observer, ElasticsearchMixin):

    index = 'event-index'
    doc_type = 'events'

    def __init__(self, *args, **kwargs):
        self.pool = Pool(1)
        self.index_name = kwargs.pop('index', '')
        super(EventObserver, self).__init__(*args, **kwargs)

    @classmethod
    def get_index_name(cls):
        return cls.index

    @classmethod
    def get_type_name(cls):
        return cls.doc_type

    @classmethod
    def get_document(cls, obj):
        return obj

    def on_next(self, data):
        self.pool.apply_async(
           self.bulk_index,
           kwds={
               'refresh': False,
               'queryset': data,  # event or place
               'index_name': self.index_name
           }
        )
        msg = 'Indexed events: %s' % len(data)
        logger.debug(msg)

    def on_error(self, e):
        msg = 'Catch {exception}'.format(
            exception=str(e),
        )
        logger.debug(msg)

    def on_completed(self):
        msg = 'Sequence events completed'
        logger.debug(msg)


class PlaceObserver(EventObserver):

    index = 'place-index'
    doc_type = 'places'

    def on_next(self, data):
        self.pool.apply_async(
           self.bulk_index,
           kwds={
               'refresh': False,
               'queryset': data,  # event or place
               'index_name': self.index_name
           }
        )
        msg = 'Indexed places: %s' % len(data)
        logger.debug(msg)

    def on_completed(self):
        msg = 'Sequence places completed'
        logger.debug(msg)
