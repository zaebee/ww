# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import time
from celery.utils.log import get_task_logger
from celery import shared_task, group

from split import chop

from . api import ApiWrapper, ApiRequestFactory, api_providers
from . observers import EventObserver, PlaceObserver

logger = get_task_logger(__name__)


@shared_task
def download_events(**params):
    """
    Fetch all events from facebook provider.
    Used `params` for build query url.
    params = {
        'city': 'city_name',
        'q': 'query_name',
        'lat': 0.0,
        'lng': 0.0,
        'radius': 80000,
    }
    """
    children = []
    if 'has_next' in params and params['has_next'] is True:
        fb = download_events_fb.s(**params)
        children.append(fb)

        kdg = download_events_kdg.s(**params)
        children.append(kdg)

        ebr = download_events_ebr.s(**params)
        children.append(ebr)

        clt = download_events_clt.s(**params)
        children.append(clt)

        tpad = download_events_tpad.s(**params)
        children.append(tpad)
    chain = group(children)
    return chain()


@shared_task
def observe_events(**params):
    """
    Observable events from api providers.
    """
    from app.models import Event, EventPlace

    events = Event.index
    events_observer = EventObserver()
    places_observer = PlaceObserver()

    api = ApiRequestFactory(**params)
    for provider_prefix, provider_api in api_providers.items():
        # TODO enable/disable provider used
        # provider_search = Event.index.search(
        # 'term',
        # observable=True,
        # provider=provider_prefix)
        api.set(provider_api)
    events_stream = api.observable_events()

    places_stream = events_stream.map(
        lambda x: x.get('place', False)
    ).publish()
    places_stream.buffer_with_count(
        50  # bulk_index by 50 places
    ).subscribe(
        places_observer
    )

    events_stream.buffer_with_count(
        100  # bulk_index by 100 events
    ).subscribe(
        events_observer
    )

    places_stream.connect()
    return events.search(
        observable=True,
        actual=True,
        **params
    ).execute().to_dict()


@shared_task
def download_events_fb(**params):
    """
    Fetch all events from facebook provider.
    Used `params` for build query url.
    params = {
        'city': 'city_name',
        'q': 'query_name',
        'lat': 0.0,
        'lng': 0.0,
        'radius': 80000,
    }
    """
    api = ApiWrapper(providers=['fb'], **params)
    result = api.get_next()
    next_data = result.get('next', None)
    next_urls = next_data.get('fb_next_urls', [])
    has_next = next_data.get('has_next', False)
    # TODO set `has_next` instead `after`
    while has_next and (next_data.get('after', None) or next_urls):
        total = result.get('total', 0)
        result = api.get_next()
        result['total'] += total
        next_data = result.get('next', None)
        next_urls = next_data.get('fb_next_urls', [])
        has_next = next_data.get('has_next', False)
    return result


@shared_task
def download_events_ebr(**params):
    """
    Fetch all events from eventbrite.com provider.
    Used `params` for build query url.
    params = {
        'q': 'query_name',
        'lat': 0.0,
        'lng': 0.0,
        'radius': 80000,
    }
    """
    api = ApiWrapper(providers=['ebr'], **params)
    result = api.get_next()
    next_data = result.get('next', None)
    next_page = next_data.get('page', 0)
    has_next = next_data.get('has_next', False)
    # TODO download all pages instead of top 10 pages
    while has_next and next_page < 10:
        total = result.get('total', 0)
        result = api.get_next()
        result['total'] += total
        next_data = result.get('next', None)
        next_page = next_data.get('page', next_page)
        has_next = next_data.get('has_next', False)
    return result


@shared_task
def download_events_kdg(**params):
    """
    Fetch all events from kudago.com provider.
    Used `params` for build query url.
    params = {
        'q': 'query_name',
        'lat': 0.0,
        'lng': 0.0,
        'radius': 80000,
    }
    """
    api = ApiWrapper(providers=['kdg'], **params)
    result = api.get_next()
    next_data = result.get('next', None)
    next_page = next_data.get('page', 0)
    has_next = next_data.get('has_next', False)
    # TODO download all pages instead of top 10 pages
    while has_next and next_page < 10:
        total = result.get('total', 0)
        result = api.get_next()
        result['total'] += total
        next_data = result.get('next', None)
        next_page = next_data.get('page', next_page)
        has_next = next_data.get('has_next', False)
    return result


@shared_task
def download_events_clt(**params):
    """
    Fetch all events from culture.ru provider.
    Used `params` for build query url.
    params = {
        'q': 'query_name',
        'cities': 'москва',
    }
    """
    api = ApiWrapper(providers=['clt'], **params)
    result = api.get_next()
    next_data = result.get('next', None)
    next_page = next_data.get('page', 0)
    has_next = next_data.get('has_next', False)
    # TODO download all pages instead of top 10 pages
    while has_next and next_page < 10:
        total = result.get('total', 0)
        result = api.get_next()
        result['total'] += total
        next_data = result.get('next', None)
        next_page = next_data.get('page', next_page)
        has_next = next_data.get('has_next', False)
    return result


@shared_task
def download_events_tpad(**params):
    """
    Fetch all events from timepad.ru provider.
    Used `params` for build query url.
    params = {
        'q': 'query_name',
        'lat': 0.0,
        'lng': 0.0,
        'radius': 80000,
    }
    """
    api = ApiWrapper(providers=['tpad'], **params)
    result = api.get_next()
    next_data = result.get('next', None)
    next_page = next_data.get('page', 0)
    has_next = next_data.get('has_next', False)
    # TODO download all pages instead of top 10 pages
    while has_next and next_page < 10:
        total = result.get('total', 0)
        result = api.get_next()
        result['total'] += total
        next_data = result.get('next', None)
        next_page = next_data.get('page', next_page)
        has_next = next_data.get('has_next', False)
    return result


@shared_task
def calculate_rank(provider=None, **kwargs):
    """
    Update `rank` for all events.
    """
    from app.models import Event

    only_new = kwargs.pop('only_new', False)
    events = Event.index.search(observable=True)
    if kwargs:
        events = Event.index.search(observable=True, **kwargs)
    if only_new:
        # recalculate only events without `rank`
        events = events.exclude('exists', field='rank')

    events = events.filter('range',
                           **{'schedules.end_date': {'gte': 'now/d'}})
    events = events.filter('range',
                           **{'schedules.start_date': {'lte': 'now+60d/d'}})
    if provider:
        events = events.query('term', provider=provider)

    events = events.sort('dates.start_date', '-rank')
    total = events.count()
    events = events.params(scroll='100m').scan()
    chunks = chop(200, events)
    for chunk in chunks:
        queryset = (dict(
            provider=event.provider,
            provider_id=event.provider_id,
            rank=event.save_rank(),
            format_rank=event.format_rank,
        ) for event in chunk)
        Event.index.bulk_index(queryset=queryset, refresh=False)
    return total


@shared_task
def translate_collection(collection_id, from_lang='auto', to_lang='en'):
    """
    Use `services.translator` to auto translate events.
    """
    from django.core.cache import cache
    from django.core.cache.utils import make_template_fragment_key

    from app.models import Event, EventCollection
    from services.translator import pool_translate

    collection = EventCollection.index.get(id=collection_id)
    ids = collection.events
    size = collection.events_count
    search = Event.index.model.search()
    # TODO use SearchCollection facet chunks instead of `ids`
    # or scan()
    events = search.query('terms', id=ids)[:size].execute()
    result = pool_translate(events, from_lang, to_lang)
    queryset = (r.get() for r in result)
    # chunk_size = 20
    chunks = chop(20, queryset)
    for chunk in chunks:
        try:
            Event.index.bulk_index(queryset=chunk, refresh=True)
        except:
            logger.error('Failed to bulk_index translated events chunk')
    status = {to_lang: 'SUCCESS'}
    fragment = 'api:collections.widget'
    cache_key = make_template_fragment_key(fragment, [collection.id])
    cache.delete(key=cache_key)
    collection.update(translate_status=status)
    return status


@shared_task
def push_notification(application_id, text, **kwargs):
    """
    Send notification from special `application_id` to android devices.
    """
    from push_notifications.models import GCMDevice
    if application_id and kwargs.get('count', None):
        devices = GCMDevice.objects.filter(
            cloud_message_type="FCM",
            application_id=application_id
        )
        notify = kwargs.get('notify', True)
        logger.info('Send notification with text: %s' % text)
        devices.send_message(
            text,
            use_fcm_notifications=notify,
            extra=kwargs
        )
        return {
            'text': text,
            'extra': kwargs
        }


@shared_task(rate_limit='10/m')
def make_preview_events_ids(events_ids):
    """
    Download provider images and upload into our S3 storage.
    """
    from app.models import Event
    from services.images import make_preview_events

    search = Event.index.model.search()
    search = search.exclude('exists', field='preview_images')
    search = search.filter(
        'range',
        **{'schedules.start_date': {'gte': 'now/d'}}
    )
    search = search.query(
        'terms', id=events_ids
    ).source(
        ['id', 'images', 'preview_images']
    )
    events_count = search.count()
    events = search[:events_count].execute()
    chunk_size = 10  # or 50 event per bulk
    chunks = chop(chunk_size, events)
    for chunk in chunks:
        queryset = make_preview_events(chunk)
        Event.index.bulk_index(queryset=queryset)
    return events_count


@shared_task
def reload_fb_avatars():
    """
    Reload fb avatars at large type
    """
    import requests

    from django.core.files.base import ContentFile
    from django.core.files.storage import default_storage

    from app.models import User

    params = {'height': 400}

    for user in User.objects.exclude(fb_profile__isnull=True):
        user_info = user.fb_profile
        try:
            avatar_url = 'https://graph.facebook.com/%s/picture' % (user_info.get('id'), )
            image_path = 'users/%s.jpg' % user_info.get('id', user.id)
            response = requests.get(avatar_url, params=params, stream=True)
            content = ContentFile(response.content)
            default_storage.save(image_path, content)
            user.headshot.delete_all_created_images()
            user.headshot.delete_sized_images()
            user.headshot = image_path
            user.save()
        except:
            logger.error('Failed to reload fb avatar')

        time.sleep(0.2)


@shared_task
def update_places(*args):
    """
    """
    from app.scripts import update_places
    update_places.run(*args)


@shared_task
def update_collection(collection_id):
    """
    """
    from app.scripts import update_collection
    update_collection.run(collection_id)
