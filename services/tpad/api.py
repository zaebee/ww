# -*- coding: utf-8 -*-

import time
import logging

from services.base import ApiBase

from . import validators
from . import schema
from . observers import TimepadObservable

API_URL = 'https://api.timepad.ru/v1'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiWrapper(ApiBase):
    """
    Timepad API wrapper.
    Implement proxy for `search_events`, `list_events` methods.
    """

    def __init__(self, session=None, token=None):
        super(ApiWrapper, self).__init__(session, token)
        self.provider = 'tpad'
        self.event_serializer = schema.EventSerializer
        self.place_serializer = schema.PlaceSerializer

    def search_events(self, **params):
        """
        Timepad api search method. params `q` is required.
        """
        time_start = time.time()
        url = '%s/events/' % API_URL
        page = params.get('page', 1)
        params = validators.validate_input(**params)
        params['limit'] = self.page_size
        params['fields'] = str(
            'tickets_limit,location,starts_at,ends_at,'
            'properties,organization,url,description_html,'
            'name,poster_image,description_short,'
            'registration_data,ticket_types,categories,'
            'moderation_status,created_at,access_status,locale,id'
        )
        if page > 1:
            params['skip'] = (page * self.page_size) - self.page_size
        response = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
            callback=self._search_callback(self.provider, time_start)
        )
        return response

    def get_events(self, **params):
        """
        Timepad api get events method. params `ids` is required.
        """
        ids = params.get('ids', None)
        if ids:
            params['event_ids'] = ','.join([str(i) for i in ids])
        return self.search_events(**params)

    def get_events_request(self, **params):
        """
        """
        ids = params.get('ids', None)
        if ids:
            params['event_ids'] = ','.join([str(i) for i in ids])

        def request(page=1, params=params):
            params['page'] = page
            return self.search_events(**params)
        return request

    def get_places_request(self, **params):
        """
        """
        places = params.get('places', None)
        if places:
            params['organization_ids'] = ','.join([str(i) for i in places])

        def request(page=1, params=params):
            params['page'] = page
            return self.search_events(**params)
        return request

    def observe(self, *args, **kwargs):
        """
        """
        if any(kwargs.values()):
            request = self.get_events_request(**kwargs)
            events = TimepadObservable.from_provider(request).publish()
        else:
            events = TimepadObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events

    def observe_places(self, *args, **kwargs):
        """
        """
        if any(kwargs.values()):
            request = self.get_places_request(**kwargs)
            events = TimepadObservable.from_provider(request).publish()
        else:
            events = TimepadObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events
