# -*- coding: utf-8 -*-

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from uuid import uuid4
from pytils.translit import slugify
from services.schema import BasePlaceSerializer, BaseEventSerializer


class PlaceSerializer(BasePlaceSerializer):
    provider = 'tpad'

    def get_place(self, obj):
        return obj.get('organization', {})

    def get_place_id(self, obj):
        place = self.get_place(obj)
        if place:
            return '%s_%s' % (self.provider, self.get_provider_id(obj))

    def get_provider_id(self, obj):
        place = self.get_place(obj)
        if place:
            return place.get('id', 0)

    def get_slug(self, obj):
        title = self.get_title(obj)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def get_title(self, obj):
        place = self.get_place(obj)
        if place:
            return place.get('name', '')

    def get_address(self, obj):
        location = obj.get('location', None)
        if location:
            return location.get('address', '')

    def get_city(self, obj):
        location = obj.get('location', None)
        if location:
            return location.get('city', '')

    def get_lat(self, obj):
        location = obj.get('location', None)
        lat = 0
        if location:
            coordinates = location.get('coordinates', None)
            if coordinates and len(coordinates) == 2:
                lat, lng = coordinates
                try:
                    lat = float(lat)
                except ValueError:
                    lat = 0
        return lat

    def get_lng(self, obj):
        location = obj.get('location', None)
        lng = 0
        if location:
            coordinates = location.get('coordinates', None)
            if coordinates and len(coordinates) == 2:
                lat, lng = coordinates
                try:
                    lng = float(lng)
                except ValueError:
                    lng = 0
        return lng

    def get_geometry(self, obj):
        return {
            'lat': self.get_lat(obj),
            'lon': self.get_lng(obj),
        }

    def get_email(self, obj):
        return obj.get('email', None)

    def get_website(self, obj):
        place = self.get_place(obj)
        if place:
            return place.get('url', None)

    def get_phone(self, obj):
        return obj.get('phone', None)


class EventSerializer(BaseEventSerializer):
    provider = 'tpad'
    place = PlaceSerializer(required=False, allow_null=True, source='*')

    def get_provider_id(self, obj):
        return obj.get('id', 0)

    def get_title(self, obj):
        return obj.get('name', '')

    def get_description(self, obj):
        html = obj.get('description_html', '')
        short = obj.get('description_short', '')
        return html or short

    def get_slug(self, obj):
        title = self.get_title(obj)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def _get_category(self, obj):
        categories = obj.get('categories', None)
        if categories and isinstance(categories, list):
            return categories

    def get_images(self, obj):
        thumbnail = obj.get('poster_image', None)
        if thumbnail:
            image_url = thumbnail['uploadcare_url']
            return [{
                'image': 'https://%s/' % image_url.strip('/')
            }]

    def get_image(self, obj):
        thumbnail = obj.get('poster_image', None)
        if thumbnail:
            image_url = thumbnail['uploadcare_url']
            return 'https://%s/' % image_url.strip('/')

    def get_schedules(self, obj):
        start = obj.get('starts_at', None)
        end = obj.get('ends_at', None)
        return [{
            'start_date': start,
            'end_date': end,
        }]

    def get_counters(self, obj):
        favorites_count = obj.get('views', 0)
        return {
            'favorites_count': favorites_count
        }

    def get_author(self, obj):
        pass

    def get_collections(self, obj):
        pass

    def get_video_url(self, obj):
        pass

    def get_source_url(self, obj):
        return obj.get('url', '')

    def get_ticket_url(self, obj):
        pass

    def get_ticket_price(self, obj):
        price = None
        min_price = self.get_min_price(obj)
        max_price = self.get_max_price(obj)
        if min_price and max_price and min_price != max_price:
            price = '%s - %s' % (min_price, max_price)
        elif min_price:
            price = min_price
        elif max_price:
            price = max_price
        return price

    def get_min_price(self, obj):
        data = obj.get('registration_data', {})
        return data.get('price_min', None)

    def get_max_price(self, obj):
        data = obj.get('registration_data', {})
        return data.get('price_max', None)

    def get_is_free(self, obj):
        min_price = self.get_min_price(obj)
        max_price = self.get_max_price(obj)
        if not min_price and not max_price:
            is_free = True
        else:
            is_free = False
        return is_free

    def get_currency(self, obj):
        return 'RUB'

    def get_formatted_price(self, obj):
        currency = self.get_currency(obj)
        price = self.get_ticket_price(obj)
        if price and currency:
            return '%s %s' % (price, currency)
