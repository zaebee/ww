# -*- coding: utf-8 -*-

from datetime import datetime
from cerberus import Validator


def get_center(doc):
    lat = doc.get('lat', 0)
    lng = doc.get('lng', 0)
    if lat and lng:
        try:
            lat = float(lat)
            lng = float(lng)
            return '%s,%s' % (round(lat, 1), round(lng, 1))
        except ValueError:
            return ''
    else:
        return ''


def validate_input(**kwargs):
    """
    Convert our request search params to Kudago search params.
    """
    schema = {
        'q': {
            'type': 'string',
            'rename': 'keywords',
        },
        'fields': {
            'type': 'string',
        },
        'organization_ids': {
            'type': 'string',
        },
        'event_ids': {
            'type': 'string',
        },
        'limit': {
            'type': 'integer',
            'coerce': int
        },
        'skip': {
            'type': 'integer',
            'coerce': int
        },
        'sort': {
            'type': 'string',
        },
        'cities': {
            'type': 'string',
        },
        'expand': {
            'type': 'string'
        },
        'order_by': {
            'type': 'string'
        },
        'order_direction': {
            'type': 'string'
        },
        'radius': {
            'type': 'integer',
            'coerce': int
        },
        'lat': {
            'type': 'float',
            'coerce': float,
        },
        'lng': {
            'type': 'float',
            'coerce': float,
        },
        'start_date': {
            'type': 'string',
            'rename': 'starts_at_min',
        },
        'end_date': {
            'type': 'string',
            'rename': 'starts_at_max',
        },
        'keywords': {
            'type': 'string',
        },
    }
    validator = Validator(schema, purge_unknown=True)
    if validator.validate(kwargs):
        params = validator.normalized(kwargs)
        return params
    else:
        # TODO raise validation error
        return validator.errors
