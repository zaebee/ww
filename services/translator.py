# -*- coding: utf-8 -*-

from gevent.pool import Pool

from textblob import TextBlob
from textblob.exceptions import NotTranslated

pool = Pool(10)


def translate_event(provider, provider_id, source_dict, from_lang, to_lang):
    """
    Uses TextBlob for translate event data.
    Returns translated event dict.
    """
    result = {}
    for key, value in source_dict.items():
        if value:
            try:
                translated = TextBlob(value).translate(from_lang=from_lang,
                                                       to=to_lang)
                source_dict[key] = translated.raw
            except NotTranslated:
                source_dict[key] = value

    result['title_localized'] = {
        to_lang: source_dict['title']
    }
    result['description_localized'] = {
        to_lang: source_dict['description']
    }
    result['place'] = {
        'title_localized': {
            to_lang: source_dict['place.title'],
        },
        'address_localized': {
            to_lang: source_dict['place.address'],
        },
        'description_localized': {
            to_lang: source_dict['place.description'],
        }
    }
    result['provider'] = provider
    result['provider_id'] = provider_id
    return result


def pool_translate(events, from_lang, to_lang):
    """
    Call async poll for `translate_event`.
    """
    for event in events:
        yield pool.apply_async(
            translate_event,
            args=(event.provider, event.provider_id,
                  event.get_translate_fields, from_lang, to_lang)
        )
