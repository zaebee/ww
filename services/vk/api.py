# -*- coding: utf-8 -*-

import time
import logging

from django.conf import settings

from services.base import ApiBase

from . import validators
from . import schema
from . observers import VkontakteObservable

# TODO get user ACCESS_TOKEN
APP_TOKEN = getattr(settings, 'VK_APP_TOKEN', '')
USER_TOKEN = getattr(settings, 'VK_USER_TOKEN', '8b202b52edf9076651aed3b832631dd39453ebb8331edb11411d78e1cb710eca8270fa4b68e3580f6c0d9')
API_URL = 'https://api.vk.com/method'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ApiWrapper(ApiBase):
    """
    VK API wrapper.
    Implement proxy for `groups.search`, `groups.getById` methods.
    """

    def __init__(self, session=None, token=None, user_token=None, **kwargs):
        super(ApiWrapper, self).__init__(session, token, **kwargs)
        self.provider = 'vk'
        self.page_size = 500  # limit group size
        self.user_token = user_token or USER_TOKEN
        self.event_serializer = schema.EventSerializer
        self.place_serializer = schema.PlaceSerializer

    def search_events(self, **params):
        """
        VK api search method. params `q` or `city_id` is required.
        """
        url = '%s/groups.search' % API_URL
        params = validators.validate_input(**params)
        params['access_token'] = self.user_token
        params['count'] = self.page_size
        params['type'] = 'event'
        params['future'] = 1
        params['v'] = 5.52
        request = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
        )
        request.send()
        data = request.response.json()
        group_ids = []
        if data.get('response', None):
            group_ids = [
                str(group['id']) for group
                in data['response']['items']
            ]
        return self.get_groups(group_ids, **params)

    def get_groups(self, group_ids=None, **params):
        """
        VK api groups.getById method. params `group_ids` is required.
        """
        time_start = time.time()
        url = '%s/groups.getById' % API_URL
        page = params.get('page', 1)
        group_ids = group_ids or []
        params['group_ids'] = ','.join(group_ids)
        params['access_token'] = self.user_token
        params['fields'] = str('city,country,place,description,wiki_page,'
                               'members_count,counters,start_date,finish_date,'
                               'can_post,can_see_all_posts,activity,status,'
                               'contacts,links,fixed_post,verified,site,'
                               'ban_info,cover')
        if page > 1:
            params['offset'] = (page * self.page_size) - self.page_size
        params['v'] = 5.52
        response = self.api.get(
            url,
            timeout=self.timeout,
            params=params,
            callback=self._search_callback('vk', time_start)
        )
        return response

    def get_events_request(self, **params):
        """
        """
        def request(page=1, params=params):
            params['page'] = page
            return self.search_events(**params)
        return request

    def get_places_request(self, **params):
        """
        """
        def request(page=1, params=params):
            params['page'] = page
            return self.get_places(**params)
        return request

    def observe(self, *args, **kwargs):
        """
        """
        if any(kwargs.values()):
            request = self.get_events_request(**kwargs)
            events = VkontakteObservable.from_provider(request).publish()
        else:
            events = VkontakteObservable.empty().publish()

        actual_events = events.map(
            self._serialize_event
        ).filter(
           self._is_actual_event
        )

        events.connect()
        return actual_events
