# -*- coding: utf-8 -*-

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from uuid import uuid4
from datetime import datetime
from pytils.translit import slugify
from services.schema import BasePlaceSerializer, BaseEventSerializer


class PlaceSerializer(BasePlaceSerializer):
    provider = 'vk'

    def get_place_id(self, obj):
        return '%s_%s' % (self.provider, self.get_provider_id(obj))

    def get_provider_id(self, obj):
        return obj.get('id', 0)

    def get_slug(self, obj):
        title = self.get_title(obj)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def get_title(self, obj):
        return obj.get('title', '')

    def get_address(self, obj):
        return obj.get('address', '')

    def get_city(self, obj):
        return obj.get('city', '')

    def get_lat(self, obj):
        lat = obj.get('latitude', None)
        if lat:
            return float(lat)

    def get_lng(self, obj):
        lng = obj.get('longitude', None)
        if lng:
            return float(lng)

    def get_geometry(self, obj):
        return {
            'lat': self.get_lat(obj),
            'lon': self.get_lng(obj),
        }

    def get_email(self, obj):
        return obj.get('email', None)

    def get_website(self, obj):
        return obj.get('site_url', None)

    def get_phone(self, obj):
        return obj.get('phone', None)


class EventSerializer(BaseEventSerializer):
    provider = 'vk'
    place = PlaceSerializer(required=False, allow_null=True)

    def get_provider_id(self, obj):
        return obj.get('id', 0)

    def get_title(self, obj):
        return obj.get('name', '')

    def get_description(self, obj):
        return obj.get('description', '')

    def get_slug(self, obj):
        title = self.get_title(obj)
        slug = slugify(title)
        if not slug:
            slug = slugify(uuid4())
        return slug

    def _get_category(self, obj):
        categories = obj.get('tags', None)
        if categories and isinstance(categories, list):
            return categories

    def get_images(self, obj):
        return [{
            'image': self.get_image(obj)
        }]

    def get_image(self, obj):
        thumbnail = obj.get('photo_200', None)
        if thumbnail:
            return thumbnail

    def get_schedules(self, obj):
        start = obj.get('start_date', None)
        end = obj.get('finish_date', None)
        if start and end:
            start = datetime.fromtimestamp(start).isoformat()
            end = datetime.fromtimestamp(end).isoformat()
            return [{
                'start_date': start,
                'end_date': end,
            }]

    def get_counters(self, obj):
        favorites_count = obj.get('members_count', 0)
        return {
            'favorites_count': favorites_count
        }

    def get_author(self, obj):
        contacts = obj.get('contacts', [])
        if contacts:
            return contacts[0]

    def get_collections(self, obj):
        pass

    def get_ticket_price(self, obj):
        return obj.get('status', None)

    def get_ticket_url(self, obj):
        pass

    def get_currency(self, obj):
        pass

    def get_video_url(self, obj):
        pass
