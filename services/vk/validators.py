# -*- coding: utf-8 -*-

from cerberus import Validator


def validate_input(**kwargs):
    """
    Convert our request search params to VK search params.
    """
    schema = {
        'access_token': {
            'type': 'string',
        },
        'q': {
            'type': 'string',
        },
        'page': {
            'type': 'integer',
            'coerce': int
        },
        'page_size': {
            'type': 'integer',
            'coerce': int
        },
        'fields': {
            'type': 'string',
        },
        'future': {
            'type': 'boolean'
        },
        'city_id': {
            'type': 'string'
        },
        'sort': {
            'type': 'integer',
            'coerce': int
        },
        'offset': {
            'type': 'integer',
            'coerce': int
        },
        'count': {
            'type': 'float',
            'coerce': float,
        },
    }
    validator = Validator(schema, purge_unknown=True)
    if validator.validate(kwargs):
        params = validator.normalized(kwargs)
        return params
    else:
        # TODO raise validation error
        return validator.errors
