var app = window.app || {};

(function (app) {
    var user = app.userProfile;
    var selectedIDList = [];

    app.AddToCollectionWidget = Ractive.extend({
        el: '#add-to-collection-widget-holder',
        append: true,
        template: "#add-to-collection-widget-template",

        data: function () {
            return {
                lang: app.lang,
                user: user,
                selectedIDList: selectedIDList,
                renderTarget: null
            };
        },

        adapt: [Ractive.adaptors.Backbone],

        oninit: function () {
            this.on({
                select: function (event, collection) {
                    event.original.preventDefault();
                    var selectedIDList = this.get('selectedIDList');
                    var selectedItemIndex = selectedIDList.indexOf(collection.id);

                    if (selectedItemIndex == -1) {
                        selectedIDList.push(collection.id);
                    } else {
                        selectedIDList.splice(selectedItemIndex, 1);
                    }

                    selectedItemIndex = selectedIDList.indexOf(collection.parent_id);

                    if (collection.parent_id && selectedItemIndex == -1) {
                        selectedIDList.push(collection.parent_id);
                    }

                    this.set('selectedIDList', selectedIDList);
                    $(this.el).find(".button.save").toggleClass("disabled", !this.get('selectedIDList').length);
                },

                save: function (event) {
                    var self = this;
                    event.original.preventDefault();
                    var eventModel = this.get('event');
                    eventModel.addToCollection({collections_id: this.get('selectedIDList')}).done(function (response) {
                        $('.counter-collection').html(response.text);
                        self.fire("onAddedToCollection");
                    });
                    this.toggleWidget("open", false);
                    this.unrender();
                },

                saveCategory: function (event) {
                    var self = this;
                    event.original.preventDefault();
                    var eventModel = this.get('event');
                    eventModel.addToCategory({categories_id: this.get('selectedIDList')}).done(function (response) {
                      console.log(response);
                      //$('.counter-collection').html(response.text);
                      //self.fire("onAddedToCollection");
                    });
                    this.toggleWidget("open", false);
                    this.unrender();
                },

                addNewCollection: function (event) {
                    event.original.preventDefault();
                    var createCollectionModal = app.createCollectionModal;
                    var self = this;
                    createCollectionModal.set({showModal: true});
                    createCollectionModal.once("collection_created", function (event) {
                        user.fetch();
                    });
                },

                cancel: function (event) {
                    var self = this;
                    var eventData = new app.Event({id: this.get("eventId")});
                    event.original.preventDefault();
                    eventData.fetch().done(function () {
                        var collections_id = _.map(self.get('event.collections'), 'id');
                        self.set('selectedIDList', collections_id);
                    });
                    this.toggleWidget("open", false);
                    this.unrender();
                }
            });

            this.observe('eventId', function (newValue, oldValue, keypath) {
                var self = this;
                if (newValue) {
                    var event = new app.Event({id: newValue});
                    this.set('event', event);
                    event.fetch().done(function () {
                        var collections_id = _.map(self.get('event.collections'), 'id');
                        self.set('selectedIDList', collections_id);
                        if (self.get('renderTarget')){
                            self.unrender();
                            self.render(self.get('renderTarget'));
                        }
                    });
                }
            });

        },

        onrender: function(event) {
            var self = this;
            this.setTooltips();
            user.on("change", function(e) {
                self.setTooltips();

            });
        },

        setTooltips: function() {
            var $longTextElList = $('#add-to-collection-widget [rel="tooltip-long-text"]');

            for(var j = 0, length = $longTextElList.length; j < length; j++) {
                $longTextElList.eq(j).data("tooltip", null);
            }

            for(var i = 0, length = $longTextElList.length; i < length; i++) {
                var $longTextEl = $longTextElList.eq(i);
                if($longTextEl[0].scrollWidth > $longTextEl.width()) {
                    $longTextEl.tooltip({placement: "top", container: "body", title: $longTextEl.text()});
                    $longTextEl.data("tooltip").options.tooltipTitle = $longTextEl.text();
                } else {
                    $longTextEl.tooltip("disable");
                    $longTextEl.data("tooltip", null);
                }
            }
        },

        toggleWidget: function (flag) {
            $(this.el).toggleClass("open", flag);
        }
    });

    app.AddToCategoryWidget = app.AddToCollectionWidget.extend({
        el: '#add-to-collection-widget-holder',
        append: true,
        template: "#add-to-category-widget-template",

        data: function () {
            return {
                lang: app.lang,
                user: user,
                selectedIDList: selectedIDList,
                categories: app.categories || [],
                renderTarget: null
            };
        },
    });

})(app);
