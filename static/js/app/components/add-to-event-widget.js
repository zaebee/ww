var app = window.app || {};

(function (app) {

  var user = app.userProfile;
  var collection = new app.EventCollection({id: window.COLLECTION_ID });

  app.AddToEventWidget = Ractive.extend({
    el: '#event-collection-form',
    template: '#add-to-event-widget-template',
    data: function (){
      return {
        _: _,
        lang: app.lang, // language gettext strings
        locale: app.USER.lang, // locale code: ru, en, ..
        collection: collection,
        showActual: true,
        errors: null,
        events: [],
        user: user,
        isPastBucket: function(date_key) {
          var now = moment();
          var date = moment(date_key);
          return date.isBefore(now, 'month');
        },
        isPastEvent: function(date_key) {
          var now = moment();
          return date_key.isBefore(now, 'day');
        },
        formatAverateRate: function(rate) {
          if (rate) {
            return rate.toFixed(1).replace('.', ',');
          }
        },
        formatFirstLetter: function (string) {
          return string.charAt(0).toUpperCase() + string.slice(1);
        },
        formatBucketTitle: function(key) {
            return moment(key).locale(this.get('locale')).format('MMMM YYYY');
        },
        formatMonthNum: function(date_key) {
          return moment(date_key).locale(this.get('locale')).format('MM');
        },
        formatSchedule: function(schedule) {
          return moment(schedule.start_date);
        },
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],
    oninit: function() {
      if (collection.id) {
        collection.events().done(function(response){
          collection.set(response);
          $('.events-buckets-initial').remove();
        });
      }
      var self = this;
    }
  });

  app.addToEventWidget = new app.AddToEventWidget();

  app.addToEventWidget.on({

    searchEvent: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var query = this.get('query');
      var self = this;
      self.set({
        loading: true,
        events: new app.Events()
      });
      var data = {
        q: query,
        exclude_events: self.get('collection.events'),
      };
      $.get('/search/', data).done(function(response) {
        self.set({
          loading: false,
          events: new app.Events(response.events),
        });
      });
      console.log('query', query);
    },

    addToCollection: function (context, index) {
      context.original.preventDefault();
      context.original.stopPropagation();
      var self = this;
      var selectedEvent = this.get('events.' + index);
      selectedEvent.set('processing', true);
      selectedEvent.addToCollection({
        collections_id: [this.get('collection.id')]
      }).done(function(response){
        collection.fetch();
        selectedEvent.set({
          processing: false,
          added: true,
        });
        self.fire("onEventAdded");
      });
    },

    removeFromCollection: function(e, eventID) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var node = $(e.node).find('span i');
      var self = this;

      var url = '/api/events/' + eventID + '/collections/' + this.get("collection").id + '/';
      node.addClass('button-spinner');

      $.ajax({
        url: url,
        type: 'DELETE',
        success: function(response) {
          collection.fetch({
            success: function() {
              node.removeClass('button-spinner');
            },
          });
          self.fire("onEventRemoved");
        }
      });
    },

    showMore: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var url = '/search/?q=' + this.get('query');
      window.open(url, '_blank');
    },
    showEventDetail: function (context, index) {
      context.original.preventDefault();
      context.original.stopPropagation();
      var selectedEvent = this.get('events.' + index);
      var url = '/events/' + selectedEvent.get('provider_id') + '-' + selectedEvent.get('slug');
      window.open(url, '_blank');
    },
    showDropdown: function (e, index) {
      e.original.preventDefault();
      e.original.stopPropagation();
      $('.search-events .autocomplete-holder').removeClass('hide');
    },
    showPast: function (e) {
      this.toggle('showActual');
    },

    scrollToMonth: function(event) {
      $.scrollTo($(event.node), {duration: 800});
    }
  });

})(app);
