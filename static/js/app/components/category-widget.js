var app = window.app || {};

(function (app) {

  var user = app.userProfile;
  var categories = new app.EventCategories();
  var events = new app.Events();
  events.on('sync', function(response) {
    var loaded = app.searchWidget && app.searchWidget.set('loading', false);
    loaded = app.categoryWidget && app.categoryWidget.set('loading', false);
  });

  app.CategoryWidget = Ractive.extend({
    el: '#category-widget',
    template: '#category-widget-template',
    data: function (){
      return {
        _: _,
        lang: app.lang, // language gettext strings
        locale: app.USER.lang, // locale code: ru, en, ..
        categories: categories,
        events: events,
        user: user,
        bounds: {},
        selected: [],
        formatAverateRate: function(rate) {
          if (rate) {
            return rate.toFixed(1).replace('.', ',');
          }
        },
        formatFirstLetter: function (string) {
          return string.charAt(0).toUpperCase() + string.slice(1);
        },
        formatBucketTitle: function(key) {
            return moment(key).locale(this.get('locale')).format('MMMM YYYY');
        },
        formatMonthNum: function(date_key) {
          return moment(date_key).locale(this.get('locale')).format('MM');
        },
        formatSchedule: function(schedule) {
          return moment(schedule.start_date);
        },
      };
    },
    computed: {
      map_bounds: function() {
        var bounds = this.get('bounds');
        var bottom_right = [bounds.south, bounds.east].join();
        var top_left = [bounds.north, bounds.west].join();
        if (bounds.north && bounds.south) {
          return {
            query: this.get('params.query'),
            start_date: this.get('params.start_date'),
            end_date: this.get('params.end_date'),
            bottom_right: bottom_right,
            top_left: top_left
          };
        }
      },
      bottom_right: function() {
        if (this.get('bounds.south')) {
          return [this.get('bounds.south'), this.get('bounds.east')].join();
        }
        // '${bounds.south}' + ',' +' ${bounds.west}',
      },
      top_left: function() {
        if (this.get('bounds.north')) {
          return [this.get('bounds.north'), this.get('bounds.west')].join();
        }
      },
      events_total: function() {
        var events = this.get('events');
        return events.TOTAL;
      },
      has_prev: function() {
        var events = this.get('events');
        return events.PREVIOUS;
      },
      has_next: function() {
        var events = this.get('events');
        return events.NEXT;
      },
      markers: function() {
        var places = this.get('places');
        return places;
      },
      getDistance: function() {
        var rad = function(x) {
            return x * Math.PI / 180;
        };
        var point1 = this.get('bottom_right') && this.get('bottom_right').split(',');
        var point2 = this.get('top_left') && this.get('top_left').split(',');
        if (point1 && point2) {
          point1 = {
            lat: point1[0],
            lng: point1[1],
          };
          point2 = {
            lat: point2[0],
            lng: point2[1],
          };
          var R = 6378137; // Earth’s mean radius in meter
          var dLat = rad(point2.lat - point1.lat);
          var dLong = rad(point2.lng - point1.lng);
          var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
          Math.cos(rad(point1.lat)) * Math.cos(rad(point2.lat)) *
          Math.sin(dLong / 2) * Math.sin(dLong / 2);
          var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
          var d = R * c;
          return d; // returns the distance in meter
        }
      },
    },
    adapt: [ Ractive.adaptors.Backbone ],
    onrender: function() {
      var self = this;
      var drawingMode = false;
      self.map = app.initMap('event-map', drawingMode);
      self.markerCluster = new MarkerClusterer(this.map, [],
          {imagePath: '/static/images/m'});

      // listener map bounds center change
      var bounds_changed_map = _.debounce(function() {
        var bounds = self.map.getBounds();
        self.set('bounds', bounds.toJSON());
        self.set('distance', self.get('getDistance'));
        self.set(self.map.getCenter().toJSON());
        categories.fetch({
          data: self.get('map_bounds'),
        });
        events.fetch({
          data: self.get('map_bounds'),
        });
      }, 800);

      // listener circle center change
      var center_changed_circle = _.debounce(function() {
        if (self.map.circle) {
          self.set(self.map.circle.center.toJSON());
          self.set('radius', self.map.circle.radius);
        }
      }, 800);

      this.map.addListener('bounds_changed', bounds_changed_map);
      this.map.addListener('center_changed', center_changed_circle);
      this.on({
        selected: function(context, category) {
          context.original.preventDefault();
          context.original.stopPropagation();
          var selected = this.get('selected');
          var selectedItemIndex = selected.indexOf(category.get('key'));

          if (selectedItemIndex == -1) {
              selected.push(category.get('key'));
          } else {
              selected.splice(selectedItemIndex, 1);
          }

          this.set('selected', selected);
          this.set('loading', true);
          events.fetch({
            data: {
              query: this.get('params.query'),
              start_date: this.get('params.start_date'),
              end_date: this.get('params.end_date'),
              categories: selected.join(','),
              top_left: this.get('top_left'),
              bottom_right: this.get('bottom_right'),
            }
          });
          console.log(context, category);
        },
        observe_events: function(context) {
          context.original.preventDefault();
          context.original.stopPropagation();
          var self = this;
          var params = {
            lat: this.get('lat'),
            lng: this.get('lng'),
            radius: this.get('radius'),
          };
          this.set('loading', true);

          // TODO change observe provider events using async/wss
          $.get('/search/', params);
          setTimeout(function() {
            self.set('loading', false);
            categories.fetch({
              data: self.get('map_bounds'),
            });
            // refresh categories after 20sec timeout
            //events.fetch();
          }, 20000);
        },
        prev_page: function(context) {
          context.original.preventDefault();
          context.original.stopPropagation();
          var url = new URL(events.PREVIOUS);
          this.set('loading', true);
          events.fetch({
            data: url.search.substring(1)
          });
          this.set('current_page', url.searchParams.get('page'));
        },
        next_page: function(context) {
          context.original.preventDefault();
          context.original.stopPropagation();
          var url = new URL(events.NEXT);
          this.set('loading', true);
          events.fetch({
            data: url.search.substring(1)
          });
          this.set('current_page', url.searchParams.get('page'));
        },
      });
      this.observe({
        places: function(newValue, oldValue) {
        },
        loading: function(newValue, oldValue) {
          var self = this;
          var selected = this.get('selected');
          var qs = this.get('categories').toJSON();
          if (selected.length) {
            qs = qs.filter(function(el) {
              return _.some(selected, function(category) {
                return category == el.key;
              });
            });
          }
          qs = _.chain(qs).map('events');
          qs = qs.flatMap('hits.hits').map('_source.place').filter('lng');
          if (!newValue) {
            qs = qs.map(function(el){
              return new google.maps.Marker({
                position: {
                  lat: el.lat,
                  lng: el.lng
                },
                label: el.title,
              });
            });
            this.markerCluster.clearMarkers();
            this.markerCluster.addMarkers(qs.value());
            this.markerCluster.redraw();
            self.set('places', qs.value());
            console.log('old', oldValue);
          }
        },
        params: function(newParams, oldParams) {
          console.log('params', newParams);
          var hasParams = (newParams && (
              newParams.query ||
              newParams.city_name ||
              newParams.start_date ||
              newParams.end_date
          ));
          if (hasParams) {
            var selected = this.get('selected');
            this.set('loading', true);
            categories.fetch({
              data: self.get('map_bounds'),
            });
            events.fetch({
              data: {
                query: self.get('params.query'),
                start_date: this.get('params.start_date'),
                end_date: this.get('params.end_date'),
                categories: selected.join(','),
                top_left: self.get('top_left'),
                bottom_right: self.get('bottom_right'),
              }
            });
          }
        }
      });
    }
  });

})(app);
