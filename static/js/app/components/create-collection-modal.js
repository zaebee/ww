var app = window.app || {};

(function (app) {

  var user = app.userProfile;
  var eventCollection = new app.EventCollection();

  app.CreateCollectionModal = Ractive.extend({
    el: 'body',
    append: true,
    template: '#create-collection-modal-template',
    data: function (){
      return {
        lang: app.lang,
        locale: app.USER.lang, // locale code: ru, en, ..
        showModal: false,
        user: user,
        errors: null,
        event: {},
        gotoInAfterCreate: false,
        eventCollection: eventCollection,
        editCollection: null
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],
    oninit: function() {
      console.log('init collection modal');
      this.decorators.dropzone.type = {
        images: {
          minImageWidth: 800,
          minImageHeight: 400,
          thumbnailWidth: 152,
          thumbnailHeight: 110,
          url:'/image/upload/',
          paramName: 'image',
          addRemoveLinks: true,
          dictRemoveFile: '',
          acceptedFiles: '.png, .jpg, .jpeg, .svg, .PNG, .JPG, .JPEG, .SVG',
          keypath: 'event.images',
        },
        collection: {
          maxFiles: 1,
          minImageWidth: 800,
          minImageHeight: 400,
          thumbnailWidth: 152,
          thumbnailHeight: 110,
          url:'/image/upload/',
          paramName: 'image',
          addRemoveLinks: true,
          dictRemoveFile: '',
          acceptedFiles: '.png, .jpg, .jpeg, .svg, .PNG, .JPG, .JPEG, .SVG',
          keypath: 'eventCollection.images',
        }
      };
    }
  });

  app.createCollectionModal = new app.CreateCollectionModal();

  app.createCollectionModal.observe('eventCollection.limit_dates', function(newValue, oldValue, keypath) {
    if (!newValue) {
      eventCollection.set('limit_date_from', null);
      eventCollection.set('limit_date_to', null);
    }
  });

  app.createCollectionModal.observe('eventCollection.images', function(newValue, oldValue, keypath) {
    console.log('eventCollection.images', newValue, oldValue);
    if (newValue && newValue.length) {
      // get first image is collection preview
      var preview = _.last(newValue);
      if (preview && preview.name) {
        eventCollection.set('preview', preview.name);
      }
    }
  });

  app.createCollectionModal.observe('showModal', function(newValue, oldValue, keypath) {
    if (newValue) {
      $('body').addClass('open-modal');
    } else {
      $('body').removeClass('open-modal');
      this.set("editCollection", null);
    }
    setTimeout(function(){
      $('.create-collection-modal [name=title]').focus();
    }, 300);
  });

  app.createCollectionModal.observe('editCollection', function (collectionID, oldValue, keypath) {
    if(collectionID) {
      eventCollection.set('id', collectionID);
      eventCollection.fetch({
        data: {
          edit: true
        }
      }).done(function(response){
        var preview = eventCollection.get('preview');
        eventCollection.set('private', _.toString(response.private));
        if (preview) {
          eventCollection.set('images', [{
            id: preview,
            name: preview,
            image: preview,
          }]);
        }
      });
      this.set({showModal: true});
    } else {
      eventCollection.clear();
      eventCollection.set('private', 'false');
    }
  });

  app.createCollectionModal.on({
    close: function(e){
      e.original.preventDefault();
      this.set('showModal', false);
    },
    save: function(e){
      var self = this;
      e.original.preventDefault();
      e.original.stopPropagation();
      var search = eventCollection.isNew() ? '?created=true' : '';
      var images = eventCollection.get('images') || [];
      if (!images.length) {
        eventCollection.set('preview', '');
      }
      this.get('eventCollection').save().done(function(response, status) {
        if(self.get("editCollection")) {
          self.fire("onCollectionUpdated");
          if (self.get('gotoInAfterCreate')){
              window.location.href = "/collections/" + response.id + "-" + response.slug + "/" + search;
          }
          self.set("eventCollection", new app.EventCollection());
        } else {
          // facebook pixel
          app.fbq('trackCustom', 'AddCollection', {id: response.id, title: response.title});
          app.reachGoal('collection_create').done(function() {
            if (self.get('gotoInAfterCreate')){
                window.location.href = "/collections/" + response.id + "-" + response.slug + "/" + search;
            }
            self.fire("onCollectionCreated");
            self.set("eventCollection", new app.EventCollection());
          });
        }
        app.userProfile.fetch();
        self.set('showModal', false);
      }).fail(function(response, status) {
        self.set('errors', response.responseJSON);
      });
    },
    remove: function(e) {
      var self = this;
      this.get("eventCollection").destroy().done(function(response) {
        self.set('showModal', false);
        document.location.href = response.redirect_url;
      });
    }
  });


})(app);
