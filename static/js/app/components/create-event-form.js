var app = window.app || {};

(function (app) {

  var user = app.userProfile;
  var event = new app.Event({id: window.EVENT_ID});

  app.CreateEventForm = Ractive.extend({
    el: '#event-create-form',
    template: '#create-event-form-template',
    data: function (){
      return {
        _: _,
        lang: app.lang, // language gettext strings
        locale: app.USER.lang, // locale code: ru, en, ..
        errors: null,
        similarEvents: [],
        event: event,
        selectedPlace: null,
        user: user,
        formatScheduleError: function(index, key) {
          var errors = this.get('errors.schedules');
          errors =  _.get(errors, index);
          if (errors) {
            return errors[key];
          }
        },
        formatDate: function(schedules) {
          var date = _.last(schedules);
          if (date) {
            return moment(date.start_date).locale(this.get('locale')).format('DD MMMM YYYY');
          } else {
            return '';
          }
        },
        formatTime: function(schedules) {
          var date = _.last(schedules);
          if(date) {
            return moment(date.start_date).locale(this.get('locale')).format('HH:MM');
          } else {
            return '';
          }
        }
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],
    oninit: function() {
      var self = this;
      if (!event.isNew()) {
        event.fetch({
          success: function(){
            self.set('selectedPlace', self.get('event.place'));
          }
        });
      } else {
        if (store.enabled) {
          var storeEvent = store.get('event', event.toJSON());
          event.set(storeEvent);
          store.clear('event');
        }
        if (!event.get('schedules')) {
          event.set('schedules', [{
            start_date: null,
            end_date: null
          }]);
        }
      }
      this.decorators.select2Placemap.type = {
        place: {
          keypath: 'selectedPlace',
          language: 'ru',
          requestParams: {
              types: ['geocode' ]
          }
        }
      };
      this.decorators.dropzone.type = {
        images: {
          minImageWidth: 800,
          minImageHeight: 400,
          thumbnailWidth: 152,
          thumbnailHeight: 110,
          url:'/image/upload/',
          paramName: 'image',
          addRemoveLinks: true,
          dictRemoveFile: '',
          acceptedFiles: '.png, .jpg, .jpeg, .svg, .PNG, .JPG, .JPEG, .SVG',
          keypath: 'event.images',
        }
      };

      this.decorators.datetimePicker.type = {
        limit_dates: {
          locale: this.get('locale'),
          format: 'YYYY-MM-DD',
        },
        search_dates: {
          locale: this.get('locale'),
          format: 'DD.MM.YYYY',
          showClear: true,
        },
        schedule_time: {
          locale: this.get('locale'),
          format: 'HH:mm',
          stepping: 20,
          widgetParent: $('#event-create-page')
        },
        schedule: {
          minDate: moment().subtract(1, 'days'),
          locale: this.get('locale'),
          format: 'DD.MM.YYYY',
          showClear: true,
          widgetParent: $('#event-create-page')
        }
      };
    },
    onrender: function() {
      var self = this;

      $('#id_ticket_price').inputmask('Regex', {regex: "^[1-9][0-9]{0,9}((,|\\.)\\d{1,2})?$"});
      $('#id_place-email').inputmask('Regex',  {regex: "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,10})$"});
      $('#id_place-phone').inputmask('Regex', {
         regex: "^\\+?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$",
         onKeyValidation: function () { //show some metadata in the console
           var metaData = $(this).inputmask("getmetadata");
           self.set('phoneMetaData', metaData.name_ru);
         }
      });
      $("#create-event-form").on('keyup keypress', function(event) {
        var keyCode = event.keyCode || event.which;
        if (keyCode === 13 && !$(event.target).is("textarea")) {
          event.preventDefault();
          return false;
        }
      });
      setTimeout(function() {
        var last_place = user.get('last_place');
        if (last_place && event.isNew()) {
          //event.set('place', last_place);
        }
      }, 1500);
    },

    confirmRemoveHandler: function() {
      var self = this;

      this.get('event').destroy().done(function(response){
        if (response.success) {
          self.set('successDeleteStatus', response.status);
          window.location.pathname = response.redirect_url;
        }
      });
    }
  });

  app.createEventForm = new app.CreateEventForm();

  app.createEventForm.observe({
    event: function(value, old, keypath) {
      var event = this.get('event').toJSON();
      var place = this.get('event.place');
      event.place = place;
      if (!event.id) {
        store.set('event', event);
      }
    },
    selectedPlace: function(value, old, keypath){
      var placeGeometry = this.get('selectedPlace.geometry.location');
      var place = this.get('event.place') || {};

      if (placeGeometry) {
        var address = this.get('selectedPlace.formatted_address').split(', ');

        var city = _.find(this.get('selectedPlace.address_components'), ['types', ['locality', 'political']]);
        var area1 = _.find(this.get('selectedPlace.address_components'), ['types', ['administrative_area_level_1', 'political']]);
        var area2 = _.find(this.get('selectedPlace.address_components'), ['types', ['administrative_area_level_2', 'political']]);
        var country = _.find(this.get('selectedPlace.address_components'), ['types', ['country', 'political']]);
        var postalCode = _.find(this.get('selectedPlace.address_components'), ['types', ['postal_code']]);

        var $cityInput = $('#id_place-city');
        var selectedValue = _.first($cityInput.select2('data'));
        // Remove country and postal code from address
        address = _.remove(address, function(el){
          return (
            el != (city ? city.long_name : '') &&
            el != (area1 ? area1.long_name : '') &&
            el != (area1 ? area1.short_name : '') &&
            el != (area2 ? area2.long_name : '') &&
            el != (area2 ? area2.short_name : '') &&
            el != (country ? country.long_name : '') &&
            el != (postalCode ? postalCode.long_name : '')
          );
        });
        address = address.join(', ');

        place.lat = placeGeometry.lat();
        place.lng = placeGeometry.lng();
        if (city) {
          place.city = city.short_name;
        } else if (area1) {
          place.city = area1.short_name;
        }
        place.address = address;

        this.set('event.place', place);
        selectedValue.text = place.city;
        $cityInput.trigger('change');
      }
    },
    'event.schedules.*.start_date': function(value, old, keypath) {
      var key = keypath.split('.');
      key[key.length - 1] = 'end_date';
      key = key.join('.');
      this.set(key, value);
    }
  });

  app.createEventForm.on({
    submitForm: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
    },
    validate : function (e) {
      this.set('errors', event.validate());
    },
    validateSchedule: function (e, context) {
        var nodeName = context.node.name,
            fieldIndex = nodeName.split('-').pop(),
            inputField = $('[name=' + nodeName + ']'),
            keypath = context.node.dataset.keypath.replace('event', 'errors'),
            dateValue = inputField.prop('value');

        if (dateValue.length){
          this.set(keypath, null);
        } else {
          this.set(keypath, app.lang.need_datetime);
        }
    },
    validatePhone: function (e) {
      var input = $('#id_place-phone');
      if (!input.inputmask('isComplete')) {
        //this.set('event.place.phone', null);
        this.set('phoneMetaData', null);
        e.set('errors.place.phone', app.lang.check_phone_number);
      } else {
        e.set('errors.place.phone', null);
      }
    },
    validateEmail: function (e) {
      var input = $('#id_place-email');
      if (!input.inputmask('isComplete')) {
        this.set('event.place.email', null);
      }
    },
    validateUrl: function (e, keypath) {
      var url = event.formatUrl(this.get(keypath)),
          regex_url=null,
          input = $(e.node),
          err_field = keypath.replace('event.', 'errors.');

      if (keypath.indexOf('fb_url') !== -1){
        regex_url = /^https?:\/\/(www.)?facebook\.com(#?\/?.+)/;
      } else if (keypath.indexOf('vk_url') !== -1){
        regex_url = /^https?:\/\/(www.)?(vk\.com|vkontakte\.ru)(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
      } else {
        regex_url = /^https?:\/\/(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
      }

      if (!this.get(keypath).length) {
        e.set(err_field, null);
      } else if (regex_url.test(url)) {
        this.set(keypath, url);
        e.set(err_field, null);
      } else {
        this.set(keypath, null);
        e.set(err_field, app.lang.invalid_url);
      }
    },
    addSchedule: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var schedules = this.get('event.schedules');
      var schedule = _.last(schedules);
      var start_date = null;
      if (schedule.start_date) {
        start_date = moment(schedule.start_date, 'DD.MM.YYYY');
        start_date.add(1, 'days');
        start_date = start_date.format('DD.MM.YYYY');
      }
      this.push('event.schedules', {
        start_date: start_date,
        end_date: start_date,
        start_time: schedule.start_time,
        end_time: schedule.end_time,
      });

    },
    removeSchedule: function (e, index) {
      e.original.preventDefault();
      e.original.stopPropagation();
      this.splice('event.schedules', index, 1);
    },
    remove: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      this.toggle('pendingDelete');
    },
    undoRemove: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      this.toggle('pendingDelete');
    },
    confirmRemove: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      this.confirmRemoveHandler();
    },
    save: function(e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var self = this;
      this.set('errors', null);
      var event = this.get('event');
      var place = this.get('event.place');
      event.set('place', place);
      event.validate();
      var isValid = event.isValid();
      var search = event.isNew() ? '?created=true' : '';
      if (isValid) {
        event.save()
          .done(function(response) {
            // facebook pixel
            if(event.isNew()) {
              app.fbq('trackCustom', 'AddEvent', {id: response.id, title: response.title});
            }
            store.clear('event');
            event.set('similar', []);
            window.location.href = response.get_absolute_url + search;
          })
          .fail(function(response) {
            self.set('errors', response.responseJSON);
            self.fire('field-error');
          });
      } else {
        this.set('errors', event.validate());
        this.fire('field-error');
      }
    },
    searchEvent: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var query = this.get('query');
      var self = this;
      self.set({
        loading: true,
        similarEvents: new app.Events()
      });
      var data = {
        q: query,
        exclude_events: _.map(self.get('event.similar'), 'id'),
      };
      $.get('/search/', data).done(function(response) {
        self.set({
          loading: false,
          similarEvents: new app.Events(response.events),
        });
      });
      console.log('query', query);
    },

    addSimilar: function (e, index) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var selected = this.get('similarEvents.' + index);
      selected.set('processing', true);
      if (selected) {
        this.push('event.similar', selected);
        selected.set({
          processing: false,
          added: true,
        });
      }
    },
    removeSimilar: function (e, index) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var selected = this.get('similarEvents.' + index);
      if (selected) {
        selected.set('added', false);
      }
      this.splice('event.similar', index, 1);
    },
    showMore: function (e) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var url = '/search/?q=' + this.get('query');
      window.open(url, '_blank');
    },
    showEventDetail: function (e, index) {
      e.original.preventDefault();
      e.original.stopPropagation();
      var selectedEvent = this.get('similarEvents.' + index);
      var url = '/events/' + selectedEvent.get('provider_id') + '-' + selectedEvent.get('slug');
      window.open(url, '_blank');
    },
    showDropdown: function (e, index) {
      e.original.preventDefault();
      e.original.stopPropagation();
      $('.search-events .autocomplete-holder').removeClass('hide');
    },
    changeTicketPriceState: function (e) {
      if (!this.get('event.currency')) {
        this.set('event.ticket_price', null);
      }
    },
  });

})(app);
