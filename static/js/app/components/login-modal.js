var app = window.app || {};

(function (app) {

  app.LoginModal = Ractive.extend({
    el: 'body',
    append: true,
    template: '#login-modal-template',
    data: function (){
      return {
        lang: app.lang,
        next: document.location.pathname,
        showModal: false,
        errors: null,
        gotoAfterSingup: null,
        navigateToAfterSingup: null,
        isSignupModal: null,
        isOrganizerSignupModal: true,
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],

    oninit: function() {
      this.on({
        close: function(context){
          if(context) context.original.preventDefault();
          if (this.hideModal) this.hideModal();
        },
      });
    },

    showModal: function(showSignUp, showOrganizersSignUp) {
      this.set("isSignupModal", !!showSignUp);
      this.set("isOrganizerSignupModal", !showOrganizersSignUp);
      $('body').toggleClass('open-modal', true);
      this.set('showModal', true);
    },

    hideModal: function() {
      $('body').toggleClass('open-modal', false);
      this.set('showModal', null);
      this.set("isSignupModal", null);
      this.set("gotoAfterSingup", null);
      this.set("navigateToAfterSingup", null);
    },

    animateModal: function(hiddenHandler) {
      var $modalEl = $(".login-modal");
      var watchOpacityChangeInterval = null;
      var FADE_PART_DURATION = 150;

      $modalEl.css("pointer-events", "none");

      $modalEl.fadeTo(FADE_PART_DURATION, 0, "easeInCubic", function() {
            if(!watchOpacityChangeInterval) {
              watchOpacityChangeInterval = setInterval(
                  function() {
                    var currentOpacity = parseFloat($modalEl.css("opacity"));
                    if(currentOpacity === 0) {
                      //true complete animation
                      $modalEl.css("pointer-events", "");
                      clearInterval(watchOpacityChangeInterval);
                      watchOpacityChangeInterval = null;
                      if(hiddenHandler) hiddenHandler();
                      $modalEl.fadeTo(FADE_PART_DURATION, 1, "easeOutCubic");
                    }
                  }, 10
              );
            }
          }
      );
    }
  });

  app.loginModal = new app.LoginModal();

  app.loginModal.observe("isSignupModal", function(newValue, oldValue, keypath) {
    if(newValue !== null) {
      var dataLayerEvent = newValue ? 'registration_popup' : 'login_popup';
      app.reachGoal(dataLayerEvent);
    }
  });

  app.loginModal.on({
    login: function(e){
      var self = this;
      e.original.preventDefault();
      e.original.stopPropagation();
      var data = {
        email: this.get('email'),
        username: this.get('email'),
        password: this.get('password')
      };

      self.set('errors', null);
      $.post('/api/auth/login/', data).done(function(response){
        if(self.get("gotoAfterSingup")) {
          document.location.href = document.location.pathname + "#" + self.get("gotoAfterSingup");
        } else if(self.get("navigateToAfterSingup")) {
          document.location.href = self.get("navigateToAfterSingup");
          return;
        }
        document.location.reload();
      }).fail(function(response){
        self.set('errors', response.responseJSON);
      });
    },

    signup: function(e){
      var self = this;
      e.original.preventDefault();
      e.original.stopPropagation();
      self.set('errors', null);
      var data = {
        email: this.get('email'),
        username: this.get('email'),
        first_name: this.get('first_name'),
        password: this.get('password'),
        business_account: this.get('isOrganizerSignupModal') === true
      };
      $.post('/api/auth/register/', data).done(function(response){
        // facebook pixel
        app.fbq('trackCustom', 'Register');
        if(self.get("gotoAfterSingup")) {
          document.location.href = document.location.pathname + "#" + self.get("gotoAfterSingup");
        } else if(self.get("navigateToAfterSingup")) {
          document.location.href = self.get("navigateToAfterSingup");
          return;
        }
        Cookies.set('is_just_signup', true);
        document.location.reload();
      }).fail(function(response){
        self.set('errors', response.responseJSON);
      });
    },

    switchModal: function(e){
      e.original.preventDefault();
      var $modalEl = $(".login-modal");
      var self = this;

      this.animateModal(
          function() {
            self.set('errors', null);
            self.toggle('isSignupModal');
          }
      );
    },

    switchSignupAsType: function(e) {
      e.original.preventDefault();
      var self = this;
      this.animateModal(
          function() {
            self.set('errors', null);
            self.toggle("isOrganizerSignupModal");
          }
      );
    },

    remind: function(e) {
      e.original.preventDefault();
      app.restorePasswordModal.showModal();
      this.hideModal();
    }
  });

})(app);
