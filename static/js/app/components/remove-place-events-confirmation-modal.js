var app = window.app || {};

(function (app) {

  var user = app.userProfile;

  app.RemovePlaceEventsConfirmationModal = Ractive.extend({
    el: 'body',
    append: true,
    template: '#remove-place-events-confirmation-modal-template',
    data: function (){
      return {
        lang: app.lang,
        showModal: false,
        user: user,
        errors: null,
        placeId: null,
        collectionId: null
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],
    oninit: function() {
    },

    showModal: function(collectionId, placeId) {
      this.set("placeId", placeId);
      this.set("collectionId", collectionId);
      this.set("errors", null);
      $('body').toggleClass('open-modal', true);
      this.set('showModal', true);
    }
  });

  app.removePlaceEventsConfirmationModal = new app.RemovePlaceEventsConfirmationModal();

  app.removePlaceEventsConfirmationModal.observe('showModal', function(newValue, oldValue, keypath) {
    if (newValue) {
      $('body').addClass('open-modal');
    } else {
      $('body').removeClass('open-modal');
    }
  });


  app.removePlaceEventsConfirmationModal.on({
    close: function(e){
      if(e) e.original.preventDefault();
      this.set('showModal', false);
    },

    confirm: function(e) {
      var self = this;
      e.original.preventDefault();
      $.post("/api/collections/" + self.get("collectionId") + "/places/", {place_id: self.get("placeId"), exclude: true})
          .done(function() {
            window.location.reload();
          });
      this.set('showModal', false);
    }
  });
})(app);
