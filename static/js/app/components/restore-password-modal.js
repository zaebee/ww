var app = window.app || {};

(function (app) {

  var user = app.userProfile;

  app.RestorePasswordModal = Ractive.extend({
    el: 'body',
    append: true,
    template: '#restore-password-modal-template',
    data: function (){
      return {
        lang: app.lang,
        showModal: false,
        user: user,
        errors: null,
        isRestoreMode: true,
        isRemindProcessed: false
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],
    oninit: function() {
      this.on({
        close: function(context){
          this.set('showModal', null);
          if (context) context.original.preventDefault();
          if (this.hideModal) this.hideModal();
        },
      });
    },

    showModal: function() {
      this.set("isRestoreMode", true);
      this.set("isRemindProcessed", false);
      this.set("errors", null);
      $("#restore-email").val("");
      $('body').toggleClass('open-modal', true);
      this.set('showModal', true);
    },
  });

  app.subscribeModal = new app.RestorePasswordModal({
    template: '#subscribe-modal-template',
    hideModal: function() {
      $('body').toggleClass('open-modal', false);
      this.set('showModal', null);
    },
  });

  app.subscribeModal.on({
    subscribe: function(context) {
      context.original.preventDefault();
      console.log(context);
      var self = this;
      var data = {
        email: this.get('email'),
        event_id: this.get('eventId'),
      };
      $.post("/api/auth/subscribe/", data).done(function(response) {
        app.reachGoal('subscribe_modal_complete', data);
        self.set("isSubscribed", true);
      }).fail(function(response){
        self.set('errors', response.responseJSON);
        self.set("isSubscribed", false);
      });
    },
  });

  app.restorePasswordModal = new app.RestorePasswordModal();

  app.restorePasswordModal.observe('showModal', function(newValue, oldValue, keypath) {
    if (newValue) {
      $('body').addClass('open-modal');
    } else {
      $('body').removeClass('open-modal');
    }
  });


  app.restorePasswordModal.on({
    remind: function(e) {
      var self = this;
      self.set("isRemindProcessed", true);
      e.original.preventDefault();
      $.post("/api/auth/password/reset/", {email: $("#restore-email").val()}).done(function(response) {
        self.set("isRestoreMode", false);
        self.set("isRemindProcessed", false);
      }).fail(function(response){
        self.set('errors', response.responseJSON);
        self.set("isRemindProcessed", false);
      });
    },

    remembered: function(e) {
      e.original.preventDefault();
      app.loginModal.showModal();
      this.set('showModal', false);
    }
  });
})(app);
