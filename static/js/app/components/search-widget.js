var app = window.app || {};

(function (app) {

  var user = app.userProfile;
  var categories = new app.EventCategories();
  var events = new app.Events();

  app.SearchWidget = Ractive.extend({
    el: '#search-widget',
    template: '#search-widget-template',
    data: function (){
      return {
        _: _,
        lang: app.lang, // language gettext strings
        locale: app.USER.lang, // locale code: ru, en, ..
        categories: categories,
        events: events,
        user: user,
        bounds: {},
      };
    },
    adapt: [ Ractive.adaptors.Backbone ],
    oninit: function() {
      var self = this;
      console.log('init search widget');
      this.on({
        submitSearch: function(context) {
          console.log('submit', context);
          context.original.preventDefault();
          context.original.stopPropagation();
          this.set('loading', true);
          app.categoryWidget.set('params', {
            query: this.get('query'),
            city_name: this.get('city_name'),
            start_date: this.get('start_date'),
            end_date: this.get('end_date'),
          });
        },
      });
    },

    onrender: function() {
      var self = this;
      $('#search-city-input').geocomplete({
        types: ['locality'],
        details: '.search-city-holder',
      }).bind("geocode:result", function(event, result) {
        self.set('loading', true);
        app.categoryWidget.map.setCenter(result.geometry.location);
      });
    },
  });

})(app);
