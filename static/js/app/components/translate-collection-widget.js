var app = window.app || {};

(function (app) {
    var user = app.userProfile;

    app.TranslateCollectionWidget = Ractive.extend({
        append: true,
        el: '#translate-collection-widget-holder',
        template: "#translate-collection-widget-template",

        data: function () {
            return {
                collectionId: false,
                selected: false,
                message: false,
                lang: app.lang,
                user: user,
                locales: [
                  {
                    code: 'ru_en',
                    from: 'ru',
                    to: 'en',
                    text: gettext('russian -> english'),
                  },
                  {
                    code: 'en_ru',
                    from: 'en',
                    to: 'ru',
                    text: gettext('english -> russian'),
                  },
                  {
                    code: 'auto_en',
                    from: 'auto',
                    to: 'en',
                    text: gettext('auto -> english'),
                  },
                  {
                    code: 'auto_ru',
                    from: 'auto',
                    to: 'ru',
                    text: gettext('auto -> russian'),
                  },
                ]
            };
        },

        adapt: [Ractive.adaptors.Backbone],

        oninit: function () {
          this.on({
              select: function (context, locale) {
                  context.original.preventDefault();
                  console.log(context, locale);
                  this.set('selected', locale);
              },

              save: function (context) {
                context.original.preventDefault();
                var self = this;
                var locale = this.get('selected');
                var collection = new app.EventCollection({
                  id: this.get('collectionId')
                });
                this.set('selected', false);
                collection.translate(locale).done(function(response){
                  console.log(response);
                  self.set('message', response.text);
                  $('.js-translate-collection')
                    .text(response.status)
                    .addClass('disabled')
                    .prop('disabled', true);
                });
              },

              cancel: function (context) {
                context.original.preventDefault();
                this.unrender();
              }
          });
        },

        onrender: function(event) {
        },

        toggleWidget: function (flag) {
            $(this.el).toggleClass("open", flag);
        }
    });
})(app);
