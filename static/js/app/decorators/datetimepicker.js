
(function ( global, factory ) {

	'use strict';

	// Common JS (i.e. browserify) environment
	if ( typeof module !== 'undefined' && module.exports && typeof require === 'function' ) {
		factory( require( 'ractive' ), require( 'jquery' ) );
	}

	// AMD?
	else if ( typeof define === 'function' && define.amd ) {
		define([ 'ractive', 'jquery' ], factory );
	}

	// browser global
	else if ( global.Ractive && global.jQuery) {
		factory( global.Ractive, global.jQuery );
	}

	else {
		throw new Error( 'Could not find Ractive or jQuery! They must be loaded before the ractive decorators select2 plugin' );
	}

}( typeof window !== 'undefined' ? window : this, function ( Ractive, $ ) {

	'use strict';

    var datetimePickerDecorator,
        startDatePicker, endDatePicker;

    datetimePickerDecorator = function (node, type, index) {

        var ractive = node._ractive.proxy.ractive;
        var setting = false;
        var observer;

        var options = {};
        if (type) {
            if (!datetimePickerDecorator.type.hasOwnProperty(type)) {
                throw new Error( 'Ractive Datetimepicker type "' + type + '" is not defined!' );
            }

            options = datetimePickerDecorator.type[type];
            if (typeof options === 'function') {
                options = options.call(this, node);
            }
        }

        // Push changes from ractive to select2
        if (node._ractive.binding) {
            observer = ractive.observe(node._ractive.binding.model.keypath, function (newvalue) {
                if (!setting) {
                    setting = true;
                    window.setTimeout(function () {
                        if (newvalue === "") {
                            $(node).data('DateTimePicker').date(null);
                        }
                        $(node).trigger('dp.change');
                        
                        setting = false;
                    }, 0);
                }
            });
        }

        // Pull changes from Datetimepicker to ractive
        if (node.name.indexOf('end-date') > -1) {
          options.useCurrent = false;
        };
        $(node).datetimepicker(options)
            .on('dp.change', function (e) {
                if (!setting) {
                    setting = true;
                    if (node.name.indexOf('start-date') > -1) {
                      options.useCurrent = true;
                      endDatePicker = $('[name=event-schedule-end-date-' + index + ']').data('DateTimePicker');
                      if (endDatePicker) {
                        endDatePicker.minDate(e.date);
                      };
                    };
                    ractive.updateModel();
                    setting = false;
                }
            });



        return {
            teardown: function () {
                //$(node).data('DateTimePicker').destroy();

                if (observer) {
                    observer.cancel();
                }
            }
        };
    };

    datetimePickerDecorator.type = {};

    Ractive.decorators.datetimePicker = datetimePickerDecorator;

}));
