(function ( global, factory ) {

	'use strict';

	// Common JS (i.e. browserify) environment
	if ( typeof module !== 'undefined' && module.exports && typeof require === 'function' ) {
		factory( require( 'ractive' ), require( 'jquery' ) );
	}

	// AMD?
	else if ( typeof define === 'function' && define.amd ) {
		define([ 'ractive', 'jquery' ], factory );
	}

	// browser global
	else if ( global.Ractive && global.jQuery) {
		factory( global.Ractive, global.jQuery );
	}

	else {
		throw new Error( 'Could not find Ractive or jQuery! They must be loaded before the ractive decorators dropzone plugin' );
	}

}( typeof window !== 'undefined' ? window : this, function ( Ractive, $ ) {

	'use strict';

    var dropzoneDecorator;

    dropzoneDecorator = function (node, type) {

        var ractive = node._ractive.proxy.ractive;
        var setting = false;
        var observer;

        var options = {};
        if (type) {
            if (!dropzoneDecorator.type.hasOwnProperty(type)) {
                throw new Error( 'Ractive Select2 type "' + type + '" is not defined!' );
            }

            options = dropzoneDecorator.type[type];
            if (typeof options === 'function') {
                options = options.call(this, node);
            }
        }
        var keypath = options.keypath;

        var dropzoneOptions = $.extend({}, {
            headers: {
              'X-CSRFToken': Cookies.get('csrftoken')
            },
            init: function() {
              // Register for the thumbnail callback.
              // When the thumbnail is created the image dimensions are set.
              this.on("thumbnail", function(file) {
                // Do the dimension checks you want to do
                var $el = $(this.element);
                var $removeContainer = $el.find(".dz-remove");
                if (file.width < options.minImageWidth || file.height < options.minImageHeight) {
                  file.rejectDimensions ? file.rejectDimensions() : false;
                }
                else {
                  file.acceptDimensions ? file.acceptDimensions() : false;
                }
              });
            },
            accept: function(file, done) {
              file.acceptDimensions = done;
              file.rejectDimensions = function() { done(app.lang.fileSizeDropzoneMessage); };
            }
        }, options);

        // Pull changes from dropzone to ractive
        var eventDropzone = new Dropzone(node, dropzoneOptions);

        eventDropzone.on("success", function(file, response) {
          file.id = response.id;
          if (!setting) {
              setting = true;
              ractive.push(keypath, response);
              setting = false;
          }
        });
        eventDropzone.on("removedfile", function(file) {
          var images = ractive.get(keypath);
          var image  = _.find(images, {id: file.id});
          var index = _.indexOf(images, image);
          if (file.id) {
            $.post('/image/delete/', {id: file.id}).done(function(response){
              if (!setting) {
                  setting = true;
                  ractive.splice(keypath, index, 1);
                  setting = false;
              }
            });
          }
        });

        // Push changes from ractive to Dropzone
        if (keypath) {
            observer = ractive.observe(keypath, function (newvalue) {
                if (!setting && newvalue) {
                    setting = true;
                    window.setTimeout(function () {
                      // TODO update dropzone for existing event instance.
                      for (var i = 0; i < newvalue.length; i++) {
                        var image = newvalue[i];
                        var mockFile = { name: "", id: image.id };
                        eventDropzone.emit('addedfile', mockFile);
                        eventDropzone.createThumbnailFromUrl(mockFile, (image.image ? image.image : image.name), null, 'Anonymous');
                        eventDropzone.emit("complete", mockFile);
                      }
                      setting = false;
                    }, 0);
                }
            });
        }

        return {
            teardown: function () {
                //$(node).dropzone('destroy');

                if (observer) {
                    observer.cancel();
                }
            }
        };
    };

    dropzoneDecorator.type = {};

    Ractive.decorators.dropzone = dropzoneDecorator;

}));
