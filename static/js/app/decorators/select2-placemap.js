(function ( global, factory ) {

	'use strict';

	// Common JS (i.e. browserify) environment
	if ( typeof module !== 'undefined' && module.exports && typeof require === 'function' ) {
		factory( require( 'ractive' ), require( 'jquery' ) );
	}

	// AMD?
	else if ( typeof define === 'function' && define.amd ) {
		define([ 'ractive', 'jquery' ], factory );
	}

	// browser global
	else if ( global.Ractive && global.jQuery) {
		factory( global.Ractive, global.jQuery );
	}

	else {
		throw new Error( 'Could not find Ractive or jQuery! They must be loaded before the ractive decorators select2 plugin' );
	}

}( typeof window !== 'undefined' ? window : this, function ( Ractive, $ ) {
    var pluginName = "placecomplete";

    window.initPlacecomplete = function() {
        GooglePlacesAPI.completeInit();
    };

    /**
     * A wrapper to simplify communicating with and contain logic specific to the
     * Google Places API
     *
     * @return {object} An object with public methods getPredictions() and
     *                  getDetails()
     */
    var GooglePlacesAPI = {

        deferred: new $.Deferred(),
        initialized: false,
        acService: null,
        pService: null,
        el: null,

        /**
         * Start loading Google Places API if it hasn't yet been loaded.
         *
         * @param  {HTMLDivElement} el
         *
         *     Container in which to "render attributions", according to
         *     https://developers.google.com/maps/documentation/javascript/reference#PlacesService.
         *     TODO(stephanie): I still don't really understand why the node is
         *     necessary, hence why I'm only ever instantiating PlacesService
         *     once, no matter how many nodes are initialized with the plugin.
         */
        init: function(el) {
            // Ensure init() is idempotent, just in case.
            if (this.initialized) {
                return;
            }

            // Store node so we can use it to intialize PlacesService in
            // completeInit()
            this.el = $.clone(el);

            // Only fetch Google Maps API if it's not already loaded
            if (window.google && google.maps && google.maps.places) {
                // Skip to completeInit() directly
                this.completeInit();
            } else {
                $.ajax({
                    url: "https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&callback=initPlacecomplete",
                    dataType: "script",
                    cache: true
                });
            }
        },

        completeInit: function() {
            var self = this;
            // AutocompleteService is needed for getting the list of options
            this.acService = new google.maps.places.AutocompleteService();

            // PlacesService is needed for getting details for the selected
            // option
            this.pService = new google.maps.places.PlacesService(this.el);

            this.geocoder = new google.maps.Geocoder();
            this.infowindow = new google.maps.InfoWindow();
            this.latlng = new google.maps.LatLng(0, 0);

            var options = {
                zoom: 12,
                center: this.latlng,
                scrollwheel: false,
                mapTypeControl: false,
            };

            var mapId = document.getElementById("map_canvas");
            if (mapId) {
              this.map = new google.maps.Map(mapId, options);

              google.maps.event.addListener(this.map, 'click', function (event) {
                  this.setOptions({scrollwheel:true});
              });

              this.marker = new google.maps.Marker({
                position: this.latlng,
                map: this.map
              });
            }
            this.initialized = true;
            this.deferred.resolve();
        },

        _handlePredictions: function(def, abbreviatedPlaceResults, status) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                def.reject(status);
                return;
            }
            def.resolve(abbreviatedPlaceResults);
        },

        _handleDetails: function(def, displayText, placeResult, status) {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                def.reject(status);
                return;
            }
            placeResult.display_text = displayText;
            def.resolve(placeResult);
        },

        _handleGeocode: function(def, results, status) {
            if (status !== google.maps.GeocoderStatus.OK) {
                def.reject(status);
                return;
            }
            var placeResult = null;
            if (results.length) {
                placeResult = results[0];
                placeResult.display_text = placeResult.formatted_address;
            }
            def.resolve(placeResult);
        },

        geocodeLatLng: function(latlng) {
            return this.deferred.then($.proxy(function() {
                var deferred = new $.Deferred();
                this.geocoder.geocode({
                    'location': latlng
                }, $.proxy(this._handleGeocode, null, deferred));
                return deferred.promise();
            }, this));
        },

        // Get list of autocomplete results for the provided search term
        getPredictions: function(searchTerm, requestParams) {
            return this.deferred.then($.proxy(function() {
                var deferred = new $.Deferred();
                requestParams = $.extend({}, requestParams, {
                    "input": searchTerm
                });
                this.acService.getPlacePredictions(
                    requestParams,
                    $.proxy(this._handlePredictions, null, deferred));
                return deferred.promise();
            }, this));
        },

        // Get details of the selected item
        getDetails: function(abbreviatedPlaceResult) {
            return this.deferred.then($.proxy(function() {
                var deferred = new $.Deferred();
                var displayText = abbreviatedPlaceResult.description;
                this.pService.getDetails({
                    reference: abbreviatedPlaceResult.reference
                }, $.proxy(this._handleDetails, null, deferred, displayText));
                return deferred.promise();
            }, this));
        },

        setMarker: function(latlng) {
            if (this.marker) {
                this.updateMarker(latlng);
            } else {
                this.addMarker({'latlng': latlng, 'draggable': false});
            }
        },

        addMarker: function(Options) {
            this.marker = new google.maps.Marker({
                map: map,
                position: Options.latlng
            });
        },

        updateMarker: function(latlng) {
            this.marker.setPosition(latlng);
        },
    };

    function initPlaceSearch(node, options, ractive, keypath) {
        var $el = $(node);

        var requestParams = options.requestParams;

        var select2options = $.extend({}, {
            ajax: {
                transport: function (params, success, failure) {
                  var $request = GooglePlacesAPI.getPredictions(params.term, requestParams);
                  $request.then(success).fail(failure);
                  return $request;
                }
            },
            query: function(query) {
                GooglePlacesAPI.getPredictions(query.term, requestParams)
                    .done(function(aprs) {
                        var results = $.map(aprs, function(apr) {
                            apr.text = apr.description;
                            return apr;
                        });
                        query.callback({results: results});
                    })
                    .fail(function(errorMsg) {
                        $el.trigger(pluginName + ":error", errorMsg);
                        query.callback({results: []});
                    });
            },
            minimumInputLength: 1,
            allowClear: false,
            multiple: false,
        }, options);

        $el.on({
            "select2:select": function(evt) {
                if (!evt.params || !evt.params.data) {
                    return;
                }
                GooglePlacesAPI.getDetails(evt.params.data)
                    .done(function(placeResult) {
                        window.place = placeResult;
                        ractive.set(keypath, placeResult);
                        $el.trigger(pluginName + ":selected", placeResult);
                        $el.trigger('blur');
                    })
                    .fail(function(errorMsg) {
                        $el.trigger(pluginName + ":error", errorMsg);
                    });
            }
        });

        ractive.observe({
          'event.place.city': function(newvalue){
            var lat = ractive.get('event.place.lat');
            var lng = ractive.get('event.place.lng');
            if (lat && lng) {
              var latLng = new google.maps.LatLng(lat, lng);
              window.setTimeout(function () {
                google.maps.event.trigger(GooglePlacesAPI.map, 'resize');
                GooglePlacesAPI.setMarker(latLng);
                GooglePlacesAPI.map.setCenter(latLng);
                GooglePlacesAPI.infowindow.setContent(newvalue);
                GooglePlacesAPI.infowindow.open(GooglePlacesAPI.map, GooglePlacesAPI.marker);
              }, 300);
            }
          }
        });
        if (GooglePlacesAPI.map) {
          google.maps.event.addListener(GooglePlacesAPI.map, 'click', function(event) {
            GooglePlacesAPI.setMarker(event.latLng);
            GooglePlacesAPI.geocodeLatLng(event.latLng).done(function(placeResult){
              ractive.set(keypath, placeResult);
              GooglePlacesAPI.infowindow.setContent(placeResult.formatted_address);
              GooglePlacesAPI.infowindow.open(GooglePlacesAPI.map, GooglePlacesAPI.marker);
            });
          });
        }

        return select2options;
    }

    select2PlacemapDecorator = function(node, type) {

        // Initialize
        GooglePlacesAPI.init(node);

        var ractive = node._ractive.proxy.ractive;
        var setting = false;
        var observer;
        var keypath;
        var options = {};

        if (type) {
            if (!select2PlacemapDecorator.type.hasOwnProperty(type)) {
                throw new Error( 'Ractive Select2 type "' + type + '" is not defined!' );
            }

            options = select2PlacemapDecorator.type[type];
            if (typeof options === 'function') {
                options = options.call(this, node);
            }
        }
        keypath = options.keypath;

        options = initPlaceSearch(node, options, ractive, keypath);

        window.setTimeout(function () {
            $(node).select2(options);
        }, 500);

        return {
            teardown: function () {
                $(node).data('select2').destroy();
                if (observer) {
                    observer.cancel();
                }
            }
        };
    };

    select2PlacemapDecorator.type = {};

    Ractive.decorators.select2Placemap = select2PlacemapDecorator;

}));
