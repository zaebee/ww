// Call after DOM ready.
//
$(document).ready(function() {
  Ractive.DEBUG = false;
  var app = window.app || {};

  $("body").toggleClass("initialized", true);
  $("body").toggleClass("isFirefox", typeof InstallTrigger !== 'undefined');

  $.ajaxSetup({
    beforeSend: function (xhr) {
      xhr.setRequestHeader('X-CSRFToken', Cookies.get('csrftoken'));
    }
  });

  $(window).click(function() {
    $('.md-select').toggleClass("active", false);
    $(".autocomplete-holder").toggleClass("hide", true);
  });

  var add_collection_modal = Cookies.get('add_collection_modal'),
    is_just_signup = Cookies.get('is_just_signup'),
    gotoInAfterCreate = false;

  Cookies.remove('add_collection_modal');
  Cookies.remove('is_just_signup');

  if (add_collection_modal && app.USER.authorized) {
    if (is_just_signup) gotoInAfterCreate = true;
    app.createCollectionModal.set({
      showModal: true,
      gotoInAfterCreate: gotoInAfterCreate
    });
  }

  var add_event = Cookies.get('add_event');
  Cookies.remove('add_event');
  if (add_event && app.USER.authorized) {
    document.location.pathname = '/events/add/';
  }

  $('#search-city').geocomplete({
    types: ['locality'],
    details: '.search-city-holder'
  });


  /*****************************************/
  //
  //  `Event detail` controls
  //
  (function() {
    if($("#event-detail-page").length) {
      var $el = $("#event-detail-page");
      var $scrollToMapButtonEl = $el.find(".event-info .more-button");
      var $mapContainerEl = $el.find(".event-map-container");
      var $pseudoMapContainerEl = $el.find(".pseudo-map-container");
      var $showAddToCollectionWidgetButton = $el.find(".js-show-add-to-collection-widget");
      var $showSubscribeWidgetButton = $el.find(".js-show-subscribe-widget");
      app.addToCollectionWidget = app.addToCollectionWidget || new app.AddToCollectionWidget();
      var $shareButtonsList = $(".social-buttons .social-links a");

      app.addToCollectionWidget.set({eventId: $el.data('event-id')});

      app.addToCollectionWidget.once("onAddedToCollection", function(event) {
        app.reachGoal('collection_event_save');
      });

      $scrollToMapButtonEl.on("click", function(event) {
        event.preventDefault();
        $.scrollTo($mapContainerEl, {duration: 800});
      });

      $showAddToCollectionWidgetButton.on("click", function(event) {
        event.preventDefault();
        if (app.USER.authorized) {
          if (!app.addToCollectionWidget.fragment.rendered) {
            app.addToCollectionWidget.render();
          }
          app.addToCollectionWidget.toggleWidget(true);
          app.reachGoal('collection_event_add');
        } else {
          app.loginModal.showModal();
        }
      });

      $showSubscribeWidgetButton.on("click", function(event) {
        console.log(event);
        event.preventDefault();
        if (app.USER.authorized) {
        } else {}
        app.subscribeModal.set({
          eventId: $el.data('event-id')
        });
        app.subscribeModal.showModal();
        app.reachGoal('subscribe_modal_show');
      });

      $shareButtonsList.on("click", function(event) {
        var $button = $(event.currentTarget);
        var networkName = $button.data("networkName");
        var eventName = 'event_share_' + networkName;
        app.reachGoal(eventName);
      });


      var padding = $("#place-detail-page").length ? 16 : 0;

      $mapContainerEl.css({
        position: "absolute",
        'max-width': "none",
        width: $(window).width(),
        left: -Math.round($mapContainerEl.offset().left) + padding
      });

      $(window).resize(function() {
        $mapContainerEl.css({
          width: $(window).width(),
          left: -Math.round($pseudoMapContainerEl.offset().left) + padding
        });
      });

      $pseudoMapContainerEl.height($mapContainerEl.height());

      $mapContainerEl.toggleClass("invisible", false);

      app.initMap('event-map');
    }
  })();
  //**************************//

  /*****************************************/
  //
  //  `Read more` control
  //
  (function() {
    var $readMoreButton, $readMoreContent, readMoreData;
    $readMoreButton = $(".read-more");
    if($readMoreButton.length) readMoreData = $readMoreButton.data();
    if(readMoreData) $readMoreContent = $(readMoreData.contentSelector);
    if(!readMoreData || !$readMoreButton.length || !$readMoreContent.length) return;

    $readMoreButton.toggleClass("hide", $readMoreContent[0].scrollHeight <= $readMoreContent.height());
    $('a.read-more').on('click', function(event) {
      event.preventDefault();
      var $readMoreButton = $(event.target);
      var $content = $($readMoreButton.data().contentSelector);
      $content.toggleClass('show');
      $readMoreButton.toggleClass("hide", true);
    });
  })();
  //**************************//


  $('a.js-social-disconnect').on('click', function(event){
    event.preventDefault();
    var target = $(event.currentTarget);
    var url = target.prop('href');
    $.post(url).done(function(){
      document.location.reload();
    });
  });

  /*****************************************/
  //
  //  Click on social buttons
  //
  $(".social-buttons .social-links a").on("click", function(event) {
    event.preventDefault();

    var $button = $(event.currentTarget),
        posX = screen.width/2 - 640/2,
        posY = screen.height/2 - 480/2;
    window.open($button.prop('href'), '', 'width=640,height=480,location=no,toolbar=no,menubar=no,top='+posY+', left='+posX);
  });



  /*****************************************/
  //
  //  `Collection detail` controls
  //
  $('.js-subscribe-collection').on('click', function(event) {
    event.preventDefault();

    if(!$(event.currentTarget).hasClass("js-subscribe-collection")) {
      return;
    }

    if (app.USER.authorized) {
      var target = $(event.currentTarget);
      var url = '/api/collections/' + target.data('collectionId') + '/subscribe/';
      var isSubscribeFromCollectionCard = target.hasClass("card-favorite");

      target.toggleClass("js-subscribe-collection", false);

      $.ajax({
        url: url,
        type: 'POST'
      }).done(function(response) {
        if(isSubscribeFromCollectionCard) {
          var $counterEl = target.parent().find(".card-footer .subscribers-counter-holder .val");
          target.toggleClass("subscribed", true);
          target.tooltip("hide");
          target.tooltip("disable");
          if($counterEl.length) {
            $counterEl.text(response.subscribers_count);
          }
        } else {
          $('.counter-subscribers').html(response.text);
          target.toggleClass("hide", true);
          target.toggleClass("js-subscribe-collection", true);
          target.parent().find(".js-unsubscribe-collection").toggleClass("hide", false);
        }
      }).fail(function() {
        target.toggleClass("js-subscribe-collection", true);
      });
    } else {
      app.loginModal.showModal();
    }
  });


  $('.js-unsubscribe-collection').on('click', function(event) {
    event.preventDefault();
    if (app.USER.authorized) {
      var target = $(event.currentTarget);
      var url = '/api/collections/' + target.data('collectionId') + '/unsubscribe/';
      target.toggleClass("js-unsubscribe-collection", false);
      $.ajax({
        url: url,
        type: 'POST'
      }).done(function(response) {
        $('.counter-subscribers').html(response.text);
        target.toggleClass("hide", true);
        target.toggleClass("js-unsubscribe-collection", true);
        target.parent().find(".js-subscribe-collection").toggleClass("hide", false);
      }).fail(function() {
        target.toggleClass("js-unsubscribe-collection", true);
      });
    } else {
      app.loginModal.showModal();
    }
  });


  $('.js-translate-collection').on('click', function(event) {
    var target = $(event.currentTarget);
    event.preventDefault();
    var widget = new app.TranslateCollectionWidget();
    if (app.USER.authorized) {
      widget.set({collectionId: target.data('collection-id')});
    } else {
      app.loginModal.showModal();
    }
  });


  $('.js-push-collection').on('click', function(event) {
    event.preventDefault();
    console.log('send push');
    var target = $(event.currentTarget);
    if (app.USER.authorized) {
      target.addClass('disabled').attr('disabled', true);
      var url = '/api/collections/' + target.data('collection-id') + '/push/';
      $.post(url).done(function(response){
        target.text(response.text);
      });
    }
  });


  $('a.js-add-collection').on('click', function(event) {
    var target = $(event.currentTarget).parent('.card-event');
    event.preventDefault();
    if (app.USER.authorized) {
      if (app.widget && app.widget.fragment.rendered) {
        app.widget.unrender();
      }
      app.widget = new app.AddToCollectionWidget();
      app.widget.set('renderTarget', target);
      app.widget.set({eventId: target.data('event-id')});

    } else {
      app.loginModal.showModal();
    }
  });


  $('a.js-add-category').on('click', function(event) {
    var target = $(event.currentTarget).parent('.card-event');
    event.preventDefault();
    if (app.USER.authorized) {
      if (app.widget && app.widget.fragment.rendered) {
        app.widget.unrender();
      }
      app.widget = new app.AddToCategoryWidget();
      app.widget.set('renderTarget', target);
      app.widget.set({eventId: target.data('event-id')});

    } else {
      app.loginModal.showModal();
    }
  });


  $("a.js-create-collection").on("click", function(event) {
    event.preventDefault();

    app.reachGoal('create_collection_click');

    if (app.USER.authorized) {
      var target = $(event.currentTarget);

      app.createCollectionModal.set({
        showModal: true,
        gotoInAfterCreate: false
      });
    } else {

      var gotoAfterSingup = null;

      switch(document.location.pathname) {
        case "/collections/" :
        case "/about/":
          gotoAfterSingup = "create-collection";
          break;
      }

      app.loginModal.showModal();
      Cookies.set('add_collection_modal', true);
      app.loginModal.set({gotoAfterSingup: gotoAfterSingup});
    }
  });


  $("a.js-edit-collection").on("click", function(event) {
    event.preventDefault();

    if (app.USER.authorized) {
      var collectionID = window.COLLECTION_ID || $(event.currentTarget).parent().parent().find(".card-favorite").data("collection-id");
      app.createCollectionModal.set({editCollection: collectionID});
      app.createCollectionModal.on("onCollectionUpdated", function(event) {
        window.location.reload();
      });
    } else {
      app.loginModal.showModal();
    }
  });
  //**************************//

  $("a.js-create-event").on("click", function(event) {
    event.preventDefault();

    if (app.USER.authorized) {
      var target = $(event.currentTarget);
      document.location.href = "/events/add/";
    } else {
      app.loginModal.showModal();
      Cookies.set('add_event', true);
      app.loginModal.set({navigateToAfterSingup: "/events/add/"});
    }

  });

  $(".js-remove-event").on("click", function(event) {
    event.preventDefault();
    var eventId = $(event.currentTarget).parent().data('event-id');
    eventId = eventId.split('_').pop();
    var instance = new app.Event({id: eventId});
    instance.destroy().done(function (response) {
      window.location.pathname = response.redirect_url;
    });
  });

  /*****************************************/
  //
  //  Rating widget
  //
  $("#star-rating").rate({
    max_value: 5,
    step_size: 1,
    initial_value: 0,
    selected_symbol_type: 'utf8_star', // Must be a key from symbols
    cursor: 'pointer',
    change_once: true // Determines if the rating can only be set once
  });

  $("#star-rating").on('change', function(e, data){
    if (app.USER.authorized) {
      e.preventDefault();
      var target = $(e.currentTarget);
      var url = '/api/events/' + target.data('eventId') + '/vote/';
      $.ajax({
        url: url,
        type: 'POST',
        data: {rating: data.to}
      }).done(function(response) {
        $('.counter-vote').html(response.text);
      });
    } else {
      app.loginModal.showModal();
    }
  });
  //**************************//

  /*****************************************/
  //
  //  Header search
  //
  if($("#header .search-form").length) {
    (function () {
      var $form = $("#header .search-form");
      var $autocompleteInputHolder = $form.find(".autocomplete-input");
      var $autocompleteInput = $autocompleteInputHolder.find("input");
      var $cityInput = $form.find("#search-city");

      $autocompleteInputHolder.find("li").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();
        var $selectedEl = $(event.target);
        var selectedVal = $selectedEl.text().trim();
        var $autocompleteInputHolder = $selectedEl.closest(".autocomplete-input");
        $autocompleteInputHolder.find("input").val(selectedVal);
        $autocompleteInputHolder.find(".autocomplete-holder").toggleClass("hide", true);
      });

      $form.one("submit", function(event) {
        if(!$cityInput.val()) {
          //Whole world
          $cityInput.val('');
          $('[name=lat]').val('');
          $('[name=lng]').val('');
        }
      });

    })();
  }

  //
  if($('#search-start-date').length) {
    (function () {

      var $startInputEl = $('#search-start-date');
      var $startDisplayEl = $('#search-start-date-display');
      var $endInputEl = $('#search-end-date');
      var $endDisplayEl = $('#search-end-date-display');

      $startInputEl.datetimepicker({
        minDate: moment(),
        locale: app.USER.lang,
        format: 'DD.MM.YYYY'
      });

      $endInputEl.datetimepicker({
        useCurrent: false,
        minDate: moment(),
        locale: app.USER.lang,
        format: 'DD.MM.YYYY'
      });

      $startInputEl.on("dp.change", function (e) {
        if(e.date) $endInputEl.data("DateTimePicker").minDate(e.date);
      });


      $endInputEl.on("dp.change", function (e) {
        if(e.date) $startInputEl.data("DateTimePicker").maxDate(e.date);
      });

      $startInputEl.on("dp.hide dp.show dp.change change input paste keydown", function (e) {
        convertDate($startInputEl, $startDisplayEl, app.lang.search_date_from);
      });

      $endInputEl.on("dp.hide dp.show dp.change change input paste keydown", function (e) {
        convertDate($endInputEl, $endDisplayEl, app.lang.search_date_to);
      });


      $startInputEl.focusin(function(event) {
        $startDisplayEl.toggleClass("infocus", true);
      });

      $startInputEl.focusout(function(event) {
        $startDisplayEl.toggleClass("infocus", false);
      });

      $endInputEl.focusin(function(event) {
        $endDisplayEl.toggleClass("infocus", true);
      });

      $endInputEl.focusout(function(event) {
        $endDisplayEl.toggleClass("infocus", false);
      });


      $startInputEl.trigger("dp.change", {date: $startInputEl.data('start')});
      $endInputEl.trigger("dp.change", {date: $endInputEl.data('end')});


      function convertDate($el, $displayEl, prefix) {
        var date = $el.data("DateTimePicker").date();
        date = date ? date._d : null;
        if(date) {
          $displayEl.text(prefix + " " + date.getDate() + " " + app.lang.getMonthNameByIndex(date.getMonth()).substr(0, 3).toLowerCase() + ".");
        }
      }
    })();
  }
  //**************************//

  $('input.upload').on('change', function(e){
    var target = $(e.currentTarget);
    var data = new FormData();
    data.append('headshot', target.prop('files').item(0));
    $.ajax({
      url: '/api/auth/headshot/',
      type: 'POST',
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false
    }).done(function(response){
      $('.profile-avatar img').prop('src', response.headshot);
    });
  });

  /*****************************************/
  //
  //  Photos slick slider
  //
  if($('#event-slider').length) {
    (function () {
      var $slickEl = $('#event-slider');

      $slickEl.on("init", function (event) {
        var $sliesList = $slickEl.find(".event-slide");
        $slickEl.css("overflow", "");
        if ($sliesList.length) {
          $sliesList.toggleClass("invisible", false);
          $slickEl.css("background-image", "none");
        }
      });

      $slickEl.slick({
        dots: true,
        infinite: true,
        speed: 500,
        cssEase: 'linear',
        arrows: true,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button class="slick-prev slick-arrow-prev"><i class="spritesheet sprite-ico-normal-prev"></i></button>',
        nextArrow: '<button class="slick-next slick-arrow-next"><i class="spritesheet sprite-ico-normal-next"></i></button>',
        customPaging: function (slider, i) {
          return '<div class="dot"></div>';
        }
      });
    })();
  }
  //**************************//

  $('.md-select').on('click', function(event) {
    event.stopPropagation();
    $(this).toggleClass('active');
  });

  $('.md-select ul li').on('click', function(event) {
    var v = $(this).html();
    $('.md-select ul li').not($(this)).removeClass('active');
    $(this).addClass('active');
    $('.md-select label [type=button]').html(v);
  });

  /*****************************************/
  //
  //  Back button
  //
  (function() {
    var $backButtonEl = $('.go-to-last-page-button');
    if($backButtonEl.length) {

      if(window.history.length) {
        $backButtonEl.toggleClass("hide", false);

        $backButtonEl.one("click", function(event) {
          event.preventDefault();
          window.history.back();
        });
      }
    }
  })();
  //**************************//

  /*****************************************/
  //
  //  Profile page
  //
  if($("#profile-detail-page").length) {
    (function () {
      var $el = $("#profile-detail-page");
      var $tabButtons = $el.find(".tabs .tab-buttons .tab-button");
      var $showMore = $el.find(".tab-container .js-show-other-cards");
      var $activeTabButton = $tabButtons.filter(".active");
      var $openSendMailForm = $el.find(".open-form-button");
      var $sendMailButton = $el.find("#sendmail");

      app.createCollectionModal.on("onCollectionCreated", function(event) {
        window.location.reload();
      });

      $openSendMailForm.on("click", function(event) {
        event.preventDefault();
        $el.find(".sendmail-form").toggleClass("hide", false);
        $el.find(".open-form-button").toggleClass("hide", true);
      });

      $tabButtons.on("click", function(event) {
        event.preventDefault();
        var $activeTabButton = $(event.currentTarget);
        $tabButtons.toggleClass("active", false);
        $el.find(".tab-container").toggleClass("hide", true);
        $el.find(".tab-head-button").toggleClass("hide", true);
        $activeTabButton.toggleClass("active", true);
        $($activeTabButton.data().tabContentSelectors).toggleClass("hide", false);
      });

      $showMore.on("click", function(event) {
        event.preventDefault();
        var $target = $(event.currentTarget);
        $target.addClass('hide');
        $target.parent().find('.card').removeClass('hide');
      });

      if($activeTabButton.length) {
        $($activeTabButton.data().tabContentSelectors).toggleClass("hide", false);
      }

	  $sendMailButton.on("click", function(event){
	  	$.post("/api/auth/send_mail/", {
			mailtext: $el.find("#mailtext").val(),
			receiver: $el.find("#user-id").html()
	  	})
		.done(function() {
		  var $sendMailForm = $el.find(".sendmail-form");
		  $sendMailForm.html('<p>' + app.lang.sended + '</p>');
		});
	  });

    })();
  }

  /*****************************************/
  //
  //  Profile update close button
  //
  if($("#profile-update-page").length) {
    $("#profile-update-page .close-button").on("click", function(event) {
      window.location = "/profiles/" + app.USER.id + "/";
    });
  }
  //**************************//


  /*****************************************/
  //
  //  Call Signup and Signin modals
  //

  $("[href='/login/']").toggleClass("hide", false);
  $("[href='/register/']").toggleClass("hide", false);

  $("[href='/login/']").on("click", function(event) {
    event.preventDefault();
    app.loginModal.showModal(false);
  });

  $("[href='/register/']").on("click", function(event) {
    event.preventDefault();
    app.loginModal.showModal(true);
  });
  //**************************//

  /*****************************************/
  //
  //  Create event page
  //
  if($("#event-create-page").length) {
    var $el = $("#event-create-page");
    var $submitFormButton = $el.find("button[form='create-event-form']");
    var $hiddenSubmitFormButton = $el.find("input.submit-button");
    var createEventForm = app.createEventForm;
    var $fieldErrorHolder = $el.find(".field-errors");
    var $fieldErrorButtonsHolder = $fieldErrorHolder.find(".field-errors-buttons");

    $el.find(".close-button").on("click", function(event) {
      var $target = $(event.currentTarget);

      if($target.data("is_clicked")) {
        return;
      }

      $target.data("is_clicked", true);

      if(window.history.length) {
        window.history.back();
      } else {
        window.location = "/";
      }
    });

    createEventForm.on("field-error", function(event) {
      var errors = createEventForm.get("errors");
      $fieldErrorHolder.toggleClass("hide", false);
      $fieldErrorButtonsHolder.empty();

      for(var fieldID in errors) {
        var fieldLabel;
        var fieldIDTag;

        if(fieldID != "place") {
          fieldIDTag = "id_" + fieldID;
          fieldLabel = $el.find("label[for='" + fieldIDTag + "']").text();
          $fieldErrorButtonsHolder.append("<a for='" + fieldIDTag + "' class='error-text-link field-error-button' href='#'>" + fieldLabel + "</a><span>, </span>");
        } else if(fieldID == "place") {
          for(fieldID in errors.place) {
            if(fieldID != "lat" && fieldID != "lng") {
              fieldIDTag = "id_place-" + fieldID;
              fieldLabel = $el.find("label[for='" + fieldIDTag + "']").text();
              $fieldErrorButtonsHolder.append("<a for='" + fieldIDTag + "' class='error-text-link field-error-button' href='#'>" + fieldLabel + "</a><span>, </span>");
            }
          }
        }
      }

      $fieldErrorButtonsHolder.children().last().remove();

      $fieldErrorButtonsHolder.find(".field-error-button").on("click", function(event) {
        event.preventDefault();
        var $button = $(event.currentTarget);
        var fieldID = $button.attr("for");
        var $field = $el.find("#" + fieldID);
        $el.find(".body-container .content").scrollTo($field, {duration: 800});
        $($field).trigger('focus');
      });
    });

    $submitFormButton.on("click", function(event) {
      $fieldErrorHolder.toggleClass("hide", true);
      $hiddenSubmitFormButton.click();
    });

    $el.find(".body-container .content").on("scroll", function(event) {
      var $dpWidget = $el.find(".bootstrap-datetimepicker-widget");
      var $focused = $(':focus');

      if($dpWidget.length) {
        $dpWidget.hide();
        $focused.blur();
      }
    });

    //Remove event
    var $deleteButton = $el.find(".delete-button");
    var $deleteConfirm = $el.find(".delete-confirm");
    var $confirmDeleteButton = $el.find(".confirm-delete-button");
    var $undoDeleteButton = $el.find(".undo-delete-button");
    var $fieldErrors = $el.find(".field-errors");
    var $saveButton = $el.find(".save-button");

    $deleteButton.on("click", function(event) {
      $deleteButton.toggleClass("invisible", true);
      $deleteConfirm.toggleClass("hide", false);
      $fieldErrors.toggleClass("hide", true);
    });

    $undoDeleteButton.on("click", function(event) {
      $deleteButton.toggleClass("invisible", false);
      $deleteConfirm.toggleClass("hide", true);
      $saveButton.toggleClass("hide", false);
    });

    $confirmDeleteButton.on("click", function(event) {
      app.createEventForm.confirmRemoveHandler();
      $deleteConfirm.toggleClass("hide", true);
      $saveButton.toggleClass("hide", true);
    });

  }

  //**************************//


  /*****************************************/
  //
  //  Collection detail page
  //

  if($("#collection-detail-page").length) {
    (function () {
      var $el = $("#collection-detail-page");
      app.addToEventWidget.on("onEventAdded", function(event) {
        app.reachGoal('collection_search_add');
      });

      $("a.date-col").on("click", function(event) {
        $.scrollTo($(event.currentTarget), {duration: 800});
      });

      //author bio more
      $el.find(".author-bio-more").toggleClass("hide", $el.find(".author-bio")[0].scrollHeight <= $el.find(".author-bio").height());


      //Places control
      if($el.find(".places .place-item").length) {
        var $autoaddButtonsList = $el.find(".js-places-autoadd-toggle");
        var $moreButtonsList = $el.find(".js-places-more");
        var $placesMoreMenu = $el.find(".places-more-menu");
        var $placesMoreMenuItemsList = $placesMoreMenu.find(".places-more-menu-item");

        $autoaddButtonsList.on("click", function(event) {
          var $button = $(event.currentTarget);
          event.preventDefault();


          $button.css("pointer-events", "none");

          var collectionID = $el.data("collectionId");
          var url = "/api/collections/" + collectionID + "/places/";
          var data = {
            place_id: $button.parent().data("placeId"),
            enable: !$button.is(".selected")
          };

          $.post(url, data)
              .done(function() {

              })
              .fail(function() {

              })
              .always(function() {
                $button.css("pointer-events", "");
              });

          $button.toggleClass("selected");
        });

        $moreButtonsList.on("click", function(event) {
          var $target = $(event.currentTarget);
          var $liEl = $target.parent();
          event.preventDefault();
          event.stopImmediatePropagation();

          while(!$liEl.is("li")) {
            $liEl = $liEl.parent();
          }


          $placesMoreMenu.find(".auto-add").text($liEl.find(".place-item .js-places-autoadd-toggle").is(".selected") ?  app.lang.places_auto_add_disable : app.lang.places_auto_add_enable);
          $placesMoreMenu.detach().appendTo($liEl);
          $placesMoreMenu.toggleClass("hide", false);

        });

        $placesMoreMenuItemsList.on("click", function(event) {
          var $target = $(event.currentTarget);
          var $placeItem = $target.parent().parent().find(".place-item");

          if($target.is(".delete-events")) {
            app.removePlaceEventsConfirmationModal.showModal($el.data("collectionId"), $placeItem.data("placeId"));
          } else if ($target.is(".auto-add")) {
            $placeItem.find(".js-places-autoadd-toggle").click();

          }
        });

        $(window).click(function() {
          $placesMoreMenu.toggleClass("hide", true);

        });

      }

      //Widget share
      if($el.find(".widget").length) {
        var $showWidgetButtonEl = $el.find(".js-show-widget-code");
        var $hideWidgetButtonEl = $el.find(".js-hide-widget-code");
        var $widgetContentEl = $el.find(".share-widget-content");

        $showWidgetButtonEl.on("click", function(event) {
          event.preventDefault();
          $showWidgetButtonEl.toggleClass("hide", true);
          $widgetContentEl.toggleClass("hide", false);
          $el.find(".widget-partner-code")[0].scrollTop = 0;
        });

        $hideWidgetButtonEl.on("click", function(event) {
          event.preventDefault();
          $showWidgetButtonEl.toggleClass("hide", false);
          $widgetContentEl.toggleClass("hide", true);
        });

      }


    })();
  }

  //**************************//


  /*****************************************/
  //
  //  Collections
  //

  if($("#collection-list-page").length) {
    (function () {
      var $el = $("#collection-list-page");
      var $bottomPopupEl = $el.find("#collection-list-page-bottom-popup");
      var $bottomPopupCloseButton = $bottomPopupEl.find(".close-button");
      var $insertContainerEl = $el.find(".insert-item");

      app.createCollectionModal.on("onCollectionCreated", function(event) {
        var search = '?created=true';
        var collectionModel = app.createCollectionModal.get("eventCollection");
        window.location.href = "/collections/" + collectionModel.get("id") + "-" + collectionModel.get("slug") + "/" + search;
      });

      $bottomPopupCloseButton.one("click", function(event) {
        event.preventDefault();
        $insertContainerEl.html($bottomPopupEl.find(".container").html());
        $insertContainerEl.find("a.js-create-collection").on("click", function(event) {
          event.preventDefault();
          $el.find("a.js-create-collection").eq(0).click();
        });
        $insertContainerEl.toggleClass("show", true);
        $bottomPopupEl.remove();
      });
      $.endlessPaginate({
        paginateOnScroll: true,
        paginateOnScrollMargin: 400
      });
    })();
  }

  //**************************//

  /*****************************************/
  //
  //  About
  //

  if($("#about-page").length) {
    $("#body-wrapper").css("padding-bottom", $("#footer").outerHeight());
    (function () {
      app.createCollectionModal.on("onCollectionCreated", function(event) {
        var search = '?created=true';
        var collectionModel = app.createCollectionModal.get("eventCollection");
        window.location.href = "/collections/" + collectionModel.get("id") + "-" + collectionModel.get("slug") + "/" + search;
      });
    })();
  }

  //**************************//


  /*****************************************/
  //
  //  Organizers
  //

  if($("#organizers-page").length) {
    (function () {
    })();
  }

  /*****************************************/
  //
  //  Oracle
  //

  if($("#genre-oracle").length) {
    (function () {
      console.log('oracle page');
    })();
  }

  /*****************************************/
  //
  //  Place details page
  //

  if($("#place-detail-page").length) {
    (function() {
      var $el = $("#place-detail-page");
      var calendarOfEventsBtn = $el.find(".calendar-of-event-button");

      calendarOfEventsBtn.on("click", function(event) {
        event.preventDefault();
        $.scrollTo($el.find(".events-buckets-initial"), {duration: 800});
      });

      $("a.date-col").on("click", function(event) {
        $.scrollTo($(event.currentTarget), {duration: 800});
      });

    })();
  }

  //**************************//

  /*****************************************/
  //
  //  Cards
  //

  //**************************//

  if($(".card").length) {
    var fnCollectionTitles = function () {
      var $img;
      var $collectionsCardsList = $(".card.card-collection:not(.hide)");
      var $eventsCardsList = $(".card.card-event");

      $img = $eventsCardsList.find(".card-image img");
      $img.on('load', function(event) {
        $(this).parent().css("background", "none");
      });

      var $imgsList = $collectionsCardsList.find(".collection-previews .pic");
      $img = $imgsList.find("img");
      $img.on('load', function(event) {
        $(this).parent().css("background", "none");
      });


      (function() {
        var MAX_LINES = 5;
        var CARD_NAME_SELECTOR = ".collection-card-name";
        var getLinesCount = function(el) {
          var elHeight = el.outerHeight();
          var elLineHeight = parseInt(el.css("line-height"));
          var result = elHeight / elLineHeight;
          return Math.round(result);
        };

        //Long collection name fix

        for(var i = 0; i < $collectionsCardsList.length; i++) {
          var $card = $collectionsCardsList.eq(i);
          var $cardNameFieldEl = $card.find(CARD_NAME_SELECTOR);
          var cardNameFieldElText = $cardNameFieldEl.text();

          while(getLinesCount($cardNameFieldEl) > MAX_LINES) {
            cardNameFieldElText = cardNameFieldElText.slice(0, -1);
            $cardNameFieldEl.text(cardNameFieldElText + "...");
          }
        }

        // Collection name text background fix

        for(i = 0; i < $collectionsCardsList.length; i++) {
          $card = $collectionsCardsList.eq(i);
          $cardNameFieldEl = $card.find(CARD_NAME_SELECTOR);

          var linesCount = getLinesCount($cardNameFieldEl);
          var wordsList = $cardNameFieldEl.text().split(" ");
          var linesTextList = [];
          var lineWords = [];
          var $cardNameFieldParent = $cardNameFieldEl.parent();

          while(wordsList.length) {
            lineWords.unshift(wordsList.pop());
            $cardNameFieldEl.text(wordsList.join(" "));

            if(getLinesCount($cardNameFieldEl) < linesCount) {
              linesTextList.unshift(lineWords.join(" "));
              lineWords = [];
              linesCount = getLinesCount($cardNameFieldEl);
            }
          }

          $cardNameFieldParent.empty();

          for(var j = 0; j < linesTextList.length; j++) {
            var $lineField = $("<div/>", {
              class: "collection-name-line",
              text: linesTextList[j]
            });
            $cardNameFieldParent.append($lineField);
          }
        }

        $(".card-collection .title.invisible").toggleClass("invisible", false);

      })();

      //Add to collection button on hover
      $eventsCardsList.on("mouseover", function(event) {
        $(event.currentTarget).toggleClass("hover", true);
      });

      $eventsCardsList.on("mouseleave", function(event) {
        $(event.currentTarget).toggleClass("hover", false);
      });

    };
    fnCollectionTitles();

    if($("#profile-detail-page").length) {
        var $el = $("#profile-detail-page");
        var $showMore = $el.find(".tab-container .js-show-other-cards");
        $showMore.on("click", fnCollectionTitles);
    }
  }

  /*****************************************/
  //
  //  Hashes
  //

  (function() {
    var hash = document.location.hash;
    var hashValue = hash.substr(1);
    switch(hash.toLowerCase()) {
      case "#_=_" :
        history.pushState("", document.title, window.location.pathname);
        break;
      case "#create-collection" :
        history.pushState("", document.title, window.location.pathname);
        setTimeout(function() {
          $("a.js-create-collection").click();
        }, 500);
        break;
    }

    if(($("#collection-detail-page").length || $("#place-detail-page").length) && hash.length && parseInt(hashValue)) {
      var monthNum = parseInt(hashValue);
      var $dateColsList = $(".events-in-collection .date-col") || $(".events-buckets-initial .date-col");
      for(var i = 0; i < $dateColsList.length; i++) {
        var $dateColEL = $dateColsList.eq(i);
        var dateColMonthNum = $dateColEL.data("month");
        if(dateColMonthNum && parseInt(dateColMonthNum) == monthNum) {
          $.scrollTo($dateColEL, {duration: 800});
          return;
        }
      }
    }

  })();

  //**************************//


  /*****************************************/
  //
  //  Tooltips
  //


  (function() {
    var $el = $('[rel="tooltip"], [rel="tooltip-long-text"]');
    var $longTextElList = $('[rel="tooltip-long-text"]');
    $el.tooltip({placement: "top", container: "body"});
    $el.tooltip('show');
    $el.tooltip('hide');

    for(var i = 0, length = $longTextElList.length; i < length; i++) {
      var $longTextEl = $longTextElList.eq(i);
      if($longTextEl[0].scrollWidth < $longTextEl.width()) {
        $longTextEl.tooltip("disable");
      }
    }

  })();

  /*****************************************/
  //
  //  Restore password
  //

  /*$(".js-restore-password").on("click", function(event) {
    event.preventDefault();
    app.restorePasswordModal.showModal();
  })*/


  /*****************************************/
  //
  //  Sticky items
  //

  if($(".sticky-item").length) {
    (function() {
      var a = document.querySelector('.sticky-item'),
          f = document.querySelector('.sticky-stop-item') || document.querySelector("#footer"),
          b = null,
          p = 12;
      window.addEventListener('scroll', checkSticky, false);

      function checkSticky() {
        if (b == null) {
          var sa = getComputedStyle(a, ''), s = '';
          for (var i = 0; i < sa.length; i++) {
            if (sa[i].indexOf('overflow') == 0 || sa[i].indexOf('padding') == 0 || sa[i].indexOf('border') == 0 || sa[i].indexOf('outline') == 0 || sa[i].indexOf('box-shadow') == 0 || sa[i].indexOf('background') == 0) {
              s += sa[i] + ': ' +sa.getPropertyValue(sa[i]) + '; '
            }
          }
          b = document.createElement('div');
          b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
          a.insertBefore(b, a.firstChild);
          var l = a.childNodes.length;
          for (var i = 1; i < l; i++) {
            b.appendChild(a.childNodes[1]);
          }
          a.style.height = b.getBoundingClientRect().height + 'px';
          a.style.padding = '0';
          a.style.border = '0';
        }
        var ra = a.getBoundingClientRect(),
            r = Math.round(ra.top + b.getBoundingClientRect().height - f.getBoundingClientRect().top + 0);
        if ((ra.top - p) <= 0) {
          if ((ra.top - p) <= r) {
            b.className = 'sticky-stop';
            b.style.top = - r +'px';
          } else {
            b.className = 'sticky-start';
            b.style.top = p + 'px';
          }
        } else {
          b.className = '';
          b.style.top = '';
        }
        window.addEventListener('resize', function() {
          a.children[0].style.width = getComputedStyle(a, '').width
        }, false);
      }
    })();
  }
});
