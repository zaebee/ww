var app = window.app || {};

(function (app) {
  var reachGoal = function (action, params) {
    var deferred = $.Deferred();
    var result = window.yaCounter43939494 ? window.yaCounter43939494.reachGoal : function (){console.log(arguments);};
    var goalCallbackMetrika = function () {
      console.log('request successfully sent to Metrica: ', action);
      deferred.resolve();
    };

    if (window.ga) {
      window.ga('send', 'pageview', '/' + action + '/', {
        hitCallback: function() {
          console.log('request successfully sent to Analytics: ', action);
          result(action, params, goalCallbackMetrika);
        }
      });
    } else {
      result(action, params, goalCallbackMetrika);
    }
    return deferred.promise();
  };

  var reachFbq = function (trackType, eventName, params) {
    var result = window.fbq ? window.fbq : function () {console.log(arguments);};
    return result(trackType, eventName, params);
  };

  app.reachGoal = reachGoal;
  app.fbq = reachFbq;

})(app);
