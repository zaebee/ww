var app = window.app || {};

(function (app) {
  app.initMap = function(elementId, drawing) {
    var map, marker, title, el, lat, lng;
    el = document.getElementById(elementId);
    if (el) {
      lat = parseFloat(el.dataset.lat);
      lng = parseFloat(el.dataset.lng);
      title = el.dataset.title;

      map = new google.maps.Map(el, {
        center: {lat: lat, lng: lng},
        zoom: 13,
        scrollwheel: false,
        mapTypeControl: false,
      });

      google.maps.event.addListener(map, 'click', function(event){
        this.setOptions({scrollwheel:true});
      });

      marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map,
        title: title,
      });
    }
    if (drawing) {
      var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [

            google.maps.drawing.OverlayType.CIRCLE
          ]
        },

        circleOptions: {
          fillColor: '#01939A',
          fillOpacity: 0.4,
          strokeWeight: 5,
          clickable: false,
          editable: true,
          zIndex: 1
        }
      });

      drawingManager.setMap(map);
      google.maps.event.addListener(drawingManager, 'circlecomplete', function(circle) {
        if (map.circle) {
          map.circle.setMap(null);
          google.maps.event.clearInstanceListeners(map.circle);
        }
        var radius = circle.getRadius();
        map.circle = circle;
        map.setCenter(map.circle.center);
        google.maps.event.addListener(map.circle, 'center_changed', function() {
          console.log('center_changed', map.circle.center.toJSON());
          map.setCenter(map.circle.center);
        });
        console.log('circlecomplete', radius, circle.center.toJSON());
      });
    }
    return map;

  };

})(app);
