var app = window.app || {};

(function (app) {


  app.BaseModel = Backbone.Model.extend({
    url: function() {
      var original_url = Backbone.Model.prototype.url.call( this );
      var parsed_url = original_url + ( original_url.charAt( original_url.length - 1 ) == '/' ? '' : '/' );

      return parsed_url;
    }
  });
  _.extend(app.BaseModel.prototype, Backbone.Validation.mixin);

  app.User = app.BaseModel.extend({
    urlRoot: '/api/users/',
  });

  app.UserProfile = app.BaseModel.extend({
    url: '/api/users/profile/',
  });

  app.Event = app.BaseModel.extend({
    urlRoot: '/api/events/',
    validation: {
      title: function(value, attr, computedState) {
        if(!value) {
          return gettext('Field is required');
        }
      },
      schedules: function(value, attr, computedState) {
        var errors = [];
        for (var i = 0; i < value.length; i++){
          var schedule = value[i],
            error = {};
          if (!schedule.start_date) {
            error.start_date = gettext('Field is required');
          }
          if (!schedule.start_time) {
            error.start_time = gettext('Field is required');
          }
          if (!schedule.end_time) {
            error.end_time = gettext('Field is required');
          }
          if (!_.isEmpty(error)) {
            errors[i] = error;
          }
        }

        if (!_.isEmpty(errors)) {
          return errors;
        }
      },
      place: function(value, attr, computedState) {
        var errors = {};
        if(!value.title) {
          errors.title = gettext('Field is required');
        }
        if(!value.city) {
          errors.city = gettext('Field is required');
        }
        if(!value.address) {
          errors.address = gettext('Field is required');
        }
        if (!_.isEmpty(errors)) {
          return errors;
        }
      }

    },
    addToCollection: function(data) {
      return $.post('/api/events/' + this.id + '/collections/', data);
    },

    addToCategory: function(data) {
      return $.post('/api/events/' + this.id + '/categories/', data);
    },

    formatUrl: function(url) {
      if(url.length && url.indexOf("http://") < 0 && url.indexOf("https://") < 0) {
        url = "http://" + url;
      }
      return url;
    }
  });

  app.EventCollection = app.BaseModel.extend({
    urlRoot: '/api/collections/',
    defaults: {
      private: 'false'
    },
    translate: function(data) {
      return $.post('/api/collections/' + this.id + '/translate/', data);
    },
    events: function() {
      return $.get('/api/collections/' + this.id + '/widget/');
    },
  });

  app.EventCategory = app.BaseModel.extend({
    urlRoot: '/api/categories/',
  });

  app.Events = Backbone.Collection.extend({
    url: '/api/events/',
    model: app.Event,
    parse: function (response) {
      this.NEXT = response.links && response.links.next;
      this.PREVIOUS = response.links && response.links.previous;
      this.TOTAL = response.count;
      return response.results;
    }
  });

  app.EventCollections = Backbone.Collection.extend({
    url: '/api/collections/',
    model: app.EventCollection,
    parse: function (response) {
      this.NEXT = response.next;
      this.PREVIOUS = response.previous;
      return response.results;
    }
  });

  app.EventCategories = Backbone.Collection.extend({
    url: '/api/categories/',
    model: app.EventCategory,
    parse: function (response) {
      this.NEXT = response.next;
      this.PREVIOUS = response.previous;
      return response.results || response;
    },
    tree: function() {
      return $.get('/api/categories/tree/');
    },
  });

  app.Users = Backbone.Collection.extend({
    url: '/api/users/',
    model: app.User,
    parse: function (response) {
      this.NEXT = response.next;
      this.PREVIOUS = response.previous;
      return response.results;
    }
  });

  app.Spinner = function($el) {
    return new app.buttonSpinner($el, '&nbsp;', $el);
  };

  app.userProfile = new app.UserProfile();
  app.categories = new app.EventCategories();
  if (app.USER.authorized) {
    app.userProfile.fetch();
    app.categories.tree().done(function(response) {
      app.categories.set(response);
    });
  }

})(app);
