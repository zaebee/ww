(function(window, document) {

  var _W = {};
  var LANG = {
    ru: {
      monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
      monthNamesForCards: ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"],
      priceNotSpecified: "Цена не указана",
      priceFree: "Бесплатно",
      buyTicket: "Купить билет",
      whatToExpect: "Чего ожидать",
      readMore: "Читать дальше",
      gotoWebsite: "Перейти на сайт",
      more: "подробнее",
      place: "Место",
      eventsNearbyYou: "Посмотреть события рядом с вами"
    },
    en: {
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthNamesForCards: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      priceNotSpecified: "Price not specified",
      priceFree: "Free",
      buyTicket: "Buy ticket",
      whatToExpect: "What to expect",
      readMore: "Читать дальше",
      gotoWebsite: "Go to website",
      more: "more",
      place: "Place",
      eventsNearbyYou: "Look for events near you"
    }
  };



  function httpGet(url, onCompleteCallback) {
    var x = new XMLHttpRequest();
    x.open("GET", url, true);
    x.onload = function () {
      if(onCompleteCallback) {
        onCompleteCallback(JSON.parse(x.responseText));
      }
    };
    x.send(null);
  }



  function loadCSS(url, CSS_ID) {
    if (!document.getElementById(CSS_ID)) {
      var link = document.createElement('link');
      link.id = CSS_ID;
      link.rel  = 'stylesheet';
      link.type = 'text/css';
      link.href = url;
      link.media = 'all';
      document.getElementsByTagName('head')[0].appendChild(link);
    }
  }



  function loadScript(url, scriptID) {
    if(!document.getElementById(_W.widgetGooglemapsSctiptID)) {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.id = scriptID;
      script.onload = function() {};
      script.src = url;
      document.getElementsByTagName('head')[0].appendChild(script);
    }
  }



  function uppercaseFirstLetter(text) {
    return text[0].toUpperCase() + text.slice(1);
  }



  function getLocalizedField(data, field) {
    var locale = _W.locale.toLocaleLowerCase();
    var result = data[field + "_localized"] ? (data[field + "_localized"][locale] ? data[field + "_localized"][locale] : data[field]) : data[field];
    return result || "";
  }



  function drawCollection() {
    _W.collectionWidgetEl = document.createElement('div');
    _W.collectionWidgetEl.id = _W.collectionWidgetElID;

    _W.collectionTemplate = getEl("h2", null, _W.collectionData.title);

    for(var i = 0, bucketsLength = _W.collectionData.facets.day_events.length; i < bucketsLength; i++) {
      var bucketData = _W.collectionData.facets.day_events[i];
      var bucketEventsTemplate = "";
      for(var j = 0, eventsLength = bucketData['events'].length; j < eventsLength; j++) {
        var eventData = bucketData['events'][j];
        bucketEventsTemplate += getEventCardTemplate(eventData);
        _W.collectionEventsByID[eventData.id] = eventData;
      }

      _W.collectionTemplate += getCollectionBucketTemplate(bucketData, bucketEventsTemplate);
    }

    _W.collectionWidgetEl.innerHTML = _W.collectionTemplate;
    _W.containerEl.appendChild(_W.collectionWidgetEl);


    for(var i = 0, bucketTitlesList = _W.containerEl.querySelectorAll("[data-month]"), length = bucketTitlesList.length; i < length; i++) {
      bucketTitlesList[i].addEventListener("click", function(event) {
        event.currentTarget.scrollIntoView();
      });
    }

    for(var i = 0, eventDetailsBtnList = _W.containerEl.querySelectorAll(".event-card-details-btn"), length = eventDetailsBtnList.length; i < length; i++) {
      eventDetailsBtnList[i].addEventListener("click", function(event) {
        event.preventDefault();

        //find card el
        var parentNode = event.currentTarget.parentNode;
        var cardEl;
        while(parentNode && parentNode.className.indexOf("card-event") == -1) {
          parentNode = parentNode.parentNode;
        }

        if(parentNode) {
          cardEl = parentNode;
          drawEventDetails(_W.collectionEventsByID[cardEl.dataset.eventId]);
        }
      });
    }


    window.addEventListener("resize", onResize);

    function alignWWLink() {
      //calculate and set ww-link position
      var cardsElList = _W.containerEl.querySelectorAll(".card-event");
      var cardBounds = cardsElList[0].getBoundingClientRect();
      var cardY = cardBounds.top;
      var lastCardRight = cardBounds.right;
      var firstCardLeft = cardBounds.left;
      var wwLinkElList = _W.containerEl.querySelectorAll(".ww-link-main");
      var cardsContainerWidth, cardsContentWidth, wwLinkRight;

      for(i = 0, length = cardsElList.length; i < length; i++) {
        var cardEl = cardsElList[i], newCardY;
        cardBounds = cardEl.getBoundingClientRect();

        lastCardRight = Math.max(lastCardRight, cardBounds.right);
      }

      cardsContainerWidth = _W.containerEl.querySelectorAll(".events-list")[0].offsetWidth;
      cardsContentWidth = lastCardRight - firstCardLeft;
      wwLinkRight = cardsContainerWidth - cardsContentWidth;

      for(i = 0, length = wwLinkElList.length; i < length; i++) {
        var linkEl = wwLinkElList[i];
        linkEl.style.right = wwLinkRight + "px";
      }
    }

    function onResize(event) {
      var isOneCol = _W.collectionWidgetEl.offsetWidth < 673;
      var eventBucketsList = _W.collectionWidgetEl.querySelectorAll(".event-bucket");
      _W.collectionWidgetEl.classList.toggle('one-col', isOneCol);

      if(isOneCol) {
        var cardLeftX = _W.collectionWidgetEl.querySelectorAll(".card-event")[0].offsetLeft - _W.collectionWidgetEl.querySelectorAll(".event-bucket-title-holder")[0].offsetLeft;
        for(var i = 0, length = eventBucketsList.length; i < length; i++) {
          var bucketEl = eventBucketsList[i];
          bucketEl.querySelector(".bucket-title").style.marginLeft = cardLeftX + "px";
          bucketEl.querySelector(".ww-link-main").style.marginLeft = cardLeftX + "px";
        }
      } else {

        var eventBucketsList = _W.collectionWidgetEl.querySelectorAll(".event-bucket");
        for(var i = 0, length = eventBucketsList.length; i < length; i++) {
          var bucketEl = eventBucketsList[i];
          bucketEl.querySelector(".bucket-title").style.marginLeft = "auto";
          bucketEl.querySelector(".ww-link-main").style.marginLeft = "auto";
        }

        alignWWLink();
      }
    }

    onResize(null);
    initAnchors();
  }



  function drawEventDetails(eventData) {
    window.location.hash = "event-" + eventData.id;

    _W.eventDetailsEl = document.createElement("div");
    _W.eventDetailsEl.id = _W.eventDetailsElID;

    _W.eventTemplate =
        getEl("div", {class: "event-content"},
          getEl("div", {class: "close-modal-button"}, getEl("i", {class: "close-button-ico"})) +
          getEl("div", {class: "ww-row"},
            getEl("a", {href: "https://whatwhere.world/", class: "caption ww-link"}, _W.lang.eventsNearbyYou) +
            getEl("h2", {class: "event-title"}, capitalizeFirstLetter(getLocalizedField(eventData, "title"))) +
            getEl("div", {class: "event-info with-clearfix"},
              getEl("div", {class: "event-place"},
                getEl("div", {class: "caption"}, getLocalizedField(eventData.place, "title")) +
                getEl("a", {class: "caption-link more-button"}, uppercaseFirstLetter(_W.lang.more))
              ) +
              getEl("div", {class: "event-schedule-dropdown caption"},
                getEl("div", {class: "schedule-button"},  getScheduleDate(eventData.actual_schedule)) +
                getEl("ul", {class: "schedule-list"}, getScheduleListTemplate(eventData.dates))
              ) +
              getEl("div", {class: "event-price"},
                getEl("div", {class: "caption"}, eventData.ticket_price === "0" ? _W.lang.priceFree : (eventData.ticket_price ? eventData.ticket_price : _W.lang.priceNotSpecified)) +
                getEl("a", {class: "caption-link", href: eventData.ticket_url, target: "_blank"}, eventData.ticket_url ? _W.lang.buyTicket : "")
              )
            ) +
            getImagesGalleryTemplate(eventData.images) +
            getEl("div", {class: "event-description"},
              eventData.description ?
                getEl("h4", null, _W.lang.whatToExpect) +
                getEl("div", {class: "body read-more-content"}, getLocalizedField(eventData, "description")) +
                getEl("a", {class: "read-more body-link", href: "#", 'data-content-selector': ".read-more-content"}, _W.lang.readMore)
              : ""
            )
          ) +
          getEl("div", {class: "event-map-container invisible"},
            getEl("div", {id: "event-map"}) +
            getEl("div", {class: "event-place-card"},
              getEl("h4", null, _W.lang.place) +
              getEl("div", {class: "body place-title"}, getLocalizedField(eventData.place, "title")) +
              getEl("div", {class: "caption place-address"}, getLocalizedField(eventData.place, "address")) +
              getEl("div", {class: "body place-phone"}, eventData.place.phone) +
              getEl("a", {class: "body-link place-email", href: "mailto:" + eventData.place.email}, eventData.place.email) +
              getEl("a", {class: "body-link place-website", href: eventData.place.website, target: "_blank"}, eventData.place.website ? _W.lang.gotoWebsite : "")
            )
          )
        );

    _W.eventDetailsEl.innerHTML = _W.eventTemplate;
    _W.containerEl.appendChild(_W.eventDetailsEl);


    initMap(_W.eventDetailsEl.querySelector("#event-map"), getLocalizedField(eventData.place, "title"), eventData.place.lat, eventData.place.lng);
    _W.eventDetailsEl.querySelector(".event-map-container").classList.toggle('invisible', false);

    // Place more
    var moreButton = _W.eventDetailsEl.querySelector(".event-place .more-button");

    function onPlaceMoreClicked(event) {
      event.preventDefault();
      _W.eventDetailsEl.querySelector(".event-map-container").scrollIntoView();
    }

    moreButton.addEventListener("click", onPlaceMoreClicked);
    _W.removeListenersList.push(
        function() {
          moreButton.removeEventListener("click", onPlaceMoreClicked);
        }
    );


    //Close modal

    var closeModalButton = _W.eventDetailsEl.querySelector(".close-modal-button");

    function closeModal() {
      window.removeEventListener("click", onWindowClicked);
      closeModalButton.removeEventListener("click", onCloseModalBtnCkicked);
      trackEl.removeEventListener("webkitTransitionEnd", onTransitionEnd);
      trackEl.removeEventListener("transitionend", onTransitionEnd);
      trackEl.removeEventListener("msTransitionEnd", onTransitionEnd);
      trackEl.removeEventListener("oTransitionEnd", onTransitionEnd);
      btnPrevEl.removeEventListener("click", onPrevBtnClicked);
      btnNextEl.removeEventListener("click", onNextBtnClicked);

      for(var i = 0, length = _W.removeListenersList.length; i < length; i++) {
        _W.removeListenersList[i].call();
        _W.removeListenersList[i] = null;
      }


      _W.containerEl.removeChild(_W.eventDetailsEl);
      _W.eventDetailsEl = null;
      _W.eventTemplate = null;
      _W.removeListenersList = [];

      window.history.pushState("", document.title, window.location.pathname);
    }

    function onWindowClicked(event) {
      if (event.target == _W.eventDetailsEl) {
        closeModal();
      } else if(!event.target.classList.contains("schedule-list-item") && !event.target.classList.contains("schedule-button")) {
        _W.eventDetailsEl.querySelector(".schedule-list").classList.toggle("active", false);
      }
    }

    function onCloseModalBtnCkicked(event) {
      closeModal();
    }

    closeModalButton.addEventListener("click", onCloseModalBtnCkicked);
    window.addEventListener("click", onWindowClicked);


    //Gallery
    var galleryEl = _W.eventDetailsEl.querySelector(".event-images");
    var trackEl = galleryEl.querySelector(".track");
    var slidesElList = trackEl.querySelectorAll(".slide");
    var btnPrevEl = galleryEl.querySelector(".btn-prev");
    var btnNextEl = galleryEl.querySelector(".btn-next");
    var dotsEl = galleryEl.querySelector(".dots");
    var galleryWidth;
    var galleryHeight = galleryEl.offsetHeight;
    var trackPositionIndex = 0;
    var inMotion = false;
    var imagesLength = eventData.images.length;
    var slidesLength = slidesElList.length;
    var trackFullWidth;
    var trackPosition = null;

    function resizeGallery() {
      galleryWidth = galleryEl.offsetWidth;
      trackFullWidth = galleryWidth * slidesLength;
      for (var i = 0, length = slidesElList.length; i < length; i++) {
        var slideEl = slidesElList[i];
        slideEl.style.width = galleryWidth + "px";
        slideEl.style.height = galleryHeight + "px";
      }
      if(!isNaN(trackPosition) && imagesLength > 1) {
        trackPosition = -(galleryWidth + (galleryWidth * trackPositionIndex));
        setTrackPosition(trackPosition, true);
      }

      //close button position
      var closeButtonEl = _W.eventDetailsEl.querySelector(".close-modal-button");
      var contentEl = _W.eventDetailsEl.querySelector(".event-content");
      closeButtonEl.style.marginLeft = Math.min((contentEl.offsetWidth / 2 + 90), (window.outerWidth / 2) - closeButtonEl.offsetWidth) + "px";
    }


    function onTransitionEnd(event) {
      inMotion = false;

      if(trackPosition === 0) {
        //goto last slide
        trackPosition = -(galleryWidth * (slidesLength - 2));
        setTrackPosition(trackPosition, true);
      } else if(trackPosition === -(trackFullWidth - galleryWidth)) {
        //goto first slide
        trackPosition = -galleryWidth;
        setTrackPosition(trackPosition, true);
      }
    }

    function setTrackPosition(x, disableTransition) {
      var activeDotEl = dotsEl.querySelector(".active");
      trackEl.classList.toggle("without-transition", !!disableTransition);
      trackEl.style.transform = "translate3d(" + x + "px, 0px, 0px)";

      if(activeDotEl) {
        activeDotEl.classList.toggle("active", false);
      }

      dotsEl.querySelector("#dot-item" + trackPositionIndex).classList.toggle("active", true);
    }

    function onPrevBtnClicked(event) {
      if (inMotion) return;
      var newTrackPosition = trackPosition + galleryWidth;
      trackPositionIndex --;
      if(trackPositionIndex < 0) {
        trackPositionIndex = imagesLength - 1;
      }
      setTrackPosition(newTrackPosition);
      trackPosition = newTrackPosition;
      inMotion = true;
    }

    function onNextBtnClicked(event) {
      if (inMotion) return;
      var newTrackPosition = trackPosition - galleryWidth;
      trackPositionIndex++;
      if(trackPositionIndex > imagesLength - 1) {
        trackPositionIndex = 0;
      }
      setTrackPosition(newTrackPosition);
      trackPosition = newTrackPosition;
      inMotion = true;
    }

    function onDotClicked(event) {
      var dotItem = event.currentTarget;

      trackPositionIndex = parseInt(dotItem.dataset.slideIndex);
      trackPosition = -(galleryWidth + (galleryWidth * trackPositionIndex));

      setTrackPosition(trackPosition)
    }

    window.addEventListener("resize", resizeGallery);
    _W.removeListenersList.push(
        function() {
          window.removeEventListener("resize", resizeGallery);
        }
    );

    resizeGallery();
    trackPosition = -galleryWidth;
    trackEl.style.width = trackFullWidth + "px";
    trackEl.style.height = galleryHeight + "px";

    for(i = 0; i < imagesLength; i++) {
      var dotItemEl = dotsEl.querySelector("#dot-item" + i);
      dotItemEl.addEventListener("click", onDotClicked);
      _W.removeListenersList.push(
          function() {
            dotItemEl.removeEventListener("click", onDotClicked);
          }
      );
    }

    if(imagesLength > 1) {
      setTrackPosition(trackPosition, true);

      btnPrevEl.addEventListener("click", onPrevBtnClicked);
      btnNextEl.addEventListener("click", onNextBtnClicked);

      trackEl.addEventListener("webkitTransitionEnd", onTransitionEnd);
      trackEl.addEventListener("transitionend", onTransitionEnd);
      trackEl.addEventListener("msTransitionEnd", onTransitionEnd);
      trackEl.addEventListener("oTransitionEnd", onTransitionEnd);
    } else {
      btnNextEl.classList.toggle("hidden", true);
      btnPrevEl.classList.toggle("hidden", true);
    }

    initReadMoreButtons();

    //Schedule list
    var scheduleButtonEl = _W.eventDetailsEl.querySelector(".schedule-button");
    var scheduleItemList = _W.eventDetailsEl.querySelectorAll(".schedule-list-item");

    function onScheduleButtonClick(event) {
      _W.eventDetailsEl.querySelector(".schedule-list").classList.toggle("active", true);
    }

    function onScheduleItemClick(event) {
      _W.eventDetailsEl.querySelector(".schedule-list").classList.toggle("active", false);
      _W.eventDetailsEl.querySelector(".schedule-button").innerHTML = event.currentTarget.innerHTML;
    }

    scheduleButtonEl.addEventListener("click", onScheduleButtonClick);

    _W.removeListenersList.push(
        function() {
          scheduleButtonEl.removeEventListener("click", onScheduleButtonClick);
        }
    );

    for(var i = 0, length = scheduleItemList.length; i < length; i++) {
      var scheduleItemEl = scheduleItemList[i];
      scheduleItemEl.addEventListener("click", onScheduleItemClick);
      _W.removeListenersList.push(
          function() {
            scheduleItemEl.removeEventListener("click", onScheduleItemClick);
          }
      );
    }

  }

  function getScheduleListTemplate(schedulesList) {
    var schedulesTemplate = "";

    for(var i = 0, length = schedulesList.length; i < length; i++) {
      var scheduleData = schedulesList[i];
      schedulesTemplate += getEl("li", {class: "schedule-list-item"}, getScheduleDate(scheduleData));
    }
    return schedulesTemplate;
  }


  function initReadMoreButtons() {
    var readMoreButtonElList;

    function onReadMoreButtonClicked(event) {
      event.preventDefault();
      var readMoreButton = event.target;
      var content = _W.containerEl.querySelector(readMoreButton.dataset.contentSelector);
      content.classList.toggle("show", true);
      readMoreButton.classList.toggle("hidden", true);
    }

    readMoreButtonElList = _W.containerEl.querySelectorAll(".read-more");

    for(var i = 0, length = readMoreButtonElList.length; i < length; i++) {
      var readMoreButtonEl, readMoreData, readMoreContentEl = null;

      readMoreButtonEl = readMoreButtonElList[i];
      if(readMoreButtonEl) {
        readMoreData = readMoreButtonEl.dataset;
      }

      if(readMoreData) {
        readMoreContentEl = _W.containerEl.querySelector(readMoreData.contentSelector);
      }

      if(readMoreContentEl) {
        readMoreButtonEl.classList.toggle("hidden", readMoreContentEl.scrollHeight <= readMoreContentEl.offsetHeight);
        readMoreButtonEl.addEventListener("click", onReadMoreButtonClicked);
      }

      _W.removeListenersList.push(
        function() {
          readMoreButtonEl.removeEventListener("click", onReadMoreButtonClicked);
        }
      );

    }
  }



  function capitalizeFirstLetter(string) {
    return string ? string[0].toUpperCase() + string.slice(1) : "";
  }



  function getEl(tagName, attrs, value) {
    var result = "<" + tagName;
    if(attrs) {
      for(var key in attrs) {
        result += " " + key + "='" + attrs[key] + "'";
      }
    }
    result += ">";
    if(value) {
      result += value;
    }
    result += "</" + tagName + ">";
    return result
  }



  function getEventCardTemplate(eventData) {
    var eventURL = _W.serverAddress + "events/" + eventData.id + "-" + eventData.slug + "/";
    var eventDate = new Date(eventData.actual_schedule.start_date);
    var template = "";
    var eventDateFormated = eventDate.getDate() + " " + _W.lang.monthNamesForCards[eventDate.getMonth()] + " " + eventDate.getFullYear() + ", ";
    if(eventData.actual_schedule.hasOwnProperty("start_time")) {
      startTime = eventData.actual_schedule.start_time.substr(0, 5);
    } else {
      var startHour = eventDate.getHours();
      var startMin = eventDate.getMinutes();
      var startTime = (startHour < 10 ? "0" : "" + startHour) + ":" + (startMin < 10 ? "0" : "") + startMin;
    }
    eventDateFormated += startTime;
    //eventDateFormated += eventDate.getMinutes() > 9 ? "" : "0" + eventDate.getMinutes();

    template +=
            getEl("li", {class: "card card-event", 'data-event-id': eventData.id},
              getEl("div", {class: "card-holder"},
                getEl("a", {href: "#", class: "card-image event-card-details-btn"},
                    getEl("div", {class: "card-img", style: "background-image: url(" + eventData.image + ")"})
                ) +
                getEl("div", {class: "card-section"},
                  getEl("div", {class: "title"}, getLocalizedField(eventData, "title")) +
                  getEl("div", {class: "subtitle place"}, eventData.place.city + " &bull; " + getLocalizedField(eventData.place, "title")) +
                  getEl("div", {class: "subtitle date"},
                      eventDateFormated +
                      getEl("a", {class: "caption-link more-link event-card-details-btn", href: "#"}, _W.lang.more + "...")
                  )
                )
              )
            );
    return template;
  }



  function getCollectionBucketTemplate(bucketData, bucketEventsTemplate) {
    var template = "";
    var date = new Date(bucketData.key);
    var monthIndex = date.getMonth();
    var title = _W.lang.monthNames[monthIndex] + " " + date.getFullYear();
    var monthNum = (monthIndex > 8 ? "" : "0") + (monthIndex + 1);

    template +=
        getEl("div", {class: "event-bucket"},
            getEl("div", {class: "event-bucket-title-holder"},
              getEl("a", {href: "#" + monthNum, class: "bucket-title subtitle", 'data-month': monthNum}, title) +
              getEl("a", {href: "https://whatwhere.world/", class: "caption-link ww-link-main"}, _W.lang.eventsNearbyYou + getEl("i"))
            ) +
            getEl("ul", {class: "events-list with-clearfix"}, bucketEventsTemplate)
        );
    return template;
  }



  function getImagesGalleryTemplate(images) {
    var template = "";
    var slidesTemplate = "";
    var dotsTemplate = "";

    // add last image to the front of track
    if(images.length > 1) {
      slidesTemplate +=
          getEl("div", {class: "slide"},
              getEl("img", {src: images[images.length - 1].image})
          );
    }

    // fill track
    for(var i = 0, length = images.length; i < length; i++) {
      slidesTemplate += getEl("div", {class: "slide"},
          getEl("img", {src: images[i].image})
      );

      dotsTemplate += getEl("li", {class: "dot-item", id: "dot-item" + i, 'data-slide-index': i},
        getEl("div", {class: "dot"})
      );
    }

    // add first image to the end of track
    if(images.length > 1) {
      slidesTemplate +=
          getEl("div", {class: "slide"},
              getEl("img", {src: images[0].image})
          );
    }

    template +=
        getEl("div", {class: "event-images"},
            getEl("div", {class: "scene"},
              getEl("div", {class: "track"}, slidesTemplate)
            ) +
            getEl("div", {class: "btn-prev"}, getEl("i")) +
            getEl("div", {class: "btn-next"}, getEl("i")) +
            getEl("ul", {class: "dots" + (images.length < 2 ? " hidden" : "")}, dotsTemplate)
        );

    return template
  }



  function initAnchors() {
    var hash = document.location.hash;
    var hashValue = hash.substr(1);
    var EVENT_PREFIX = "event-";

    if(hash.length) {
      if(parseInt(hashValue) && _W.collectionWidgetEl) {
        var el = _W.collectionWidgetEl.querySelector("[data-month='"+ hashValue +"']");
        if(el) {
          el.scrollIntoView();
        }
      }

      if(hashValue.indexOf(EVENT_PREFIX) === 0) {
        var hashEventId = hashValue.slice(EVENT_PREFIX.length);

        if (_W.collectionEventsByID.hasOwnProperty(hashEventId)) {
          drawEventDetails(_W.collectionEventsByID[hashEventId]);
        }
      }
    }
  }


  function initMap(el, title, lat, lng) {

    lat = parseFloat(lat.toString().split(",").join("."));
    lng = parseFloat(lng.toString().split(",").join("."));

    var map = new google.maps.Map(el, {
      center: {lat: lat, lng: lng - 0.027},
      zoom: 13,
      scrollwheel: false,
      mapTypeControl: false
    });

    google.maps.event.addListener(map, 'click', function(event) {
      this.setOptions({scrollwheel: true});
    });

    var marker = new google.maps.Marker({
      position: {lat: lat, lng: lng},
      map: map,
      title: title
    });
  }


  function getScheduleDate(scheduleData) {
    var startDate = new Date(scheduleData.start_date);
    var endDate = new Date(scheduleData.end_date);
    var day = startDate.getDate();
    var month = _W.lang.monthNamesForCards[startDate.getMonth()];
    var year = startDate.getFullYear();
    var startTime, endTime, startHour, startMin, endHour, endMin;

    if(scheduleData.hasOwnProperty("start_time")) {
      startTime = scheduleData.start_time.substr(0, 5);
    } else {
      startHour = startDate.getHours();
      startMin = startDate.getMinutes();
      startTime = (startHour < 10 ? "0" : "" + startHour) + ":" + (startMin < 10 ? "0" : "") + startMin;
    }

    if(scheduleData.hasOwnProperty("end_time")) {
      endTime = scheduleData.end_time.substr(0, 5);
    } else {
      endHour = endDate.getHours();
      endMin = endDate.getMinutes();
      endTime = (endHour < 10 ? "0" : "" + endHour) + ":" + (endMin < 10 ? "0" : "") + endMin;
    }

    return day + " " + month + " " + year + "<br>" + startTime + "–" + endTime;
  }



  /******************************
  /*  Init collection widget
  /*****************************/

  function initCollectionWidget(collectionID, locale) {
    _W.locale =                       locale;
    _W.lang =                         LANG[_W.locale.toLowerCase()];
    _W.serverAddress =                "https://whatwhere.world/";
    _W.containerSelector =            "[data-ww-type='collection']";
    _W.containerEl =                  document.querySelector(_W.containerSelector);
    _W.collectionDataURL =            _W.serverAddress + "api/collections/" + collectionID + "/widget/";
    _W.widgetStylesURL =              _W.serverAddress + "static/widget/styles.css";
    _W.slickLibURL =                  _W.serverAddress + "static/widget/slick.js";
    _W.widgetStylesID =               "WWWidgetStyles";
    _W.widgetGooglemapsSctiptID =     "WWWidgetGooglemapsScript";
    _W.widgetGooglemapsSctiptKEY =    "AIzaSyAFzETh7AEn0R1AVSUQjq9TM05m8LGqWQo";
    _W.collectionData =               null;
    _W.collectionWidgetElID =         "what_where_collection";
    _W.collectionWidgetEl =           document.getElementById(_W.collectionWidgetElID);
    _W.collectionEventsByID =         {};
    _W.eventDetailsElID =             "what_where_event";
    _W.eventElTemplate =              document.getElementById(_W.eventDetailsElID);
    _W.removeListenersList =          [];

    loadCSS(_W.widgetStylesURL, _W.widgetStylesID);
    if(!(window.google && window.google.maps)) {
      loadScript("https://maps.googleapis.com/maps/api/js?libraries=places&key=" + _W.widgetGooglemapsSctiptKEY, _W.widgetGooglemapsSctiptID);
    }


    loadScript(_W.slickLibURL, "WWWidgetSlickSlider");

    httpGet(_W.collectionDataURL, function(response) {
      _W.collectionData = response;
      drawCollection();
    });

  }



  function init() {
    var collectionContainerEl = document.querySelector("[data-ww-type='collection']");
    if(collectionContainerEl) {
      var dataset = collectionContainerEl.dataset;
      initCollectionWidget(dataset.wwId, dataset.wwLang || "en");
    }
  }



  function onReadyStateChange(event) {
    if (document.readyState == "interactive" || document.readyState == "complete") {
      document.removeEventListener("readystatechange", onReadyStateChange);
      init();
    }
  }

  document.addEventListener("readystatechange", onReadyStateChange);

})(window, document);
